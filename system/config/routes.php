<?php  

///////////////////////////////////////////////// Default /////////////////////////////////////////////////
$route['default_controller'] = 'home';
$route['newindex'] = 'home/newindex';
///////////////////////////////////////////////// League //////////////////////////////////////////////////
$route['พรีเมียร์ลีก'] = 'tournament/index/premierleague';
$route['เซเรียอา'] = 'tournament/index/seriea';
$route['ลาลีกา'] = 'tournament/index/laliga';
$route['บุนเดสลีกา'] = 'tournament/index/bundesliga';
$route['ไทยพรีเมียร์ลีก'] = 'tournament/index/thaipremierleague';
$route['ลีกเอิง'] = 'tournament/index/ligue1';
$route['europaleague'] = 'tournament/index/europaleague';
$route['championsleague'] = 'tournament/index/championsleague';

$route['พรีเมียร์ลีก/table'] = 'tournament/table/premierleague';
$route['เซเรียอา/table'] = 'tournament/table/seriea';
$route['ลาลีกา/table'] = 'tournament/table/laliga';
$route['บุนเดสลีกา/table'] = 'tournament/table/bundesliga';
$route['ไทยพรีเมียร์ลีก/table'] = 'tournament/table/thaipremierleague';
$route['ลีกเอิง/table'] = 'tournament/table/ligue1';
$route['europaleague/table'] = 'tournament/table/europaleague';
$route['championsleague/table'] = 'tournament/table/championsleague';

$route['พรีเมียร์ลีก/topscorer'] = 'tournament/topscorer/premierleague';
$route['เซเรียอา/topscorer'] = 'tournament/topscorer/seriea';
$route['ลาลีกา/topscorer'] = 'tournament/topscorer/laliga';
$route['บุนเดสลีกา/topscorer'] = 'tournament/topscorer/bundesliga';
$route['ไทยพรีเมียร์ลีก/topscorer'] = 'tournament/topscorer/thaipremierleague';
$route['ลีกเอิง/topscorer'] = 'tournament/topscorer/ligue1';
$route['europaleague/topscorer'] = 'tournament/topscorer/europaleague';
$route['championsleague/topscorer'] = 'tournament/topscorer/championsleague';

$route['พรีเมียร์ลีก/result-program'] = 'tournament/program/premierleague';
$route['เซเรียอา/result-program'] = 'tournament/program/seriea';
$route['ลาลีกา/result-program'] = 'tournament/program/laliga';
$route['บุนเดสลีกา/result-program'] = 'tournament/program/bundesliga';
$route['ไทยพรีเมียร์ลีก/result-program'] = 'tournament/program/thaipremierleague';
$route['ลีกเอิง/result-program'] = 'tournament/program/ligue1';
$route['europaleague/result-program'] = 'tournament/program/europaleague';
$route['championsleague/result-program'] = 'tournament/program/championsleague';


$route['พรีเมียร์ลีก/news'] = 'tournament/news/premierleague';
$route['เซเรียอา/news'] = 'tournament/news/seriea';
$route['ลาลีกา/news'] = 'tournament/news/laliga';
$route['บุนเดสลีกา/news'] = 'tournament/news/bundesliga';
$route['ไทยพรีเมียร์ลีก/news'] = 'tournament/news/thaipremierleague';
$route['ลีกเอิง/news'] = 'tournament/news/ligue1';
$route['europaleague/news'] = 'tournament/news/europaleague';
$route['championsleague/news'] = 'tournament/news/championsleague';

///////////////////////////////////////////////// Cup-Tournament //////////////////////////////////////////////////

$route['tournament/euro2016qual/result-program']	=		'tournament/program/euro2016';
$route['tournament/euro2016qual/topscorer']			=		'tournament/topscorer/euro2016';
$route['tournament/euro2016qual/news']				=		'tournament/news/euro2016';
$route['tournament/euro2016qual/table']				=		'tournament/table/euro2016';
$route['tournament/euro2016qual']					=		'tournament/index/euro2016';

//$route['euro2016/result-program']		=		'tournament/program/euro2016qual';
//$route['euro2016/topscorer']			=		'tournament/topscorer/euro2016qual';
//$route['euro2016/news']				=		'tournament/news/euro2016qual';
//$route['euro2016/table']				=		'tournament/table/euro2016qual';
//$route['euro2016']					=		'tournament/index/euro2016qual';

$route['result.php'] = 'livescore/result';
$route['livescore.php'] = 'livescore';
$route['program.php'] = 'livescore/program';
///////////////////////////////////////////////// Error /////////////////////////////////////////////////
$route['404_override'] = 'notpage';

///////////////////////////////////////////////// Admin /////////////////////////////////////////////////
$route[FOLDER_AM] = FOLDER_AM.'/dashboard';
$route['(news)-(:num)-(:num)'] = 'comment/sub_reply/$2/$3/$1';
$route['news-(:num)'] = 'news/view/$1';
//$route['result.php'] = 'result';
$route['newslist.php'] = 'news';
$route['feature'] = 'news/top10';
$route['transfer-market'] = 'transfer';
$route['วิเคราะห์บอล'] = 'livescore/analyze';
$route['match-(:num)-(:any)'] = 'livescore/match/$1';
//$route['(match)-(:num)-(:any)-(:num)'] = 'comment/sub_reply/$2/$4/$1';

$route['team-(:any)/news'] = 'news/squad/$1';
$route['team-(:any)'] = 'squad/index/$1';
$route['profile/(:any)'] = 'profile/dashboard/$1';
$route['profile'] = 'profile/dashboard';
$route['apiprofile'] = 'profile/api';
$route['games/rule-help'] = 'games/helprule';

$route['tournament/(:any)/result-program']		=		'tournament/program/$1';
$route['tournament/(:any)/topscorer']			=		'tournament/topscorer/$1';
$route['tournament/(:any)/news']				=		'tournament/news/$1';
$route['tournament/(:any)/table']				=		'tournament/table/$1';
$route['tournament/(:any)']						=		'tournament/index/$1';


?>
