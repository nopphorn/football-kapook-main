<!DOCTYPE html>
<html lang="th">
    <head>
	    
	    <!-- Ads : Criteo -->
		<script type='text/javascript'>
		var crtg_nid = '4908';
		var crtg_cookiename = 'crtg_rta';
		var crtg_varname = 'crtg_content';
		function crtg_getCookie(c_name){ var i,x,y,ARRCookies=document.cookie.split(";");for(i=0;i<ARRCookies.length;i++){x=ARRCookies[i].substr(0,ARRCookies[i].indexOf("="));y=ARRCookies[i].substr(ARRCookies[i].indexOf("=")+1);x=x.replace(/^\s+|\s+$/g,"");if(x==c_name){return unescape(y);} }return'';}
		var crtg_content = crtg_getCookie(crtg_cookiename);
		var crtg_rnd=Math.floor(Math.random()*99999999999);
		(function(){
		var crtg_url=location.protocol+'//rtax.criteo.com/delivery/rta/rta.js?netId='+escape(crtg_nid);
		crtg_url +='&cookieName='+escape(crtg_cookiename);
		crtg_url +='&rnd='+crtg_rnd;
		crtg_url +='&varName=' + escape(crtg_varname);
		var crtg_script=document.createElement('script');crtg_script.type='text/javascript';crtg_script.src=crtg_url;crtg_script.async=true;
		if(document.getElementsByTagName("head").length>0)document.getElementsByTagName("head")[0].appendChild(crtg_script);
		else if(document.getElementsByTagName("body").length>0)document.getElementsByTagName("body")[0].appendChild(crtg_script);
		})();
		</script>
		<script type='text/javascript'>
		var googletag = googletag || {};
		googletag.cmd = googletag.cmd || [];
		googletag.cmd.push(function() {
		    var crtg_split = (crtg_content || '').split(';');
		    var pubads = googletag.pubads();
		    for (var i=1;i<crtg_split.length;i++){
		        pubads.setTargeting("" + (crtg_split[i-1].split('='))[0] + "", "" + (crtg_split[i-1].split('='))[1] + "");
		    }
		});
		</script>
		<!-- Ads : Zedo -->
		<script type="text/javascript" charset="UTF-8" src="http://avd.innity.net/audsync.js"></script>
		<script type="text/javascript">
		    var innity_dingo_ukv = innityaudsync._setTargeting('zedo', {"cl": "143", "targetKeys": ["kapook_aud"]});
		</script>
	    
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Title -->
        <?php
        if ($this->seo['seo_title'])
            $seo_title = $this->seo['seo_title'];
        else
            $seo_title = 'ผลบอล ผลบอลสด บอลวันนี้ วิเคราะห์บอล ข่าว ฟุตบอล คลิกเลย';
        ?>
        <title><?php echo $seo_title; ?></title>

        <!-- SEO -->
        <?php
        if ($this->seo['seo_description'])
            $seo_description = $this->seo['seo_description'];
        else
            $seo_description = 'ผลบอล ผลบอลสด ตารางบอล บอลวันนี้ เช็คผลบอล ผลบอลเมื่อคืน livescore โปรแกรมบอล วิเคราะห์บอล ทีเด็ด แม่นๆ ข่าว ฟุตบอล ข่าวซื้อขายนักเตะ ดู ตารางถ่ายทอดสด คลิปบอล พรีเมียร์ลีก ผลบอลไทย ตารางคะแนน อันดับดาวซัลโว';

        if ($this->seo['seo_keywords'])
            $seo_keywords = $this->seo['seo_keywords'];
        else
            $seo_keywords = 'ผลบอล, ผลบอลเมื่อคืน, ผลบอลวันนี้, ผลบอลล่าสุด, เช็คผลบอล, ผลบอลสด, livescore, วิเคราะห์บอล, วิเคราะห์บอลวันนี้, ทีเด็ด, ราคาบอล, ตารางบอล, ตารางบอลวันนี้, โปรแกรมบอลพรุ่งนี้, ตารางบอลพรุ่งนี้, โปรแกรมบอลล่วงหน้า, ดูบอล, ข่าว, ข่าวฟุตบอล, ข่าวบอล, ข่าวแมนยู, ข่าวลิเวอร์พูล, พรีเมียร์ลีก, ผลบอลพรีเมียร์ลีก, ตารางคะแนน, ไทยพรีเมียร์ลีก';
        ?>

        <meta name="description" content="<?php echo $seo_description; ?>" />
        <meta name="keywords" content="<?php echo $seo_keywords; ?>" />
        <meta name="author" content="กระปุกดอทคอม เว็บแรกที่คุณเลือก">
        <link rel="shortcut icon" href="<?php echo BASE_HREF; ?>favicon.ico">

        <!-- SMO -->
        <?php
        if ($this->smo['fb_title'])
            $fb_title = $this->smo['fb_title'];
        else
            $fb_title = 'ผลบอล บอลวันนี้ ผลบอลสด วิเคราะห์บอล รวม ข่าว ฟุตบอล';

        if ($this->smo['fb_description'])
            $fb_description = $this->smo['fb_description'];
        else
            $fb_description = 'ผลบอล ผลบอลสด ตารางบอล เช็คผลบอลเมื่อคืน วิเคราะห์บอล ทีเด็ด แม่นๆ ข่าวฟุตบอล พรีเมียร์ลีก ผลบอลไทย ตารางคะแนน';

        if ($this->smo['fb_img'])
            $fb_img = $this->smo['fb_img'];
        else
            $fb_img = BASE_HREF . 'assets/img/og-football.jpg';
        ?>

        <link rel="image_src" type="image/jpeg" href="<?php echo $fb_img; ?>" />
    <!--    <meta property="fb:app_id" content="<?php echo FB_APP_ID; ?>">
        <meta property="fb:admins" content="100000352102435" />-->

        <meta property="fb:app_id" content="306795119462">
        <meta property="fb:admins" content="592216870" />
        <meta property="fb:admins" content="100000517048862" />
        <meta property="fb:admins" content="100000607392589" />
        <meta property="fb:admins" content="100000087878814" />
        <meta property="fb:admins" content="1140102705" />


        <meta property="og:type" content="website" />
        <meta property="og:type" content="article">
		<meta property="article:author" content="https://www.facebook.com/kapookdotcom">
        <meta property="og:title" content="<?php echo $fb_title; ?>" />
        <meta property="og:description" content="<?php echo $fb_description; ?>" />
        <meta property="og:image" content="<?php echo $fb_img; ?>" />
        <meta property="og:url" content="<?php echo HTTP_REQUEST_URL; ?>" />
        <meta property="og:site_name" content="kapook" />

        <meta name="twitter:title" content="<?php echo $fb_title; ?>">
        <meta name="twitter:description" content="<?php echo $fb_description; ?>" />
        <meta name="twitter:image:src" content="<?php echo $fb_img; ?>">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@kapookdotcom">
        <meta name="twitter:creator" content="@kapookdotcom">
        <meta name="twitter:domain" content="kapook.com">
        
        <?php if ($this->meta_category) { ?>
        <!-- DMP TAG -->        
            <meta name="category" content="<?php echo $this->meta_category; ?>" />
        <!-- //DMP TAG --> 
        <?php } ?>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
        <link href="/default_kapook/style.css" rel="stylesheet" type="text/css" media="all">

        <!--Style-->
        <link rel="stylesheet" href="<?php echo BASE_HREF; ?>assets/css/style.css">
        <link rel="stylesheet" href="<?php echo BASE_HREF; ?>assets/css/responsive.css">
        <script src="<?php echo BASE_HREF; ?>assets/js/vendor/jquery-1.11.0.min.js"></script>
        <!-- This code snippet is to be placed in the <head> -->



<?php if($_SERVER["HTTP_MOBILE"]=="1"){?>
<script type="text/javascript">
var zmt_mtag;
function zd_get_placements(){
zmt_mtag = zmt_get_tag(3326,"543519");
p543519_1 = zmt_mtag.zmt_get_placement("zt_543519_1", "543519", "1" , "78" , "18" , "2" ,"970", "250");
p543519_2 = zmt_mtag.zmt_get_placement("zt_543519_2", "543519", "2" , "78" , "9" , "2" ,"300", "250");
p543519_3 = zmt_mtag.zmt_get_placement("zt_543519_3", "543519", "3" , "78" , "24" , "2" ,"300", "250");
p543519_1.zmt_add_ct("kapook_aud:" + innity_dingo_ukv + "^criteo:"+crtg_content);
p543519_2.zmt_add_ct("kapook_aud:" + innity_dingo_ukv + "^criteo:"+crtg_content);
p543519_3.zmt_add_ct("kapook_aud:" + innity_dingo_ukv + "^criteo:"+crtg_content);
zmt_mtag.zmt_set_async();
zmt_mtag.zmt_load(zmt_mtag); 
} 
</script> 
<?php } else { ?>
<script type="text/javascript">
 var zmt_mtag;
function zd_get_placements(){
 zmt_mtag = zmt_get_tag(3326,"39433");
 p39433_1 = zmt_mtag.zmt_get_placement("zt_39433_1", "39433", "1" , "78" , "22" , "2" ,"320", "100");
 p39433_2 = zmt_mtag.zmt_get_placement("zt_39433_2", "39433", "2" , "78" , "23" , "2" ,"300", "250");
 p39433_1.zmt_add_ct("kapook_aud:" + innity_dingo_ukv + "^criteo:"+crtg_content);
 p39433_2.zmt_add_ct("kapook_aud:" + innity_dingo_ukv + "^criteo:"+crtg_content);
 zmt_mtag.zmt_set_async();
 zmt_mtag.zmt_load(zmt_mtag); 
} 
</script> 
<?php } ?>
<script type="text/javascript" src="http://s.f1.impact-ad.jp/client/xp1/fmos.js" async ></script>
<style>
body.adfloat{}
body.ad-float header{margin-bottom: 100px} 
body.ad-float header+div.bg_banner {  position: fixed;  top: 0;  z-index: 999;  left: 0;  right: 0; }
.bg_banner { background-color:#fff; width:100%; overflow:hidden; display:block;}

</style>
    </head>
    <body>
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-36103152-15', 'kapook.com');
            ga('send', 'pageview');
        </script>
        <?php if ($_SERVER['REQUEST_URI'] == '/') { ?> <h1 id="logo">ฟุตบอล</h1> <?php } ?>

        <?php if ($_REQUEST['app'] != 'iphone') { ?>    
            <nav id="topbar" style=" position:relative">
                <ul>
                    <li class="menu left"><a href="http://www.kapook.com" class="topnav"><span class="icon-navicon"></span>เว็บแรกที่คุณเลือก</a></li>

                    <?php if (Login == 1) { ?>

                        <li class="login right" id="login_box" style="position: relative;">
                            <a href="" class="btnloginnow"><img id="user_avatar" src="<?php echo Avatar; ?>" alt="" width="40" height="40"><span id="user_name" class="usernametop"><?php echo Name; ?></span></a>

                            <div id="loged_box" class="loginnow">
                                <a href="http://football.kapook.com/profile/<?php echo Name; ?>"><img src="<?php echo Avatar; ?>" alt="" id="loged_box_avatar" width="60" height="60"></a>
                                <strong><a href="http://football.kapook.com/profile/<?php echo Name; ?>" id="loged_box_username" > <?php echo Name; ?></a></strong>
                                <a href="http://football.kapook.com/profile/<?php echo Name; ?>" class="subbtn">หน้าฟุตบอลของฉัน</a>
                                <a href="http://signup-demo.kapook.com/account/profile?callback=<?php echo HTTP_REQUEST_URL; ?>" class="subbtn">แก้ไขข้อมูลส่วนตัว</a>
                                <a href="http://signup-demo.kapook.com/account/signout?apikey=cxsw3ed21qaz&amp;callback=<?php echo HTTP_REQUEST_URL; ?>" class="subbtn">ออกจากระบบ</a>
                            </div>
                        </li>

                    <?php } else { ?>

                        <li class="login right" style="position: relative;"><a href="" class="btnlogin"><span class="icon-lock"></span>เข้าระบบ</a>
                            <form id="form_login_box" class="loginbox" method="post" action="http://signup-demo.kapook.com/connect/kapook">
                                <input type="hidden" name="apikey"  value="cxsw3ed21qaz">
                                <input type="hidden" name="callback" value="<?php echo HTTP_REQUEST_URL; ?>">
                                <fieldset>
                                    <legend>เข้าระบบด้วย User Kapook</legend>
                                    <label>Email</label>
                                    <input name="username" type="text" class="input-login">
                                    <br>
                                    <label>Password</label>
                                    <input name="password" type="password" class="input-login">
                                    <br>
                                    <input name="submit" type="submit" class="btn-login" value="เข้าระบบ">
                                </fieldset>
                                <p class="connectFB"><a href="http://signup-demo.kapook.com/connect/facebook?callback=<?php echo HTTP_REQUEST_URL; ?>"><span class="icon-facebook"></span> เข้าระบบด้วย Facebook</a></p>
                            </form>
                        </li>

                    <?php } ?>





                    <?php if (Login != 1) { ?>
                        <li class="signin right"><a href="http://signup-demo.kapook.com/?apikey=cxsw3ed21qaz&callback=<?php echo HTTP_REQUEST_URL; ?>"><span class="icon-user-plus"></span>สมัครสมาชิก</a></li>
                    <?php } ?>

                    <li class="fav right"><a href="http://today.kapook.com/howto" target="_blank"><span class="icon-location"></span>ตั้งเป็นหน้าแรก</a></li>
                    <li class="right"> 

                        <?php if ($_GET['fb'] == 1) { ?>
                            <script type="text/javascript" language="javascript1.1">page = 'fb-click';</script>
                        <?php } else if ($this->truehit_key == '') { ?>
                            <script type="text/javascript" language="javascript1.1">page = "football";</script> 
                        <?php } elseif ($_GET['keypages'] != "") { ?>
                            <script type="text/javascript" language="javascript1.1">page = '<?php echo $_GET['keypages']; ?>_football';</script>

                        <?php } else { ?>
                            <script type="text/javascript" language="javascript1.1">page = "<?php echo $this->truehit_key; ?>";</script>            
                        <?php } ?>

                        <!-- New Truehits 02-09-57 -->        
                        <div id="truehits_div"></div>
                        <script type="text/javascript">
                            (function() {
                                var ga = document.createElement('script');
                                ga.type = 'text/javascript';
                                ga.async = true;
                                ga.src = "http://hits.truehits.in.th/dataa/a0000034.js";
                                var s = document.getElementsByTagName('script')[0];
                                s.parentNode.insertBefore(ga, s);
                            })();
                        </script>
                        <!-- \\New Truehits 02-09-57 -->

                    </li>
                </ul>
                <ol class="topupmenu">
                    <li><a href="http://headline.kapook.com" target="_blank" title="ข่าวด่วน">ข่าวด่วน</a></li>
                    <li><a href="http://sn.kapook.com" target="_blank" title="ข่าวสั้น">ข่าวสั้น</a></li>
                    <li><a href="http://women.kapook.com/star" target="_blank" title="ข่าวดารา">ข่าวดารา</a></li>
                    <li><a href="http://tv.kapook.com" target="_blank" title="ดูทีวี">ดูทีวี</a></li>
                    <li><a href="http://drama.kapook.com" target="_blank" title="ละคร">ละคร</a></li>
                    <li><a href="http://movie.kapook.com" target="_blank" title="หนังใหม่">หนังใหม่</a></li>
                    <li><a href="http://musicstation.kapook.com" target="_blank" title="ฟังเพลง">ฟังเพลง</a></li>
                    <li><a href="http://radio.kapook.com" target="_blank" title="ฟังวิทยุออนไลน์">ฟังวิทยุออนไลน์</a></li>
                    <li><a href="http://gamecenter.kapook.com" target="_blank" title="เกม">เกม</a></li>
                    <li><a href="http://gamecenter.kapook.com/thaichess" target="_blank" title="หมากรุกไทย">หมากรุกไทย</a></li>
                    <li><a href="http://gamecenter.kapook.com/checker" target="_blank" title="แชทหมากฮอส">แชทหมากฮอส</a></li>
                    <li><a href="http://glitter.kapook.com" target="_blank" title="Glitter">Glitter</a></li>
                    <li><a href="http://lottery.kapook.com" target="_blank" title="ตรวจหวย">ตรวจหวย</a></li>
                    <li><a href="http://women.kapook.com" target="_blank" title="ผู้หญิง">ผู้หญิง</a></li>
                    <li><a href="http://wedding.kapook.com" target="_blank" title="แต่งงาน">แต่งงาน</a></li>
                    <li><a href="http://baby.kapook.com" target="_blank" title="แม่และเด็ก">แม่และเด็ก</a></li>
                    <li><a href="http://horoscope.kapook.com" target="_blank" title="ดูดวง">ดูดวง</a></li>
                    <li><a href="http://dream.kapook.com" target="_blank" title="ทำนายฝัน">ทำนายฝัน</a></li>
                    <li><a href="http://health.kapook.com" target="_blank" title="สุขภาพ">สุขภาพ</a></li>
                    <li><a href="http://pet.kapook.com" target="_blank" title="สัตว์เลี้ยง">สัตว์เลี้ยง</a></li>
                    <li><a href="http://men.kapook.com" target="_blank" title="ผู้ชาย">ผู้ชาย</a></li>
                    <li><a href="http://football.kapook.com" target="_blank" title="ผลบอล">ผลบอล</a></li>
                    <li><a href="http://home.kapook.com" target="_blank" title="บ้านและการตกแต่ง">บ้านและการตกแต่ง</a></li>
                    <li><a href="http://travel.kapook.com" target="_blank" title="ท่องเที่ยว">ท่องเที่ยว</a></li>
                    <li><a href="http://guide.kapook.com" target="_blank" title="แวะชิมแวะพัก">แวะชิมแวะพัก</a></li>
                    <li><a href="http://poem.kapook.com" target="_blank" title="กลอน">กลอน</a></li>
                    <li><a href="http://icare.kapook.com" target="_blank" title="iCare">iCare</a></li>
                    <li><a href="http://education.kapook.com" target="_blank" title="การศึกษา">การศึกษา</a></li>
                    <li><a href="http://dict.kapook.com" target="_blank" title="dictionary">dictionary</a></li>
                    <li><a href="http://speedtest.kapook.com" target="_blank" title="เช็คความเร็วเน็ต">เช็คความเร็วเน็ต</a></li>
                    <li><a href="http://iphone.kapook.com" target="_blank" title="iPhone">iPhone</a></li>
                    <li><a href="http://facebook.kapook.com" target="_blank" title="Facebook">Facebook</a></li>
                    <li><a href="http://twitter.kapook.com" target="_blank" title="Twitter">Twitter</a></li>
                    <li><a href="http://instagram.kapook.com/star" target="_blank" title="instagram ดารา">instagram ดารา</a></li>
                    <li><a href="http://instagram.kapook.com" target="_blank" title="อินสตาแกรม">อินสตาแกรม</a></li>
                </ol>

            </nav>
            <div class="header-container " >
                <header class="wrapper clearfix container">
                    <div  class="logo"><a class="kapook" href="http://www.kapook.com/"></a> <a  class="football" href="<?php echo BASE_HREF; ?>"><span class="head">FOOTBALL</span><span class="des">เว็บฟุตบอลประจำวันของคุณ</span></a></div>
                </header>
            </div>
            <div class="nav-container" >
                <div class="nav-wrap">
                    <div class="nav-line">
                        <nav class="navbar navbar-default main" >

                            <div class="navbar-header">
                                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="<?php echo BASE_HREF; ?>"><i class="fa fa-home"></i> ฟุตบอล</a>
                                <?php if (Login == 1) { ?>

                                    <a href="http://football.kapook.com/profile/<?php echo Name; ?>" class="pull-right visible-xs-inline-block nav-bar-link"   ><i class="avartar" style="background: url(<?php echo Avatar; ?>) no-repeat;background-size: cover;"></i></a>
                                <?php } else { ?>

                                    <a href="http://football.kapook.com/modal/Remote-Modal-login.php?url=<?php echo urlencode(BASE_HREF); ?>" class="pull-right visible-xs-inline-block nav-bar-link" data-toggle="modal" data-target="#useful-modal"  ><i class="fa fa-user"></i> Login </a>
                                <?php } ?>	

                            </div>
                            
                            



                            <div class="collapse navbar-collapse js-navbar-collapse nopad-left">
                                <ul class="nav navbar-nav nav-top">
                                    <li class=""><a href="<?php echo BASE_HREF; ?>tournament/euro2016qual/" style="color: #004873; background: #E4EDF3;">ยูโร 2016</a></li>
                                    <li class=""><a href="<?php echo BASE_HREF; ?>livescore.php">ผลบอลสด</a></li>
                                    <li class="dropdown mega-dropdown"><a href="<?php echo BASE_HREF; ?>result.php" class="dropdown-toggle" data-toggle="dropdown">ผลบอล<span class="fa fa-angle-down pull-right"></span></a>
                                        <ul class="dropdown-menu mega-dropdown-menu">
                                            <li class="col-sm-12"><h3><a href="<?php echo BASE_HREF; ?>result.php" title="โปรแกรมบอล">ผลบอล</a></h3></li>
                                            <li class="col-sm-2">
                                                <ul>
                                                    <li class="dropdown-header"  > Leagues</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>พรีเมียร์ลีก/result-program" title="ผลบอลพรีเมียร์ลีก">ผลบอลพรีเมียร์ลีก</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>ลาลีกา/result-program" title="ผลบอลลาลีกา">ผลบอลลาลีกา</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>บุนเดสลีกา/result-program" title="บอลวันนี้">ผลบอลบุนเดสลีกา</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>เซเรียอา/result-program" title="ผลบอลเซเรียอา">ผลบอลเซเรียอา</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>ลีกเอิง/result-program" title="ผลบอลลีกเอิง">ผลบอลลีกเอิง</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>ไทยพรีเมียร์ลีก/result-program" title="ผลบอลไทยพรีเมียร์ลีก">ผลบอลไทยพรีเมียร์ลีก</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header"  > Cups</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>championsleague/result-program" title="ผลบอลยูฟา แชมเปียนส์ ลีก">ผลบอลยูฟา แชมเปียนส์ ลีก</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>europaleague/result-program" title="ผลบอลยูฟา ยูโรป้า ลีก">ผลบอลยูฟา ยูโรป้า ลีก</a></li>
													<li><a href="<?php echo BASE_HREF; ?>tournament/afcchampionsleague/result-program" title="ผลบอลเอเอฟซี แชมเปี้ยนส์ ลีก">เอเอฟซี แชมเปี้ยนส์ ลีก</a></li>
                                                    <li><a name="<?php echo BASE_HREF; ?>euro/program-result" title="ผลบอลเอฟเอ คัพ">ผลบอลเอฟเอ คัพ</a></li>
                                                    <li><a name="<?php echo BASE_HREF; ?>worldcup2018/program-result" title="ผลบอลแคปิตอล วัน คัพ">ผลบอลแคปิตอล วัน คัพ</a></li>
                                                </ul>
                                            </li>
                                            <!--<label class="live blink_me">NEW</label>-->
                                            <li class="col-sm-4">
                                                <ul>
                                                    <li class="dropdown-header"  > Tournaments</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>tournament/euro2016qual/result-program" title="ผลบอลยูโร 2016">ผลบอลยูโร 2016 <label class="live blink_me" style="width: inherit;">NEW</label></a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>" title="ผลบอลโคปา อเมริกา 2016">ผลบอลโคปา อเมริกา 2016</a></li>
                                                    <li><a name="<?php echo BASE_HREF; ?>worldcup2018/program-result" title="ผลบอลฟุตบอลโลก 2018">ผลบอลฟุตบอลโลก 2018</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>tournament/worldcup2018asianqual/result-program">ผลบอลฟุตบอลโลก 2018 รอบคัดเลือก โซนเอเชีย</a></li>
													<li><a href="<?php echo BASE_HREF; ?>tournament/worldcup2018saqual/result-program"> ผลบอลฟุตบอลโลก 2018 รอบคัดเลือก โซนอเมริกาใต้  </a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header"  > Teams</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-manchester-utd/program-result" title="ผลบอลแมนฯ ยูไนเต็ด">ผลบอลแมนฯ ยูไนเต็ด</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-liverpool/program-result" title="ผลบอลลิเวอร์พูล">ผลบอลลิเวอร์พูล</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-chelsea/program-result" title="ผลบอลเชลซี">ผลบอลเชลซี</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-arsenal/program-result" title="ผลบอลอาร์เซน่อล">ผลบอลอาร์เซน่อล</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-manchester-city/program-result" title="ผลบอลแมนเชสเตอร์ ซิตี้">ผลบอลแมนเชสเตอร์ ซิตี้</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-real-madrid/program-result" title="ผลบอลเรอัล มาดริด">ผลบอลเรอัล มาดริด</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-barcelona/program-result" title="ผลบอลบาร์เซโลน่า">ผลบอลบาร์เซโลน่า</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-bayern-munich/program-result" title="ผลบอลบาเยิร์น มิวนิค">ผลบอลบาเยิร์น มิวนิค</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-ac-milan/program-result" title="ผลบอลเอซี มิลาน">ผลบอลเอซี มิลาน</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown mega-dropdown"><a href="<?php echo BASE_HREF; ?>program.php" class="dropdown-toggle" data-toggle="dropdown">ตารางบอล <span class="fa fa-angle-down pull-right"></span></a>
                                        <ul class="dropdown-menu mega-dropdown-menu">


                                            <li class="col-sm-12"><h3><a href="<?php echo BASE_HREF; ?>program.php" title="โปรแกรมบอล">ตารางบอล</a></h3></li>
                                            <li class="col-sm-2">
                                                <ul>
                                                    <li class="dropdown-header"  > Leagues</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>พรีเมียร์ลีก/result-program" title="ตารางบอลพรีเมียร์ลีก">ตารางบอลพรีเมียร์ลีก</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>ลาลีกา/result-program" title="ตารางบอลลาลีกา">ตารางบอลลาลีกา</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>บุนเดสลีกา/result-program" title="บอลวันนี้">ตารางบอลบุนเดสลีกา</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>เซเรียอา/result-program" title="ตารางบอลเซเรียอา">ตารางบอลเซเรียอา</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>ลีกเอิง/result-program" title="ตารางบอลลีกเอิง">ตารางบอลลีกเอิง</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>ไทยพรีเมียร์ลีก/result-program" title="ตารางบอลไทยพรีเมียร์ลีก">ตารางบอลไทยพรีเมียร์ลีก</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header"  > Cups</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>championsleague/result-program" title="ตารางบอลยูฟา แชมเปียนส์ ลีก">ตารางบอลยูฟา แชมเปียนส์ ลีก</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>europaleague/result-program" title="ตารางบอลยูฟา ยูโรป้า ลีก">ตารางบอลยูฟา ยูโรป้า ลีก</a></li>
													<li><a href="<?php echo BASE_HREF; ?>tournament/afcchampionsleague/result-program" title="ตารางบอลเอเอฟซี แชมเปี้ยนส์ ลีก">ตารางบอลเอเอฟซี แชมเปี้ยนส์ ลีก</a></li>
                                                    <li><a name="<?php echo BASE_HREF; ?>ลาลีกา/program-result" title="ตารางบอลเอฟเอ คัพ">ตารางบอลเอฟเอ คัพ</a></li>
                                                    <li><a name="<?php echo BASE_HREF; ?>บุนเดสลีกา/program-result" title="ตารางบอลแคปิตอล วัน คัพ">ตารางบอลแคปิตอล วัน คัพ</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-4">
                                                <ul>
                                                    <li class="dropdown-header"  > Tournaments</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>tournament/euro2016qual/result-program">ตารางบอลยูโร 2016 <label class="live blink_me" style="width: inherit;">NEW</label></a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>" >ตารางบอลโคปา อเมริกา 2016</a></li>
                                                    <li><a name="<?php echo BASE_HREF; ?>worldcup2018/program-result">ตารางบอลฟุตบอลโลก 2018</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>tournament/worldcup2018asianqual/result-program">ตารางบอลฟุตบอลโลก 2018 รอบคัดเลือก โซนเอเชีย</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>tournament/worldcup2018saqual/result-program"> ตารางบอลฟุตบอลโลก 2018 รอบคัดเลือก โซนอเมริกาใต้  </a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header"  > Teams</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-manchester-utd/program-result" title="ตารางบอลแมนฯ ยูไนเต็ด">ตารางบอลแมนฯ ยูไนเต็ด</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-liverpool/program-result" title="ตารางบอลลิเวอร์พูล">ตารางบอลลิเวอร์พูล</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-chelsea/program-result" title="ตารางบอลเชลซี">ตารางบอลเชลซี</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-arsenal/program-result" title="ตารางบอลอาร์เซน่อล">ตารางบอลอาร์เซน่อล</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-manchester-city/program-result" title="ตารางบอลแมนเชสเตอร์ ซิตี้">ตารางบอลแมนเชสเตอร์ ซิตี้</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-real-madrid/program-result" title="ตารางบอลเรอัล มาดริด">ตารางบอลเรอัล มาดริด</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-barcelona/program-result" title="ตารางบอลบาร์เซโลน่า">ตารางบอลบาร์เซโลน่า</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-bayern-munich/program-result" title="ตารางบอลบาเยิร์น มิวนิค">ตารางบอลบาเยิร์น มิวนิค</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-ac-milan/program-result" title="ตารางบอลเอซี มิลาน">ตารางบอลเอซี มิลาน</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="<?php echo BASE_HREF; ?>วิเคราะห์บอล">วิเคราะห์บอล</a></li>
                                    <li class="dropdown mega-dropdown"> <a href="<?php echo BASE_HREF; ?>พรีเมียร์ลีก" class="dropdown-toggle" data-toggle="dropdown">พรีเมียร์ลีก <span class="fa fa-angle-down pull-right"></span></a>
                                        <ul class="dropdown-menu mega-dropdown-menu">
                                            <li class="col-sm-3">
                                                <ul>
                                                    <?php $LastestNews = $this->mem_lib->get('NewsIndexTournament-Latest-premierleague'); ?>
                                                    <li class="dropdown-header">ข่าวพรีเมียร์ลีกล่าสุด</li>
                                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                                        <div class="carousel-inner">
                                                            <div class="item active"> <a href="<?php echo $LastestNews['link']; ?>"><img src="<?php echo $LastestNews['picture']; ?>" class="img-responsive" alt="product 3"></a>
                                                                <h4><small><?php echo $LastestNews['subject']; ?></small></h4>
                                                            </div>
                                                            <!-- End Item --> 
                                                        </div>
                                                        <!-- End Carousel Inner --> 
                                                    </div>
                                                    <!-- /.carousel -->
                                                    <li class="divider"></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>พรีเมียร์ลีก/news">ดูทั้งหมด <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header"> พรีเมียร์ลีก</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>พรีเมียร์ลีก/result-program">ผลบอล & โปรแกรม พรีเมียร์ลีก</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>พรีเมียร์ลีก/table">ตารางคะแนนพรีเมียร์ลีก</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>พรีเมียร์ลีก/topscorer">ดาวซัลโวพรีเมียร์ลีก</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>พรีเมียร์ลีก/news">ข่าวพรีเมียร์ลีก</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-manchester-utd">แมนฯ ยูไนเต็ด</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-liverpool">ลิเวอร์พูล</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-chelsea">เชลซี</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-arsenal" title="ผลบอลอาร์เซน่อล">อาร์เซน่อล</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-manchester-city" title="ผลบอลแมนเชสเตอร์ ซิตี้">แมนเชสเตอร์ ซิตี้</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header"> Leagues</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>พรีเมียร์ลีก">พรีเมียร์ลีก</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>ลาลีกา">ลา ลีกา สเปน</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>บุนเดสลีกา"> บุนเดสลีกา เยอรมนี</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>เซเรียอา">เซเรีย อา อิตาลี</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>ลีกเอิง">ลีกเอิง ฝรั่งเศส</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>ไทยพรีเมียร์ลีก">ไทยพรีเมียร์ลีก</a></li>

                                                    <li class="divider"></li>

                                                    <li class="dropdown-header">Tournaments</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>tournament/euro2016qual">ยูโร 2016 <label class="live blink_me" style="width: inherit;">NEW</label></a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>">  โคปา อเมริกา 2016</a></li>
                                                    <li><a name="<?php echo BASE_HREF; ?>worldcup2018" title="ฟุตบอลโลก 2018">ฟุตบอลโลก 2018</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>tournament/worldcup2018asianqual"> ฟุตบอลโลก 2018 รอบคัดเลือก โซนเอเชีย</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>tournament/worldcup2018saqual"> ฟุตบอลโลก 2018 รอบคัดเลือก โซนอเมริกาใต้  </a></li>

                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header"> Cups</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>championsleague">ยูฟา แชมเปียนส์ ลีก</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>europaleague">ยูฟา ยูโรป้า ลีก</a></li>
													<li><a href="<?php echo BASE_HREF; ?>tournament/afcchampionsleague">เอเอฟซี แชมเปี้ยนส์ ลีก</a></li>
                                                    <li><a name="">เอฟเอ คัพ</a></li>
                                                    <li><a name="">แคปิตอล วัน คัพ</a></li>

                                                </ul>
                                            </li>
                                        </ul>
                                        <!-- End Mega Dropdown--> 
                                    </li>
                                    <li class="dropdown mega-dropdown"> <a href="<?php echo BASE_HREF; ?>newslist.php" class="dropdown-toggle" data-toggle="dropdown">ข่าว<span class="fa fa-angle-down pull-right"></span></a>
                                        <ul class="dropdown-menu mega-dropdown-menu">
                                            <li class="col-sm-12"><h3><a href="<?php echo BASE_HREF; ?>program.php" title="โปรแกรมบอล">ข่าว</a></h3></li>
                                            <li class="col-sm-2">
                                                <ul>
                                                    <li class="dropdown-header" > Leagues</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>พรีเมียร์ลีก/news" title="ข่าวพรีเมียร์ลีก">ข่าวพรีเมียร์ลีก</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>ลาลีกา/news" title="ข่าวลาลีกา">ข่าวลาลีกา</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>บุนเดสลีกา/news" title="บอลวันนี้">ข่าวบุนเดสลีกา</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>เซเรียอา/news" title="ข่าวเซเรียอา">ข่าวเซเรียอา</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>ลีกเอิง/news" title="ข่าวลีกเอิง">ข่าวลีกเอิง</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>ไทยพรีเมียร์ลีก/news" title="ข่าวไทยพรีเมียร์ลีก">ข่าวไทยพรีเมียร์ลีก</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-2">
                                                <ul>
                                                    <li class="dropdown-header" > Cups</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>championsleague/news" title="ข่าวยูฟา แชมเปียนส์ ลีก">ข่าวยูฟา แชมเปียนส์ ลีก</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>europaleague/news" title="ข่าวยูฟา ยูโรป้า ลีก">ข่าวยูฟา ยูโรป้า ลีก</a></li>
													<li><a href="<?php echo BASE_HREF; ?>tournament/afcchampionsleague/news" title="ข่าวเอเอฟซี แชมเปี้ยนส์ ลีก">ข่าวเอเอฟซี แชมเปี้ยนส์ ลีก</a></li>
                                                    <li><a name="<?php echo BASE_HREF; ?>ลาลีกา/news" title="ข่าวเอฟเอ คัพ">ข่าวเอฟเอ คัพ</a></li>
                                                    <li><a name="<?php echo BASE_HREF; ?>บุนเดสลีกา/news" title="ข่าวแคปิตอล วัน คัพ">ข่าวแคปิตอล วัน คัพ</a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-5">
                                                <ul>
                                                    <li class="dropdown-header" > Tournaments</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>tournament/euro2016qual/news">ข่าวฟุตบอลยูโร 2016 <label class="live blink_me" style="width: inherit;">NEW</label></a></li>
                                                    <li><a name="<?php echo BASE_HREF; ?>worldcup2018/news" title="ข่าวฟุตบอลโลก 2018">ข่าวฟุตบอลโลก 2018</a></li>
													<li><a href="<?php echo BASE_HREF; ?>tournament/worldcup2018asianqual/news">ข่าวฟุตบอลโลก 2018 รอบคัดเลือก โซนเอเชีย</a></li>
													<li><a href="<?php echo BASE_HREF; ?>tournament/worldcup2018saqual/news">ข่าวฟุตบอลโลก 2018 รอบคัดเลือก โซนอเมริกาใต้  </a></li>
                                                </ul>
                                            </li>
                                            <li class="col-sm-3">
                                                <ul>
                                                    <li class="dropdown-header"  > Teams</li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-manchester-utd/news" title="ข่าวแมนฯ ยูไนเต็ด">ข่าวแมนฯ ยูไนเต็ด</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-liverpool/news" title="ข่าวลิเวอร์พูล">ข่าวลิเวอร์พูล</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-chelsea/news" title="ข่าวเชลซี">ข่าวเชลซี</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-arsenal/news" title="ข่าวอาร์เซน่อล">ข่าวอาร์เซน่อล</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-manchester-city/news" title="ข่าวแมนเชสเตอร์ ซิตี้">ข่าวแมนเชสเตอร์ ซิตี้</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-real-madrid/news" title="ข่าวเรอัล มาดริด">ข่าวเรอัล มาดริด</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-barcelona/news" title="ข่าวบาร์เซโลน่า">ข่าวบาร์เซโลน่า</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-bayern-munich/news" title="ข่าวบาเยิร์น มิวนิค">ข่าวบาเยิร์น มิวนิค</a></li>
                                                    <li><a href="<?php echo BASE_HREF; ?>team-ac-milan/news" title="ข่าวเอซี มิลาน">ข่าวเอซี มิลาน</a></li>
                                                </ul>
                                            </li>
                                        </ul> 
                                        <!-- End Mega Dropdown--> 

                                    </li>
                                    <li><a href="<?php echo BASE_HREF; ?>transfer-market">ตลาดนักเตะ</a></li>
                                </ul>
                            </div>
                            <!-- /.nav-collapse --> 
                        </nav>
                    </div>
                    <div class="top-bar">
                        <div class="container">
                            <ol class="breadcrumb pull-left">
                                <?php echo $this->breadcrum; ?>
                            </ol>

                            <!-- Social media starts --> 



                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End nav Contiainer-->
            
            


<?php if($_SERVER["HTTP_MOBILE"]==1 && false){?>
<div class="container  ad_banner_desktop">
    <div id="zt_543519_1" style="display:show"> 
        <script id="zt_543519_1" language="javascript"> 
            if(typeof zmt_mtag !='undefined' && typeof zmt_mtag.zmt_render_placement !='undefined')
                {
                    zmt_mtag.zmt_render_placement(p543519_1);
                }
        </script>
    </div>
</div>
<?php //}else{ ?>
<div id="zt_39433_1" style="display:show; width:320px; height:100px; margin:0 auto"> 
 <script id="zt_39433_1" language="javascript"> 
if(typeof zmt_mtag !='undefined' && typeof zmt_mtag.zmt_render_placement !='undefined')
{
     zmt_mtag.zmt_render_placement(p39433_1);
}
 </script>
 </div>
<?php } ?>


            <?php
            $tempURL = explode('/', $_SERVER['REQUEST_URI']);
            $pureURL = explode('?', $tempURL[1]);

            if (($pureURL[0] != 'games') && ($pureURL[0] != 'profile')) {
                ?>
                <div class="container">
                    <a href="http://football.kapook.com/games"><img src="<?php echo BASE_HREF; ?>assets/img/Game-bar.jpg" alt="เกมทายผลบอล football Game" style="width: 100%;"></a>
                </div>
                <br>
            <?php } ?>
        <?php } ?>


        <?php include "notice-policy.tpl.php"; ?>

<script language="JavaScript">
	$('.link-play-game').on('click', function(event){
		event.stopPropagation();
		event.preventDefault();
	});
</script>

