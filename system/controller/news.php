<?php

if (!defined('MINIZONE'))
    exit;

class News extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {

        parent::__construct();

        $this->news_model = $this->minizone->model('news_model');
    }

    ///////////////////////////////////////////////// Index /////////////////////////////////////////////////

    function index() {
		/*-------------REDIS----------------*/
		$redis_server = '192.168.1.78-6379-9';
		$redis = $this->minizone->library('Predis');
		$redis->setConfig($redis_server);
		/*----------------------------------*/
        $this->view->assign('mem_lib', $this->mem_lib);
        if (isset($_GET['page']))
            $page = $_GET['page'];
        else
            $page = 1;
		
		$NewsListContents = json_decode($redis->get("newslist_12_" . $page . "_news_id"),true);
		if((!$NewsListContents)||($_REQUEST['remove_cache']==1)){
			$NewsListContents = $this->news_model->getNewsList($page,$_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsListContents', $NewsListContents['data']);
        $this->view->assign('NewsPage', $NewsListContents['paged']);
        $this->view->assign('NewsPageTotal', $NewsListContents['total_pages']);
		
		$NewsTopHitsContents = json_decode($redis->get("newstophit_5_1"),true);
		if((!$NewsTopHitsContents)||($_REQUEST['remove_cache']==1)){
			 $NewsTopHitsContents = $this->news_model->getNewsTopHits($_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsTopHitsContents', $NewsTopHitsContents['data']);

        $breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = 'ข่าว';
        $breadcrum[1]['active'] = true;
        parent::_setBreadcrum($breadcrum);

        $data['seo_title'] = 'ข่าว ข่าวฟุตบอล ข่าวบอลวันนี้ ข่าวแมนยู ข่าวซื้อขายนักเตะ';
        $data['seo_description'] = 'ข่าว ข่าวฟุตบอล อ่านข่าวบอล ข่าวแมนยู ติดตาม ข่าวแมนยูล่าสุด ข่าวแมนยูซื้อนักเตะ ข่าวแมนยูวันนี้ ข่าวลิเวอร์พูล ข่าวลิเวอร์พูลล่าสุด ข่าวพรีเมียร์ลีก ตลาดซื้อขายนักเตะ ข่าวย้ายทีม';
        $data['seo_keywords'] = 'ข่าว, ข่าวฟุตบอล, ข่าวบอล, ข่าวแมนยู, ข่าวแมนยูล่าสุด, ข่าวแมนยูซื้อนักเตะ, ข่าวแมนยูวันนี้, ข่าวลิเวอร์พูล, ข่าวลิเวอร์พูลล่าสุด, ข่าวอาร์เซน่อล, ข่าวพรีเมียร์ลีก, ตลาดซื้อขายนักเตะ';

        $data['fb_title'] = 'ข่าว ข่าวฟุตบอล ข่าวแมนยู ข่าวลิเวอร์พูล ข่าวซื้อขายนักเตะ';
        $data['fb_description'] = 'ข่าว ข่าวฟุตบอล อ่านข่าวบอล ข่าวแมนยู ข่าวลิเวอร์พูล ข่าวซื้อขายนักเตะ';
		$data['fb_img'] = 'http://football.kapook.com/assets/og/2014-11/page/ข่าว.jpg';

        parent::_setSocial($data);

        $this->_header();
        $this->view->render($this->root_view . '/home/newslist.tpl');
        $this->_footer();
    }

    function top10() {
        $this->view->assign('mem_lib', $this->mem_lib);
        if (isset($_GET['page']))
            $page = $_GET['page'];
        else
            $page = 1;
		/*-------------REDIS----------------*/
		$redis_server = '192.168.1.78-6379-9';
		$redis = $this->minizone->library('Predis');
		$redis->setConfig($redis_server);
		/*----------------------------------*/
		$NewsListContents = json_decode($redis->get("newstop10_8_" . $page . "_news_id"),true);
		if((!$NewsListContents)||($_REQUEST['remove_cache']==1)){
			$NewsListContents = $this->news_model->getNewsTop10List($page,$_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsListContents', $NewsListContents['data']);
        $this->view->assign('NewsPage', $NewsListContents['paged']);
        $this->view->assign('NewsPageTotal', $NewsListContents['total_pages']);
        
		$NewsTopHitsContents = json_decode($redis->get("newstophit_5_1"),true);
		if((!$NewsTopHitsContents)||($_REQUEST['remove_cache']==1)){
			 $NewsTopHitsContents = $this->news_model->getNewsTopHits($_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsTopHitsContents', $NewsTopHitsContents['data']);

        $breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = 'บทความฟุตบอล';
        $breadcrum[1]['active'] = true;
        parent::_setBreadcrum($breadcrum);

        $this->_header();
        $this->view->render($this->root_view . '/home/feature.tpl');
        $this->_footer();
    }

    function view($id) {
		/*-------------REDIS----------------*/
		$redis_server = '192.168.1.78-6379-9';
		$redis = $this->minizone->library('Predis');
		$redis->setConfig($redis_server);
		/*----------------------------------*/
        if (!isset($id))
            $id = 1;
        $id = intval($id);
		$NewsContents = json_decode($redis->get('news_' . $id),true);

		if((!$NewsContents)||($_REQUEST['remove_cache']==1)){
			$NewsContents = $this->news_model->getNewsSingle($id,$_REQUEST['remove_cache'] == 1);
		}
            
		if ($NewsContents['rowContent'][0]['isRedirect'] != false) {
			header("Location: " . $NewsContents['rowContent'][0]['link']);
			exit();
		}
		
		if($this->mem_lib->get('Football2014-news-views-trueViews' . $id))
			$allview = $this->mem_lib->get('Football2014-news-views-trueViews' . $id);
		else
			$allview = $NewsView['view'];

        $this->view->assign('NewsContents', $NewsContents);
        
		$NewsTopHitsContents = json_decode($redis->get("newstophit_5_1"),true);
		if((!$NewsTopHitsContents)||($_REQUEST['remove_cache']==1)){
			 $NewsTopHitsContents = $this->news_model->getNewsTopHits($_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsTopHitsContents', $NewsTopHitsContents['data']);

        $Views              =       $this->mem_lib->get('Football2014-news-view-' . $id);
        $this->mem_lib->set('Football2014-news-view-' . $id, $Views + 1);
//
//        if (($Views+1) >= 10)
//            file_get_contents("http://202.183.165.27:8001/api_news/update_view.php?news_id=$id");
        
        $this->view->assign('MC_Views', $allview);
        $breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = 'ข่าว';
        $breadcrum[1]['link'] = BASE_HREF . "newslist.php";
        $breadcrum[2]['text'] = $NewsContents['rowContent'][0]['subject'];
        $breadcrum[2]['active'] = true;
        parent::_setBreadcrum($breadcrum);

        $data['seo_title'] = $NewsContents['seo']['meta']['title'];
        $data['seo_description'] = $NewsContents['seo']['meta']['des'];
        $data['seo_keywords'] = $NewsContents['seo']['meta']['tag'];
        if ($_REQUEST['debug']==1) {
            echo "<pre>";
            var_dump($NewsContents);
            exit();
        }
        $data['fb_title'] = $NewsContents['seo']['fb']['title'];
        $data['fb_description'] = $NewsContents['seo']['fb']['des'];
        $data['fb_img'] = $NewsContents['seo']['fb']['img'];
        parent::_setSocial($data);

//        echo "mobile".$_SERVER["HTTP_MOBILE"];
        if($_SERVER["HTTP_MOBILE"] == "1"  && $_GET['line'] == 1) {
            $this->view->assign('truehit_key', 'Line_'.$id.'_football');
         } else if( $_SERVER["HTTP_MOBILE"] == "2" && $_GET['line'] == 1  ) {
            $this->view->assign('truehit_key', 'm_Line_'.$id.'_football');
        } else {
            $this->view->assign('truehit_key', 'content_football');
        }
        
        $this->view->assign('content', 'content');
        
        $this->info_lib = $this->minizone->library('info_lib');
        $content_display = $this->info_lib->get_news($id);
        $this->view->assign('content_display', $content_display);
        

        $this->_header();
        $page_render = $this->root_view . '/home/news.tpl';
        if ($_GET['comment']==1) {
            $page_render = $this->root_view . '/home/news_comment.tpl';
        }
        $this->view->render($page_render);
        $this->_footer();
    }

    function viewdemo($id) {
        echo "<!-- new -->";
        if (!isset($id))
            $id = 1;
        intval($id);
		$NewsContents = $this->news_model->getNewsSingle($id,$_REQUEST['remove_cache'] == 1);

        $tmpViews = $this->mem_lib->get('Football2014-news-views-tmpViews' . $id);
        $currentViews = $tmpViews + 1;
        $this->mem_lib->set('Football2014-news-views-tmpViews' . $id, $currentViews);

        /*$trueview = $this->mem_lib->get('Football2014-news-views-trueViews' . $id);

        if (!$this->mem_lib->get('Football2014-news-views-trueViews' . $id)) {
            $NewsView = $this->news_model->updateNewsView($id,1);
            $this->mem_lib->set('Football2014-news-views-trueViews' . $id,$NewsView['view']);
            $allview = $this->mem_lib->get('Football2014-news-views-trueViews' . $id);
        }*/
 
        if (($currentViews >= 10) || (!$this->mem_lib->get('Football2014-news-views-trueViews' . $id))){
            $NewsView = $this->news_model->updateNewsView($id,$currentViews);
            $this->mem_lib->set('Football2014-news-views-tmpViews' . $id, 0);
            $this->mem_lib->set('Football2014-news-views-trueViews' . $id, $NewsView['view']);
            $allview = $NewsView['view'];

        }else{
            
            $this->mem_lib->set('Football2014-news-views-trueViews' . $id, ($this->mem_lib->get('Football2014-news-views-trueViews' . $id)+1));
            $allview = $this->mem_lib->get('Football2014-news-views-trueViews' . $id);
        }

        $this->view->assign('NewsContents', $NewsContents);
        
		$NewsTopHitsContents = json_decode($redis->get("newstophit_5_1"),true);
		if((!$NewsTopHitsContents)||($_REQUEST['remove_cache']==1)){
			 $NewsTopHitsContents = $this->news_model->getNewsTopHits($_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsTopHitsContents', $NewsTopHitsContents['data']);

        $Views              =       $this->mem_lib->get('Football2014-news-view-' . $id);
        $this->mem_lib->set('Football2014-news-view-' . $id, $Views + 1);

        if (($Views+1) >= 10)
            file_get_contents("http://202.183.165.27:8001/api_news/update_view.php?news_id=$id");
        
        $this->view->assign('MC_Views', $allview);
        $breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = 'ข่าว';
        $breadcrum[1]['link'] = BASE_HREF . "newslist.php";
        $breadcrum[2]['text'] = $NewsContents['rowContent'][0]['subject'];
        $breadcrum[2]['active'] = true;
        parent::_setBreadcrum($breadcrum);

        $data['seo_title'] = $NewsContents['seo']['meta']['title'];
        $data['seo_description'] = $NewsContents['seo']['meta']['des'];
        $data['seo_keywords'] = $NewsContents['seo']['meta']['tag'];

        $data['fb_title'] = $NewsContents['seo']['fb']['title'];
        $data['fb_description'] = $NewsContents['seo']['fb']['des'];
        $data['fb_img'] = $NewsContents['seo']['fb']['img'];
        parent::_setSocial($data);

        $this->view->assign('truehit_key', 'content_football');

        $this->info_lib = $this->minizone->library('info_lib');
        $content_display = $this->info_lib->get_news($id);
        $this->view->assign('content_display', $content_display);

        $this->_header();
        $page_render = $this->root_view . '/home/news.tpl';
        if ($_GET['comment']==1) {
            $page_render = $this->root_view . '/home/news_comment.tpl';
        }
        $this->view->render($page_render);
        $this->_footer();
    }

    /*
      function country($league) {
      $page = 1;
      $NewsLeagueContents = $this->news_model->getNewsLeague($league,$page,$_REQUEST['remove_cache'] == 1);
      $this->view->assign('NewsLeagueContents', $NewsLeagueContents['data']);


      $this->_header();
      $this->view->render($this->root_view.'/home/league.tpl');
      $this->_footer();
      } */

    function league($league) {
		
		/*-------------REDIS----------------*/
		$redis_server = '192.168.1.78-6379-9';
		$redis = $this->minizone->library('Predis');
		$redis->setConfig($redis_server);
		/*----------------------------------*/
	
		$LeagueName['อังกฤษ'] = 'พรีเมียร์ลีก';
        $LeagueName['อิตาลี'] = 'เซเรียอา';
        $LeagueName['สเปน'] = 'ลาลีกา';
        $LeagueName['ผรั่งเศส'] = 'ลีกเอิง';
        $LeagueName['เยอรมัน'] = 'บุนเดสลีกา';
        $LeagueName['ไทย'] = 'ไทยพรีเมียร์ลีก';
	
        $this->view->assign('mem_lib', $this->mem_lib);

        if (isset($_GET['page']))
            $page = $_GET['page'];
        else
            $page = 1;

		$NewsListContents = json_decode($redis->get("newsconutry_" . $league . "_12_" . $page . "_news_id" ),true);
		if((!$NewsListContents)||($_REQUEST['remove_cache']==1)){
			$NewsListContents = $this->news_model->getNewsLeague($league, $page, $_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsListContents', $NewsListContents['data']);
        $this->view->assign('NewsPage', $NewsListContents['paged']);
        $this->view->assign('NewsPageTotal', $NewsListContents['total_pages']);
        $this->view->assign('NewsPageTitle', $LeagueName[$NewsListContents['country_news']['name_th']]);
        
		$NewsTopHitsContents = json_decode($redis->get("newstophit_5_1"),true);
		if((!$NewsTopHitsContents)||($_REQUEST['remove_cache']==1)){
			 $NewsTopHitsContents = $this->news_model->getNewsTopHits($_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsTopHitsContents', $NewsTopHitsContents['data']);

        $name_league = $NewsListContents['country_news']['name_th'];

        $breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = $name_th = $LeagueName[$NewsListContents['country_news']['name_th']];
        $breadcrum[1]['link'] = BASE_HREF . $LeagueName[$NewsListContents['country_news']['name_th']];
        $breadcrum[2]['text'] = "ข่าว" . $LeagueName[$NewsListContents['country_news']['name_th']];
        $breadcrum[2]['active'] = true;
        parent::_setBreadcrum($breadcrum);

        $data['seo_title'] = "ข่าว$name_th สรุปข่าว$name_th ข่าว ข่าวฟุตบอลล่าสุด";
        $data['seo_description'] = "ข่าว ข่าว$name_th ข่าว$name_thวันนี้ ข่าว$name_thล่าสุด ข่าวฟุตบอล สรุปข่าว$name_th ความเคลื่อนไหวฟุตบอล$name_th รวมข่าว$name_th";
        $data['seo_keywords'] = "ข่าว$name_th, ข่าวฟุต$name_th, ข่าว, สรุปข่าว$name_th, ข่าวฟุตบอล, ข่าวบอล, $name_th, สรุปผล$name_th";

        $data['fb_title'] = "ข่าว$name_th สรุปข่าว$name_th อัพเดทข่าว$name_thล่าสุด คลิกเลย";
        $data['fb_description'] = "$name_th สรุปข่าว$name_th ข่าวฟุต$name_th ข่าว อัพเดทความเคลื่อนไหว ข่าว$name_thล่าสุด";
        parent::_setSocial($data);

        $this->_header();
        $this->view->render($this->root_view . '/home/newslist.tpl');
        $this->_footer();
    }

	function squad($teamNameEN) {
		/*-------------REDIS----------------*/
		$redis_server = '192.168.1.78-6379-9';
		$redis = $this->minizone->library('Predis');
		$redis->setConfig($redis_server);
		/*----------------------------------*/
	
		$this->view->assign('mem_lib', $this->mem_lib);
				
		if (isset($_GET['page']))
			$page = $_GET['page'];
		else $page = 1;
		$teamID = $this->mem_lib->get('Football2014-Team-IDByName-'.$teamNameEN);
		$team = $teamNameTH = $this->mem_lib->get('Football2014-Team-NameTHShort-'.$teamID);
		$teamNameEN = str_replace("-"," ",$teamNameEN);
		$teaml = $this->mem_lib->get('Football2014-Team-NameTH-'.$teamID);
		
		$NewsListContents = json_decode($redis->get("newsteam_" . $teamID . "_12_" . $page . "_news_id_-1"),true);
		if((!$NewsListContents)||($_REQUEST['remove_cache']==1)){
			$NewsListContents = $this->news_model->getNewsByTeam($teamID,$page,$_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsListContents', $NewsListContents['data']);
		$this->view->assign('NewsPage', $NewsListContents['paged']);
		$this->view->assign('NewsPageTotal', $NewsListContents['total_pages']);
		$this->view->assign('TeamTitle', $team);
		
		$NewsTopHitsContents = json_decode($redis->get("newstophit_5_1"),true);
		if((!$NewsTopHitsContents)||($_REQUEST['remove_cache']==1)){
			 $NewsTopHitsContents = $this->news_model->getNewsTopHits($_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsTopHitsContents', $NewsTopHitsContents['data']);
		
		
		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = $teamNameTH;
		$breadcrum[1]['link'] = BASE_HREF."team-".strtolower(str_replace(" ","-",$teamNameEN));
		$breadcrum[2]['text'] = "ข่าว".$teamNameTH;
        $breadcrum[2]['active'] = true;
        parent::_setBreadcrum($breadcrum);
		
		$data['seo_title'] = "$team ผลบอล ข่าว$team";
		$data['seo_description'] = "$team ผลบอล ข่าว$team โปรแกรมการแข่งขัน โปรแกรมแข่ง ตารางบอล ดูตารางบอล ตารางแข่ง $teaml สรุปผลบอล$team สถิติย้อนหลัง $teaml สรุปข่าว$team คลิกเลย";
		$data['seo_keywords'] = "$team, $teaml, ข่าว$team, ผลบอล$team, โปรแกรมการแข่งขัน$team, ผลบอล, ตารางบอล, โปรแกรมบอล, สถิติย้อนหลัง";

		$data['fb_title'] = "$team ข่าว$team ผลบอล ข่าว$teaml";
		$data['fb_description'] = "ผลบอล$team ข่าว$team โปรแกรมการแข่งขัน ดูตารางแข่ง $teaml สรุปผลบอล$team คลิกเลย";
		parent::_setSocial($data);
                
	$meta_category = array(
            'manchester utd' => 'football_manchesterutdnews',
            'liverpool' => 'football_liverpoolnews',
            'chelsea' => 'football_chelseanews',
            'arsenal' => 'football_arsenalnews',
            'manchester city' => 'football_manchestercitynews',
            'real madrid' => 'football_realmadridnews',
            'barcelona' => 'football_barcelonanews',
            'bayern munich' => 'football_bayernmunichnews',
            'ac milan' => 'football_acmilannews',
        );
                
        // meta_category
        $this->view->assign('meta_category', $meta_category[$teamNameEN]);
        
        $this->_header();
        $this->view->render($this->root_view.'/home/newslist.tpl');
        $this->_footer();

    }
	
	function view_test($id) {
        $url = "http://192.168.100.166/pong_spark/";
		
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $url); 
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_TIMEOUT, 120);
		$result = 	curl_exec($ch);
		curl_close($ch);
		var_dump($result);
    }
    
    function newsview($id) {
        if ($id!="") {
            $tmpViews = $this->mem_lib->get('Football2014-news-views-tmpViews' . $id);
            $currentViews = $tmpViews + 1;
            $this->mem_lib->set('Football2014-news-views-tmpViews' . $id, $currentViews);
            echo $currentViews . '<br>';	
       
            if ($currentViews >= 10){
                $NewsView = $this->news_model->updateNewsView($id,$currentViews);
				
				$this->mem_lib->set('Football2014-news-views-trueViews' . $id, intval($NewsView['view']));
				echo $NewsView['view'] . '<br>';
                $this->mem_lib->set('Football2014-news-views-tmpViews' . $id, 0);
            }
			echo '-------------';
        }
    }
}