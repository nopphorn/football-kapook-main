<?php /* var $tmpMatch */ ?>
<a href="<?php echo strtolower($tmpMatch['MatchPageURL']); ?>" class="btn <?php echo $this->football_lib->getClassColorByMatchStatus($tmpMatch['MatchStatus']); ?> btn-xs tooltip_top aVS<?php echo $tmpMatch['id']; ?>" target="_blank" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">
<?php
	$ETScoreArr = explode('-', $tmpMatch['ETScore']);
	$PNScoreArr = explode('-', $tmpMatch['PNScore']);
	if($tmpMatch['MatchStatus']=='Sched'){?>
		vs
	<?php }else if($tmpMatch['MatchStatus']=='Fin'){
		if(( $tmpMatch['PNScore'] == '-' )&&( $tmpMatch['ETScore'] == '-')){ /*90 min fulltime*/
			echo $tmpMatch['Team1FTScore'] . ' - ' . $tmpMatch['Team2FTScore']; 
		}else if(( $tmpMatch['PNScore'] != '-' )&&( $tmpMatch['ETScore'] == '-')){ /*90 min + pen*/
			echo $tmpMatch['Team1FTScore'] . ' - ' . $tmpMatch['Team2FTScore'] . '<br>( ' . $PNScoreArr[0] . ' PEN ' . $PNScoreArr[1] . ' )';
		}else if(( $tmpMatch['PNScore'] == '-' )&&( $tmpMatch['ETScore'] != '-')){ /*120 min*/
			echo $ETScoreArr[0] . ' - ' . $ETScoreArr[1];
		}else { /*120 min + pen*/
			echo $ETScoreArr[0] . ' - ' . $ETScoreArr[1] . '<br>( ' . $PNScoreArr[0] . ' PEN ' . $PNScoreArr[1] . ' )';
		}
	}else if($tmpMatch['MatchStatus']=='Canc'){?>
		c-c
	<?php }else if($tmpMatch['MatchStatus']=='Post'){?>
		p-p
	<?php }else if($tmpMatch['MatchStatus']=='1 HF'){
		echo $tmpMatch['Team1FTScore']; ?> - <?php echo $tmpMatch['Team2FTScore']; ?><br>
		1 HF <?php echo $tmpMatch['Minute']; ?>'
	<?php }else if($tmpMatch['MatchStatus']=='2 HF'){
		echo $tmpMatch['Team1FTScore']; ?> - <?php echo $tmpMatch['Team2FTScore']; ?><br>
		2 HF <?php echo $tmpMatch['Minute']; ?>'
	<?php }else if($tmpMatch['MatchStatus']=='H/T'){
		echo $tmpMatch['Team1FTScore']; ?> - <?php echo $tmpMatch['Team2FTScore']; ?><br>
		H/T
	<?php }else if($tmpMatch['MatchStatus']=='Pen'){
		if( $tmpMatch['ETScore'] == '-'){ /*90 min + pen*/
			echo $tmpMatch['Team1FTScore'] . ' - ' . $tmpMatch['Team2FTScore'] . '<br>( ' . $PNScoreArr[0] . ' PEN ' . $PNScoreArr[1] . ' )';
		}else { /*120 min + pen*/
			echo $ETScoreArr[0] . ' - ' . $ETScoreArr[1] . '<br>( ' . $PNScoreArr[0] . ' PEN ' . $PNScoreArr[1] . ' )';
		}
	}else if($tmpMatch['MatchStatus']=='E/T'){
		echo $ETScoreArr[0] . ' - ' . $ETScoreArr[1]; ?><br>
		E/T <?php echo $tmpMatch['Minute'];?>'
	<?php }else{?>
		N/A
	<?php }?>
</a>