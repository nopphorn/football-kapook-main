<link rel="stylesheet" href="<?php echo BASE_HREF; ?>assets/css/bracketz.css">
<script src="<?php echo BASE_HREF; ?>assets/js/bracketz.js"></script>

<style>
.match_unit.highlight{
    padding: 0px 0px;
    margin-bottom: 0px;
    border-radius: 0px;
	border:none;
	background-color: #F0F0F0;
}
.m_segment.highlight{
    padding: 0px 0px;
    margin-bottom: 0px;
    border-radius: 0px;
	border:none;
	background-color: #F0F0F0;
	border-right: solid 2px;
}
</style>

<div class="container league euro2016">
	<?php
	
	$allow_id = array(43,94,363252,478011,786992,797014,856305,930736,36305,775500);
	$userid = uid;
	if($_REQUEST['tbug']=='debug'){
		echo uid;
		exit;
	}
	
	$cupname 		= 	$this->InfoCupContents['nameTH'];
	$cupshortname 	= 	$this->InfoCupContents['nameTHShort'];
	$cuppathname	=	$this->InfoCupContents['nameURL'];
	$tabSelect		=	$this->InfoCupContents['TabTable'];
	
	include dirname(__FILE__) . "/clip_table.tpl.php";
    include dirname(__FILE__) . "/menubar.tpl.php";
	
	if ( in_array($userid,$allow_id) && ($_SERVER['REQUEST_METHOD'] == 'POST') && ($_POST['do']=="updateclip")) {
		//var_dump($_POST);
		$this->mem_lib->set('Football2014-Clip-League-URL',$_POST['clip_league_url']);
		echo "Clip Updated";
	}
	
    ?>


  
<div class="row">
	<div class="col-md-12">
		<div class="news ">
			<h1 class="font-display" style="margin-top: 0px;"><i class="fa fa-sort-amount-desc"></i>ตารางคะแนน</h1>
			<!-- Nav tabs -->
			<ul class="nav nav-tabs tab-i" role="tablist">
				<?php
				$i = 0;
				$isActive = false;
				if(count($this->TableContents)) {
					$i++;
					if(intval($tabSelect) == -1){
						$class = 'active';
						$isActive = true;
					}else{
						$class = '';
					} ?>
					<li role="presentation" class="<?php echo $class; ?>" >
						<a href="#tab-i-<?php echo $i; ?>" aria-controls="home" role="tab" data-toggle="tab">ตารางคะแนน</a>
					</li>
				<?php }
				foreach($this->TableRoundContents as $tmpRound){
					$i++;
					if(	(intval($tabSelect) == intval($tmpRound['id']))	||
						(intval($tabSelect) == -1)&&(!$isActive)
					){
						$class = 'active';
						$isActive = true;
					}else{
						$class = '';
					} ?>
					<li role="presentation" class="<?php echo $class; ?>"><a href="#tab-i-<?php echo $i; ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $tmpRound['name']; ?></a></li>
				<?php } ?>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<?php
				$i = 0;
				$isActive = false;
				if(count($this->TableContents)) {
					$i++;
					if(intval($tabSelect) == -1){
						$class = 'tab-pane active';
						$isActive = true;
					}else{
						$class = 'tab-pane';
					} ?>
				<div role="tabpanel" class="<?php echo $class; ?>" id="tab-i-<?php echo $i; ?>">
					<div class="col-md-12">
						<div class="table_standing">
							<?php foreach( $this->TableContents as $tmpTable ){ ?>
								<h2 class="font-display" style="margin-top: 0;"><?php echo $tmpTable['name'];?></h2>
								<div class="col-md-12">
									<?php if(($tmpTable['type']==1)||($tmpTable['type']==2)){
										include dirname(__FILE__) . "/table_standing.tpl.php";
									}elseif($tmpTable['type']==3){
										include dirname(__FILE__) . "/table_bracket.tpl.php";
									}?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<?php }
				foreach($this->TableRoundContents as $tmpRound){
					$i++;
					if(	(intval($tabSelect) == intval($tmpRound['id']))	||
						(intval($tabSelect) == -1)&&(!$isActive)
					){
						$class = 'tab-pane active';
						$isActive = true;
					}else{
						$class = 'tab-pane';
					} ?>
				<div role="tabpanel" class="<?php echo $class; ?>" id="tab-i-<?php echo $i; ?>">
					<div class="col-md-12">
						<div class="table_standing">
							<?php foreach( $tmpRound['table'] as $tmpTable ){ ?>
								<h2 class="font-display" style="margin-top: 0;"><?php echo $tmpTable['name'];?></h2>
								<div class="col-md-12">
									<?php if(($tmpTable['type']==1)||($tmpTable['type']==2)){
										include dirname(__FILE__) . "/table_standing.tpl.php";
									}elseif($tmpTable['type']==3){
										include dirname(__FILE__) . "/table_bracket.tpl.php";
									}?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
</div></div>
