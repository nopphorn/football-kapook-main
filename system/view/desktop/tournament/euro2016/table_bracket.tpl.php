<?php
	$monthtothai['01'] ="ม.ค.";
	$monthtothai['02'] ="ก.พ.";
	$monthtothai['03'] ="มี.ค.";
	$monthtothai['04'] ="เม.ย.";
	$monthtothai['05'] ="พ.ค.";
	$monthtothai['06'] ="มิ.ย.";
	$monthtothai['07'] ="ก.ค.";
	$monthtothai['08'] ="ส.ค.";
	$monthtothai['09'] ="ก.ย.";
	$monthtothai['10'] ="ต.ค.";
	$monthtothai['11'] ="พ.ย.";
	$monthtothai['12'] ="ธ.ค.";
	
	$daytothai['Sun'] = "อา.";
	$daytothai['Mon'] = "จ.";
	$daytothai['Tue'] = "อ.";
	$daytothai['Wed'] = "พ.";
	$daytothai['Thu'] = "พฤ.";
	$daytothai['Fri'] = "ศ.";
	$daytothai['Sat'] = "ส.";
?>
<div class="brackets_container">
	<table>
	<!--rounds container-->
	<thead><tr>
		<?php if(count($tmpTable['RoundData'])>=4){ ?>
		<th style="text-align: center;">
			<span>รอบ 16 ทีม</span>
		</th>
		<?php }
		if(count($tmpTable['RoundData'])>=3){ ?>
		<th style="text-align: center;">
			<span>รอบ 8 ทีม</span>
		</th>
		<?php }
		if(count($tmpTable['RoundData'])>=2){ ?>
		<th style="text-align: center;">
			<span>รอบรองชนะเลิศ</span>
		</th>
		<?php } ?>
		<th style="text-align: center;">
			<span>รอบชิงชนะเลิศ</span>
		</th>
		<?php if(count($tmpTable['RoundData'])>=2){ ?>
		<th style="text-align: center;">
			<span>รอบรองชนะเลิศ</span>
		</th>
		<?php }
		if(count($tmpTable['RoundData'])>=3){ ?>
		<th style="text-align: center;">
			<span>รอบ 8 ทีม</span>
		</th>
		<?php }
		if(count($tmpTable['RoundData'])>=4){ ?>
		<th style="text-align: center;">
			<span>รอบ 16 ทีม</span>
		</th>
		<?php } ?>
	</tr></thead>
	<!--matches container-->
	<tbody><tr id="playground">
		<?php
		$index_round 	= 	0;
		?>
		<!-- Round of 16 -->
		<?php if(count($tmpTable['RoundData'])>=4){ ?>
			<td class="round_column r_16">
				<?php for($i=0;$i<4;$i++) {
				$tmpMatch = $tmpTable['RoundData'][$index_round][$i]; ?>
				<div class="mtch_container">
					<div class="match_unit">
						<!-- Team 1 -->
						<div class="m_segment m_top <?php
							if($tmpMatch['TeamWinner'] == 1){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team1KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team1KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team1URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team1']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team1FTScore']; ?></strong>
							</span>
						</div>
						<!-- Team 2 -->
						<div class="m_segment m_botm <?php
							if($tmpMatch['TeamWinner'] == 2){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team2KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team2KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team2URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team2']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team2FTScore']; ?></strong>
							</span>
						</div>
						<!-- Result And Link -->
						<div class="m_dtls"><span>
						<?php
						foreach($tmpMatch['MatchData'] as $key => $tmpSubMatch ){
							if(date('G',strtotime($tmpSubMatch['MatchDateTime'])) < 6){
								$tmpSubMatch['MatchDateTime']	=	date('Y-m-d H:i:s',strtotime($tmpSubMatch['MatchDateTime'] . ' -1 days'));
							}
							if($key >= 1){
								echo '<br/>';
							}
							if(count($tmpMatch['MatchData'])==2){
								if($key == 0){
									echo "นัดแรก, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}else{
									echo "นัดสอง, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}
							}else{
								echo $daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
							}
							
							if(strlen($tmpSubMatch['Team1'])||strlen($tmpSubMatch['Team2'])){
							?>
								<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
									<?php } ?>
								</a>
								<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
								if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
									vs<?php
								}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
									<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
								}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
									c-c<?php
								}else if($tmpSubMatch['MatchStatus']=='Post'){?>
									p-p<?php
								}else{ ?>
									N/A<?php
								} ?>
								</a>
								<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
									<?php } ?>
								</a>
							<?php }
						} ?>
						</span></div>
					</div>
				</div>
				<?php }
				$index_round++; ?>
			</td>
			<?php } ?>
			<!-- Round of 8 -->
			<?php if(count($tmpTable['RoundData'])>=3){ ?>
			<td class="round_column r_8">
				<?php for($i=0;$i<2;$i++) {
				$tmpMatch = $tmpTable['RoundData'][$index_round][$i]; ?>
				<div class="mtch_container">
					<div class="match_unit">
						<!-- Team 1 -->
						<div class="m_segment m_top <?php
							if($tmpMatch['TeamWinner'] == 1){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team1KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team1KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team1URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team1']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team1FTScore']; ?></strong>
							</span>
						</div>
						<!-- Team 2 -->
						<div class="m_segment m_botm <?php
							if($tmpMatch['TeamWinner'] == 2){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team2KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team2KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team2URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team2']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team2FTScore']; ?></strong>
							</span>
						</div>
						<!-- Result And Link -->
						<div class="m_dtls"><span>
						<?php
						foreach($tmpMatch['MatchData'] as $key => $tmpSubMatch ){
							if(date('G',strtotime($tmpSubMatch['MatchDateTime'])) < 6){
								$tmpSubMatch['MatchDateTime']	=	date('Y-m-d H:i:s',strtotime($tmpSubMatch['MatchDateTime'] . ' -1 days'));
							}
							if($key >= 1){
								echo '<br/>';
							}
							if(count($tmpMatch['MatchData'])==2){
								if($key == 0){
									echo "นัดแรก, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}else{
									echo "นัดสอง, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}
							}else{
								echo $daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
							}
							if(strlen($tmpSubMatch['Team1'])||strlen($tmpSubMatch['Team2'])){
							?>
							<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
							if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
								vs<?php
							}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
								<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
							}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
								c-c<?php
							}else if($tmpSubMatch['MatchStatus']=='Post'){?>
								p-p<?php
							}else{ ?>
								N/A<?php
							} ?>
							</a>
							<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
						<?php }
						} ?>
						</span></div>
					</div>
				</div>
				<?php }
				$index_round++; ?>
			</td>
			<?php } ?>
			<!-- Round of 4 -->
			<?php if(count($tmpTable['RoundData'])>=2){ $tmpMatch = $tmpTable['RoundData'][$index_round][0]; ?>
			<td class="round_column r_4">
				<div class="mtch_container">
					<div class="match_unit">
						<!-- Team 1 -->
						<div class="m_segment m_top <?php
							if($tmpMatch['TeamWinner'] == 1){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team1KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team1KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team1URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team1']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team1FTScore']; ?></strong>
							</span>
						</div>
						<!-- Team 2 -->
						<div class="m_segment m_botm <?php
							if($tmpMatch['TeamWinner'] == 2){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team2KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team2KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team2URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team2']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team2FTScore']; ?></strong>
							</span>
						</div>
						<!-- Result And Link -->
						<div class="m_dtls"><span>
						<?php
						foreach($tmpMatch['MatchData'] as $key => $tmpSubMatch ){
							if(date('G',strtotime($tmpSubMatch['MatchDateTime'])) < 6){
								$tmpSubMatch['MatchDateTime']	=	date('Y-m-d H:i:s',strtotime($tmpSubMatch['MatchDateTime'] . ' -1 days'));
							}
							if($key >= 1){
								echo '<br/>';
							}
							if(count($tmpMatch['MatchData'])==2){
								if($key == 0){
									echo "นัดแรก, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}else{
									echo "นัดสอง, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}
							}else{
								echo $daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
							}
							if(strlen($tmpSubMatch['Team1'])||strlen($tmpSubMatch['Team2'])){
							?>
							<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
							if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
								vs<?php
							}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
								<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
							}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
								c-c<?php
							}else if($tmpSubMatch['MatchStatus']=='Post'){?>
								p-p<?php
							}else{ ?>
								N/A<?php
							} ?>
							</a>
							<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<?php }
						} ?>
						</span></div>
					</div>
				</div>
			</td>
			<?php
			$index_round++;
			} ?>
			<!-- Final & 3rd -->
			<?php $tmpMatch = $tmpTable['RoundData'][$index_round][0]; ?>
			<td class="round_column r_2 final">
				<div class="mtch_container">
					<div class="match_unit">
						<!-- Team 1 -->
						<div class="m_segment m_top <?php
							if($tmpMatch['TeamWinner'] == 1){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team1KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team1KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team1URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team1']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team1FTScore']; ?></strong>
							</span>
						</div>
						<!-- Team 2 -->
						<div class="m_segment m_botm <?php
							if($tmpMatch['TeamWinner'] == 2){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team2KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team2KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team2URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team2']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team2FTScore']; ?></strong>
							</span>
						</div>
						<!-- Result And Link -->
						<div class="m_dtls"><span>
						<?php
						foreach($tmpMatch['MatchData'] as $key => $tmpSubMatch ){
							if(date('G',strtotime($tmpSubMatch['MatchDateTime'])) < 6){
								$tmpSubMatch['MatchDateTime']	=	date('Y-m-d H:i:s',strtotime($tmpSubMatch['MatchDateTime'] . ' -1 days'));
							}
							if($key >= 1){
								echo '<br/>';
							}
							if(count($tmpMatch['MatchData'])==2){
								if($key == 0){
									echo "นัดแรก, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}else{
									echo "นัดสอง, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}
							}else{
								echo $daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
							}
							if(strlen($tmpSubMatch['Team1'])||strlen($tmpSubMatch['Team2'])){
							?>
							<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
							if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
								vs<?php
							}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
								<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
							}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
								c-c<?php
							}else if($tmpSubMatch['MatchStatus']=='Post'){?>
								p-p<?php
							}else{ ?>
								N/A<?php
							} ?>
							</a>
							<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
						<?php }
						} ?>
						</span></div>
					</div>
				</div>
				<?php if(isset($tmpTable['RoundData'][$index_round][1])) {
				$tmpMatch = $tmpTable['RoundData'][$index_round][1]; ?>
				<div class="mtch_container third_position">
					<div class="match_unit">
						<!-- Team 1 -->
						<div class="m_segment m_top <?php
							if($tmpMatch['TeamWinner'] == 1){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team1KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team1KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team1URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team1']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team1FTScore']; ?></strong>
							</span>
						</div>
						<!-- Team 2 -->
						<div class="m_segment m_botm <?php
							if($tmpMatch['TeamWinner'] == 2){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team2KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team2KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team2URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team2']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team2FTScore']; ?></strong>
							</span>
						</div>
						<!-- Result And Link -->
						<div class="m_dtls"><span>
						<?php
						foreach($tmpMatch['MatchData'] as $key => $tmpSubMatch ){
							if(date('G',strtotime($tmpSubMatch['MatchDateTime'])) < 6){
								$tmpSubMatch['MatchDateTime']	=	date('Y-m-d H:i:s',strtotime($tmpSubMatch['MatchDateTime'] . ' -1 days'));
							}
							if($key >= 1){
								echo '<br/>';
							}
							if(count($tmpMatch['MatchData'])==2){
								if($key == 0){
									echo "นัดแรก, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}else{
									echo "นัดสอง, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}
							}else{
								echo $daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
							}
							if(strlen($tmpSubMatch['Team1'])||strlen($tmpSubMatch['Team2'])){ ?>
							<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
							if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
								vs<?php
							}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
								<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
							}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
								c-c<?php
							}else if($tmpSubMatch['MatchStatus']=='Post'){?>
								p-p<?php
							}else{ ?>
								N/A<?php
							} ?>
							</a>
							<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
						<?php }
						} ?>
						</span></div>
					</div>
				</div>
				<?php }
				$index_round--;
				?>
			</td>
			<!-- Round of 4 -->
			<?php if(count($tmpTable['RoundData'])>=2){ $tmpMatch = $tmpTable['RoundData'][$index_round][1]; ?>
			<td class="round_column r_4 reversed">
				<div class="mtch_container">
					<div class="match_unit">
						<!-- Team 1 -->
						<div class="m_segment m_top <?php
							if($tmpMatch['TeamWinner'] == 1){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team1KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team1KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team1URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team1']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team1FTScore']; ?></strong>
							</span>
						</div>
						<!-- Team 2 -->
						<div class="m_segment m_botm <?php
							if($tmpMatch['TeamWinner'] == 2){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team2KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team2KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team2URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team2']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team2FTScore']; ?></strong>
							</span>
						</div>
						<!-- Result And Link -->
						<div class="m_dtls"><span>
						<?php
						foreach($tmpMatch['MatchData'] as $key => $tmpSubMatch ){
							if(date('G',strtotime($tmpSubMatch['MatchDateTime'])) < 6){
								$tmpSubMatch['MatchDateTime']	=	date('Y-m-d H:i:s',strtotime($tmpSubMatch['MatchDateTime'] . ' -1 days'));
							}
							if($key >= 1){
								echo '<br/>';
							}
							if(count($tmpMatch['MatchData'])==2){
								if($key == 0){
									echo "นัดแรก, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}else{
									echo "นัดสอง, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}
							}else{
								echo $daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
							}
							if(strlen($tmpSubMatch['Team1'])||strlen($tmpSubMatch['Team2'])){ ?>
							<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
							if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
								vs<?php
							}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
								<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
							}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
								c-c<?php
							}else if($tmpSubMatch['MatchStatus']=='Post'){?>
								p-p<?php
							}else{ ?>
								N/A<?php
							} ?>
							</a>
							<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<?php }
						} ?>
						</span></div>
					</div>
				</div>
			</td>
			<?php
			$index_round--;
			} ?>
			<!-- Round of 8 -->
			<?php if(count($tmpTable['RoundData'])>=3){ ?>
			<td class="round_column r_8 reversed">
				<?php for($i=2;$i<4;$i++) {
				$tmpMatch = $tmpTable['RoundData'][$index_round][$i]; ?>
				<div class="mtch_container">
					<div class="match_unit">
						<!-- Team 1 -->
						<div class="m_segment m_top <?php
							if($tmpMatch['TeamWinner'] == 1){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team1KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team1KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team1URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team1']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team1FTScore']; ?></strong>
							</span>
						</div>
						<!-- Team 2 -->
						<div class="m_segment m_botm <?php
							if($tmpMatch['TeamWinner'] == 2){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team2KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team2KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team2URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team2']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team2FTScore']; ?></strong>
							</span>
						</div>
						<!-- Result And Link -->
						<div class="m_dtls"><span>
						<?php
						foreach($tmpMatch['MatchData'] as $key => $tmpSubMatch ){
							if(date('G',strtotime($tmpSubMatch['MatchDateTime'])) < 6){
								$tmpSubMatch['MatchDateTime']	=	date('Y-m-d H:i:s',strtotime($tmpSubMatch['MatchDateTime'] . ' -1 days'));
							}
							if($key >= 1){
								echo '<br/>';
							}
							if(count($tmpMatch['MatchData'])==2){
								if($key == 0){
									echo "นัดแรก, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}else{
									echo "นัดสอง, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}
							}else{
								echo $daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
							}
							if(strlen($tmpSubMatch['Team1'])||strlen($tmpSubMatch['Team2'])){ ?>
							<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
							if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
								vs<?php
							}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
								<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
							}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
								c-c<?php
							}else if($tmpSubMatch['MatchStatus']=='Post'){?>
								p-p<?php
							}else{ ?>
								N/A<?php
							} ?>
							</a>
							<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<?php }
						} ?>
						</span></div>
					</div>
				</div>
				<?php }
				$index_round--; ?>
			</td>
			<?php } ?>
			<!-- Round of 16 -->
			<?php if(count($tmpTable['RoundData'])>=4){ ?>
			<td class="round_column r_16 reversed">
				<?php for($i=4;$i<8;$i++) {
				$tmpMatch = $tmpTable['RoundData'][$index_round][$i]; ?>
				<div class="mtch_container">
					<div class="match_unit">
						<!-- Team 1 -->
						<div class="m_segment m_top <?php
							if($tmpMatch['TeamWinner'] == 1){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team1KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team1KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team1URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team1']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team1FTScore']; ?></strong>
							</span>
						</div>
						<!-- Team 2 -->
						<div class="m_segment m_botm <?php
							if($tmpMatch['TeamWinner'] == 2){
								echo 'winner';
							}else {
								echo 'loser';
							}
							?>" <?php if(intval($tmpMatch['Team2KPID']) > 0){
								?>data-team-id="<?php echo $tmpMatch['Team2KPID'];?>"<?php } ?>
						>
							<span>
								<a href="<?php echo $tmpMatch['Team2URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
									<?php } ?>
									<span><?php echo $tmpMatch['Team2']; ?></span>
								</a>
								<strong><?php echo $tmpMatch['Team2FTScore']; ?></strong>
							</span>
						</div>
						<!-- Result And Link -->
						<div class="m_dtls"><span>
						<?php
						foreach($tmpMatch['MatchData'] as $key => $tmpSubMatch ){
							if(date('G',strtotime($tmpSubMatch['MatchDateTime'])) < 6){
								$tmpSubMatch['MatchDateTime']	=	date('Y-m-d H:i:s',strtotime($tmpSubMatch['MatchDateTime'] . ' -1 days'));
							}
							if($key >= 1){
								echo '<br/>';
							}
							if(count($tmpMatch['MatchData'])==2){
								if($key == 0){
									echo "นัดแรก, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}else{
									echo "นัดสอง, ".$daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
								}
							}else{
								echo $daytothai[date("D",strtotime($tmpSubMatch['MatchDateTime']))]." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".$monthtothai[date("m",strtotime($tmpSubMatch['MatchDateTime']))]."<br/>";
							}
							if(strlen($tmpSubMatch['Team1'])||strlen($tmpSubMatch['Team2'])){ ?>
							<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
							if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
								vs<?php
							}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
								<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
							}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
								c-c<?php
							}else if($tmpSubMatch['MatchStatus']=='Post'){?>
								p-p<?php
							}else{ ?>
								N/A<?php
							} ?>
							</a>
							<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<?php }
						} ?>
						</span></div>
					</div>
				</div>
				<?php }
				$index_round--; ?>
			</td>
			<?php } ?>
		</tr></tbody>
	</table>
</div>