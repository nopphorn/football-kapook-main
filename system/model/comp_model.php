<?php if (!defined('MINIZONE')) exit;

class Comp_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
		$this->minizone = minizone::getzone();
        $this->mem_lib = $this->minizone->library('memcache_lib');
    }
	
	function file_get_curl($url){
	
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		//curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return $result;
		
	}
    
    function getInfoByName($cupname = null){
	
        if ($cupname) {

			$url = 'http://football.kapook.com/api/adminfootball/api/comp/' . $cupname . '/info?remove_cache=1&type=json';
            $result = json_decode($this->file_get_curl($url),true);

            return $result;
        }
        else {
            return false;
        }
		
    }
	
	function getProgramByName($cupname = null,$is_clearcache = false){
	
        if ($cupname) {

			$url = 'http://football.kapook.com/api/adminfootball/api/program/' . $cupname . '?type=json';
			if($is_clearcache){
				$url = $url . '&remove_cache=1';
			}
            $result = json_decode($this->file_get_curl($url),true);

            return $result;
        }
        else {
            return false;
        }
		
    }
	
	function getProgramByNameToday($cupname = null,$is_clearcache = false){
	
        if ($cupname) {

			$url = 'http://football.kapook.com/api/adminfootball/api/program/' . $cupname . '?type=json&date=today';
			if($is_clearcache){
				$url = $url . '&remove_cache=1';
			}
            $result = json_decode($this->file_get_curl($url),true);

            return $result;
        }
        else {
            return false;
        }
		
    }
	
	function getProgramByNameDate($cupname = null,$date = null,$is_clearcache = false){
        if ($cupname && $date) {
			$url = 'http://football.kapook.com/api/adminfootball/api/program/' . $cupname . '?type=json&date=' . $date;
			if($is_clearcache){
				$url = $url . '&remove_cache=1';
			}
            $result = json_decode($this->file_get_curl($url),true);
            return $result;
        }
        else {
            return false;
        }
    }
	
	function getTableByName($cupname = null){
	
        if ($cupname) {

			$url = 'http://football.kapook.com/api/adminfootball/api/table/' . $cupname . '?remove_cache=1&type=json';
            $result = json_decode($this->file_get_curl($url),true);

            return $result;
        }
        else {
            return false;
        }
		
    }
	
	function getNewsByName($cupname = null,$page = 0){
	
        if ($cupname) {
		
			if($page <= 0){
				$url = 'http://football.kapook.com/api/adminfootball/api/news/' . $cupname . '/5/1?remove_cache=1&type=json';
			}else{
				$url = 'http://football.kapook.com/api/adminfootball/api/news/' . $cupname . '/12/' . $page . '?remove_cache=1&type=json';
			}
            $result = json_decode($this->file_get_curl($url),true);
			

            return $result;
        }
        else {
            return false;
        }
		
    }
	
	function getScorerByName($cupname = null){
	
        if ($cupname) {
		
			$url = 'http://football.kapook.com/api/adminfootball/api/scorer/' . $cupname . '?remove_cache=1&type=json';
            $result = json_decode($this->file_get_curl($url),true);

            return $result;
        }
        else {
            return false;
        }
		
    }
	
	function getStatByName($cupname = null){
	
        if ($cupname) {
		
			$url = 'http://football.kapook.com/api/adminfootball/api/statdata/' . $cupname . '?remove_cache=1&type=json';
            $result = json_decode($this->file_get_curl($url),true);

            return $result;
        }
        else {
            return false;
        }
		
    }
    
    function getProgramAll($cupname = null) {
        if ($cupname) {
            $url = 'http://football.kapook.com/api/adminfootball/api/program/' . $cupname . '?type=json&date=all&remove_cache=1';
            $result = json_decode($this->file_get_curl($url),true);

            return $result;
        } else {
            return FALSE;
        }
    }
}
?>