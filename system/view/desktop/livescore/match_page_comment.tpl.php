
<div class="container match">
    <?php
    $MonthArr['01'] = 'ม.ค.';
    $MonthArr['02'] = 'ก.พ.';
    $MonthArr['03'] = 'มี.ค.';
    $MonthArr['04'] = 'เม.ย.';
    $MonthArr['05'] = 'พ.ค.';
    $MonthArr['06'] = 'มิ.ย.';
    $MonthArr['07'] = 'ก.ค.';
    $MonthArr['08'] = 'ส.ค.';
    $MonthArr['09'] = 'ก.ย.';
    $MonthArr['10'] = 'ต.ค.';
    $MonthArr['11'] = 'พ.ย.';
    $MonthArr['12'] = 'ธ.ค.';

    $OddsRate["0.25"] = "เสมอควบครึ่ง";
    $OddsRate["0.5"] = "ครึ่งลูก";
    $OddsRate["0.75"] = "ครึ่งควบลูก";
    $OddsRate["1"] = "หนึ่งลูก";
    $OddsRate["1.25"] = "ลูกควบลูกครึ่ง";
    $OddsRate["1.5"] = "ลูกครึ่ง";
    $OddsRate["1.75"] = "ลูกครึ่งควบสองลูก";
    $OddsRate["2"] = "สองลูก";
    $OddsRate["2.25"] = "สองลูกควบสองลูกครึ่ง";
    $OddsRate["2.5"] = "สองลูกครึ่ง";
    $OddsRate["2.75"] = "สองลูกครึ่งควบสามลูก";
    $OddsRate["3"] = "สามลูก";
    $OddsRate["3.25"] = "สามลูกควบสามลูกครึ่ง";
    $OddsRate["3.5"] = "สามลูกครึ่ง";
    $OddsRate["3.75"] = "สามลูกครึ่งควบสี่ลูก";
    $OddsRate["4"] = "สี่ลูก";
    $OddsRate["4.25"] = "สี่ลูกควบสี่ลูกครึ่ง";
    $OddsRate["4.5"] = "สี่ลูกครึ่ง";
    $OddsRate["4.75"] = "สี่ลูกครึ่งควบห้าลูก";
    $OddsRate["5"] = "ห้าลูก";
    $OddsRate["5.25"] = "ห้าลูกควบห้าลูกครึ่ง";
    $OddsRate["5.5"] = "ห้าลูกครึ่ง";
    $OddsRate["5.75"] = "ห้าลูกครึ่งควบหกลูก";
    $OddsRate["6"] = "หกลูก";
    $OddsRate["6.25"] = "หกลูกควบหกลูกครึ่ง";
    $OddsRate["6.5"] = "หกลูกครึ่ง";
    $OddsRate["6.75"] = "หกลูกครึ่งควบเจ็ดลูก";
    $OddsRate["7"] = "เจ็ดลูก";
    $OddsRate["7.25"] = "เจ็ดลูกควบเจ็ดลูกครึ่ง";
    $OddsRate["7.5"] = "เจ็ดลูกครึ่ง";
    $OddsRate["7.75"] = "เจ็ดลูกครึ่งควบแปดลูก";
    $OddsRate["8"] = "แปดลูก";
    $OddsRate["8.25"] = "แปดลูกควบแปดลูกครึ่ง";
    $OddsRate["8.5"] = "แปดลูกครึ่ง";
    $OddsRate["8.75"] = "แปดลูกครึ่งควบเก้าลูก";
    $OddsRate["9"] = "เก้าลูก";
    $OddsRate["9.25"] = "เก้าลูกควบเก้าลูกครึ่ง";
    $OddsRate["9.5"] = "เก้าลูกครึ่ง";
    $OddsRate["9.75"] = "เก้าลูกครึ่งควบสิบลูก";
    $OddsRate["10"] = "สิบลูก";


    $MatchContents 		= $this->MatchContents;
    $MatchInfo 			= $MatchContents['MatchInfo'];

    $LeagueName 		= $this->mem_lib->get('Football2014-League-NameTHShort-' . $MatchInfo['KPLeagueID']);
    $LeagueNameFull 	= $this->mem_lib->get('Football2014-League-NameTH-' . $MatchInfo['KPLeagueID']);
    $Team1Name		= $this->mem_lib->get('Football2014-Team-NameTHShort-' . $MatchInfo['Team1KPID']);
    $Team2Name 		= $this->mem_lib->get('Football2014-Team-NameTHShort-' . $MatchInfo['Team2KPID']);

    $MatchDateTmp 	= explode('-', substr($MatchInfo['MatchDateTime'], 0, 10));
    $MatchTimeTmp 	= explode(':', substr($MatchInfo['MatchDateTime'], 11, 8));

    $MatchDate 			= intval($MatchDateTmp[2]) . ' ' . $MonthArr[$MatchDateTmp[1]] . ' ' . intval($MatchDateTmp[0] - 1957);
    $MatchTime 			= substr($MatchInfo['MatchDateTime'], 11, 5);
    $DateShow 			= intval(date("d")) . ' ' . $MonthArr[date('m')] . ' ' . intval(date('Y') - 1957);
    
	$FTScoreArr = explode('-', $MatchInfo['FTScore']);
    $ETScoreArr = explode('-', $MatchInfo['ETScore']);
    $PNScoreArr = explode('-', $MatchInfo['PNScore']);

    $LastScorersArr = $MatchInfo['LastScorers'];

    if (isset($MatchInfo['count_playside'])) {
        $totalplayside = $MatchInfo['count_playside'];
        if (isset($MatchInfo['count_playside_team1'])) {
            $playside1 = $MatchInfo['count_playside_team1'];
        } else {
            $playside1 = 0;
        }
    } else {
        $totalplayside = 0;
        $playside1 = 0;
    }
    //$playside2		=	$MatchInfo['count_playside_team2'];


    foreach ($LastScorersArr as $ScoreInfo) {

        if (substr($ScoreInfo, '0', 1) == '(') {
            $Team1ScorersArr[] = $ScoreInfo;
        } else {
            $Team2ScorersArr[] = $ScoreInfo;
        }
    } 

    $MatchOfTeam1Arr = $this->MatchOfTeam1;
    $MatchOfTeam2Arr = $this->MatchOfTeam2;

    function fnShowScorer($Scorer) {

        $Scorer = str_replace('(Own', '(ทำเข้าประตูตัวเอง ', $Scorer);
        $Scorer = str_replace('(Pen', '(จุดโทษ ', $Scorer);
        $Scorer = str_replace(')', '\') ', $Scorer);
        $Scorer = str_replace('+', '\' ทดนาทีที่ ', $Scorer);
        return $Scorer;
		
    }

    if (date('G') >= 6) {
        $todayDate = date('Y-m-d');
    } else {
        $todayDate = date("Y-m-d", strtotime("-1 day"));
    }
    ?>

    <?php include dirname(__FILE__) . "../../leage-header.tpl"; ?>


    <h1 class="page-header font-display"> <?php echo $LeagueName; ?> : <a href="/team-<?php echo strtolower(str_replace(" ", "-", $this->mem_lib->get('Football2014-Team-NameEN-' . $MatchInfo['Team1KPID']))); ?>" target="_blank"><?php echo $Team1Name; ?></a> - <a href="/team-<?php echo strtolower(str_replace(" ", "-", $this->mem_lib->get('Football2014-Team-NameEN-' . $MatchInfo['Team2KPID']))); ?>" target="_blank"><?php echo $Team2Name; ?></a></h1>
    <div class="row">


        <div class="social pull-right" style="margin: -70px 0 10px 0;">

            <div class="share share_size_large share_type_facebook">
                <span class="share__count">0</span>
                <a class="share__btn" href="" >Share</a>
            </div>
            
            <div class="share share_size_large share_type_twitter">
                <span class="share__count">0</span>
                <a class="share__btn" href="" >Tweet</a>
            </div>

            <div class="share share_size_large share_type_gplus">
                <span class="share__count">0</span>
                <a class="share__btn" href="" >+1</a>
            </div>

            <div class="share share_size_large share_type_email">
                <span class="share__count">0</span>
                <a class="share__btn" href="">E-mail</a>
            </div>


        </div>
    </div>
    

    <!-- Live Ex Bar-->
    <div class="container scoreboard" style="display:block; background-color:#333; ">
        <div id="carousel-live" class="carousel slide" data-ride="carousel" >
            <div class="control">
                <div class="head" > 
                    <!-- Controls -->
                    <div class="controls pull-right hidden-xs"></div>
                    <span class="label label-danger">Live ผลบอลสด</span> ผลบอล LiveScore <?php echo $LeagueName; ?> <a href="/team-<?php echo strtolower(str_replace(" ", "-", $this->mem_lib->get('Football2014-Team-NameEN-' . $MatchInfo['Team1KPID']))); ?>" target="_blank"><?php echo $Team1Name; ?></a> - <a href="/team-<?php echo strtolower(str_replace(" ", "-", $this->mem_lib->get('Football2014-Team-NameEN-' . $MatchInfo['Team2KPID']))); ?>" target="_blank"><?php echo $Team2Name; ?></a> </div>
            </div>

            <!-- Wrapper for slides -->
            <div class="carousel-inner ">
                <div class="item active">
                    <div class="">

                        <div class="col-md-12 " ><?php echo $LeagueNameFull; ?></div>
                        <div class="col-md-2 " > <br> <?php echo $MatchDate; ?> <br><?php echo $MatchTime; ?> น.<br><br><br>
                            <p class="handicap">
                                <?php if ($this->MatchContents['MatchInfo']['Odds'] != -1) { ?>

                                    <?php if ($this->MatchContents['MatchInfo']['Odds'] == 0) { ?> 
                                        เสมอ
                                    <?php } else { ?>
                                        <?php
                                        if ($this->MatchContents['MatchInfo']['TeamOdds'] == 1) {
                                            echo $Team1Name;
                                        } else {
                                            echo $Team2Name;
                                        }
                                        ?>
                                        </br>ต่อ <?php echo $OddsRate[(string) $this->MatchContents['MatchInfo']['Odds']]; ?>
                                    <?php } ?>
<?php } ?>
                            </p>
                        </div>
                        <div class="col-md-8 match_a"  >
                            <div class="wrapflag">
                                <div class="row">
                                    <div>
                                        <br>
                                    </div>

                                    <div class="col-md-4"><a href="/team-<?php echo strtolower(str_replace(" ", "-", $this->mem_lib->get('Football2014-Team-NameEN-' . $MatchInfo['Team1KPID']))); ?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchInfo['Team1Logo']); ?>" class="flag" onerror="this.src='http://football.kapook.com/uploads/logo/default.png';"  ></a>
                                        <a href="/team-<?php echo strtolower(str_replace(" ", "-", $this->mem_lib->get('Football2014-Team-NameEN-' . $MatchInfo['Team1KPID']))); ?>"><div class="teamname" id="TeamName<?php echo $MatchInfo['id']; ?>_1"><?php echo $Team1Name; ?></div></a>
                                        <div class="footballer"><?php echo fnShowScorer(implode(', ', $Team1ScorersArr)); ?>  </div>
                                        <div class="card">
                                            <?php if ($MatchInfo['Team1RC'] > 0) { ?>
                                                <span class="red"><i class="fa fa-file"></i> <?php echo $MatchInfo['Team1RC']; ?></span>
                                            <?php } ?>

                                            <?php if ($MatchInfo['Team1YC'] > 0) { ?>
                                                <span class="yellow"> <i class="fa fa-file"></i>  <?php echo $MatchInfo['Team1YC']; ?></span>
											<?php } ?>
                                        </div>
                                    </div>





                                    <?php
                                    $LoadLiveScore = false;
                                    if ($MatchInfo['MatchStatus'] == 'Fin') {
                                        ?>  

                                        <div class="col-md-4"> 

                                            <div id="divMatchStatus"><span class="label label-default">จบการแข่งขัน</span></div>
											
											<?php if(( $MatchInfo['PNScore'] == '-' )&&( $MatchInfo['ETScore'] == '-')){ /*90 min fulltime*/ ?>
												<div class="score" id="divFTScore"><?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?></div>
											<?php }else if(( $MatchInfo['PNScore'] != '-' )&&( $MatchInfo['ETScore'] == '-')){ /*90 min + pen*/ ?>
												<div class="score" id="divFTScore"><?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?></div>
												<font color="#FFD400">
													<div id="divExtScore" class="paneltykick">ดวลจุดโทษ<br><?php echo $PNScoreArr[0]; ?> - <?php echo $PNScoreArr[1]; ?></div>   
												</font>
											<?php }else if(( $MatchInfo['PNScore'] == '-' )&&( $MatchInfo['ETScore'] != '-')){ /*120 min*/ ?>
												<div class="score" id="divFTScore"><?php echo $ETScoreArr[0]; ?> - <?php echo $ETScoreArr[1]; ?></div>
												<font color="#FFD400">
													<div id="divExtScore" class="paneltykick">90 นาที<br><?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?></div>   
												</font>
											<?php }else { /*120 min + pen*/ ?>
												<div class="score" id="divFTScore"><?php echo $ETScoreArr[0]; ?> - <?php echo $ETScoreArr[1]; ?></div>
												<font color="#FFD400">
													<div id="divExtScore" class="paneltykick">90 นาที<br><?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?><br>ดวลจุดโทษ<br><?php echo $PNScoreArr[0]; ?> - <?php echo $PNScoreArr[1]; ?></div>
												</font>
											<?php }?>
                                        </div>

<?php } else if ($MatchInfo['MatchStatus'] == 'Sched') { ?>


                                        <div class="col-md-4">
                                            <?php
                                            $KickDateTime = strtotime($MatchInfo['MatchDateTime']);
                                            $CurrentDateTime = strtotime(date("Y-m-d H:i:s"));
                                            $KickHour = round(abs($KickDateTime - $CurrentDateTime) / (60 * 60), 2);

                                            if ($KickHour < 24) {
                                                $LoadCountdown = true;
                                                echo 'เริ่มเตะใน<br>';
                                            }
                                            ?> 

                                            <div id="countdown"></div>
                                            <div class="score" id="divFTScore">vs</div>                                  
                                        </div>

<?php } else if ($MatchInfo['MatchStatus'] == 'Post') { ?> 

                                        <div class="col-md-4">เลื่อนการแข่งขัน
                                            <div class="score" id="divFTScore">vs</div>					   
                                        </div>
<?php } else if ($MatchInfo['MatchStatus'] == 'Canc') { ?> 

                                        <div class="col-md-4">ยกเลิกการแข่งขัน
                                            <div class="score" id="divFTScore">vs</div>                                  
                                        </div>
                                    <?php } else if ($MatchInfo['MatchStatus'] == '1 HF') {
                                        $LoadLiveScore = true;
                                        ?>  

                                        <div class="col-md-4"> 
                                            <div id="divMatchStatus"><span class="label label-success">ครึ่งแรก นาที <?php echo $MatchInfo['Minute']; ?>'</span></div>
                                            <div class="score" id="divFTScore"><?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?></div>
                                            <font color="#FFD400">
                                            <div class="extratime" id="divExtScore"></div>
                                            </font> 							
                                        </div>  

<?php } else if ($MatchInfo['MatchStatus'] == '2 HF') {
    $LoadLiveScore = true;
    ?>  

                                        <div class="col-md-4">
                                            <div id="divMatchStatus"><span class="label label-success">ครึ่งหลัง นาที <?php echo $MatchInfo['Minute']; ?>'</span></div>
                                            <div class="score" id="divFTScore"><?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?></div>
                                            <font color="#FFD400">
                                            <div class="extratime" id="divExtScore"></div>
                                            </font>  					   
                                        </div>    

<?php } else if ($MatchInfo['MatchStatus'] == 'H/T') {
    $LoadLiveScore = true;
    ?>  

                                        <div class="col-md-4"> 
                                            <div id="divMatchStatus"><span class="label label-warning">ช่วงพักครึ่ง</span></div>
                                            <div class="score" id="divFTScore"><?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?></div>
                                            <font color="#FFD400">
                                            <div class="extratime" id="divExtScore"></div>
                                            </font>  					   
                                        </div>

<?php } else if ($MatchInfo['MatchStatus'] == 'E/T') {
    $LoadLiveScore = true;
    ?>  

                                        <div class="col-md-4"> 
                                            <div id="divMatchStatus"><span class="label label-success">ต่อเวลาพิเศษ นาที <?php echo $MatchInfo['Minute']; ?>'</span></div>
                                            <div class="score" id="divFTScore"><?php echo $ETScoreArr[0]; ?> - <?php echo $ETScoreArr[1]; ?></div>
                                            <font color="#FFD400">
                                            <div class="extratime" id="divExtScore"></div>
                                            </font>  
                                        </div>

<?php } else if ($MatchInfo['MatchStatus'] == 'Pen') {
    $LoadLiveScore = true;
    ?>

                                        <div class="col-md-4"> 
                                            <div id="divMatchStatus"><span class="label label-success">ดวลจุดโทษ</span></div>
                                            <div class="score" id="divFTScore"><?php echo $PNScoreArr[0]; ?> - <?php echo $PNScoreArr[1]; ?></div>                                  

                                            <font color="#FFD400">
    <?php if (($MatchInfo['ETScore'] != '-') && ($ETScoreArr[0] != '' || $ETScoreArr[1] != '')) { ?>
                                                <div class="extratime" id="divExtScore">120 นาที<br><?php echo $ETScoreArr[0]; ?> - <?php echo $ETScoreArr[1]; ?></div>   
                                        <?php } else { ?>
                                                <div class="extratime" id="divExtScore">90 นาที<br><?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?></div>
    <?php } ?>
                                            </font> 
                                        </div> 

                                    <?php } else { ?>

                                        <div class="col-md-4">N/A
                                            <div class="score" id="divFTScore">vs</div>                                  
                                        </div>

                                   <?php } ?>

                                    <div class="col-md-4"><a href="/team-<?php echo strtolower(str_replace(" ", "-", $this->mem_lib->get('Football2014-Team-NameEN-' . $MatchInfo['Team2KPID']))); ?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchInfo['Team2Logo']); ?>" class="flag" onerror="this.src='http://football.kapook.com/uploads/logo/default.png';" > </a>

                                        <a href="/team-<?php echo strtolower(str_replace(" ", "-", $this->mem_lib->get('Football2014-Team-NameEN-' . $MatchInfo['Team2KPID']))); ?>"><div class="teamname" id="TeamName<?php echo $MatchInfo['id']; ?>_2"><?php echo $Team2Name; ?></div></a>
                                        <div class="footballer"> <?php echo fnShowScorer(implode(', ', $Team2ScorersArr)); ?> </div>
                                        <div class="card">
                                            <?php if ($MatchInfo['Team2RC'] > 0) { ?>
                                                <span class="red"><i class="fa fa-file"></i> <?php echo $MatchInfo['Team2RC']; ?></span>
<?php } ?>

<?php if ($MatchInfo['Team2YC'] > 0) { ?>
                                                <span class="yellow"> <i class="fa fa-file"></i>  <?php echo $MatchInfo['Team2YC']; ?></span>
<?php } ?>
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- AAA -->
                        
                        <div class="toggle">
                        <input name="inputSoundStatus" type="hidden" value="0">
                        <input id="inputSoundStatus" name="inputSoundStatus" type="checkbox" value="1">
                        <div class="btn btn-sm" onClick="changeSoundCookie();">
                            <label for="inputSoundStatus">
                                <div class="toggle-on btn btn-sm btn-success" > <i class="fa fa-volume-up"></i> เปิดเสียง</div>
                                <div class="toggle-handle btn btn-sm btn-default"></div>
                                <div class="toggle-off btn btn-sm btn-danger"> ปิดเสียง <i class="fa fa-volume-off"></i>
                    
                                </div>
                            </label>
                        </div>
                        </div>
                        
                        <script language="javascript">
								
								<?php if($_COOKIE['SoundStatus']==-1){?>
										document.getElementById("inputSoundStatus").checked = false;
								<?php }else{ ?>
										document.getElementById("inputSoundStatus").checked = true;
								<?php } ?>
                        
                        </script>
                        
                        
                                <?php if (count($MatchInfo['TVLiveList']) > 0) { ?>    
                            <div class="col-md-2 chennel"  > <br> ช่องถ่ายทอด

                                <div class="channel">
                            <?php foreach ($MatchInfo['TVLiveList'] as $ChannelKey) { ?>
                                        <img src="http://football.kapook.com/uploads/tvlogo/<?php echo $ChannelKey; ?>.png" title="<?php echo $this->tv_lib->getTVText($ChannelKey); ?>">
    <?php } ?>

                                </div>
                                

                            </div>
            <?php } ?>

                    </div>
                </div>
            </div>
            <?php
            if (date('G', strtotime($MatchInfo['MatchDateTime'])) >= 6) {
                $tmpDateMatch = date("Y-m-d", strtotime($MatchInfo['MatchDateTime']));
            } else {
                $tmpDateMatch = date("Y-m-d", strtotime($MatchInfo['MatchDateTime'] . " - 1 days"));
            }
            if (($MatchInfo['MatchStatus'] == 'Sched') && ($MatchInfo['Odds'] >= 0) && ($tmpDateMatch == $todayDate)) {
                ?>
                <div class="ui-group-buttons " style="margin: 0 auto; display:block; width:180px; margin-top: 10px;">
                    <a href="#game-single" role="tab" data-toggle="tab" class="btn btn-success btn-xs <?php if (uid == 0) {
                       echo 'btn_no_login ';
                   } ?>" style="width: 85px;" 
    <?php if (uid > 0) { ?> onclick="AddPlaySideSingle(<?php echo $MatchInfo['id']; ?>, '<?php echo $Team1Name; ?>', '<?php echo $Team2Name; ?>', '<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchInfo['Team1Logo']); ?>', '<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchInfo['Team2Logo']); ?>', '<?php echo $MatchInfo['Odds']; ?>',<?php echo $MatchInfo['TeamOdds']; ?>, '<?php echo $MatchInfo['MatchPageURL']; ?>')" <?php } ?> >ทายบอลเดี่ยว</a>
                    <div class="or or-xs"></div>
                    <a href="#dropdown1" role="tab" data-toggle="tab" class="btn btn-primary btn-xs  <?php if (uid == 0) {
        echo 'btn_no_login ';
    } ?>" style="width: 85px;" 
    <?php if (uid > 0) { ?> onclick="AddPlaySideMulti(1,<?php echo $MatchInfo['id']; ?>, '<?php echo $Team1Name; ?>', '<?php echo $Team2Name; ?>', '<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchInfo['Team1Logo']); ?>', '<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchInfo['Team2Logo']); ?>', '<?php echo $MatchInfo['Odds']; ?>',<?php echo $MatchInfo['TeamOdds']; ?>, '<?php echo $MatchInfo['MatchPageURL']; ?>')" <?php } ?> >ทายบอลชุด</a>
                </div>
    <?php
}
?>
        </div>
    </div>


    
   <div class="row" >
   
         <div class="col-md-8">
          <!--h2 class="font-display"><i class="fa fa-file"></i> แสดงความคิดเห็น</h2-->
         
         
 <?php // if ($_GET['comment'] == 1) :  ?>
    <?php
	
    define('CONTENT_ID', $MatchInfo['id']);
    define('PAGE_TYPE', 'football_match');

	define('COMMENT_ID', strval($this->comment['_id']));
    define('viewType', 'sub');

    if ($MatchInfo['question'] == '')
        $MatchInfo['question'] = 'แสดงความคิดเห็น  '.$Team1Name." vs ".$Team2Name." ?";
	
	$MatchInfo['question'] = $this->comment['message'];
		
    define('QUESTION_SUBJECT', $MatchInfo['question']);
    define('QUESTION_IMAGE', $MatchInfo['PictureOG']);
    ?>    

<?php // include dirname(__FILE__) . "../../comment/frm_comment_main.tpl.php"; ?>
<?php include dirname(__FILE__) . "../../comment/frm_comment_main_nologin_reply.tpl.php"; ?>
<?php include dirname(__FILE__) . "../../comment/comment_jsrender.inc.tpl.php"; ?>



    <link href="<?php echo BASE_HREF; ?>default_kapook/comment_new.css" rel="stylesheet" type="text/css" media="all" />    

    
<?php // endif;  ?>
         </div>
          <div class="col-md-4 aside" style="padding-top: 0;">
          
                     <div class="banner">
        <script type="text/javascript" src="http://ads.kapook.com/banner/adshow.php?zid=146"></script>
        </div>
<?php
$NewsArr = $this->NewsLeagueContents;
if (sizeof($NewsArr) > 0) {
    ?>
               <h2 class="font-display" style="margin-top: 20px;">ข่าวอื่นๆ ที่เกี่ยวข้อง</h2>
               <div class="hotnews">
    <?php for ($i = 0; $i < 4; $i++) { ?>
                    <div class="media">
              <a class="" href="<?php echo $NewsArr[$i]['link'] ?>"  target="_blank">
                <img class="media-object pull-left" alt="64x64" src="<?php echo $NewsArr[$i]['picture'] ?>" >
                <?php echo $NewsArr[$i]['subject'] ?> 
              </a>
              <br>
                <span class="date">  <?php echo "วันที่ ".date("d", $NewsArr[$i]['CreateDate'])." ".$monthtothai[date("M", $NewsArr[$i]['CreateDate'])]; ?></span>
            </div>    
                    
    <?php } ?>

            </div>
<?php } ?>        
            
            
            
            
            
            
            
            
            
            

    
        
               
           <div class="banner">
        <script type="text/javascript" src="http://ads.kapook.com/banner/adshow.php?zid=401"></script>
        </div>
             
          
          
          
          
          
          
          
          </div>
   </div> 
    

