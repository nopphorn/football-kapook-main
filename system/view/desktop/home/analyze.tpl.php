<?php 
	$MatchArr = $this->MatchContents["Today"];
	$MatchArrYesterday = $this->MatchContents["Yesterday"];
	$monthtothai['01']['short'] ="ม.ค.";
	$monthtothai['02']['short'] ="ก.พ.";
	$monthtothai['03']['short'] ="มี.ค.";
	$monthtothai['04']['short'] ="เม.ย.";
	$monthtothai['05']['short'] ="พ.ค.";
	$monthtothai['06']['short'] ="มิ.ย.";
	$monthtothai['07']['short'] ="ก.ค.";
	$monthtothai['08']['short'] ="ส.ค.";
	$monthtothai['09']['short'] ="ก.ย.";
	$monthtothai['10']['short'] ="ต.ค.";
	$monthtothai['11']['short'] ="พ.ย.";
	$monthtothai['12']['short'] ="ธ.ค.";
	
	$monthtothai['01']['full'] ="มกราคม";
	$monthtothai['02']['full'] ="กุมภาพันธ์";
	$monthtothai['03']['full'] ="มีนาคม";
	$monthtothai['04']['full'] ="เมษายน";
	$monthtothai['05']['full'] ="พฤษภาคม";
	$monthtothai['06']['full'] ="มิถุนายน";
	$monthtothai['07']['full'] ="กรกฎาคม";
	$monthtothai['08']['full'] ="สิงหาคม";
	$monthtothai['09']['full'] ="กันยายน";
	$monthtothai['10']['full'] ="ตุลาคม";
	$monthtothai['11']['full'] ="พฤศจิกายน";
	$monthtothai['12']['full'] ="ธันวาคม";
	
	$daytothai['Sun'] = "วันอาทิตย์";
	$daytothai['Mon'] = "วันจันทร์";
	$daytothai['Tue'] = "วันอังคาร";
	$daytothai['Wed'] = "วันพุธ";
	$daytothai['Thu'] = "วันพฤหัสบดี";
	$daytothai['Fri'] = "วันศุกร์";
	$daytothai['Sat'] = "วันเสาร์";
	
	$OddsRate["0"] 		= "เสมอ";
	$OddsRate["0.25"] 	= "เสมอควบครึ่ง";
	$OddsRate["0.5"] 	= "ครึ่งลูก";
	$OddsRate["0.75"] 	= "ครึ่งควบลูก";
	$OddsRate["1"] 	= "หนึ่งลูก";
	$OddsRate["1.25"] 	= "ลูกควบลูกครึ่ง";
	$OddsRate["1.5"] 	= "ลูกครึ่ง";
	$OddsRate["1.75"] 	= "ลูกครึ่งควบสองลูก";
	$OddsRate["2"] 	= "สองลูก";
	$OddsRate["2.25"] 	= "สองลูกควบสองลูกครึ่ง";
	$OddsRate["2.5"] 	= "สองลูกครึ่ง";
	$OddsRate["2.75"] 	= "สองลูกครึ่งควบสามลูก";
	$OddsRate["3"] 	= "สามลูก";
	$OddsRate["3.25"] 	= "สามลูกควบสามลูกครึ่ง";
	$OddsRate["3.5"] 	= "สามลูกครึ่ง";
	$OddsRate["3.75"] 	= "สามลูกครึ่งควบสี่ลูก";
	$OddsRate["4"] 	= "สี่ลูก";
	$OddsRate["4.25"] 	= "สี่ลูกควบสี่ลูกครึ่ง";
	$OddsRate["4.5"] 	= "สี่ลูกครึ่ง";
	$OddsRate["4.75"] 	= "สี่ลูกครึ่งควบห้าลูก";
	$OddsRate["5"] 	= "ห้าลูก";
	$OddsRate["5.25"] 	= "ห้าลูกควบห้าลูกครึ่ง";
	$OddsRate["5.5"] 	= "ห้าลูกครึ่ง";
	$OddsRate["5.75"] 	= "ห้าลูกครึ่งควบหกลูก";
	$OddsRate["6"] 	= "หกลูก";
	$OddsRate["6.25"] 	= "หกลูกควบหกลูกครึ่ง";
	$OddsRate["6.5"] 	= "หกลูกครึ่ง";
	$OddsRate["6.75"] 	= "หกลูกครึ่งควบเจ็ดลูก";
	$OddsRate["7"] 	= "เจ็ดลูก";
	$OddsRate["7.25"] 	= "เจ็ดลูกควบเจ็ดลูกครึ่ง";
	$OddsRate["7.5"] 	= "เจ็ดลูกครึ่ง";
	$OddsRate["7.75"] 	= "เจ็ดลูกครึ่งควบแปดลูก";
	$OddsRate["8"] 	= "แปดลูก";
	$OddsRate["8.25"] 	= "แปดลูกควบแปดลูกครึ่ง";
	$OddsRate["8.5"] 	= "แปดลูกครึ่ง";
	$OddsRate["8.75"] 	= "แปดลูกครึ่งควบเก้าลูก";
	$OddsRate["9"] 	= "เก้าลูก";
	$OddsRate["9.25"] 	= "เก้าลูกควบเก้าลูกครึ่ง";
	$OddsRate["9.5"] 	= "เก้าลูกครึ่ง";
	$OddsRate["9.75"] 	= "เก้าลูกครึ่งควบสิบลูก";
	$OddsRate["10"] 	= "สิบลูก";
	
	$ClassColorByMatchStatus['Fin'] = 'btn-grey';
    $ClassColorByMatchStatus['1 HF'] = 'btn-success';
    $ClassColorByMatchStatus['2 HF'] = 'btn-success';
    $ClassColorByMatchStatus['Sched'] = '';
    $ClassColorByMatchStatus['H/T'] = 'btn-warning';

?>
  <div class="container analyze">
  
<!--  <div class="page-header-option">
            <p>
            <?php echo $daytothai[date("D")]." ".date("d")." ".$monthtothai[date("m")]['full']." ".(date("Y")+543); ?> &nbsp; &nbsp;
          
          </p>
</div> --> 
  <h1 class="page-header font-display">วิเคราะห์บอล</h1>
   <div class="row">
   
   
<div class="social pull-right" style="margin: -70px 0 10px 0;">
                 <div class="share share_size_large share_type_facebook">
                    <span class="share__count">11.3K</span>
                    <a class="share__btn" href="" >Share</a>
                </div>
                 <div class="share share_size_large share_type_twitter">
                    <span class="share__count">868</span>
                    <a class="share__btn" href="" >Tweet</a>
                </div>
               
                <div class="share share_size_large share_type_gplus">
                    <span class="share__count">280</span>
                    <a class="share__btn" href="" >+1</a>
                </div>
                
                <div class="share share_size_large share_type_email">
                    <span class="share__count">1k</span>
                    <a class="share__btn" href="">E-mail</a>
                </div>

                
     </div>


</div>
  
    
  
<div class="row">

    <!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
	<?php
		
		if(date('G') >= 6){
			$date = strtotime(date("Y-m-d"));
			$Yesterday = strtotime(date("Y-m-d",strtotime(' - 1 days')));
		}else{
			$date = strtotime(date("Y-m-d",strtotime(' - 1 days')));
			$Yesterday = strtotime(date("Y-m-d",strtotime(' - 2 days')));
		}
	
		if( ((date('G') >= 6) && (date('G') < 12)) || (count($MatchArr)<=0) ){
			$today_class = '';
			$yesterday_class = ' class="active"';
			$today_tap = 'class="tab-pane"';
			$yesterday_tap = 'class="tab-pane active"';
		}else{
			$today_class = ' class="active"';
			$today_tap = 'class="tab-pane active"';
			$yesterday_tap = 'class="tab-pane"';
			$yesterday_class = '';
		}
	?>
  <li <?php echo $today_class; ?>><a href="#home" role="tab" data-toggle="tab">วิเคราะห์บอลวันนี้ <?php echo date("d",$date)." ".$monthtothai[date("m",$date)]['short']." ".(date("Y",$date)+543); ?></a></li>
  <li <?php echo $yesterday_class; ?>><a href="#profile" role="tab" data-toggle="tab">สรุปผลวิเคราะห์เมื่อวานนี้ <?php echo date("d",$Yesterday)." ".$monthtothai[date("m",$Yesterday)]['short']." ".(date("Y",$Yesterday)+543); ?></a></li>
</ul>

    <br>

<!-- Tab panes -->
<div class="tab-content">
	<div <?php echo $today_tap; ?> id="home"><table class="table table-striped border-1">
		<!-- Start LEFT COLUME --> 
		<div class="col-md-12"> 
		<!--วิเคราะห์-->	
		<?php 
		foreach($MatchArr as $MatchTmp){
			if(isset($MatchTmp['count_playside'])){
				$totalplayside		=	$MatchTmp['count_playside'];
				if(isset($MatchTmp['count_playside_team1'])){
					$playside1		=	$MatchTmp['count_playside_team1'];
				}else{
					$playside1		=	0;
				}
			}else{
				$totalplayside		=	0;
				$playside1			=	0;
			}?>
			<div class="row">
				<div class="col-md-12">
					<div class="bs-example">
						<h3>
							<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team1KPID']))); ?>" class="tooltip_top " title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank">
								<?php echo $MatchTmp['Team1'];?>
							</a>
							<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team1KPID']))); ?>" class="tooltip_top " title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank">
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team1Logo']);?>" class="flag" width="64">
							</a>
							<a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
								if($MatchTmp['MatchStatus']=='Fin'){
									$ETScoreArr = explode('-', $MatchTmp['ETScore']);
									$PNScoreArr = explode('-', $MatchTmp['PNScore']);
									if(( $MatchTmp['PNScore'] == '-' )&&( $MatchTmp['ETScore'] == '-')){ /*90 min fulltime*/
										echo $MatchTmp['Team1FTScore'] . ' - ' . $MatchTmp['Team2FTScore']; 
									}else if(( $MatchTmp['PNScore'] != '-' )&&( $MatchTmp['ETScore'] == '-')){ /*90 min + pen*/
										echo $MatchTmp['Team1FTScore'] . '(' . $PNScoreArr[0] .') - (' . $PNScoreArr[1] . ')' . $MatchTmp['Team2FTScore'];
									}else if(( $MatchTmp['PNScore'] == '-' )&&( $MatchTmp['ETScore'] != '-')){ /*120 min*/
										echo $ETScoreArr[0] . ' - ' . $ETScoreArr[1];
									}else { /*120 min + pen*/
										echo $ETScoreArr[0] . '(' . $PNScoreArr[0] .') - (' . $PNScoreArr[1] . ')' . $ETScoreArr[1];
									}
								}else{
									echo "vs";}
								?></a>
							<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team2KPID']))); ?>" class="tooltip_top " title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank">
								<img onerror="this.src='http://football.kapook.com/uploads/logo/default.png'; " src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team2Logo']);?>" class="flag" width="64">
							</a>
							<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team2KPID']))); ?>" class="tooltip_top " title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank">
								<?php echo $MatchTmp['Team2'];?>
							</a>
							<?php
							if(($MatchTmp['MatchStatus'] == 'Sched')&&($MatchTmp['Odds']>0)&&($this->isCanPlayGame)){
							?>
								<div class="ui-group-buttons pull-right">
									<a href="#game-single" role="tab" data-toggle="tab" class="btn btn-success btn-sm <?php if(uid==0){ echo 'btn_no_login '; }?>"
									<?php if(uid>0){ ?> style="width: 95px;" onclick="AddPlaySideSingle(<?php echo $MatchTmp['id'];?>,'<?php echo $MatchTmp['Team1'];?>','<?php echo $MatchTmp['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team2Logo']);?>','<?php echo $MatchTmp['Odds'];?>',<?php echo $MatchTmp['TeamOdds'];?>,'<?php echo $MatchTmp['MatchPageURL']; ?>')" <?php } ?> >ทายบอลเดี่ยว</a>
									<div class="or or-sm"></div>
									<a href="#dropdown1" role="tab" data-toggle="tab" class="btn btn-primary btn-sm  <?php if(uid==0){ echo 'btn_no_login '; } ?>"
									<?php if(uid>0){ ?> style="width: 85px;" onclick="AddPlaySideMulti(1,<?php echo $MatchTmp['id'];?>,'<?php echo $MatchTmp['Team1'];?>','<?php echo $MatchTmp['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team2Logo']);?>','<?php echo $MatchTmp['Odds'];?>',<?php echo $MatchTmp['TeamOdds'];?>,'<?php echo $MatchTmp['MatchPageURL']; ?>')" <?php } ?> >ทายบอลชุด</a>
								</div>
							<?php
							}
							?>            
						</h3>
						<br>
						<div class="row">
							<div class="col-md-4"> 
								<?php
									$date = explode('-',$MatchTmp['MatchDate']);
									$thaidate = $date[2]." ".$monthtothai[$date[1]]['short']." ".($date[0]+543);
								?>
								<span class="date"><i class="fa fa-calendar"></i> <?php echo $thaidate; ?>, <?php echo $MatchTmp['MatchTime']; ?> น. </span>
								<div class="channel">ช่องถ่ายทอด : <?php
									foreach($MatchTmp['TVLiveList'] as $ChannelKey){?>
										<img src="http://football.kapook.com/uploads/tvlogo/<?php echo $ChannelKey; ?>.png" title="<?php echo $this->tv_lib->getTVText($ChannelKey); ?>">
								<?php } ?>
								</div>
								<b><?php
									if($MatchTmp['Odds'] >0){
										echo 'ราคาบอล : ' . $MatchTmp['Team'.$MatchTmp['TeamOdds']] . ' ต่อ ' . $OddsRate[(string)$MatchTmp['Odds']]; 
									}else if($MatchTmp['Odds'] ==0){
										echo 'ราคาบอล : ' . $OddsRate[(string)$MatchTmp['Odds']];
									}
								?></b>
							</div>
							<div class="col-md-8">
								<div class="row">
									<div class="col-md-4 text_left"><?php echo $MatchTmp['Team1'];?></div>
									<div class="col-md-4 text_center"><b>ประมวลผลสมาชิกทายผลบอล</b></div>
									<div class="col-md-4 text_right"><?php echo $MatchTmp['Team2'];?></div>
								</div>
								<?php
								if($totalplayside>0){
									$tmpPercent		=	round(($playside1/$totalplayside)*100);
								?>
									<div class="progress">
										<div class="progress-bar" style="width: <?php echo $tmpPercent; ?>%"><?php echo $tmpPercent; ?>%</div>
										<div class="progress-bar progress-bar-danger" style="width: <?php echo (100-$tmpPercent); ?>%"><?php echo (100-$tmpPercent); ?>%</div>
									</div>
								<?php
								}else{
								?>
									<div class="progress">
										<div class="progress-bar progress-bar-warning" style="width: 100%"><?php echo $tmpPercent; ?>กำลังรอข้อมูล</div>
									</div>
								<?php
								}
								?>
							</div>
						</div>
					</div>
					<div class="highlight"> 
						<div class="row">
							<div class="col-md-12"><b>บทวิเคราะห์จากทีมงาน</b></div>
							<div class="col-md-12">
								<?php if($MatchTmp['Analysis']) {?>
									<br>
									ทัศนะ : <?php echo $MatchTmp['Analysis']; ?>
									<br>
								<?php } ?>
								<div style="color:#428bca; font-weight: bold;">ฟันธง : <?php echo $MatchTmp['Predict']; ?></div>
								<?php if($MatchTmp['Result']) {?>
									<br>
									<div style="color:#00C231; font-weight: bold;">สรุปผล : </div><?php echo $MatchTmp['Result']; ?>
									<br>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<?php /*
		<div class="row">
			<div class="col-md-12">
				<div class="bs-example"><h3>เพทกระปุกวิเคราะห์บอล  </h3></div>
				<div class="highlight">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4 text_left"><span class="date"><i class="fa fa-calendar"></i> 26 พ.ค. 2557, 03:00 น. </span></div>
								<div class="col-md-4 text_center"><b>Chelsea <img src="assets/img/icon_sample/9.ico" class="flag" width="24"></b> vs <b><img src="assets/img/icon_sample/23.ico" class="flag" width="24"> Manchester United</b></div>
								<div class="col-md-4 text_right">Premiere Leage &nbsp; 
									<div class="ui-group-buttons pull-right">
										<button type="button" class="btn btn-success btn-xs"> ทายผล</button>
										<div class="or or-sm"></div>
										<button type="button" class="btn btn-success btn-xs">  วางบอล</button>
									</div> 
								</div>
							</div>
																									   
							<div class="progress">
								<div class="progress-bar" style="width: 63%">63%</div>
								<div class="progress-bar progress-bar-danger" style="width: 37%">37%</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4 text_left"><span class="date"><i class="fa fa-calendar"></i> 26 พ.ค. 2557, 03:00 น. </span></div>
								<div class="col-md-4 text_center"><b>Chelsea <img src="assets/img/icon_sample/9.ico" class="flag" width="24"></b> vs <b><img src="assets/img/icon_sample/23.ico" class="flag" width="24"> Manchester United</b></div>
								<div class="col-md-4 text_right">Premiere Leage &nbsp;   
									<div class="ui-group-buttons pull-right">
										<button type="button" class="btn btn-success btn-xs"> ทายผล</button>
										<div class="or or-sm"></div>
										<button type="button" class="btn btn-success btn-xs">  วางบอล</button>
									</div> 
								</div>
							</div>
																									   
							<div class="progress">
								<div class="progress-bar" style="width: 63%">63%</div>
								<div class="progress-bar progress-bar-danger" style="width: 37%">37%</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		*/ ?>
		</div>
    </table></div>
	<div <?php echo $yesterday_tap; ?> id="profile">
		<!-- Start LEFT COLUME --> 
		<div class="col-md-12"> 
		<!--วิเคราะห์-->	
		<?php 
		foreach($MatchArrYesterday as $MatchTmp){
			if(isset($MatchTmp['count_playside'])){
				$totalplayside		=	$MatchTmp['count_playside'];
				if(isset($MatchTmp['count_playside_team1'])){
					$playside1		=	$MatchTmp['count_playside_team1'];
				}else{
					$playside1		=	0;
				}
			}else{
				$totalplayside		=	0;
				$playside1			=	0;
			}?>
			<div class="row">
				<div class="col-md-12">
					<div class="bs-example">
						<h3>
							<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team1KPID']))); ?>" class="tooltip_top " title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank">
								<?php echo $MatchTmp['Team1'];?>
							</a>
							<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team1KPID']))); ?>" class="tooltip_top " title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank">
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team1Logo']);?>" class="flag" width="64">
							</a>
							<a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
								if($MatchTmp['MatchStatus']=='Fin'){
									$ETScoreArr = explode('-', $MatchTmp['ETScore']);
									$PNScoreArr = explode('-', $MatchTmp['PNScore']);
									if(( $MatchTmp['PNScore'] == '-' )&&( $MatchTmp['ETScore'] == '-')){ /*90 min fulltime*/
										echo $MatchTmp['Team1FTScore'] . ' - ' . $MatchTmp['Team2FTScore']; 
									}else if(( $MatchTmp['PNScore'] != '-' )&&( $MatchTmp['ETScore'] == '-')){ /*90 min + pen*/
										echo $MatchTmp['Team1FTScore'] . '(' . $PNScoreArr[0] .') - (' . $PNScoreArr[1] . ')' . $MatchTmp['Team2FTScore'];
									}else if(( $MatchTmp['PNScore'] == '-' )&&( $MatchTmp['ETScore'] != '-')){ /*120 min*/
										echo $ETScoreArr[0] . ' - ' . $ETScoreArr[1];
									}else { /*120 min + pen*/
										echo $ETScoreArr[0] . '(' . $PNScoreArr[0] .') - (' . $PNScoreArr[1] . ')' . $ETScoreArr[1];
									}
								}else{
									echo "vs";}
								?></a>
							<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team2KPID']))); ?>" class="tooltip_top " title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank">
								<img onerror="this.src='http://football.kapook.com/uploads/logo/default.png'; " src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team2Logo']);?>" class="flag" width="64">
							</a>
							<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team2KPID']))); ?>" class="tooltip_top " title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank">
								<?php echo $MatchTmp['Team2'];?>
							</a>
							<?php
							if(($MatchTmp['MatchStatus'] == 'Sched')&&($MatchTmp['Odds']>0)&&($this->isCanPlayGame)){
							?>
								<div class="ui-group-buttons pull-right">
									<a href="#game-single" role="tab" data-toggle="tab" class="btn btn-success btn-sm <?php if(uid==0){ echo 'btn_no_login '; }?>"
									<?php if(uid>0){ ?> style="width: 95px;" onclick="AddPlaySideSingle(<?php echo $MatchTmp['id'];?>,'<?php echo $MatchTmp['Team1'];?>','<?php echo $MatchTmp['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team2Logo']);?>','<?php echo $MatchTmp['Odds'];?>',<?php echo $MatchTmp['TeamOdds'];?>,'<?php echo $MatchTmp['MatchPageURL']; ?>')" <?php } ?> >ทายบอลเดี่ยว</a>
									<div class="or or-sm"></div>
									<a href="#dropdown1" role="tab" data-toggle="tab" class="btn btn-primary btn-sm  <?php if(uid==0){ echo 'btn_no_login '; } ?>"
									<?php if(uid>0){ ?> style="width: 85px;" onclick="AddPlaySideMulti(1,<?php echo $MatchTmp['id'];?>,'<?php echo $MatchTmp['Team1'];?>','<?php echo $MatchTmp['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team2Logo']);?>','<?php echo $MatchTmp['Odds'];?>',<?php echo $MatchTmp['TeamOdds'];?>,'<?php echo $MatchTmp['MatchPageURL']; ?>')" <?php } ?> >ทายบอลชุด</a>
								</div>
							<?php
							}
							?>            
						</h3>
						<br>
						<div class="row">
							<div class="col-md-4"> 
								<?php
									$date = explode('-',$MatchTmp['MatchDate']);
									$thaidate = $date[2]." ".$monthtothai[$date[1]]['short']." ".($date[0]+543);
								?>
								<span class="date"><i class="fa fa-calendar"></i> <?php echo $thaidate; ?>, <?php echo $MatchTmp['MatchTime']; ?> น. </span>
								<div class="channel">ช่องถ่ายทอด : <?php
									foreach($MatchTmp['TVLiveList'] as $ChannelKey){?>
										<img src="http://football.kapook.com/uploads/tvlogo/<?php echo $ChannelKey; ?>.png" title="<?php echo $this->tv_lib->getTVText($ChannelKey); ?>">
								<?php } ?>
								</div>
								<b><?php
									if($MatchTmp['Odds'] >0){
										echo 'ราคาบอล : ' . $MatchTmp['Team'.$MatchTmp['TeamOdds']] . ' ต่อ ' . $OddsRate[(string)$MatchTmp['Odds']]; 
									}else if($MatchTmp['Odds'] ==0){
										echo 'ราคาบอล : ' . $OddsRate[(string)$MatchTmp['Odds']];
									}
								?></b>
							</div>
							<div class="col-md-8">
								<div class="row">
									<div class="col-md-4 text_left"><?php echo $MatchTmp['Team1'];?></div>
									<div class="col-md-4 text_center"><b>ประมวลผลสมาชิกทายผลบอล</b></div>
									<div class="col-md-4 text_right"><?php echo $MatchTmp['Team2'];?></div>
								</div>
								<?php
								if($totalplayside>0){
									$tmpPercent		=	round(($playside1/$totalplayside)*100);
								?>
									<div class="progress">
										<div class="progress-bar" style="width: <?php echo $tmpPercent; ?>%"><?php echo $tmpPercent; ?>%</div>
										<div class="progress-bar progress-bar-danger" style="width: <?php echo (100-$tmpPercent); ?>%"><?php echo (100-$tmpPercent); ?>%</div>
									</div>
								<?php
								}else{
								?>
									<div class="progress">
										<div class="progress-bar progress-bar-warning" style="width: 100%"><?php echo $tmpPercent; ?>กำลังรอข้อมูล</div>
									</div>
								<?php
								}
								?>
							</div>
						</div>
					</div>
					<div class="highlight"> 
						<div class="row">
							<div class="col-md-12"><b>บทวิเคราะห์จากทีมงาน</b></div>
							<div class="col-md-12">
								<?php if($MatchTmp['Analysis']) {?>
									<br>
									ทัศนะ : <?php echo $MatchTmp['Analysis']; ?>
									<br>
								<?php } ?>
								<div style="color:#428bca; font-weight: bold;">ฟันธง : <?php echo $MatchTmp['Predict']; ?></div>
								<?php if($MatchTmp['Result']) {?>
									<br>
									<div style="color:#00C231; font-weight: bold;">สรุปผล : </div><?php echo $MatchTmp['Result']; ?>
									<br>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<?php /*
		<div class="row">
			<div class="col-md-12">
				<div class="bs-example"><h3>เพทกระปุกวิเคราะห์บอล  </h3></div>
				<div class="highlight">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4 text_left"><span class="date"><i class="fa fa-calendar"></i> 26 พ.ค. 2557, 03:00 น. </span></div>
								<div class="col-md-4 text_center"><b>Chelsea <img src="assets/img/icon_sample/9.ico" class="flag" width="24"></b> vs <b><img src="assets/img/icon_sample/23.ico" class="flag" width="24"> Manchester United</b></div>
								<div class="col-md-4 text_right">Premiere Leage &nbsp; 
									<div class="ui-group-buttons pull-right">
										<button type="button" class="btn btn-success btn-xs"> ทายผล</button>
										<div class="or or-sm"></div>
										<button type="button" class="btn btn-success btn-xs">  วางบอล</button>
									</div> 
								</div>
							</div>
																									   
							<div class="progress">
								<div class="progress-bar" style="width: 63%">63%</div>
								<div class="progress-bar progress-bar-danger" style="width: 37%">37%</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4 text_left"><span class="date"><i class="fa fa-calendar"></i> 26 พ.ค. 2557, 03:00 น. </span></div>
								<div class="col-md-4 text_center"><b>Chelsea <img src="assets/img/icon_sample/9.ico" class="flag" width="24"></b> vs <b><img src="assets/img/icon_sample/23.ico" class="flag" width="24"> Manchester United</b></div>
								<div class="col-md-4 text_right">Premiere Leage &nbsp;   
									<div class="ui-group-buttons pull-right">
										<button type="button" class="btn btn-success btn-xs"> ทายผล</button>
										<div class="or or-sm"></div>
										<button type="button" class="btn btn-success btn-xs">  วางบอล</button>
									</div> 
								</div>
							</div>
																									   
							<div class="progress">
								<div class="progress-bar" style="width: 63%">63%</div>
								<div class="progress-bar progress-bar-danger" style="width: 37%">37%</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		*/ ?>
		</div>
	</div>
</div>
</div>
</div>

<!-- DMP SCRIPT --> 
<script type="text/javascript" charset="UTF-8" src="http://cache.my.kapook.com/js_tag/dmp.js"></script>
<!-- //DMP SCRIPT -->