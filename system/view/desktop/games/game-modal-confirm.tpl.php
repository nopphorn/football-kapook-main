<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Remote Modal</title>
	<script language="JavaScript">
		function checkPlay(){
		
			$( "#page_confirm_01" ).html( "<i class=\"fa fa-spinner fa-spin\"></i>" );
			
		<?php if($this->dataSubmit['type']=='single'){ ?>
			fnPlaySide_SM(<?php echo $this->dataSubmit['match_id']; ?>,<?php echo $this->dataSubmit['selectSide']; ?>,<?php echo $this->dataSubmit['point']; ?>);
		<?php }else if($this->dataSubmit['type']=='multi'){ ?>
			fnPlaySideMulti_SM(<?php echo $this->dataSubmit['set_id']; ?>,<?php echo $this->dataSubmit['point']; ?>);
		<?php } ?>
		}
		
		function clearSelf(){
			$( ".page_confirm" ).html( "..." );
			$( "#useful-modal" ).modal('hide');
		}
		
	</script>
	<?php

	$monthtothai['01'] ="ม.ค.";
	$monthtothai['02'] ="ก.พ.";
	$monthtothai['03'] ="มี.ค.";
	$monthtothai['04'] ="เม.ย.";
	$monthtothai['05'] ="พ.ค.";
	$monthtothai['06'] ="มิ.ย.";
	$monthtothai['07'] ="ก.ค.";
	$monthtothai['08'] ="ส.ค.";
	$monthtothai['09'] ="ก.ย.";
	$monthtothai['10'] ="ต.ค.";
	$monthtothai['11'] ="พ.ย.";
	$monthtothai['12'] ="ธ.ค.";
	
	$daytothai['0'] = "อาทิตย์";
	$daytothai['1'] = "จันทร์";
	$daytothai['2'] = "อังคาร";
	$daytothai['3'] = "พุธ";
	$daytothai['4'] = "พฤหัสบดี";
	$daytothai['5'] = "ศุกร์";
	$daytothai['6'] = "เสาร์";
	
	?>
</head>
<body>
	<div class="modal-header" style="height: 47px;border:none ;">
		<button type="button" class="close" data-dismiss="modal">×</button>
	</div>

	<!-- /modal-header -->
	<div class="modal-body clearfix kp-theme"  >
		<div id="game-confirm" class="carousel slide " data-ride="carousel" data-interval="false">
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				
				<!--<div class="item active"> 
					<h4 class="modal-title text_center" > ยืนยันการทายผลบอล</h4>
					<div class="" style="text-align: center; padding: 40px;"><a class="btn btn-success btn-lg" style="width: 240px;" data-target="#game-confirm" data-slide-to="1"> ตกลง</a></div>
				</div>-->
				
				<div class="item active" id="page_confirm_01"> 
					<h4 class="modal-title text_center" > ยืนยันการทายผลบอล</h4>
					<?php 
						//var_dump($this->NewsMatchContents);
					?>
					<div style="width: 106%;padding-top: 4%;padding-left: 25%;">
						<button type="button" class="btn btn-success btn-lg" style="width: 140px;" onclick="checkPlay();"> ตกลง</button>
						<button class="btn btn-danger btn-lg" style="width: 140px;" onclick="clearSelf();"> ยกเลิก</button>
					</div>
				</div>
    
				<div class="item page_confirm">
					<h4 class="modal-title text_center" > ข้อมูลการทายผลถูกบันทึกแล้ว</h4>
					<div class="lo" onclick="clearSelf();" style="text-align: center; padding-top: 40px;"><span class="btn btn-success btn-lg" style="width: 240px;" ><i class="fa fa-check"></i> บันทึกเรียบร้อย</span></div>
					<?php /*<hr>
					<h4 class="modal-title"> ข่าวที่คุณอาจสนใจ</h4>
					<div class="row subnews">
						<?php 
							if($this->NewsMatchContents['rows']>3){
								$this->NewsMatchContents['rows'] = 3;
							}
							for( $i = 0 ; $i < $this->NewsMatchContents['rows'] ; $i++ ){
								$tmpNews	=	$this->NewsMatchContents['data'][$i];
							?>
								<div class="news_block  col-md-4 col-sm-4 col-xs-4"><a class="post.php" href="<?php echo $tmpNews['link']; ?>" target="_blank"><img class="media-object" alt="" src="<?php echo $tmpNews['picture']; ?>"><span class="date">  <?php
									echo $daytothai[date('w',$tmpNews['CreateDate'])] . ', ' . date('j',$tmpNews['CreateDate']) . ' ' . $monthtothai[date('m',$tmpNews['CreateDate'])] . ' ' . (date('Y',$tmpNews['CreateDate'])+543);
								?></span><?php echo $tmpNews['subject']; ?></a></div>
							<?php
							}
						?>
					</div>*/?>
				</div>
				
				<div class="item page_confirm">
					<h4 class="modal-title text_center" > ข้อมูลมีความผิดพลาด</h4>
					<div style="width: 106%;padding-top: 4%;padding-right: 7%;">
						<div class="lo" onclick="clearSelf();" style="text-align: center;"><span class="btn btn-danger btn-lg" style="width: 240px;" ></i> ปิด</span></div>
					</div>
				</div>
			</div>
		</div>   
	</div>
</body>
</html>