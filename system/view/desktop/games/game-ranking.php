<?php 
$dataRanking	=	$this->listRanking;
$listRanking	=	$dataRanking['data'];
$countRanking	=	$dataRanking['total_user'];
if($countRanking>50)
	$countRanking	=	50;
?>
<div class="container ranking">

   <div class="page-header-option">
            <!-- <p>
          <span class="btn btn-success btn-xs " onclick="$('#calendar').toggle();"> อันดับโลก</span>
          </p> -->
</div>
 <h1  class="page-header font-display">Ranking</h1>

  
<div class="row">
  
  <div class="social pull-right" style="margin: -70px 0 10px 0;">
                 <div class="share share_size_large share_type_facebook">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" >Share</a>
                </div>
                 <div class="share share_size_large share_type_twitter">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" >Tweet</a>
                </div>
               
                <div class="share share_size_large share_type_gplus">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" >+1</a>
                </div>
                
                <div class="share share_size_large share_type_email">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="">E-mail</a>
                </div>

                
     </div>
	 
	    <!-- Start LEFT COLUME --> 
  <div class="col-md-12">

  <div class="">
    <table  class="table  table-striped border-1 ">
      <thead>
        <tr>
            <th width="10%">อันดับ</th>
			<th  class="text_center" colspan="2" width="20%">ชื่อ</th>
			<th  class="text_center">ยศ</th>
			<th  class="text_center">คะแนน</th>
        </tr>
      </thead>
      <tbody>
      
        <?php
		for( $i=0 ; $i<$countRanking ; $i++ ){
			$data	=	$listRanking[$i];
			?>
			<tr>
			<td class="text_center"><?php   echo $i+1; ?></td>
			<td><a href=""><img src="<?php echo $data['picture']; ?>" class="img-circle" title="<?php echo $data['username'];?>"></a></td>
			<td><a href="" ><h2 class="font-display"><?php echo $data['username'];?></h2></a></td>
            <td></td>
            <td><?php echo $data['point']; ?><span class="unit"> คะแนน</span></td>
        </tr>


          <?php
        }
           ?>
        
      </tbody>
    </table>
    </div> 

  
   

     
     
  
  </div>

</div>