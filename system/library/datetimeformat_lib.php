<?php

class Datetimeformat_lib {

    var $month = array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม');
    var $month_short = array('ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.');
    var $month_en_short = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

    function ago($s) {
        $s = time() - strtotime($s);
        if ($s < 0)
            $s = 0;
        foreach (array("60:sec", "60:min", "24:hour", "30:day", "12:month", "0:year") as $x) {
            $y = explode(":", $x);
            if ($y[0] > 1) {
                $v = $s % $y[0];
                $s = floor($s / $y[0]);
            } else {
                $v = $s;
            }
            $t[$y[1]] = $v;
        }
        foreach (array("ปี:year", "เดือน:month", "วัน:day", "ชั่วโมง:hour", "นาที:min") as $x) {
            $y = explode(":", $x);
            if ($t[$y[1]])
                return $t[$y[1]] . " " . $y[0];
        }
        return ">1 นาที";
    }
    
    function thai_full($s, $time = true) {
        if(!strstr($s, '-')) $s = date('Y-m-d H:i:s');
        return (int) (substr($s, 8, 2)) . " " . $this->month[(int) (substr($s, 5, 2)) - 1] . " " . (int) (substr(substr($s, 0, 4) + 543, 0)) . ($time ? " " . substr($s, 11) : "");
    }
    

    static function thai($s, $time = true) {
         if(!strstr($s, '-')) $s = date('Y-m-d H:i:s');
        return (int) (substr($s, 8, 2)) . " " . $this->month[(int) (substr($s, 5, 2)) - 1] . " " . (int) (substr($s, 0, 4) + 543) . ($time ? " เวลา " . substr($s, 11) : "");
    }

    function thaishort($s, $time = true) {
         if(!strstr($s, '-')) $s = date('Y-m-d H:i:s');
        return (int) (substr($s, 8, 2)) . " " . $this->month_short[(int) (substr($s, 5, 2)) - 1] . " " . (int) (substr(substr($s, 0, 4) + 543, 2, 2)) . ($time ? " " . substr($s, 11) : "");
    }
    
    function enshort($s, $time = true) {
         if(!strstr($s, '-')) $s = date('Y-m-d H:i:s');
        return (int) (substr($s, 8, 2)) . " " . $this->month_en_short[(int) (substr($s, 5, 2)) - 1] . " " . (int) (substr(substr($s, 0, 4), 2, 2)) . ($time ? " " . substr($s, 11) : "");
    }

}

/* End of file timeshort.php */
/* Location: ./application/libraries/timeshort.php */