<?php if (!defined('MINIZONE')) exit;

class My_con 
{
    var $view; 
    var $minizone;
    
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
     
        /* -- Set : Header -- */
        header ('Content-type: text/html; charset=utf-8');
        
        /* -- ========= Set : Minizone ========= -- */
        $this->minizone = minizone::getzone();
        /* -- ========= Load : Libraries ========= -- */
        $this->mem_lib = $this->minizone->library('memcache_lib');
		$this->football_lib = $this->minizone->library('football_lib');
        
        $this->login_lib = $this->minizone->library('getlogin_lib'); 
        $this->getIP_lib = $this->minizone->library('getIP_lib');
        $_SERVER['REMOTE_ADDR']=$this->getIP_lib->getUserIP();
        /* -- ========= View ========= -- */
        $this->view = $this->minizone->view();
        $this->root_view = FOLDER_VIEW_DESKTOP;
		
		/* -- Load : Data Game(Member only) -- */
		if( uid > 0 ){
			$this->isCanPlayGame		=		true;
		}else{
			$this->isCanPlayGame		=		false;
		}
		
		//var_dump(uid);
		
		if($this->isCanPlayGame){
			
			/* -- ========= Load : Model ========= -- */
			$this->games_model = $this->minizone->model('games_model');
			$this->livescore_model = $this->minizone->model('livescore_model');

			// Get a date/month/year
			if(date('G')>=6){
				$date	=	date("Y-m-d",strtotime(date('Y-m-d H:i:s')));
			}else{
				$date	=	date("Y-m-d",strtotime(date('Y-m-d H:i:s')  . ' - 1 day'));
			}
			$month		=	date("Y-m",strtotime($date));
			
			//Get a Game Data Cache
			if($_REQUEST['remove_cache'] != 1){
				$userGameData			=	$this->mem_lib->get('Football2014-GameData-' . uid);
			}
			
			if($userGameData){
				$userGameData['isCache']		=		true;
			}else{
				$userGameData['isCache']		=		false;
			}

			//---------------------Info---------------------//
			if(!isset($userGameData['point'])){
				$this->MyGameInfo							=	$this->games_model->getMemberSidebarInfo();
				$userGameData['point']						=	$this->MyGameInfo['point_remain'];
				$userGameData['total_rate_calculate']		=	$this->MyGameInfo['total_rate_calculate'];
			}else{
				$this->MyGameInfo['point_remain']			=	$userGameData['point'];
				$this->MyGameInfo['total_rate_calculate']	=	$userGameData['total_rate_calculate'];
			}
			
			//---------------------Reward---------------------//
			$isNoti['Monthly']	=	false;
			$isNoti['Daily']	=	false;
			
			if(!isset($userGameData['lastlogin'])){
				$RewardMonthlyLogin		=	$this->games_model->addReward(1);
				if($RewardMonthlyLogin['code_id']==200){
					$isNoti['Monthly']	=	true;
					$userGameData['point']	=	2000;
				}
				$RewardDailyLogin		=	$this->games_model->addReward(2);
				if($RewardDailyLogin['code_id']==200){
					$isNoti['Daily']	=	true;
					$userGameData['point']	=	$userGameData['point'] + 200;
				}
			}else{
			
				if($_REQUEST['pong_debug']==1){
					echo $userGameData['lastlogin'];
					$isNoti['Monthly']	=	true;
					$isNoti['Daily']	=	true;
				}

				if(date('G',strtotime($userGameData['lastlogin']))>=6){
					$userDate	=	date("Y-m-d",strtotime(strtotime($userGameData['lastlogin'])));
					$userMonth	=	date("Y-m",strtotime(strtotime($userGameData['lastlogin'])));
				}else{
					$userDate	=	date("Y-m-d",strtotime($userGameData['lastlogin'] . ' - 1 day'));
					$userMonth	=	date("Y-m",strtotime($userGameData['lastlogin'] . ' - 1 day'));
				}
				
				//Monthly Reward
				if($month != $userMonth){
					$RewardMonthlyLogin		=	$this->games_model->addReward(1);
					if($RewardMonthlyLogin['code_id']==200){
						$isNoti['Monthly']		=	true;
						$userGameData['point']	=	2000;
					}
				}
				
				//Daily Reward
				if($date != $userDate){
					$RewardDailyLogin		=	$this->games_model->addReward(2);
					if($RewardDailyLogin['code_id']==200){
						$isNoti['Daily']		=	true;
						$userGameData['point']	=	$userGameData['point'] + 200;
					}
				}
			}
			$userGameData['lastlogin']				=	date('Y-m-d H:i:s');
			$this->MyGameInfo['point_remain']		=	$userGameData['point'];

			$MyGameDate			=	$date;
		
			if($_REQUEST['remove_cache']==1)
				$clearGamePlay	=	true;
			else
				$clearGamePlay	=	false;
			
			if(isset($userGameData['myGame'])){
				if( $userGameData['myGame']['date_play'] != $MyGameDate ){
					$userGameData['myGame']		=		$this->games_model->getListInfoGamePlaySide($InfoMember->userid,$MyGameDate,$clearGamePlay);
				}else if($_REQUEST['pong_debug']==1){
					echo '<pre>';
					var_dump($userGameData);
					echo '</pre>';
				}
			}else{
				$userGameData['myGame']			=		$this->games_model->getListInfoGamePlaySide($InfoMember->userid,$MyGameDate,$clearGamePlay);
			}
			
			$this->MyGameSideToday	=	$userGameData['myGame'];
			$this->MyListPlayside	=	$userGameData['myGame']['game_info'];
			
			//PlaySide Single
			if(isset($this->MyListPlayside[0])){
				$this->MyListSinglePlayside		=		$this->MyListPlayside[0];
				$sizeOfPlay		=	count($this->MyListSinglePlayside['list']);
				for($x=0;$x<$sizeOfPlay;$x++) {
					$tmpGame			=	$this->MyListSinglePlayside['list'][$x];
					
					$MatchContents_MC 	= 	$this->mem_lib->get('Football2014-MatchContents-'.$tmpGame['match_id']);
					if($MatchContents_MC){
						if($_REQUEST['pong_debug']==1){
							var_dump($MatchContents_MC);
						}
					}else{
						$MatchContents_MC = $this->livescore_model->getLiveScoreMatch($tmpGame['match_id']);
						$MatchContents_MC = $MatchContents_MC['MatchInfo'];

						$this->mem_lib->set('Football2014-MatchContents-'.$tmpGame['match_id'],$MatchContents_MC, MEMCACHE_COMPRESSED);
					}
					
					$tmpGame['MatchStatus']		=		$MatchContents_MC['MatchStatus'];
					$tmpGame['Minute']			=		$MatchContents_MC['Minute'];
					$tmpGame['Team1FTScore']	=		$MatchContents_MC['Team1FTScore'];
					$tmpGame['Team2FTScore']	=		$MatchContents_MC['Team2FTScore'];
					
					$this->MyListSinglePlayside['list'][$x]		=		$tmpGame;
				}
				
				$this->view->assign('MyListSinglePlayside', $this->MyListSinglePlayside);
			}else{
				$this->view->assign('MyListSinglePlayside', array());
			}
			
			//PlaySide Multi
			$arrMatchCookie					=		array();
			$this->MyListMultiPlayside		=		array();
			
			$sizeOfGame		=		count($this->MyListPlayside);
			if($sizeOfGame>0){
				foreach($this->MyListPlayside as $keyList => $dataPlaySide){
				
					if($keyList==0){
						continue;
					}
					
					$sizeOfPlay		=	count($dataPlaySide['list']);
					for($x=0;$x<$sizeOfPlay;$x++) {
					
						$tmpGame			=	$dataPlaySide['list'][$x];
						
						$MatchContents_MC 	= 	$this->mem_lib->get('Football2014-MatchContents-'.$tmpGame['match_id']);
						if($MatchContents_MC){
							if($_REQUEST['pong_debug']==1){
								var_dump($MatchContents_MC);
							}
						}else{
							$MatchContents_MC = $this->livescore_model->getLiveScoreMatch($tmpGame['match_id']);
							$MatchContents_MC = $MatchContents_MC['MatchInfo'];

							$this->mem_lib->set('Football2014-MatchContents-'.$tmpGame['match_id'],$MatchContents_MC, MEMCACHE_COMPRESSED);
						}
						$tmpGame['MatchStatus']		=		$MatchContents_MC['MatchStatus'];
						$tmpGame['Minute']			=		$MatchContents_MC['Minute'];
						$tmpGame['Team1FTScore']	=		$MatchContents_MC['Team1FTScore'];
						$tmpGame['Team2FTScore']	=		$MatchContents_MC['Team2FTScore'];
						
						$dataPlaySide['list'][$x]	=		$tmpGame;
					}
					
					$this->MyListMultiPlayside[$keyList]	=	$dataPlaySide;
					$arrGameCookie[$keyList]				=	-1;
				}
			}
			$this->view->assign('MyListMultiPlayside', $this->MyListMultiPlayside);
			
			//Check a playside 's cookie

			for($i=1;$i<=5;$i++){
				if($arrGameCookie[$i]==-1){
					setcookie('SET' . $i,'-1');
					$arrGameCookie[$i]		=		array();
				}else{
					$arrGameTmp				=	explode('|',$_COOKIE['SET' . $i]);
					$sizeOfPlay				=	count($arrGameTmp);
					if($sizeOfPlay==0){
						setcookie('SET' . $i,'');
						$arrGameCookie[$i]		=		array();
					}else{
						for($x=0;$x<$sizeOfPlay;$x++){
							$tmpGame		=	explode(':',$arrGameTmp[$x]);
							if(!isset($tmpGame[1])){
								setcookie('SET' . $i,'');
								$arrGameCookie[$i]		=		array();
								break;
							}
							
							$MatchContents_MC 	= 	$this->mem_lib->get('Football2014-MatchContents-'.$tmpGame[0]);
							if($MatchContents_MC){
								if($_REQUEST['pong_debug']==1){
									var_dump($MatchContents_MC);
								}
							}else{
								$expire = (3600*12); 
								$MatchContents_MC = $this->livescore_model->getLiveScoreMatch($tmpGame['match_id']);
								$MatchContents_MC = $MatchContents_MC['MatchInfo'];
								$this->mem_lib->set('Football2014-MatchContents-'.$tmpGame[0],$MatchContents_MC, MEMCACHE_COMPRESSED, $expire);
							}
							
							$Team1Name 		= 		$this->mem_lib->get('Football2014-Team-NameTHShort-' . $MatchContents_MC['MatchInfo']['Team1KPID']);
							$Team2Name 		= 		$this->mem_lib->get('Football2014-Team-NameTHShort-' . $MatchContents_MC['MatchInfo']['Team2KPID']);
								
							$Team1Name 		= 		empty($Team1Name) ? $MatchContents_MC['MatchInfo']['Team1'] : $Team1Name;
							$Team2Name 		= 		empty($Team2Name) ? $MatchContents_MC['MatchInfo']['Team2'] : $Team2Name;
								
							$arrMatchCookie[$tmpGame[0]]	=	array(
								'Team1'		=>		$Team1Name,
								'Team2'		=>		$Team2Name,
								'Team1Logo'	=>		$MatchContents_MC['MatchInfo']['Team1Logo'],
								'Team2Logo'	=>		$MatchContents_MC['MatchInfo']['Team2Logo'],
								'TeamOdds'	=>		$MatchContents_MC['MatchInfo']['TeamOdds'],
								'Odds'		=>		$MatchContents_MC['MatchInfo']['Odds']
							);
								
							//setcookie('MatchInfo_' . $tmpGame[0], html_entity_decode($Team1Name . '|' . $Team2Name));
							$arrGameCookie[$i][$x]		=		array( 'match_id' => (int)$tmpGame[0],'team_win' => (int)$tmpGame[1]);
								
						}
					}
				}
			}
			$this->view->assign('arrMatchCookie', $arrMatchCookie);
			$this->view->assign('arrGameCookie', $arrGameCookie);
			
			$this->mem_lib->set('Football2014-GameData-' . uid ,$userGameData, MEMCACHE_COMPRESSED);
			
			/*
			echo '<pre>';
			var_dump($arrMatchCookie);
			echo '</pre>';
			*/
			
			/*
			echo '<pre>';
			var_dump($this->MyListSinglePlayside);
			echo '</pre>';
			*/
			
			/* -- Assign : Data -- */
			$this->MyGameInfo['quota_single']			=	3;
			
			$this->MyGameInfo['quota_multi_per_game_min']	=	2;
			$this->MyGameInfo['quota_multi_per_game_max']	=	6;
			
			$this->MyGameInfo['quota_multi']			=	1;
			$this->MyGameInfo['quota_multi_vip']		=	5;
			
			$this->view->assign('MyGameInfo', $this->MyGameInfo);
			
		}
		
		if($_REQUEST['pong_debug']==1){
			echo '<pre>';
			var_dump(json_decode($this->mem_lib->get('Football2016-MatchInfo-92250')));
			echo '</pre>';
		}
        
        /* -- Assign : Data -- */
		$this->view->assign('isNoti', $isNoti);
		
        $this->view->assign('isCanPlayGame', $this->isCanPlayGame);
		$this->view->assign('isInterval', false);
		$this->view->assign('LiveMatchListTime', array());
		
		$this->view->assign('CounterMatch', array('Live'=>0,'Sched'=>0,'Fin'=>0) );
		$this->view->assign('AllLiveMatchID', '');
		$this->view->assign('NextLiveDate', '');
		
        $this->view->assign('mem_lib', $this->mem_lib);
        $this->view->assign('minizone', $this->minizone);
        $this->view->assign('view', $this->view);
        $this->view->assign('login_obj', $this->login_lib);
        $this->view->assign('root_view', $this->root_view);
		$this->view->assign('football_lib', $this->football_lib);
		

        if(!is_bool(Login) and $_REQUEST['error'] > 0) {
             $this->view->assign('loginError', $_REQUEST['error']);
        }
        
        
    }
    ///////////////////////////////////////////////// Header /////////////////////////////////////////////////
    protected function _detect_device() 
    {
        $this->mobile_detect_lib = $this->minizone->library('mobile_detect_lib');
        if ($this->mobile_detect_lib->isMobile() && $_GET['view'] != 'full') {
            $this->root_view = FOLDER_VIEW_MOBILE;
        } else {
            $this->root_view = FOLDER_VIEW_DESKTOP;
        } 
    }
    
    ///////////////////////////////////////////////// Header /////////////////////////////////////////////////
    protected function _header($template='header.tpl') 
    {
       
        /* -- Load : Template (s) -- */
        $this->view->render($this->root_view.'/'.$template);
    }
    
    ///////////////////////////////////////////////// Footer /////////////////////////////////////////////////
    protected function _footer($template='footer.tpl') 
    {
        
        /* -- Load : Template (s) -- */
        $this->view->render($this->root_view.'/'.$template);
    }
    
    ///////////////////////////////////////////////// Blank /////////////////////////////////////////////////
    protected function _blank($msg, $redirect=TRUE,$template) 
    {
        $this->view->assign('msg', $msg);
        if ($redirect == TRUE)
            $this->view->assign('url', $_SERVER['HTTP_REFERER']);
        
        /* -- Load : Template (s) -- */
        $this->_header($template);
        $this->view->render($this->root_view.'/includes/blank.tpl');
        $this->_footer();
    }
    
    ///////////////////////////////////////////////// Set : Social Search /////////////////////////////////////////////////
    protected function _setSocial($data)
    {
        /* -- Set : Data (SEO) -- */
        if ($data['seo_title'] != '') $seo['seo_title'] = $data['seo_title'];
        if ($data['seo_description'] != '') $seo['seo_description'] = $data['seo_description'];
        if ($data['seo_keywords'] != '') $seo['seo_keywords'] = $data['seo_keywords'];

        /* -- Set : Data (SMO) -- */
        if ($data['fb_title'] != '') $smo['fb_title'] = $data['fb_title'];
        if ($data['fb_description'] != '') $smo['fb_description'] = $data['fb_description'];
        if ($data['fb_img'] != '') $smo['fb_img'] = "{$data['fb_img']}";

        /* -- Assign : Data (SEO & SMO) -- */
        $this->view->assign('seo', $seo);
        $this->view->assign('smo', $smo);
    }
    
    ///////////////////////////////////////////////// Set : Social Search /////////////////////////////////////////////////
    protected function _setBreadcrum($data=null) 
    {
        /* -- Set : Data (SEO) -- */
        if (is_array($data)) {
            $i = 0;
            foreach ($data as $p) {
                $arr_breadcrum[$i]['text'] = $p['text'];
                $arr_breadcrum[$i]['link'] = $p['link'];
                $arr_breadcrum[$i]['active'] = $p['active'];
                $i++;
            }
        } else {
            $arr_breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
            $arr_breadcrum[0]['link'] = BASE_HREF;
        }
        
        foreach ($arr_breadcrum as $b) {
            if ($b['active']) 
                $breadcrum .= '<li class="active">'.$b['text'].'</li>';
            elseif ($b['link']) 
                $breadcrum .= '<li><a href="'.$b['link'].'">'.$b['text'].'</a></li>';
            else
                $breadcrum .= '<li>'.$b['text'].'</li>';
        }

        /* -- Assign -- */
        $this->view->assign('breadcrum', $breadcrum);
    }
    
    
    
}
/* End of file my_con.php */
/* Location: ./system/controller/my_con.php */