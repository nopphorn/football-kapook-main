<?php if (!defined('MINIZONE')) exit;

class Games_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
		$this->minizone = minizone::getzone();
		$this->mem_lib = $this->minizone->library('memcache_lib');
        
    }
	
	function insertPlayScore($matchid ,$Team1Score ,$Team2Score){
	
		$url		= 	'http://football.kapook.com/api/gameapi_playscore_insert.php?match_id='.$matchid.'&Team1Score='.$Team1Score.'&Team2Score='.$Team2Score;
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return json_decode($result,true);
	}
	
	function insertPlaySide($matchid ,$select_team ,$select_point){
	
		//$url		=  'http://football.kapook.com/api/gameapi_playside_insert.php?team_win['.$matchid.']='.$select_team.'&point='.$select_point.'&isSingle=1';
		$url		=  'http://footballapi.kapook.com/game/insert?team_win['.$matchid.']='.$select_team.'&point='.$select_point.'&isSingle=1';
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		$returnArray 	=	json_decode($result,true);
		if($returnArray['code_id']	==	200){
			$this->mem_lib->delete('Football2014-GameData-' . uid);
			$this->mem_lib->delete('Football2014-MatchContents-'.$matchid);
		}
		return $returnArray;
	}
	
	//Use cookie data
	function insertPlaySideMulti($setNo,$point){
		
		if(!isset($_COOKIE['SET' . $setNo])){
			$result	=	array(
				'code_id'		=>	501,
				'message'		=>	'Cookie has problem.'
			);
			return $result;
		}
		$arrGameTmp		=	explode('|',$_COOKIE['SET' . $setNo]);
		$sizeOfPlay		=	count($arrGameTmp);
		
		for($x=0;$x<$sizeOfPlay;$x++){
			$tmpGame		=	explode(':',$arrGameTmp[$x]);
			if(!isset($tmpGame[1])){
				$result	=	array(
					'code_id'		=>	501,
					'message'		=>	'Cookie has problem.'
				);
				return $result;
			}
			$matchlist[]		=	'team_win['.$tmpGame[0].']=' . $tmpGame[1];
		}
		
		//$url		=  'http://football.kapook.com/api/gameapi_playside_insert.php?'. implode( '&' , $matchlist ) .'&point='.$point.'&isSingle=0&number_in_set='. $setNo;
		$url		=  'http://footballapi.kapook.com/game/insert?'. implode( '&' , $matchlist ) .'&point='.$point.'&isSingle=0&number_in_set='. $setNo;
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		$returnArray 	=	json_decode($result,true);
		
		if($returnArray['code_id']	==	200){
		
			$this->mem_lib->delete('Football2014-GameData-' . uid);
		
			$arrGameTmp		=	explode('|',$_COOKIE['SET' . $setNo]);
			$sizeOfPlay		=	count($arrGameTmp);
			
			for($x=0;$x<$sizeOfPlay;$x++){
				$tmpGame		=	explode(':',$arrGameTmp[$x]);
				$this->mem_lib->delete('Football2014-MatchContents-'.$tmpGame[0]);
			}

		}
		
		return $returnArray;
	}
	
	
	function getInfoGamePlayScore($match_id,$isClearCache=false){

		$url		= 	'http://football.kapook.com/api/gameapi_member_check_playscore.php?match_id='.$match_id;
		if($isClearCache){
			$url	=	$url . '&clear=1';
		}
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return json_decode($result,true);
	}
	
	function getListInfoGamePlayScore($userid=null,$date=null,$isClearCache=false){

		$url		= 	'http://football.kapook.com/api/gameapi_member_check_playscore.php';
		$attr		=	array();
		if($userid!=null)
			$attr[]	=	'user_id=' . $userid;
		if($date!=null)
			$attr[]	=	'date_play=' . $date;
		if($isClearCache)
			$attr[]	=	'clear=1';
		if(count($attr)>0)
			$url	=	$url . '?' . implode($attr,'&');
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return json_decode($result,true);
	}
	
	function getInfoGamePlaySide($match_id,$isClearCache=false){

		$url		= 	'http://football.kapook.com/api/gameapi_member_check_playside.php?match_id='.$match_id;
		if($isClearCache){
			$url	=	$url . '&clear=1';
		}
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return json_decode($result,true);
	}
	
	function getListInfoGamePlaySide($userid=null,$date=null,$isClearCache=false){

		$url		= 	'http://football.kapook.com/api/gameapi_member_check_playside.php';
		$attr		=	array();
		if($userid!=null)
			$attr[]	=	'user_id=' . $userid;
		if($date!=null)
			$attr[]	=	'date_play=' . $date;
		if($isClearCache)
			$attr[]	=	'clear=1';
		if(count($attr)>0)
			$url	=	$url . '?' . implode($attr,'&');
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return json_decode($result,true);
	}
	
	function getMemberScore($uid=null){
		$start_date['day']		=	1;
		$start_date['month']	=	8;
		
		$url		= 	'http://football.kapook.com/api/gameapi_member_info.php';
		if($uid!=null){
			$url		=  $url . '?id=' . $uid;
		}
		
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return json_decode($result,true);
	}
	
	function getMemberSidebarInfo(){
	
		$url		= 	'http://football.kapook.com/api/gameapi_member_info_min.php';
		
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return json_decode($result,true);
	}
	
	function getMyRanking($id,$attr=null,$value=null)
    {
        if (($attr)&&($value)) {
            $url = 'http://football.kapook.com/api/gameapi_member_rank.php?id=' . $id . '&' . $attr . '=' . $value;
        }
        else {
            $url = 'http://football.kapook.com/api/gameapi_member_rank.php?id=' . $id;
        }
		$result = json_decode(file_get_contents($url),true);
        return $result;
    }
	/*
	function getStatByMatch( $match_id , $size_show = 5)
    {
		$url = 'http://football.kapook.com/api/gameapi_playscore_match_stat.php?match_id=' . $match_id . '&show_size=' . $size_show;
		$result = json_decode(file_get_contents($url),true);
        return $result;
    }
	*/
	/*
	 * Reward Zone
	 */
	function addReward($typeid){
		$url = 'http://football.kapook.com/api/gameapi_add_reward.php?type=' . $typeid;
		
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return json_decode($result,true);
	}
	
	function getRewardByDate($id=null,$date=null,$isClearCache=false){
		if($date==null){
			if(date('G')>=6){
				$date	=	date('Y-m-d');
			}else{
				$date	=	date('Y-m-d',strtotime('now -1 day'));
			}
		}
		$url = 'http://football.kapook.com/api/gameapi_get_reward.php?date=' . $date;
		
		if($id!=null){
			$url = $url . '&user_id=' . $id;
		}
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return json_decode($result,true);
	}
	
	/*
	 * Get a Ranking
	 */
	function getRanking($type, $keydata, $sort_field, $sort_dir, $start, $size, $isClear = false, $isFull = false ){
		
		$url = 'http://football.kapook.com/api/gameapi_ranking.php?start=' . $start . '&length=' . $size;

		$url = $url . '&' . $type . '=' . $keydata;
		
		if($sort_field == 'total_point'){
			$url = $url . '&[order][0][column]=total_point';
		}else if($sort_field == 'total_playside_single_win_percent'){
			$url = $url . '&[order][0][column]=total_playside_single_win_percent';
		}else if($sort_field == 'total_playside_multi_win_percent'){
			$url = $url . '&[order][0][column]=total_playside_multi_win_percent';
		}else if($sort_field == 'total_rate_calculate'){
			$url = $url . '&[order][0][column]=total_rate_calculate';
		}

		$url = $url . '&[order][0][dir]=' . $sort_dir;
		
		if($isClear){
			$url = $url . '&clear=1';
		}
		
		if($isFull){
			$url = $url . '&full=1';
		}
		
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return json_decode($result,true);
	}
	
	/*
	 * Get a Top Playside
	 */
	function getTopPlayside($isClear = false , $sort_field = 'total', $sort_dir = 'desc', $start = 0, $size = 4, $date = '' ){
		
		$url = 'http://football.kapook.com/api/gameapi_playside_topplay.php?start=' . $start . '&length=' . $size . '&sort_field=' . $sort_field . '&sort_dir=' . $sort_dir;
		
		if($isClear){
			$url = $url . '&clear=1';
		}
		
		if($date != ''){
			$url = $url . '&date=' . $date;
		}
		
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return json_decode($result,true);
	}
	
}
/* End of file home_model.php */
/* Location: ./system/model/home_model.php */