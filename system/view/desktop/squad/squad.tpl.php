<?php 
	$monthtothai['Jan']['short'] ="ม.ค.";
	$monthtothai['Feb']['short'] ="ก.พ.";
	$monthtothai['Mar']['short'] ="มี.ค.";
	$monthtothai['Apr']['short'] ="เม.ย.";
	$monthtothai['May']['short'] ="พ.ค.";
	$monthtothai['Jun']['short'] ="มิ.ย.";
	$monthtothai['Jul']['short'] ="ก.ค.";
	$monthtothai['Aug']['short'] ="ส.ค.";
	$monthtothai['Sep']['short'] ="ก.ย.";
	$monthtothai['Oct']['short'] ="ต.ค.";
	$monthtothai['Nov']['short'] ="พ.ย.";
	$monthtothai['Dec']['short'] ="ธ.ค.";
	
	$monthtothai['01']['short'] ="ม.ค.";
	$monthtothai['02']['short'] ="ก.พ.";
	$monthtothai['03']['short'] ="มี.ค.";
	$monthtothai['04']['short'] ="เม.ย.";
	$monthtothai['05']['short'] ="พ.ค.";
	$monthtothai['06']['short'] ="มิ.ย.";
	$monthtothai['07']['short'] ="ก.ค.";
	$monthtothai['08']['short'] ="ส.ค.";
	$monthtothai['09']['short'] ="ก.ย.";
	$monthtothai['10']['short'] ="ต.ค.";
	$monthtothai['11']['short'] ="พ.ย.";
	$monthtothai['12']['short'] ="ธ.ค.";
	


	$monthtothai['Jan']['full'] ="มกราคม";
	$monthtothai['Feb']['full'] ="กุมภาพันธ์";
	$monthtothai['Mar']['full'] ="มีนาคม";
	$monthtothai['Apr']['full'] ="เมษายน";
	$monthtothai['May']['full'] ="พฤษภาคม";
	$monthtothai['Jun']['full'] ="มิถุนายน";
	$monthtothai['Jul']['full'] ="กรกฎาคม";
	$monthtothai['Aug']['full'] ="สิงหาคม";
	$monthtothai['Sep']['full'] ="กันยายน";
	$monthtothai['Oct']['full'] ="ตุลาคม";
	$monthtothai['Nov']['full'] ="พฤศจิกายน";
	$monthtothai['Dec']['full'] ="ธันวาคม";
	
	$monthtothai['01']['full'] ="มกราคม";
	$monthtothai['02']['full'] ="กุมภาพันธ์";
	$monthtothai['03']['full'] ="มีนาคม";
	$monthtothai['04']['full'] ="เมษายน";
	$monthtothai['05']['full'] ="พฤษภาคม";
	$monthtothai['06']['full'] ="มิถุนายน";
	$monthtothai['07']['full'] ="กรกฎาคม";
	$monthtothai['08']['full'] ="สิงหาคม";
	$monthtothai['09']['full'] ="กันยายน";
	$monthtothai['10']['full'] ="ตุลาคม";
	$monthtothai['11']['full'] ="พฤศจิกายน";
	$monthtothai['12']['full'] ="ธันวาคม";
	
	$daytothai['Sun'] = "วันอาทิตย์";
	$daytothai['Mon'] = "วันจันทร์";
	$daytothai['Tue'] = "วันอังคาร";
	$daytothai['Wed'] = "วันพุธ";
	$daytothai['Thu'] = "วันพฤหัสบดี";
	$daytothai['Fri'] = "วันศุกร์";
	$daytothai['Sat'] = "วันเสาร์";
	
	$ClassColorByMatchStatus['Fin'] = 'btn-grey';
	$ClassColorByMatchStatus['1 HF'] = 'btn-success';
	$ClassColorByMatchStatus['2 HF'] = 'btn-success';
	$ClassColorByMatchStatus['Sched'] = '';
	$ClassColorByMatchStatus['H/T'] = 'btn-warning';
	$ClassColorByMatchStatus['Post'] = 'btn-danger';
	
	$ProgramArr = $this->teamAllMatch;
	
	$teamNextMatchArr = $this->teamNextMatch;
	$teamNameEN = $this->teamName;
	$teamNameTH = $this->teamNameTH;
	$teamNews = $this->teamNews;
	
?>
<div class="container team">
  
  
<div class="row">


  <div class="col-md-4 side">

      <div id="space"></div>

    <div class="row ">
        <div class="team-side">
            <div class="profile">
                <img src="<?php echo $this->teamInfo['Logo']; ?>" alt="<?php echo $teamNameTH; ?>" class="team-logo">
                <p><?php echo $teamNameTH; ?> </p>
                
                <?php foreach($teamNextMatchArr as $tmpMatch){ ?>
                <div class="team-last-score">
				
                 <a href="<?php echo $tmpMatch['MatchPageURL']; ?>">
                 <div class="date">เกมถัดไป <?php echo date("d", strtotime($tmpMatch['MatchDate']))." ".$monthtothai[date("M", strtotime($tmpMatch['MatchDate']))]['short']." ".(date("Y", strtotime($tmpMatch['MatchDate']))+543); ?></div>
                                 <div class="row">
                                       <div class="col-md-5 "><?php echo $tmpMatch['Team1']; ?>
                                        </div>
                                        <div class="col-md-2"> 
                                       <br>VS
                                         </div>
                                       <div class="col-md-5"><?php echo $tmpMatch['Team2']; ?>
                                         </div>
                                 </div>
                 </a>
                </div>
                <?php break; } ?>

            </div>
            <ul class="list-group">
                <li class="list-group-item"><a href="#" class="scroll-link" data-id="section-about">เกี่ยวกับทีม</a></li>
                <li class="list-group-item"><a href="#" class="scroll-link" data-id="carousel-featured">ข่าวล่าสุด</a></li>
                <li class="list-group-item"><a href="#" class="scroll-link" data-id="section-result">ผลบอล & ตารางแข่งขัน</a></li> 
            </ul>
        </div>
    </div>                                                                            


  
  
  
  </div><!--End right 4 Aside -->






	
   <!-- Start LEFT COLUME --> 
  <div class="col-md-8 ">
  
  <h1  class="page-header font-display" id="section-about"><?php echo $teamNameTH; ?></h1>
  
<div class="page-header-social">
                 <div class="share share_size_large share_type_facebook">
                    <span class="share__count"></span>
                    <a class="share__btn" href="" onclick="fb_share();">Like</a>
                </div>
                 <div class="share share_size_large share_type_twitter">
                    <span class="share__count"></span>
                    <a class="share__btn" href="" onclick="twitter_share();">Tweet</a>
                </div>
               
                <div class="share share_size_large share_type_gplus">
                    <span class="share__count"></span>
                    <a class="share__btn" href="" onclick="google_share();">+1</a>
                </div>
                
                <div class="share share_size_large share_type_email">
                    <span class="share__count"></span>
                    <a class="share__btn" href="">E-mail</a>
                </div>

                
     </div>


  <div style="margin-top: 10px;">
  <?php echo $this->teamInfo['EmbedCode']; ?>
  </BR>
  <?php echo $this->teamInfo['Info']; ?>
  </div>


 
  <div id="carousel-featured" class="carousel slide" data-ride="carousel" style="margin-top: 10px;" >
       <h2 class="font-display">ข่าว <?php echo $teamNameTH; ?> ล่าสุด</h2>
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-featured" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-featured" data-slide-to="1"></li>
        <li data-target="#carousel-featured" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
	  <?php for($i=0;$i<3;$i++){ ?>
        <div class="item <?php if($i==0){ ?>active<?php }?>">
          <a href="<?php echo $teamNews[$i]['link']; ?>"><img src="<?php echo $teamNews[$i]['picture']; ?>" alt="<?php echo $teamNews[$i]['subject']; ?>">
          <div class="carousel-caption">
                <h2 ><?php echo $teamNews[$i]['subject']; ?></h2>
                <span class="date"> <?php echo $daytothai[date("D",$teamNews[$i]['CreateDate'])]." ".date("d", $teamNews[$i]['CreateDate'])." ".$monthtothai[date("M", $teamNews[$i]['CreateDate'])]['full']." ".(date("Y", $teamNews[$i]['CreateDate'])+543); ?> </span>
                <p><?php echo $teamNews[$i]['title']; ?></p>
          </div>
          </a>
        </div>
		<?php } ?>
      </div>

      <!-- Controls -->
      
     <!-- <a class="left carousel-control" href="#carousel-featured" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#carousel-featured" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a> -->
      
    </div>
  
  <div class="row subnews">
			<?php for($i=3;$i<7;$i++){ ?>
              <div class="news_block  col-md-3" ><a class="post.php" href="<?php echo $teamNews[$i]['link']; ?>"><img class="media-object" alt="" src="<?php echo $teamNews[$i]['picture']; ?>" ><?php echo $teamNews[$i]['subject']; ?> </a> </div>
			 <?php } ?>
             <div class="col-md-12 line faa-parent animated-hover" > <a href="/team-<?php echo strtolower(str_replace(" ","-",$teamNameEN)); ?>/news" class="more faa-vertical waves-effect waves-block"><i class="fa fa-plus "></i></a></div>
  </div>
</div>
<!--End Col 8 LEFT VOLUME-->
<div class="row col-md-12" >
  <h2 class="font-display" id="section-result">ผลบอล & ตารางแข่งขัน <?php echo $teamNameTH; ?></h2>
  
       <!-- Nav tabs -->
<?php
$dt = new DateTime();
$this_year = $dt->format('Y');
$year_now = '';
?>
<select class="form-control" id="year_select"><?php 
foreach($ProgramArr as $month=>$Program){
	if($year_now!=strval(substr($month,0,4))){
		if(strval(substr($month,0,4))==strval($this_year)){
			echo '<option value="'. strval(substr($month,0,4)) .'" selected>ปี '. (intval(substr($month,0,4))+543) .'</option>';
		}else{
			echo '<option value="'. strval(substr($month,0,4)) .'">ปี '. (intval(substr($month,0,4))+543) .'</option>';
		}
		$year_now = strval(substr($month,0,4));
	}
}
?>
</select>

<ul class="nav nav-tabs" role="tablist">
<?php
	$thismonth ='';
	$class='';
	$j=1;
	$dt = new DateTime();
	$monthnow = $dt->format('Y-m');
	
	foreach($ProgramArr as $month=>$Program){
		$thismonth = $monthtothai[strval(substr($month,-2))]['short']." ".((intval(substr($month,0,4))+543)%100); 
		if(strval($month)==strval($monthnow)){
			$class='class="active tab_month tab'. substr($month,0,4) .'"';
		}else{
			$class='class="tab_month tab'. substr($month,0,4) .'"';
		}
		?>
		<li id="dtab_<?php echo $j; ?>" <?php echo $class; ?>><a href="#d2<?php echo $j; ?>" role="tab" data-toggle="tab"><?php echo $thismonth; ?></a></li>
		<?php 
		$j++;
	}
  ?>
</ul>
<!-- Tab panes -->
<div class="tab-content">
<?php  

$j=1; 
$isActive	=	false;
foreach($ProgramArr as $month=>$tmpProgram){
if(strval($month)==strval($monthnow)){
	$class='class="tab-pane active tab_data_'. substr($month,0,4) .'"';
	$isActive	=	true;
}else{
	$class='class="tab-pane tab_data_'. substr($month,0,4) .'"';
}
?>
  <div <?php echo $class; ?> id="d2<?php echo $j; ?>">
    <table class="table table-striped">
	<?php
	
	foreach($tmpProgram as $tmpProgramByDate){ ?>
	<tbody>
		<?php
		if(intval(date('G'))<6){
			$TodayDate	=	date('Y-m-d',strtotime("-1 days"));
		}else{
			$TodayDate	=	date('Y-m-d');
		}

		for($i=0;$i<$tmpProgramByDate['totalmatch'];$i++){
			$MatchDate = $tmpProgramByDate[$i]['MatchDate'].' '.$tmpProgramByDate[$i]['MatchTime'].':00';
			if(intval(date('G',strtotime($MatchDate)))<6){
				$tmpDate	=	date('Y-m-d',strtotime($MatchDate . " -1 days"));
			}else{
				$tmpDate	=	date('Y-m-d',strtotime($MatchDate));
			}
			if($tmpDate == $TodayDate){
				$this->LiveMatchListTime[$tmpProgramByDate[$i]['id']]		=	$MatchDate;
				if($this->football_lib->isLiveMatch($tmpProgramByDate[$i]['MatchStatus'])){
					$this->AllLiveMatchID.=','.$tmpProgramByDate[$i]['id'];
				}
			}
			?>
			<tr>
				<td><?php echo date("d", strtotime($tmpProgramByDate[$i]['MatchDate']))." ".$monthtothai[date("m", strtotime($tmpProgramByDate[$i]['MatchDate']))]['short']." ".(date("Y", strtotime($tmpProgramByDate[$i]['MatchDate']))+543); ?></td>
				<td><?php echo $tmpProgramByDate[$i]['MatchTime']; ?></td>
				<td class="text_right"><a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$tmpProgramByDate[$i]['Team1KPID']))); ?>" class="tooltip_top match" title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><?php echo $tmpProgramByDate[$i]['Team1']; ?> <img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpProgramByDate[$i]['Team1Logo']); ?>"  width="24"></span></a></td>
				<td class="text_center"><?php $tmpMatch = $tmpProgramByDate[$i]; include INCLUDE_VIEW_ROOTPATH . 'livescore/match_boxscore.tpl.php'; ?></a></td>
				<td class="text_left"><a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$tmpProgramByDate[$i]['Team2KPID']))); ?>" class="tooltip_top match" title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpProgramByDate[$i]['Team2Logo']); ?>"  width="24"> <?php echo $tmpProgramByDate[$i]['Team2']; ?></span></a></td>
				<td class="channel"> <?php foreach($tmpProgramByDate[$i]['TVLiveList'] as $ChannelKey){?><img src="http://football.kapook.com/uploads/tvlogo/<?php echo $ChannelKey;?>.png" title="<?php echo $this->tv_lib->getTVText($ChannelKey); ?>"><?php }?> </td>
				<td> <a href="<?php echo strtolower($tmpProgramByDate[$i]['MatchPageURL']); ?>" class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><i class="fa fa-bar-chart-o"></i></a> </td>
			</tr>
		<?php } ?>
	</tbody>
	<?php } ?>
    </table>
 </div>   
<?php
 $j++; }
?>

</div>
  
<?php if(($isActive == false)&&($j > 1)){ ?>
<script language="JavaScript">
	$(document).ready(function(){
		$( "#year_select" ).val("<?php echo $year_now; ?>");
		$( "#dtab_<?php echo ($j-1); ?>" ).addClass( "active" );
		$( "#d2<?php echo ($j-1); ?>" ).addClass( "active" );
		$('.tab_month').hide();
		$( ".tab<?php echo $year_now; ?>").show();
	});
</script>
<?php }else{ ?>
<script language="JavaScript">
	$(document).ready(function(){
		year = parseInt($('#year_select').val());
		$('.tab_month').hide();
		$('.tab' + year).show();
	});
<?php } ?>
</script>

<script language="JavaScript">

	

	$('#year_select').change(function() {
		year = parseInt($('#year_select').val());
		$('.tab_month').hide();
		$('.tab' + year).show();
		$('.tab_month').removeClass( "active" );
		$('.tab' + year ).last().addClass( "active" );
		$('.tab-pane').removeClass( "active" );
		$('.tab_data_' + year ).last().addClass( "active" );
	});
</script>

   </div>
 </div>




<!--Start Right Side Colume-->



  </div>

<!-- DMP SCRIPT --> 
<script type="text/javascript" charset="UTF-8" src="http://cache.my.kapook.com/js_tag/dmp.js"></script>
<!-- //DMP SCRIPT -->