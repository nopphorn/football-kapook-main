<?php
session_start();

//if (extension_loaded('newrelic')) {
//    $app_name = 'FOOTBALL_SYSTEM';
//    newrelic_set_appname($app_name);
//    newrelic_capture_params();
//    newrelic_start_transaction($app_name);
//}

/**
 * KAPOOK
 * @version 1.0.0
 * @author Tharin Nilsri
 * @author Parinya Watta
 * @author Thitinun Reanthongchai
 */

date_default_timezone_set('Asia/Bangkok');
header('Content-type: text/html; charset=utf-8');

error_reporting(0);
ini_set('display_errors', 0);
ob_start();
define('MINIZONE', 1);

set_error_handler('handle_error');

function handle_error($type, $message, $file, $line, $context) {
    switch ($type) {
        case E_NOTICE:
            break;
        
        default:
//            if ($_SERVER['QUERY_STRING'] != 'debug') break;
//            case E_WARNING :
//            case E_PARSE :
//            case E_ERROR :
//           echo "<b>ERROR</b> [$type]: {$message}, Line {$line} of {$file}<br />\n";
            break;
    }
}
/* XHPROF Header*/
if ($_REQUEST['get_xhprof']==1) {
	echo 'start header xhprof';
	include_once '/var/web/football.kapook.com/html/xhprof/xhprof_lib/utils/xhprof_lib.php';
	include_once '/var/web/football.kapook.com/html/xhprof/xhprof_lib/utils/xhprof_runs.php';
	xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
}

/* -- ========= Include : File(s) ========= -- */
require_once('system/config/constants.php');
require_once('system/core/minizone.php');
//require_once('system/controller/'.FOLDER_AM.'/my_admin.php');
require_once('system/controller/my_con.php');
//require_once('system/model/mysql_model.php');
require_once('system/model/mongo_model.php');


/* -- ========= Set : Minizone ========= -- */
$zone = minizone::getzone();

/* -- Set : URI -- */
$uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $_SERVER['SCRIPT_NAME'];
if (strstr($uri, '?')) $uri = substr($uri, 0, strpos($uri, '?'));
list ($request, $method, $action) = explode('/', substr(substr($uri, -1)=='/' ? substr($uri, 0, -1) : $uri, strlen('/')));



$request = $request ? $request : 'home';
$method = $method ? $method : 'index';

/* -- Set : Route -- */
$RTR = $zone->router();
$RTR->_set_routing();


/* -- Set : URI (Route) -- */
$folder = $RTR->fetch_directory();
$request  = $RTR->fetch_class();
$method = $RTR->fetch_method();


if (extension_loaded('newrelic')) {
    $transaction_name = "{$request}#{$method}";
    if ($RTR->fetch_directory() != '') {
        $folder = $RTR->fetch_directory();
        $transaction_name = "{$folder}{$request}#{$method}";
    }

    newrelic_name_transaction($transaction_name);
}



/* -- Call : Controller -- */
$controller = $zone->controller($request, $folder);

call_user_func_array(array($controller, $method), array_slice($RTR->rsegments, 2));

//if (extension_loaded('newrelic')) {
//    newrelic_end_transaction();
//}
/* XHPROF FOOTER*/
if ($_REQUEST['get_xhprof']==1) {
	$profiler_namespace = 'myapp'; // namespace for your application
	$xhprof_data = xhprof_disable();
	$xhprof_runs = new XHProfRuns_Default();
	$run_id = $xhprof_runs->save_run($xhprof_data, $profiler_namespace);
	// url to the XHProf UI libraries (change the host name and path)
	$profiler_url = sprintf('http://202.183.165.188/xhprof/xhprof_html/?run=%s&source=%s', $run_id, $profiler_namespace);
	echo '<a href="'. $profiler_url .'" target="_blank">Profiler output</a>';
}

ob_end_flush();

/* End of file index.php */
/* Location: ./index.php */
