<!-- /view/desktop/comment/login.tpl -->
<div class="container">
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <strong>เกิดข้อผิดพลาด !</strong> <?php echo $this->message; ?>
    </div>
    <?php if ($this->error_type == 'login') : ?>
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6">
                <form action="http://signup-demo.kapook.com/connect/kapook" method="post">
                    <input type="hidden" name="apikey"  value="<?php echo API_KEY; ?>" /> 
                    <input type="hidden" name="callback" id="callback" value="<?php echo HTTP_REQUEST_URL; ?>/<?php echo $this->comment_id; ?>" />


                    <fieldset>
                        <h1 class="font-display"><i class="glyphicon glyphicon-user"></i> สมาชิกเข้าสู่ระบบ</h1>
                        <hr class="colorgraph">
                        <div class="form-group">
                            <input type="email" name="username" id="username"  class="form-control input-lg" placeholder="Email Address">
                        </div>
                        <div class="form-group">
                            <input name="password" id="password" type="password" class="form-control input-lg" placeholder="Password">  
                        </div>
                        <hr class="colorgraph">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <input type="submit" class="btn btn-lg btn-primary btn-block btn-log" value="เข้าระบบ">
                            </div>
                            <div class="col-xs-8 col-sm-8 col-md-8">
                                <a href="" class="btn btn-link">ลืมรหัสผ่าน?</a>,  <a href="" class="btn btn-link ">สมัครสมาชิก</a>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>



        </div>
    <?php else: ?>
        <?php
        if ($this->returnPage != '') :
            ?>
            <META HTTP-EQUIV="Refresh" CONTENT="3;URL=<?php echo $this->returnPage; ?>">
        <?php endif; ?>
    <?php endif; ?>