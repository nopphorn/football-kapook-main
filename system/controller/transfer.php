<?php if (!defined('MINIZONE')) exit;

class Transfer extends My_con 
{
    var $view; 
    var $minizone; 
    
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() 
    {

        parent::__construct();

        $this->news_model = $this->minizone->model('news_model');

    }
    
   ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
    function index() {
		$this->view->assign('mem_lib', $this->mem_lib);

		if (isset($_GET['list_page'])){
			$page = intval($_GET['list_page']);
		}else{
			$page = 1;
		}
		
		if (isset($_GET['news_page'])){
			$news_page = intval($_GET['news_page']);
		}else{
			$news_page = 1;
		}
		$size = 10;

		$NewsTransferContents = $this->news_model->getNewsTransfer($news_page,$_REQUEST['remove_cache'] == 1);
        $this->view->assign('NewsTransferContents', $NewsTransferContents);
        
		$PlayerMarketContents 			= 		$this->mem_lib->get('PlayerMarketContents-Page-' . $page);
		if((!$PlayerMarketContents)||($_REQUEST['remove_cache']=='1')){
			$expire							=		900;
			$PlayerMarketContents			=		$this->livescore_model->getPlayerMarket($page,$size);
			$this->mem_lib->set('PlayerMarketContents-Page-' . $page, $PlayerMarketContents, MEMCACHE_COMPRESSED, $expire);
		}
		
        $this->view->assign('PlayerMarketContents', $PlayerMarketContents);
       // var_dump($PlayerMarketContents);

		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = 'ตลาดนักเตะ';
		$breadcrum[1]['active'] = true;
        parent::_setBreadcrum($breadcrum);
		
		$data['seo_title'] = 'ตลาดซื้อขายนักเตะ ตลาดนักเตะ ข่าวซื้อขายนักเตะ ข้อมูลนักเตะย้ายทีม';
		$data['seo_description'] = 'ตลาดซื้อขายนักเตะ ข่าวตลาดนักเตะ สรุป ข่าวซื้อขายนักเตะ ข่าวแมนยูซื้อนักเตะ ข่าวลิเวอร์พูลซื้อนักเตะ สรุปข้อมูลนักเตะย้ายทีม ข่าวนักเตะใหม่แมนยู ข่าวนักเตะใหม่ลิเวอร์พูล ข่าวซื้อตัวผู้เล่น คลิกเลย';
		$data['seo_keywords'] = 'ตลาดซื้อขายนักเตะ, ข่าวตลาดนักเตะ, ข่าวซื้อขายนักเตะ, ข่าวแมนยูซื้อนักเตะ, ข่าวลิเวอร์พูลซื้อนักเตะ, สรุปข้อมูลนักเตะย้ายทีม, ข่าวนักเตะใหม่แมนยู, ข่าวนักเตะใหม่ลิเวอร์พูล, ข่าวซื้อตัวผู้เล่น';

		$data['fb_title'] = 'ตลาดซื้อขายนักเตะ ตลาดนักเตะ ข่าวซื้อขายนักเตะ ข้อมูลนักเตะย้ายทีม';
		$data['fb_description'] = 'ตลาดซื้อขายนักเตะ ข่าวตลาดนักเตะ สรุป ข่าวซื้อขายนักเตะ ข่าวแมนยูซื้อนักเตะ ข่าวลิเวอร์พูลซื้อนักเตะ สรุปข้อมูลนักเตะย้ายทีม';
		//$data['fb_img'] = 'http://football.kapook.com/assets/og-football/set1/ตลาดนักเตะ.jpg';
		$data['fb_img'] = 'http://football.kapook.com/assets/og/2014-11/page/ตลาดนักเตะ.jpg';
		parent::_setSocial($data);
		
        $this->_header();
        $this->view->render($this->root_view.'/home/transfer.tpl');
        $this->_footer();

    }
}
?>	