<?php

if (!defined('MINIZONE'))
    exit;

class Profile extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {

        parent::__construct();

        if(!isset($this->games_model)){
			$this->games_model = $this->minizone->model('games_model');
		}
		if(!isset($this->livescore_model)){
			$this->livescore_model = $this->minizone->model('livescore_model');
		}
		$this->news_model = $this->minizone->model('news_model');
    }
	
	function dashboard($username=null)
	{
		/*if(!$this->isCanPlayGame){
			echo 'กำลังปรับปรุงข้อมูล...ขออภัยในความไม่สะดวก';
			exit;
		}*/
		
		if($_REQUEST['remove_cache']==1)
		{
			$isClear	=	true;
		}
		else{
			$isClear	=	false;
		}
		
		if(date('G')>=6)
		{
			$date = date("Y-m-d");
		}
		else{
			$date = date("Y-m-d",strtotime('yesterday'));
		}
		
		if($username==null){
			$uid	=	$this->login_lib->checkCookie();
			if(!$uid)
			{
				header( "location: http://football.kapook.com" );
				exit(0);
			}
			else{
				$InfoMember		=	$this->login_lib->get_KMember(array( 'userid' =>  $uid));
				header( 'location: http://football.kapook.com/profile/' . $InfoMember->username );
				exit(0);
			}
		}else{
			$InfoMember												=	$this->login_lib->get_KMember(array( 'username' =>  $username));
			if(!isset($InfoMember->username)){
				
				//Write a Log Error
				
				if($username != 'game-user-dashboard.php'){				
					$tempLog	=	$this->mem_lib->get('Football2014-Log-Missing-Profile');
					$tempLog[]	=	array(
						'KMember'			=>	$InfoMember,
						'username_call'		=>	$username,
						'time_stamp'		=>	date("Y-m-d H:i:s")
					);
					$this->mem_lib->set('Football2014-Log-Missing-Profile',$tempLog);
					
					$uid		=	$this->login_lib->checkCookie();
					if(!$uid)
					{
						header( "location: http://football.kapook.com" );
						exit(0);
					}
					else{
						$InfoMember		=	$this->login_lib->get_KMember(array( 'userid' =>  $uid));
						header( 'location: http://football.kapook.com/profile/' . $InfoMember->username );
						exit(0);
					}
				}
			}
		}
		
		$InfoScoreMember 	= 		$this->mem_lib->get('MemberScore-' . $InfoMember->userid);
		if((!$InfoScoreMember)||($_REQUEST['remove_cache']=='1')){
			$InfoScoreMember									=	$this->games_model->getMemberScore($InfoMember->userid);
			$this->mem_lib->set('MemberScore-' . $InfoMember->userid, $InfoScoreMember, MEMCACHE_COMPRESSED, 900);
		}
		
		$InfoScoreMember['username']							=	$InfoMember->username;
		$InfoScoreMember['picture']								=	$InfoMember->picture;
		
		$DateProgram[0]	=	$date;
		$DateProgram[1]	=	date("Y-m-d",strtotime($date . ' - 1 day'));
		$DateProgram[2]	=	date("Y-m-d",strtotime($date . ' - 2 day'));
		$DateProgram[3]	=	date("Y-m-d",strtotime($date . ' - 3 day'));
		$DateProgram[4]	=	date("Y-m-d",strtotime($date . ' - 4 day'));
		$DateProgram[5]	=	date("Y-m-d",strtotime($date . ' - 5 day'));
		
		//var_dump($InfoMember);
		
		/*$listGame[0]		=	$this->games_model->getListInfoGamePlayScore($InfoMember->userid,$DateProgram[0],$isClear);	//	Today
		$listGame[1]		=	$this->games_model->getListInfoGamePlayScore($InfoMember->userid,$DateProgram[1],$isClear);	//	Yesterday
		$listGame[2]		=	$this->games_model->getListInfoGamePlayScore($InfoMember->userid,$DateProgram[2],$isClear);	//	- 2 day
		$listGame[3]		=	$this->games_model->getListInfoGamePlayScore($InfoMember->userid,$DateProgram[3],$isClear);	//	- 3 day
		$listGame[4]		=	$this->games_model->getListInfoGamePlayScore($InfoMember->userid,$DateProgram[4],$isClear);	//	- 4 day
		$listGame[5]		=	$this->games_model->getListInfoGamePlayScore($InfoMember->userid,$DateProgram[5],$isClear);	//	- 5 day*/
		
		$listGameSide[0]	=	$this->games_model->getListInfoGamePlaySide($InfoMember->userid,$DateProgram[0],$isClear);	//	Today
		$listGameSide[1]	=	$this->games_model->getListInfoGamePlaySide($InfoMember->userid,$DateProgram[1],$isClear);	//	Yesterday
		$listGameSide[2]	=	$this->games_model->getListInfoGamePlaySide($InfoMember->userid,$DateProgram[2],$isClear);	//	- 2 day
		$listGameSide[3]	=	$this->games_model->getListInfoGamePlaySide($InfoMember->userid,$DateProgram[3],$isClear);	//	- 3 day
		$listGameSide[4]	=	$this->games_model->getListInfoGamePlaySide($InfoMember->userid,$DateProgram[4],$isClear);	//	- 4 day
		$listGameSide[5]	=	$this->games_model->getListInfoGamePlaySide($InfoMember->userid,$DateProgram[5],$isClear);	//	- 5 day
		
		$listReward[0]		=	$this->games_model->getRewardByDate($InfoMember->userid,$DateProgram[0],$isClear);	//	Today
		$listReward[1]		=	$this->games_model->getRewardByDate($InfoMember->userid,$DateProgram[1],$isClear);	//	Yesterday
		$listReward[2]		=	$this->games_model->getRewardByDate($InfoMember->userid,$DateProgram[2],$isClear);	//	- 2 day
		$listReward[3]		=	$this->games_model->getRewardByDate($InfoMember->userid,$DateProgram[3],$isClear);	//	- 3 day
		$listReward[4]		=	$this->games_model->getRewardByDate($InfoMember->userid,$DateProgram[4],$isClear);	//	- 4 day
		$listReward[5]		=	$this->games_model->getRewardByDate($InfoMember->userid,$DateProgram[5],$isClear);	//	- 5 day
		
		$NewsContents = $this->news_model->getNewsList(1,$_REQUEST['remove_cache'] == 1);
		
		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
		$breadcrum[0]['link'] = BASE_HREF;
		$breadcrum[1]['text'] = $InfoMember->username;
		$breadcrum[1]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$data['seo_title'] = "เกมทายผลบอล กระปุกทายผลบอล เกมทายบอลเดี่ยว ทายบอลชุด";
		$data['seo_description'] = "เกมทายผลบอล ทายผลบอลเดี่ยว ทายผลบอลชุด ทายผลบอลโลก เกมทายผลบอลเก็บคะแนน จัดอันดับ เทพวางบอล";
		$data['seo_keywords'] = "ทายผลบอล, วางบอล, เกมทายผลบอล, ทายบอลเดี่ยว, ทายบอลชุด, เทพวางบอล";

		$data['fb_title'] = "กระปุกทายผลบอล เกมทายบอลเดี่ยว ทายบอลชุด";
		$data['fb_description'] = " เกมทายผลบอล ทายผลบอลเดี่ยว ทายผลบอลชุด ทายผลบอลเก็บคะแนน ทายผลบอลชิงรางวัล";
		$data['fb_img'] = '';

		parent::_setSocial($data);
		
		$this->view->assign('mem_lib', $this->mem_lib);
		$this->view->assign('InfoMember', $InfoScoreMember);
		$this->view->assign('DateProgram', $DateProgram);
		$this->view->assign('listGame', $listGame);
		$this->view->assign('listGameSide', $listGameSide);
		$this->view->assign('listReward', $listReward);
		/*echo '<pre>';
		var_dump($MyRank);
		echo '</pre>';*/
        $this->view->assign('NewsLeagueContents', $NewsContents['data']);
		
		if($_REQUEST['get_id_debug']==1){
			echo $InfoMember->userid;
		}
		
		$this->_header();
        $this->view->render($this->root_view . '/games/game-user-dashboard.tpl');
        $this->_footer();
	}
	
	function api($type_work = 'get_log' ){
		if(( uid != 856305 ) && ( uid != 363232 ) ){
			header( "location: http://football.kapook.com" );
			exit(0);
		}
		
		echo '<pre>';
		if($type_work == 'get_log'){
			$tempLog	=	$this->mem_lib->get('Football2014-Log-Missing-Profile');
			var_dump($tempLog);
		}
		echo '</pre>';
	}
    
}
