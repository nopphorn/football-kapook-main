<div class="container league euro2016">
<?php  
	$monthtothai['01'] ="มกราคม";	
	$monthtothai['02'] ="กุมภาพันธ์";
	$monthtothai['03'] ="มีนาคม";
	$monthtothai['04'] ="เมษายน";
	$monthtothai['05'] ="พฤษภาคม";
	$monthtothai['06'] ="มิถุนายน";
	$monthtothai['07'] ="กรกฎาคม";
	$monthtothai['08'] ="สิงหาคม";
	$monthtothai['09'] ="กันยายน";
	$monthtothai['10'] ="ตุลาคม";
	$monthtothai['11'] ="พฤศจิกายน";
	$monthtothai['12'] ="ธันวาคม";
	
	$daytothai['Sun'] = "วันอาทิตย์";
	$daytothai['Mon'] = "วันจันทร์";
	$daytothai['Tue'] = "วันอังคาร";
	$daytothai['Wed'] = "วันพุธ";
	$daytothai['Thu'] = "วันพฤหัสบดี";
	$daytothai['Fri'] = "วันศุกร์";
	$daytothai['Sat'] = "วันเสาร์";
	
	$OddsRate["0.25"] 	= "เสมอควบครึ่ง";
	$OddsRate["0.5"] 	= "ครึ่งลูก";
	$OddsRate["0.75"] 	= "ครึ่งควบลูก";
	$OddsRate["1"] 	= "หนึ่งลูก";
	$OddsRate["1.25"] 	= "ลูกควบลูกครึ่ง";
	$OddsRate["1.5"] 	= "ลูกครึ่ง";
	$OddsRate["1.75"] 	= "ลูกครึ่งควบสองลูก";
	$OddsRate["2"] 	= "สองลูก";
	$OddsRate["2.25"] 	= "สองลูกควบสองลูกครึ่ง";
	$OddsRate["2.5"] 	= "สองลูกครึ่ง";
	$OddsRate["2.75"] 	= "สองลูกครึ่งควบสามลูก";
	$OddsRate["3"] 	= "สามลูก";
	$OddsRate["3.25"] 	= "สามลูกควบสามลูกครึ่ง";
	$OddsRate["3.5"] 	= "สามลูกครึ่ง";
	$OddsRate["3.75"] 	= "สามลูกครึ่งควบสี่ลูก";
	$OddsRate["4"] 	= "สี่ลูก";
	$OddsRate["4.25"] 	= "สี่ลูกควบสี่ลูกครึ่ง";
	$OddsRate["4.5"] 	= "สี่ลูกครึ่ง";
	$OddsRate["4.75"] 	= "สี่ลูกครึ่งควบห้าลูก";
	$OddsRate["5"] 	= "ห้าลูก";
	$OddsRate["5.25"] 	= "ห้าลูกควบห้าลูกครึ่ง";
	$OddsRate["5.5"] 	= "ห้าลูกครึ่ง";
	$OddsRate["5.75"] 	= "ห้าลูกครึ่งควบหกลูก";
	$OddsRate["6"] 	= "หกลูก";
	$OddsRate["6.25"] 	= "หกลูกควบหกลูกครึ่ง";
	$OddsRate["6.5"] 	= "หกลูกครึ่ง";
	$OddsRate["6.75"] 	= "หกลูกครึ่งควบเจ็ดลูก";
	$OddsRate["7"] 	= "เจ็ดลูก";
	$OddsRate["7.25"] 	= "เจ็ดลูกควบเจ็ดลูกครึ่ง";
	$OddsRate["7.5"] 	= "เจ็ดลูกครึ่ง";
	$OddsRate["7.75"] 	= "เจ็ดลูกครึ่งควบแปดลูก";
	$OddsRate["8"] 	= "แปดลูก";
	$OddsRate["8.25"] 	= "แปดลูกควบแปดลูกครึ่ง";
	$OddsRate["8.5"] 	= "แปดลูกครึ่ง";
	$OddsRate["8.75"] 	= "แปดลูกครึ่งควบเก้าลูก";
	$OddsRate["9"] 	= "เก้าลูก";
	$OddsRate["9.25"] 	= "เก้าลูกควบเก้าลูกครึ่ง";
	$OddsRate["9.5"] 	= "เก้าลูกครึ่ง";
	$OddsRate["9.75"] 	= "เก้าลูกครึ่งควบสิบลูก";
	$OddsRate["10"] 	= "สิบลูก";
	
	$ProgramArr = $this->ProgramContents;
	$ProgramRoundArr = $this->ProgramRoundContents;
	
	$cupname 		= 	$this->InfoCupContents['nameTH'];
	$cupshortname 	= 	$this->InfoCupContents['nameTHShort'];
	$cuppathname	=	$this->InfoCupContents['nameURL'];

	include dirname(__FILE__) . "/clip.tpl.php";
	include dirname(__FILE__) . "/menubar.tpl.php";
	
	$LiveStatusArray = array('Sched','1 HF','2 HF','H/T','E/T','Pen');
	$ClassNameByStatusArray = array('Fin'=>'fin','Sched'=>'','1 HF'=>'success','2 HF'=>'success','E/T'=>'success','H/T'=>'success','Pen'=>'success');
	
	if(intval(date('G'))<6){
		$TodayDate	=	date('Y-m-d',strtotime("-1 days"));
	}else{
		$TodayDate	=	date('Y-m-d');
	}
	
	
 ?>

<div class="row">
    
   <!-- Start LEFT COLUME --> 
  <div class="col-md-12">

  
  
  <h2 class="font-display"></h2>
     <!-- Nav tabs -->
<ul class="nav nav-tabs tab2select" role="tablist">

  <?php
	$isTab = false;
	$thismonth ='';
	$class='';
	$i=1;
	$dt = new DateTime();
	$monthnow = $dt->format('m');
	foreach($ProgramArr as $month => $Program){

		if((strval(substr($month,-2))==strval($monthnow))&&($this->TabRoundSelect == -1)){
			$thismonth = "เดือนนี้ ".$monthtothai[strval(substr($month,-2))]." ".(date("Y")+543);  
			$class='class="active"';
			$isTab = true;
		}else{
			$thismonth = $monthtothai[strval(substr($month,-2))];
			$class='class=""';
		}
		?><li id="dtab_<?php echo $i; ?>" <?php echo $class; ?>><a href="#d2<?php echo $i; ?>" role="tab" data-toggle="tab"><?php echo $thismonth; ?></a></li><?php 
		$i++;
	}
	
	foreach($ProgramRoundArr as $tmpRound){
		if ((!$isTab)&&(($this->TabRoundSelect == -1)||($this->TabRoundSelect == $tmpRound['id']))){
			?><li id="dtab_<?php echo $i; ?>" class="active"><a href="#d2<?php echo $i; ?>" role="tab" data-toggle="tab"><?php echo $tmpRound['name']; ?></a></li><?php
			$isTab = true;
		}else{
			?><li id="dtab_<?php echo $i; ?>" class=""><a href="#d2<?php echo $i; ?>" role="tab" data-toggle="tab"><?php echo $tmpRound['name']; ?></a></li><?php
		}
		$i++;
	}
  ?>
</ul>



<!-- Tab panes -->
<div class="tab-content">
<?php
$j=1; 
$isActive	=	false;
foreach($ProgramArr as $month=>$tmpProgram){
	if((strval(substr($month,-2))==strval($monthnow))&&($this->TabRoundSelect == -1)){
		$class='class="tab-pane active"';
		$isActive	=	true;
	}else{
		$class='class="tab-pane"';
	}
	?>

	<div <?php echo $class; ?> id="d2<?php echo $j; ?>">

    <table class="table  responsive match-game style-white">
	<?php
	foreach($tmpProgram as $tmpProgramByDate){
	
?>
      <thead>
        <tr>
          <th colspan="8"><?php echo $daytothai[date("D",strtotime($tmpProgramByDate[0]['MatchDate']))]." ".date("d", strtotime($tmpProgramByDate[0]['MatchDate']))." ".$monthtothai[date("m", strtotime($tmpProgramByDate[0]['MatchDate']))]." ".(date("Y", strtotime($tmpProgramByDate[0]['MatchDate']))+543); ?></th>
        </tr>
      </thead>
      <tbody>
	  <?php
		$countMatch		=		count($tmpProgramByDate);
		for($i=0;$i<$countMatch;$i++){
			$MatchDate = $tmpProgramByDate[$i]['MatchDate'].' '.$tmpProgramByDate[$i]['MatchTime'].':00';
			if(intval(date('G',strtotime($MatchDate)))<6){
				$tmpDate	=	date('Y-m-d',strtotime($MatchDate . " -1 days"));
			}else{
				$tmpDate	=	date('Y-m-d',strtotime($MatchDate));
			}
			if($tmpDate == $TodayDate){
				$this->LiveMatchListTime[$tmpProgramByDate[$i]['id']]		=	$MatchDate;
				if($this->football_lib->isLiveMatch($tmpProgramByDate[$i]['MatchStatus'])){
					$this->AllLiveMatchID.=','.$tmpProgramByDate[$i]['id'];
				}
			}
		?>
        <tr>
			<td><?php echo $tmpProgramByDate[$i]['MatchTime']; ?></td>
			<td class="text_right"><a href="<?php echo $tmpProgramByDate[$i]['Team1URL']; ?>" class="tooltip_top match <?php if(($tmpProgramByDate[$i]['TeamOdds']==0 || $tmpProgramByDate[$i]['TeamOdds']==1)&&($tmpProgramByDate[$i]['Odds']!=-1)&&(isset($tmpProgramByDate[$i]['Odds']))){ echo 'handicap_color'; } ?>" title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><?php echo $tmpProgramByDate[$i]['Team1']; ?> <img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpProgramByDate[$i]['Team1Logo']); ?>" alt="<?php echo $tmpProgramByDate[$i]['Team1']; ?>"  width="24"></span></a></td>
			<td class="text_center"><?php $tmpMatch = $tmpProgramByDate[$i]; include INCLUDE_VIEW_ROOTPATH . 'livescore/match_boxscore.tpl.php'; ?></td>
			<td class="text_left"><a href="<?php echo $tmpProgramByDate[$i]['Team2URL']; ?>" class="tooltip_top match <?php if(($tmpProgramByDate[$i]['TeamOdds']==0 || $tmpProgramByDate[$i]['TeamOdds']==2)&&($tmpProgramByDate[$i]['Odds']!=-1)&&(isset($tmpProgramByDate[$i]['Odds']))){ echo 'handicap_color'; } ?>" title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpProgramByDate[$i]['Team2Logo']); ?>" alt="<?php echo $tmpProgramByDate[$i]['Team2']; ?>"  width="24"> <?php echo $tmpProgramByDate[$i]['Team2']; ?></span></a></td>
			<td> 
               <?php 
				if(isset($tmpProgramByDate[$i]['Odds'])){
				   if($tmpProgramByDate[$i]['Odds']==0){   
					   echo '(เสมอ)';                   
				   }else if($tmpProgramByDate[$i]['Odds']!=-1){
					   $OddsArr = explode('.',$tmpProgramByDate[$i]['Odds']);
					   if($OddsArr[0]>0){
						   for($o=1;$o<=$OddsArr[0];$o++){
							   echo '<pie class="hundreds"></pie>';
						   }
					   }
					   
					   if($OddsArr[1]=='25'){
						   echo '<pie class="twentyfive"></pie>';
					   }else if($OddsArr[1]=='5'){
						   echo '<pie class="fifty"></pie>';
					   }else if($OddsArr[1]=='75'){
						   echo '<pie class="seventyfive"></pie>';
					   }
					   
					   echo ' ('.$tmpProgramByDate[$i]['Odds'].')';
				   }
				} ?>
               &nbsp;
			</td>
           <td class="channel"> <?php foreach($tmpProgramByDate[$i]['TVLiveList'] as $ChannelKey){?><img src="http://football.kapook.com/uploads/tvlogo/<?php echo $ChannelKey;?>.png" title="<?php echo $this->tv_lib->getTVText($ChannelKey); ?>"><?php }?> </td>
          <td> <a href="<?php echo $tmpProgramByDate[$i]['MatchPageURL']; ?>" class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><i class="fa fa-bar-chart-o"></i></a> </td>
        </tr>
      <?php } ?>
      </tbody>
	<?php } ?>
    </table>
	</div>
	<?php
	 ++$j;

}
?>

<?php 
foreach($ProgramRoundArr as $tmpRound){
	if((!$isActive)&&(($this->TabRoundSelect == -1)||($this->TabRoundSelect == $tmpRound['id']))){ ?>
		<div class="tab-pane active" id="d2<?php echo $j; ?>"><?php
		$isActive = true;
	}else { ?> 
		<div class="tab-pane" id="d2<?php echo $j; ?>">
	<?php } ?>
    <table class="table  responsive match-game style-white">
	<?php
	$LastDate = '';
	foreach($tmpRound['Program'] as $tmpProgram){
		$MatchDate = $tmpProgram['MatchDate'].' '.$tmpProgram['MatchTime'].':00';
		if(intval(date('G',strtotime($MatchDate)))<6){
			$tmpDate	=	date('Y-m-d',strtotime($MatchDate . " -1 days"));
		}else{
			$tmpDate	=	date('Y-m-d',strtotime($MatchDate));
		}

		if($tmpDate == $TodayDate){
			$this->LiveMatchListTime[$tmpProgram['id']]		=	$MatchDate;
			if($this->football_lib->isLiveMatch($tmpProgram['MatchStatus'])){
				$this->AllLiveMatchID.=','.$tmpProgram['id'];
				
			}
		}
		if($LastDate != $tmpProgram['MatchDate']){
			if($LastDate != ''){ ?></tbody><?php } ?>
			<thead>
				<tr>
					<th colspan="8"><?php echo $daytothai[date("D",strtotime($tmpProgram['MatchDate']))]." ".date("d", strtotime($tmpProgram['MatchDate']))." ".$monthtothai[date("m", strtotime($tmpProgram['MatchDate']))]." ".(date("Y", strtotime($tmpProgram['MatchDate']))+543); ?></th>
				</tr>
			</thead>
			<tbody>
			<?php
			$LastDate = $tmpProgram['MatchDate'];
		} ?>
        <tr>
			<td><?php echo $tmpProgram['MatchTime']; ?></td>
			<td class="text_right"><a href="<?php echo $tmpProgram['Team1URL']; ?>" class="tooltip_top match <?php if(($tmpProgram['TeamOdds']==0 || $tmpProgram['TeamOdds']==1)&&($tmpProgram['Odds']!=-1)&&(isset($tmpProgram['Odds']))){ echo 'handicap_color'; } ?>" title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><?php echo $tmpProgram['Team1']; ?> <img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpProgram['Team1Logo']); ?>" alt="<?php echo $tmpProgram['Team1']; ?>"  width="24"></span></a></td>
			<td class="text_center"><?php $tmpMatch = $tmpProgram; include INCLUDE_VIEW_ROOTPATH . 'livescore/match_boxscore.tpl.php'; ?></td>
			<td class="text_left"><a href="<?php echo $tmpProgram['Team2URL']; ?>" class="tooltip_top match <?php if(($tmpProgram['TeamOdds']==0 || $tmpProgram['TeamOdds']==2)&&($tmpProgram['Odds']!=-1)&&(isset($tmpProgram['Odds']))){ echo 'handicap_color'; } ?>" title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpProgram['Team2Logo']); ?>" alt="<?php echo $tmpProgram['Team2']; ?>"  width="24"> <?php echo $tmpProgram['Team2']; ?></span></a></td>
			<td> 
               <?php 
				if(isset($tmpProgram['Odds'])){
				   if($tmpProgram['Odds']==0){   
					   echo '(เสมอ)';                   
				   }else if($tmpProgram['Odds']!=-1){
					   $OddsArr = explode('.',$tmpProgram['Odds']);
					   if($OddsArr[0]>0){
						   for($o=1;$o<=$OddsArr[0];$o++){
							   echo '<pie class="hundreds"></pie>';
						   }
					   }
					   
					   if($OddsArr[1]=='25'){
						   echo '<pie class="twentyfive"></pie>';
					   }else if($OddsArr[1]=='5'){
						   echo '<pie class="fifty"></pie>';
					   }else if($OddsArr[1]=='75'){
						   echo '<pie class="seventyfive"></pie>';
					   }
					   
					   echo ' ('.$tmpProgram['Odds'].')';
				   }
				} ?>
               &nbsp;
			</td>
           <td class="channel"> <?php foreach($tmpProgram['TVLiveList'] as $ChannelKey){?><img src="http://football.kapook.com/uploads/tvlogo/<?php echo $ChannelKey;?>.png" title="<?php echo $this->tv_lib->getTVText($ChannelKey); ?>"><?php }?> </td>
          <td> <a href="<?php echo $tmpProgram['MatchPageURL']; ?>" class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><i class="fa fa-bar-chart-o"></i></a> </td>
        </tr>
	<?php }
	if($LastDate != ''){ ?></tbody><?php } ?>
    </table>
	</div>
	<?php
	 ++$j;
}
?>

</div>

<?php if(($isActive == false)&&($j > 1)){ ?>
<script language="JavaScript">
	$(document).ready(function(){
		$( "#dtab_<?php echo ($j-1); ?>" ).addClass( "active" );
		$( "#d2<?php echo ($j-1); ?>" ).addClass( "active" );
	});
</script>
<?php } ?>
  
   <br> <br>
</div>
</div>