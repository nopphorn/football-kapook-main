<div class="container scorer premiere">
<?php  

$cupname 		= 	$this->InfoCupContents['nameTH'];
$cupshortname 	= 	$this->InfoCupContents['nameTHShort'];
$cuppathname	=	$this->InfoCupContents['nameURL'];
	
include dirname(__FILE__) . "/header.tpl.php";
 ?>

  
<div class="row">
   <!-- Start LEFT COLUME --> 
  <div class="col-md-8">

  <div class="top_scorer">
    <h2 class="font-display" ><i class="fa fa-star" ></i>  ดาวซัลโว<?php echo $cupname; ?> </h2>
    <table  class="table  table-striped border-1 ">
      <thead>
        <tr>
            <th class="text_center">#</th>
            <th>ชื่อผู้เล่น</th>
			<th></th>
			<th class="text_center">ประตู</th>
      
        </tr>
      </thead>
      <tbody>
      
        <?php
			if(count($this->ScorerContents)>0){
				$i = 1;
				foreach ($this->ScorerContents as $tmpData) { ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><a name=""><img src="<?php echo $tmpData['PlayerImg']; ?>" class="img-circle" title="<?php echo $tmpData['PlayerName']; ?>"></a></td>
						<td><a name="" class="tooltip_top" title="คลิกดูรายละเอียด" target="_blank"><h2 class="font-display"><?php echo $tmpData['PlayerName']; ?></h2></a>
							<a href="<?php echo $tmpData['TeamURL']; ?>" class="tooltip_top" title="คลิกดูรายละเอียด" target="_blank"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpData['TeamLogo']); ?>" width="24" height="24"  alt=""/><?php echo $tmpData['TeamName']; ?></a>
						</td>
						<td><?php echo $tmpData['goal']; ?></td>
					</tr>


				<?php
					$i++;
				}
			}else{
				?>
					<tr>
						<td colspan="4" >กำลังรอข้อมูล</td>
					</tr>
				<?php
			}
           ?>
        
      </tbody>
    </table>
    </div> 
     <br><br>
</div>
<!--End Col 8 LEFT VOLUME-->
  
  
<div class="col-md-4 aside ">
   <div class="sidebar_margin " ><h2 class="font-display"><i class="fa fa-list-alt"></i> ดาวซัลโว</h2>
    <ul class="nav">
      <li><a href="/พรีเมียร์ลีก/topscorer" class="tooltip_top" title="คลิกดูรายละเอียด" > ดาวซัลโวพรีเมียร์ลีก</a></li>
      <li><a href="/ลาลีกา/topscorer" class="tooltip_top" title="คลิกดูรายละเอียด" > ดาวซัลโวลาลีกา</a></li>
      <li><a href="/บุนเดสลีกา/topscorer" class="tooltip_top" title="คลิกดูรายละเอียด" > ดาวซัลโวบุนเดสลีกา</a></li>
	  <li><a href="/เซเรียอา/topscorer" class="tooltip_top" title="คลิกดูรายละเอียด" > ดาวซัลโวเซเรียอา</a></li>
      <li><a href="/ลีกเอิง/topscorer" class="tooltip_top" title="คลิกดูรายละเอียด" > ดาวซัลลีกเอิง</a></li>
      <li><a href="/ไทยพรีเมียร์ลีก/topscorer" class="tooltip_top" title="คลิกดูรายละเอียด" > ดาวซัลโวไทยพรีเมียร์ลีก</a></li>
    </ul>

</div>
</div><!--End right 4 Aside -->
</div>



<!-- DMP SCRIPT --> 
<script type="text/javascript" charset="UTF-8" src="http://cache.my.kapook.com/js_tag/dmp.js"></script>
<!-- //DMP SCRIPT --> 