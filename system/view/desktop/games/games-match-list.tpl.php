<?php 

$AllMatch1 = $this->ProgramContents1;
$TotalMatch1 = $AllMatch1['TotalMatch'];

for($i=1;$i<=$TotalMatch1;$i++){
    $AllMatchByLeague1[$AllMatch1[$i]['KPLeagueID']][] = $AllMatch1[$i];
}

$AllMatchByLeague[1] = $AllMatchByLeague1;


	$monthtothai['01']['short'] ="ม.ค.";
	$monthtothai['02']['short'] ="ก.พ.";
	$monthtothai['03']['short'] ="มี.ค.";
	$monthtothai['04']['short'] ="เม.ย.";
	$monthtothai['05']['short'] ="พ.ค.";
	$monthtothai['06']['short'] ="มิ.ย.";
	$monthtothai['07']['short'] ="ก.ค.";
	$monthtothai['08']['short'] ="ส.ค.";
	$monthtothai['09']['short'] ="ก.ย.";
	$monthtothai['10']['short'] ="ต.ค.";
	$monthtothai['11']['short'] ="พ.ย.";
	$monthtothai['12']['short'] ="ธ.ค.";
	
	$monthtothai['01']['full'] ="มกราคม";
	$monthtothai['02']['full'] ="กุมภาพันธ์";
	$monthtothai['03']['full'] ="มีนาคม";
	$monthtothai['04']['full'] ="เมษายน";
	$monthtothai['05']['full'] ="พฤษภาคม";
	$monthtothai['06']['full'] ="มิถุนายน";
	$monthtothai['07']['full'] ="กรกฎาคม";
	$monthtothai['08']['full'] ="สิงหาคม";
	$monthtothai['09']['full'] ="กันยายน";
	$monthtothai['10']['full'] ="ตุลาคม";
	$monthtothai['11']['full'] ="พฤศจิกายน";
	$monthtothai['12']['full'] ="ธันวาคม";
	
	$daytothai['Sun'] = "วันอาทิตย์";
	$daytothai['Mon'] = "วันจันทร์";
	$daytothai['Tue'] = "วันอังคาร";
	$daytothai['Wed'] = "วันพุธ";
	$daytothai['Thu'] = "วันพฤหัสบดี";
	$daytothai['Fri'] = "วันศุกร์";
	$daytothai['Sat'] = "วันเสาร์";

	$OddsRate["0.25"] 	= "เสมอควบครึ่ง";
	$OddsRate["0.5"] 	= "ครึ่งลูก";
	$OddsRate["0.75"] 	= "ครึ่งควบลูก";
	$OddsRate["1"] 		= "หนึ่งลูก";
	$OddsRate["1.25"] 	= "ลูกควบลูกครึ่ง";
	$OddsRate["1.5"] 	= "ลูกครึ่ง";
	$OddsRate["1.75"] 	= "ลูกครึ่งควบสองลูก";
	$OddsRate["2"] 		= "สองลูก";
	$OddsRate["2.25"] 	= "สองลูกควบสองลูกครึ่ง";
	$OddsRate["2.5"] 	= "สองลูกครึ่ง";
	$OddsRate["2.75"] 	= "สองลูกครึ่งควบสามลูก";
	$OddsRate["3"] 		= "สามลูก";
	$OddsRate["3.25"] 	= "สามลูกควบสามลูกครึ่ง";
	$OddsRate["3.5"] 	= "สามลูกครึ่ง";
	$OddsRate["3.75"] 	= "สามลูกครึ่งควบสี่ลูก";
	$OddsRate["4"] 		= "สี่ลูก";
	$OddsRate["4.25"] 	= "สี่ลูกควบสี่ลูกครึ่ง";
	$OddsRate["4.5"] 	= "สี่ลูกครึ่ง";
	$OddsRate["4.75"] 	= "สี่ลูกครึ่งควบห้าลูก";
	$OddsRate["5"] 		= "ห้าลูก";
	$OddsRate["5.25"] 	= "ห้าลูกควบห้าลูกครึ่ง";
	$OddsRate["5.5"] 	= "ห้าลูกครึ่ง";
	$OddsRate["5.75"] 	= "ห้าลูกครึ่งควบหกลูก";
	$OddsRate["6"] 		= "หกลูก";
	$OddsRate["6.25"] 	= "หกลูกควบหกลูกครึ่ง";
	$OddsRate["6.5"] 	= "หกลูกครึ่ง";
	$OddsRate["6.75"] 	= "หกลูกครึ่งควบเจ็ดลูก";
	$OddsRate["7"] 		= "เจ็ดลูก";
	$OddsRate["7.25"] 	= "เจ็ดลูกควบเจ็ดลูกครึ่ง";
	$OddsRate["7.5"] 	= "เจ็ดลูกครึ่ง";
	$OddsRate["7.75"] 	= "เจ็ดลูกครึ่งควบแปดลูก";
	$OddsRate["8"] 		= "แปดลูก";
	$OddsRate["8.25"] 	= "แปดลูกควบแปดลูกครึ่ง";
	$OddsRate["8.5"] 	= "แปดลูกครึ่ง";
	$OddsRate["8.75"] 	= "แปดลูกครึ่งควบเก้าลูก";
	$OddsRate["9"] 		= "เก้าลูก";
	$OddsRate["9.25"] 	= "เก้าลูกควบเก้าลูกครึ่ง";
	$OddsRate["9.5"] 	= "เก้าลูกครึ่ง";
	$OddsRate["9.75"] 	= "เก้าลูกครึ่งควบสิบลูก";
	$OddsRate["10"] 	= "สิบลูก";

	$DateProgram[5]=date("Y-m-d");
	$DateProgram[4]=date("Y-m-d",strtotime($date . ' - 1 day'));
	$DateProgram[3]=date("Y-m-d",strtotime($date . ' - 2 day'));
	$DateProgram[2]=date("Y-m-d",strtotime($date . ' - 3 day'));
	$DateProgram[1]=date("Y-m-d",strtotime($date . ' - 4 day'));

	$ClassColorByMatchStatus['Fin'] 	= 'btn-grey';
	$ClassColorByMatchStatus['1 HF']	= 'btn-success';
	$ClassColorByMatchStatus['2 HF']	= 'btn-success';
	$ClassColorByMatchStatus['Sched'] 	= '';
	$ClassColorByMatchStatus['H/T'] 	= 'btn-warning';
	$ClassColorByMatchStatus['Fin'] 	= 'btn-grey';

	$ClassColorByMatchStatus['Canc'] 	= 'btn-danger';
	$ClassColorByMatchStatus['Post'] 	= 'btn-danger';
	$ClassColorByMatchStatus['Abd'] 	= 'btn-danger';
?>
<link rel="stylesheet" href="<?php echo BASE_HREF; ?>assets/css/separator.css">
     <!--<style type="">.nav-container{display: none;}</style>-->
	<div class="landing-page sep-container">
		<div class="landing-page">
			<header class="landing-header" style="">
				<div class="container" style="position: relative;">
					<div class="row">
						<div class="col-lg-12"></div>
						<a href="javascript:void(0)"   class="goto-game-section"><img src="<?php echo BASE_HREF; ?>assets/img/game_head.png" border="0" style="max-width:100%; margin-top: 20px; width:100%"></a> 
					</div>	
				</div>
			</header>  
			<article class="separator separator-top" style="padding-top:0px"></article>
		</div>
			
		 <section class="col-2 " style=" background: #FFF;z-index: 2; padding-bottom: 0">
        <div class="container" style="margin-top: -180px">

    
	<!--br><br><br>
	<div class="alert alert-danger alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <h4>ขออภัยในความไม่สะดวก!</h4>
      <p>ขณะนี้ทางทีมงานกำลังปรับปรุงระบบเกมทายผลบอล</p>
    </div-->
	
	
	
	
	
	
      <h2 class="page-header font-display"> <i class="fa fa-certificate " style="color: orange;"></i>  กระปุกฟุตบอล แจกเสื้อบอลฟรีทุกเดือน </h2> 
    <div class="social pull-right" style="margin: -70px 0 10px 0;">
                 <div class="share share_size_large share_type_facebook">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="">Share</a>
                </div>
                 <div class="share share_size_large share_type_twitter">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="">Tweet</a>
                </div>
               
                <div class="share share_size_large share_type_gplus">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="">+1</a>
                </div>    
     </div>

    
    
     
              
     <div class="row">

<img src="<?php echo BASE_HREF; ?>image-content/pro1-gamePage.jpg" border="0" width="100%"  alt="" style="margin-bottom: 10px; display:none">

<div class="col-sm-12">

<span class="font-display" style="font-size:2.8em; line-height:1em; margin-top: -31px; display: block;">
กระปุกฟุตบอล แจกรางวัล เกมทายผลบอล 2014-15 แชมป์ประจำเดือน <br> รับเสื้อทีมฟุตบอลของแท้ ทีมไหนก็ได้ตามที่คุณต้องการ</span>
<br><br>

<div class="">
        <b><i class="fa fa-check-square-o"></i> กติกา</b> 
        <!--<i class="fa fa-check-square-o fa-check-square-o pull-right fa-5x" ></i>-->
        <ul>
       <li>ตัดสินจากคะแนนรวมการทายผลบอลและคะแนนโบนัสของทั้งเดือน </li>
<li>ดูผลอันดับคะแนนในต้นเดือนถัดไป อันดับ 1 ในตารางอันดับรายเดือน ได้รับรางวัลเสื้อชุดแข่งฟุตบอลของแท้</li>
<li>ผู้ได้รับรางวัลสามารถเลือกได้ด้วยตัวเองว่าอยากได้เสื้อแข่งของทีมไหน</li>
<li>สงวนสิทธิ์ของรางวัล เป็นเสื้อทีมฟุตบอลเฉพาะที่มีจำหน่ายอย่างถูกต้องในประเทศไทย</li>
<li>คำตัดสินของคณะกรรมการถือเป็นที่สุด   </li>
        </ul>
   </div>       
        

		<img src="http://football.kapook.com/image-content/t-shirt.png" style="position: absolute; right: -20px; top: -20px; width: 400px;">
      
        
        
        
</div>
     </div>  




       
              


        </div>

    </section>
	
	
	
	    <article class="separator separator-top" style="padding-top:0px; "></article>

    <section class="ss-style-normal dark hall-of-frame" >
        <div class="container" style="margin-bottom: -50px">

			<?php


			/*$userlist = array(
				array(name=>'test')
			)*/


			$mons = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");



			$userlist_json = '{
				"items":[
					{"Name":"jorddon", "year":"15", "month":"8","point":"182,800","img":"http://my.kapook.com/avatar/avatar_120x120.jpg"},
					{"Name":"catolica", "year":"15", "month":"9","point":" 4,400,650","img":"http://my.kapook.com/avatar/avatar_120x120.jpg"},
					{"Name":"maychelsea", "year":"15", "month":"10","point":"782,400","img":"http://signup-demo.kapook.com/files/member/201412/54a252ddc3101.jpg"},
					{"Name":"lek145", "year":"15", "month":"11","point":"1,600,400","img":"http://signup-demo.kapook.com/images/nopic.png"},
					{"Name":"fres10", "year":"15", "month":"12","point":"382,050","img":"http://signup-demo.kapook.com/images/nopic.png"},
					{"Name":"", "year":"16", "month":"1","point":"","img":""},
					{"Name":"", "year":"16", "month":"2","point":"","img":""},
					{"Name":"", "year":"16", "month":"3","point":"","img":""},
					{"Name":"", "year":"16", "month":"4","point":"","img":""},
					{"Name":"", "year":"16", "month":"5","point":"","img":""},
					{"Name":"", "year":"16", "month":"6","point":"","img":""},
					{"Name":"", "year":"16", "month":"7","point":"","img":""}
					]}';
			$userlist_obj = json_decode($userlist_json) ;


			$monthly = $userlist_obj->items[4];

			//var_dump($monthly->Name);



			?>

            

             <div class="row index-topscore">

                 <div class="col-sm-4">
				 
					<h2 class="font-display" style="font-size: 50px; color: white; margin-top: -50px;">Honours and Awards</h2>
					<br>

                     <h3 style="font-size: 30px; position: relative"><i class="fa fa-trophy" style="color: orange;"></i> ผู้ชนะประจำเดือน <span style="top: -20px; font-size: 14px; opacity: 0.5; position:absolute; white-space: nowrap; right: 20px"><?php echo $mons[$monthly->month] .' 20'.$monthly->year ?></span></h3>
                     <!--img class="img-circle img-responsive img-center" src="http://signup-demo.kapook.com/files/member/201406/539ea9a0b0378.jpg" alt="">
                     <br-->
                     <div class="hilight clearfix" style="color: #3e3e3e">
                         <div class="col-sm-5 text_center"><p style="margin: 10px;"><a href="http://football.kapook.com/profile/<?php echo $monthly->Name ?>" class="game-user-avartar img-circle"  style="background: url(<?php echo $monthly->img ?>) no-repeat; background-size: cover;"></a></p>

                         </div>
                         <div class="col-sm-7 text_center" style="font-size: 18px;">  <br>
                             คุณ <a href="http://football.kapook.com/profile/<?php echo $monthly->Name ?>"><?php echo $monthly->Name ?></a>    <br>

                             <div class="statistic">

                                 <span class="point"><?php echo $monthly->point ?>  </span>แต้ม  <br>
                             </div>

                         </div>

                         <span class="position">1</span>
                     </div>
                    <div class="text-center" style="margin-top: 10px; display:none"> เลือกเสื้อทีม Manchester United มูลค่า 2,790 บาท </div>
					
					
						 <div class="alert alert-warning" style="margin-top: 10px;">
					สมาชิกผู้ชนะประจำเดือน กรุณาติดต่อกลับที่หมายเลขโทรศัพท์ 02-911-0915 ในช่วงเวลา 09.00 - 18.00 วันจันทร์-ศุกร์ เพื่อดำเนินการรับของรางวัล
					</div>

                 </div>
				 
			



           



    <div class="col-sm-4">

        <h4>ผู้ชนะในแต่ละเดือน ฤดูกาล 2015-16</h4>

        <p>
        </p><table class="table short-ranking full">
            <thead>
            <tr>
                <th>เดือน</th>
                <th></th>
                <th></th>
                <th></th>
                <th>แต้ม</th>
            </tr>
            </thead>
            <tbody><?php

			// var_dump($userlist->items[0]);

			//var_dump($userlist_obj->items[0]);
			foreach ( $userlist_obj->items as $item ){

				$name = $item->Name !="" ? $item->Name : "???";
				$link = $item->Name !="" ? "http://football.kapook.com/profile/".$item->Name : "";

				?><tr>
                <td><?php echo $mons[$item->month]  ?> <?php echo $item->year  ?></td>
                <td>
                    <a href="<?php echo $link  ?>" class="game-user-avartar img-circle" title="<?php echo $name  ?>" style="background: url(<?php echo $item->img  ?>) no-repeat; background-size: cover;"></a>
                </td>
                <td><a href="<?php echo $link  ?>"><?php echo $name  ?></a></td>
                <td class="text_center"></td>
                <td><?php echo $item->point != "" ? $item->point : "???"  ?></td>
            	</tr>
				<?php } ?>
<!--            <tr>
                <td>2014 DEC</td>
                <td>
                    <a href="http://football.kapook.com/profile/nontawat01non" class="game-user-avartar img-circle" title="nontawat01non" style="background: url(http://signup-demo.kapook.com/images/nopic.png) no-repeat; background-size: cover;"></a>
                </td>
                <td><a href="http://football.kapook.com/profile/nontawat01non">nontawat01non</a></td>
                <td class="text_center"></td>
                <td>4,447,000</td>
            </tr>
            <tr>
                <td>2015 JAN</td>
                <td><a href="http://football.kapook.com/profile/noylateefa" class="game-user-avartar img-circle" title="noylateefa" style="background: url(http://signup-demo.kapook.com/images/nopic.png) no-repeat; background-size: cover;"></a>
                </td>
                <td><a href="http://football.kapook.com/profile/noylateefa">noylateefa</a></td>
                <td class="text_center"></td>
                <td>245,687</td>
            </tr>
			    <tr>
                <td>2015 FEB</td>
                <td><a href="http://football.kapook.com/profile/gasiraky" class="game-user-avartar img-circle" title="noylateefa" style="background: url(http://my.kapook.com/avatar/avatar_120x120.jpg) no-repeat; background-size: cover;"></a>
                </td>
                <td><a href="http://football.kapook.com/profile/gasiraky">Gasiraky</a></td>
                <td class="text_center"></td>
                <td>355,640</td>
            </tr>
			 <tr>
                <td>2015 MAR</td>
                <td><a href="http://football.kapook.com/profile/25141272" class="game-user-avartar img-circle" title="25141272" style="background: url(http://signup-demo.kapook.com/files/member/201406/539ea9a0b0378.jpg) no-repeat; background-size: cover;"></a>
                </td>
                <td><a href="http://football.kapook.com/profile/25141272">25141272</a></td>
                <td class="text_center"></td>
                <td>300,200</td>
            </tr>
			
			
			
			 <tr>
                <td>2015 APR</td>
                <td><a href="http://football.kapook.com/profile/goterza" class="game-user-avartar img-circle"  style="background: url(http://graph.facebook.com/100002953634759/picture) no-repeat; background-size: cover;"></a>
                </td>
                <td><a href="http://football.kapook.com/profile/goterza">goterza</a></td>
                <td class="text_center"></td>
                <td>800,000</td>
            </tr>
			
			<tr>
                <td>2015 MAY</td>
                <td><a href="http://football.kapook.com/profile/costacota" class="game-user-avartar img-circle"  style="background: url(http://my.kapook.com/avatar/avatar_120x120.jpg) no-repeat; background-size: cover;"></a>
                </td>
                <td><a href="http://football.kapook.com/profile/costacota">costacota</a></td>
                <td class="text_center"></td>
                <td>3,321,600</td>
            </tr>
			
			<tr>
                <td>2015 JUN</td>
                <td><a href="http://football.kapook.com/profile/zsomton" class="game-user-avartar img-circle"  style="background: url(http://my.kapook.com/avatar/avatar_120x120.jpg) no-repeat; background-size: cover;"></a>
                </td>
                <td><a href="http://football.kapook.com/profile/zsomton">zsomton</a></td>
                <td class="text_center"></td>
                <td>358,400</td>
            </tr>
			
			
			
				<tr>
                <td>2015 JUL</td>
					<td><a href="http://football.kapook.com/profile/roysok" class="game-user-avartar img-circle"  style="background: url(http://signup-demo.kapook.com/files/member/201507/55b5886dc8fb4.jpg) no-repeat; background-size: cover;"></a>
					</td>
					<td><a href="http://football.kapook.com/profile/roysok">roysok</a></td>
					<td class="text_center"></td>
					<td>358,400</td>
				</tr>
-->


			</tbody>
        </table>

		
		
		
		
    </div>
	
	 <div class="col-sm-4">
		 <h3 class="font-display" style="font-size: 40px;line-height: 40px; color:#FFC800; margin-top: -50px;">ทำเนียบแชมป์เกมทายผลบอล</h3>

		 <style>
			 .table-fame th{color: #f2d561}
			 .table-fame tr.winner{color: #56390b; font-size: 1.1em; font-weight: bold}
			 .table-fame tr.winner a{color: inherit}
			 .table-fame tr.noborder>td{border-top:2px solid #b19b46 !important; padding:4px; color: #c2ab52; text-align: justify;     line-height: 17px !important;  font-size: 12px;}
			 .table-fame tr.noborder>td a{color: #663300}


		 </style>
		 <div style="display: block; background:#f2d561">

			<table class="table table-fame">
			<thead style="background: #b29c44">
			<tr>
				<th>ฤดูการ</th>
				<th>ชื่อ</th>
				<th>แต้ม</th>
			</tr>
			</thead>
			<tbody>
			<tr class="fontface winner">
				<td>2014-15</td>
				<td><a href="">nontawat01non</a></td>
				<td>4,447,000</td>
			</tr>
			<tr style="color: #56390b;" class="noborder">
				<td colspan="3">
					DEC14 <a href="">nontawat01non</a>
					, JAN15 <a href="">noylateefa</a>
					, FEB15 <a href="">Gasiraky</a>
					, MAR15 <a href="">25141272</a>
					,APR15 <a href="">goterza</a>
					, MAY15 <a href="">costacota</a>
					, JUN15 <a href="">zsomton</a>
					, JUL15 <a href="">roysok</a></td>
			</tr>
			</tbody>




			</table>





		 </div>



		
		
		
    </div>




</div>





			<div class="text-danger">
			<strong>* หมายเหตุ</strong>
			<ul>  
			<li>เกมทายผลบอล แข่งขันโดยสะสมคะแนนเดือนต่อเดือน     </li>
			<li>คะแนนประจำซีซั่น นับโดยเอาคะแนนของแต่ละเดือนมารวมกัน  </li>
			<li>ซีซั่น 2015-16 เริ่มจาก 1 ส.ค. 2015 และสิ้นสุด 31 ก.ค. 2016   </li>
			</ul>
			  </div>  

    </div>
</section>


<article class="separator separator-top" style="padding-top:0px; "></article>
		
		
		
		
		
		
		
		
		
		
	</div>	

	<div class="container" >
		<div class="page-header-option">
			<p style="top:40px">
				
			</p>
		</div>
		<h2 class="page-header font-display"><i class="fa fa-trophy" style="color: orange;"></i> อันดับคะแนน</h2> 
<div class="row">
   
   



</div>

		
		<div class="row">          
			<div class="col-sm-4">                                           
				<h3>ประจำวัน</h3>
				<p>
					<table class="table short-ranking">
						<thead>
							<tr>
								<th>อันดับ  </th> <th></th> <th></th> <th>ระดับ</th><th>แต้ม</th>
							</tr>
						</thead>
							<tbody>
								<?php
								foreach($this->RankingDaily['data'] as $data){?>
									<tr>
										<td><?php echo $data['ranking']; ?></td> <td><?php echo $data['photo']; ?></td><td><?php echo $data['name']; ?></td><td><?php echo $data['gameA']; ?></td><td><?php echo number_format($data['gameD']); ?></td>
									</tr>
								<?php   }
									if(isset($this->MyInfoScoreMember)){
									?> 
									<tr>
										<td><?php echo $this->MyRankingDaily['rank']; ?></td>
										<td><a href="<?php echo BASE_HREF . 'profile/' . $this->MyInfoScoreMember['username']; ?>" class="game-user-avartar img-circle" title="<?php echo $this->MyInfoScoreMember['username']; ?>" style="background: url(<?php echo $this->MyInfoScoreMember['picture']; ?>) no-repeat; background-size: cover;"></a></td>
										<td><a href="<?php echo BASE_HREF . 'profile/' . $this->MyInfoScoreMember['username']; ?>"><?php echo $this->MyInfoScoreMember['username']; ?></a></td>
										<td><?php echo $this->MyRankingDaily['rate']; ?></td>
										<td><?php echo $this->MyRankingDaily['score']; ?></td>
									</tr>
									<?php
									}
									?>
								</tbody>
					</table>
				</p> 
			</div>
			<div class="col-sm-4">                                           
				<h3>ประจำเดือน</h3>
				<p>
					<table class="table short-ranking">
						<thead>
							<tr>
								<th>อันดับ  </th> <th></th> <th></th> <th>ระดับ</th><th>แต้ม</th>
							</tr>
						</thead>
							<tbody>
								<?php
								foreach($this->RankingMonth['data'] as $data){?>
									<tr>
										<td><?php echo $data['ranking'] ?></td> <td><?php echo $data['photo']; ?></td><td><?php echo $data['name']; ?></td><td><?php echo $data['gameA']; ?></td><td><?php echo number_format($data['gameD']); ?></td>
									</tr>
								<?php   }
									if(isset($this->MyInfoScoreMember)){
									?> 
									<tr>
										<td><?php echo $this->MyRankingMonth['rank']; ?></td>
										<td><a href="<?php echo BASE_HREF . 'profile/' . $this->MyInfoScoreMember['username']; ?>" class="game-user-avartar img-circle" title="<?php echo $this->MyInfoScoreMember['username']; ?>" style="background: url(<?php echo $this->MyInfoScoreMember['picture']; ?>) no-repeat; background-size: cover;"></a></td>
										<td><a href="<?php echo BASE_HREF . 'profile/' . $this->MyInfoScoreMember['username']; ?>"><?php echo $this->MyInfoScoreMember['username']; ?></a></td>
										<td><?php echo $this->MyRankingMonth['rate']; ?></td>
										<td><?php echo $this->MyRankingMonth['score']; ?></td>
									</tr>
									<?php
									}
									?>
								</tbody>
					</table>
				</p> 
			</div>
			<div class="col-sm-4">                                           
				<h3>ฤดูกาล 2014-15</h3>
				<p>
					<table class="table short-ranking">
						<thead>
							<tr>
								<th>อันดับ  </th> <th></th> <th></th> <th>ระดับ</th><th>แต้ม</th>
							</tr>
						</thead>
							<tbody>
								<?php
								foreach($this->RankingYear['data'] as $data){?>
									<tr>
										<td><?php echo $data['ranking'] ?></td> <td><?php echo $data['photo']; ?></td><td><?php echo $data['name']; ?></td><td><?php echo $data['gameA']; ?></td><td><?php echo number_format($data['gameD']); ?></td>
									</tr>
								<?php   }
									if(isset($this->MyInfoScoreMember)){
									?> 
									<tr>
										<td><?php echo $this->MyRankingYear['rank']; ?></td>
										<td><a href="<?php echo BASE_HREF . 'profile/' . $this->MyInfoScoreMember['username']; ?>" class="game-user-avartar img-circle" title="<?php echo $this->MyInfoScoreMember['username']; ?>" style="background: url(<?php echo $this->MyInfoScoreMember['picture']; ?>) no-repeat; background-size: cover;"></a></td>
										<td><a href="<?php echo BASE_HREF . 'profile/' . $this->MyInfoScoreMember['username']; ?>"><?php echo $this->MyInfoScoreMember['username']; ?></a></td>
										<td><?php echo $this->MyRankingYear['rate']; ?></td>
										<td><?php echo $this->MyRankingYear['score']; ?></td>
									</tr>
									<?php
									}
									?>
								</tbody>
					</table>
				</p> 
			</div>
		</div>
		<div class="text_center"><a href="<?php echo BASE_HREF; ?>games/ranking" class="btn btn-primary ranking-link">+ ดูอันดับทั้งหมดคลิก !</a></div>
	</div>

	
	
<div class="container" id="game-section"> 
  
<div class="page-header-option">
	<p style="top:40px">
		<a href="javascript:void(0)"   onclick="tour.restart();" class="btn btn-link"><i class="fa fa-info-circle"></i> คำแนะนำก่อนเล่น</a>
		<a href="<?php echo BASE_HREF . 'games/rule-help/'; ?>" class="btn btn-link"><i class="fa fa-file-text"></i> กติกาการเล่น & คำถาม?</a>
	</p>
</div>

<!--<div class="page-header-option">
            <p>
           วันนี้ วันอาทิตย์ 5 ส.ค. 2557  &nbsp; &nbsp;

          </p>
</div>
            -->
  <?php 
	$i=1;
	$tmpDateArr = explode('-',$DateProgram[$i]);             
	//$AllLiveMatchID = '0'; //use $this->AllLiveMatchID
	//$Counter['Live'] = 0; 
	//$Counter['Sched'] = 0; 
	//$NextLiveDate = ''; //user $this->$NextLiveDate
	$LiveStatusArray = array('Sched','1 HF','2 HF','E/T','Pen');

  ?>
<h2 class="page-header font-display"><i class="fa fa-check-circle-o" style="color: #6DB600;"></i> เลือกทีมทายผลบอลวันนี้</h2>

<!--div class="alert alert-danger" role="alert">
      <strong>แจ้งสมาชิก : </strong>ขณะนี้อยู่ในระหว่างดำเนินการปรับปรุงระบบ อาจส่งผลให้การแสดงข้อมูลบางส่วนไม่ถูกต้อง
    </div-->

<div class="row">
	  <!--div class="social pull-right" style="margin: -70px 0 10px 0;">
                 <div class="share share_size_large share_type_facebook">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" >Share</a>
                </div>
                 <div class="share share_size_large share_type_twitter">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" >Tweet</a>
                </div>
               
                <div class="share share_size_large share_type_gplus">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" >+1</a>
                </div>
                
                <div class="share share_size_large share_type_email">
                    <span class="share__count">1k</span>
                    <a class="share__btn" href="">E-mail</a>
                </div>

                
     </div-->
  <div class="col-md-12">


<!-- Tab panes -->

      <table class="table responsive match-game  boder-1">
      <thead>
        <tr>
          <th colspan="6"><?php echo $daytothai[date("D")]." ".date("d")." ".$monthtothai[date("m")]['full']." ".(date("Y")+543); ?></th>
        </tr>
      </thead>
      <tbody>
      
      <?php 
      
      ksort($AllMatch1['KPLeagueIDOrder']);
      
      //foreach($AllMatchByLeague[$i] as $League=>$AllMatchInLeague){ 
      
      foreach($AllMatch1['KPLeagueIDOrder'] as $Order=>$League){    
          $AllMatchInLeague = $AllMatchByLeague[$i][$League];
      ?>
      
        <tr>
          <td colspan="6" class="table_tr_head"><?php echo $AllMatch1[$i]['KPLeagueNameTHShort'][$League];?></td>
        </tr>
                
        <?php foreach( $AllMatchInLeague as $MatchInfo ){
				
				$MatchDate = $MatchInfo['MatchDate'].' '.$MatchInfo['MatchTime'].':00';
				$this->LiveMatchListTime[$MatchInfo['id']]		=	$MatchDate;
				if($this->football_lib->isLiveMatch($MatchInfo['MatchStatus'])){
					$this->AllLiveMatchID.=','.$MatchInfo['id'];
				}
            ?>
        <tr>
			<td><?php echo $MatchInfo['MatchTime']; ?></td>
			<td class="text_right"><a href="/team-<?php echo strtolower(str_replace(" ","-",$MatchInfo['Team1EN'])); ?>" class="tooltip_top match <?php if(($MatchInfo['TeamOdds']==0 || $MatchInfo['TeamOdds']==1)&&($MatchInfo['Odds']!=-1)){ echo 'handicap_color'; } ?>" target="_blank" title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><?php echo $MatchInfo['Team1']; ?> <img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team1Logo']); ?>"  width="24" onerror="this.src='http://football.kapook.com/uploads/logo/default.png';"></span></a></td>
			<td class="text_center"><?php $tmpMatch = $MatchInfo; include INCLUDE_VIEW_ROOTPATH . 'livescore/match_boxscore.tpl.php'; ?></td>
			<td class="text_left"><a href="/team-<?php echo strtolower(str_replace(" ","-",$MatchInfo['Team2EN'])); ?>" class="tooltip_top match <?php if(($MatchInfo['TeamOdds']==0 || $MatchInfo['TeamOdds']==2)&&($MatchInfo['Odds']!=-1)){ echo 'handicap_color'; } ?>" target="_blank" title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team2Logo']); ?>"  width="24" > <?php echo $MatchInfo['Team2']; ?></span></a></td>
			<td class="handicap_sign"><span class="tooltip_top" title="อัตราต่อรอง">
          
               <?php 
               if($MatchInfo['Odds']==0){   
                   echo '(เสมอ)';
               }else if($MatchInfo['Odds']!=-1){
                   $OddsArr = explode('.',$MatchInfo['Odds']);
                   if($OddsArr[0]>0){
                       for($o=1;$o<=$OddsArr[0];$o++){
                           echo '<pie class="hundreds"></pie>';
                       }
                   }
                   
                   if($OddsArr[1]=='25'){
                       echo '<pie class="twentyfive"></pie>';
                   }else if($OddsArr[1]=='5'){
                       echo '<pie class="fifty"></pie>';
                   }else if($OddsArr[1]=='75'){
                       echo '<pie class="seventyfive"></pie>';
                   }
                   
                   echo ' ('.$MatchInfo['Odds'].')';
               }?>
        </span></td>
          <td>
			<?php
			if($MatchInfo['MatchStatus']=='Sched'){
				if($MatchInfo['Odds']!=-1){
				?>
					<div class="ui-group-buttons pull-right">
						<?php
						/*
						<a href="http://football.kapook.com/games/match/<?php echo $MatchInfo['id'];?>/playscore" <?php if(uid>0){?> data-toggle="modal" data-target="#gameModal" <?php } ?>class="<?php if(uid==0){ echo 'btn_no_login '; }?><?php 
							$dataGame	=	$this->infoGameScore[$MatchInfo['id']];
							if(isset($dataGame)){
								echo 'btn btn-default btn-xs';
							}else{
								echo 'btn btn-primary btn-xs';
							}
							?> tooltip_top" title="กดเพื่อทายผล" id="aPlayScore<?php echo $MatchInfo['id'];?>"><?php
							if(isset($dataGame)){
								echo '<i class="fa fa-check"></i> ' . $dataGame['MyScoreTeam1'] . ' - ' . $dataGame['MyScoreTeam2'];
							}else{
								echo 'ทายผล';
							}
						?></a>

						<div class="or or-xs"></div>
						
						*/
						?>
							
						<a href="#game-single" role="tab" data-toggle="tab"><div class="btn btn-success btn-xs tooltip_top <?php if(uid==0){ echo 'btn_no_login '; }?>" style="width: inherit;"
							<?php if(uid>0){ ?> onclick="AddPlaySideSingle(<?php echo $MatchInfo['id'];?>,'<?php echo $MatchInfo['Team1'];?>','<?php echo $MatchInfo['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team2Logo']);?>','<?php echo $MatchInfo['Odds'];?>',<?php echo $MatchInfo['TeamOdds'];?>,'<?php echo $MatchInfo['MatchPageURL']; ?>')" <?php } ?>
						>ทายบอลเดี่ยว</div></a>
						
						<div class="or or-xs"></div>
						
						<a href="#dropdown1" role="tab" data-toggle="tab"><div class="btn btn-primary btn-xs tooltip_top <?php if(uid==0){ echo 'btn_no_login '; } ?>" style="width: inherit;"
							<?php if(uid>0){ ?> onclick="AddPlaySideMulti(1,<?php echo $MatchInfo['id'];?>,'<?php echo $MatchInfo['Team1'];?>','<?php echo $MatchInfo['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team2Logo']);?>','<?php echo $MatchInfo['Odds'];?>',<?php echo $MatchInfo['TeamOdds'];?>,'<?php echo $MatchInfo['MatchPageURL']; ?>')" <?php } ?>
						>ทายบอลชุด</div></a>
						
						<?php
						/*
						<div class="btn btn-primary btn-xs tooltip_top" data-original-title="" title="" disabled style="width: 64px;padding-left: 13px;">บอลชุด</div>
						<a href="#dropdown1" role="tab" data-toggle="tab"><div class="btn btn-primary btn-xs tooltip_top <?php if(uid==0){ echo 'btn_no_login '; } ?>" <?php if(uid>0){ ?> onclick="AddPlaySideMulti(1,<?php echo $MatchInfo['id'];?>,'<?php echo $MatchInfo['Team1'];?>','<?php echo $MatchInfo['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team2Logo']);?>','<?php echo $MatchInfo['Odds'];?>',<?php echo $MatchInfo['TeamOdds'];?>)" <?php } ?> data-original-title="" title="" style="width: inherit;padding-left: 5px;padding-right: 5px;">1</div></a>
						<a href="#dropdown2" role="tab" data-toggle="tab"><div class="btn btn-primary btn-xs tooltip_top <?php if(uid==0){ echo 'btn_no_login '; } ?>" <?php if(uid>0){ ?> onclick="AddPlaySideMulti(2,<?php echo $MatchInfo['id'];?>,'<?php echo $MatchInfo['Team1'];?>','<?php echo $MatchInfo['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team2Logo']);?>','<?php echo $MatchInfo['Odds'];?>',<?php echo $MatchInfo['TeamOdds'];?>)" <?php } ?> data-original-title="" title="" style="width: inherit;padding-left: 5px;padding-right: 5px;">2</div></a>
						<a href="#dropdown3" role="tab" data-toggle="tab"><div class="btn btn-primary btn-xs tooltip_top <?php if(uid==0){ echo 'btn_no_login '; } ?>" <?php if(uid>0){ ?> onclick="AddPlaySideMulti(3,<?php echo $MatchInfo['id'];?>,'<?php echo $MatchInfo['Team1'];?>','<?php echo $MatchInfo['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team2Logo']);?>','<?php echo $MatchInfo['Odds'];?>',<?php echo $MatchInfo['TeamOdds'];?>)" <?php } ?> data-original-title="" title="" style="width: inherit;padding-left: 5px;padding-right: 5px;">3</div></a>
						<a href="#dropdown4" role="tab" data-toggle="tab"><div class="btn btn-primary btn-xs tooltip_top <?php if(uid==0){ echo 'btn_no_login '; } ?>" <?php if(uid>0){ ?> onclick="AddPlaySideMulti(4,<?php echo $MatchInfo['id'];?>,'<?php echo $MatchInfo['Team1'];?>','<?php echo $MatchInfo['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team2Logo']);?>','<?php echo $MatchInfo['Odds'];?>',<?php echo $MatchInfo['TeamOdds'];?>)" <?php } ?> data-original-title="" title="" style="width: inherit;padding-left: 5px;padding-right: 5px;">4</div></a>
						<a href="#dropdown5" role="tab" data-toggle="tab"><div class="btn btn-primary btn-xs tooltip_top <?php if(uid==0){ echo 'btn_no_login '; } ?>" <?php if(uid>0){ ?> onclick="AddPlaySideMulti(5,<?php echo $MatchInfo['id'];?>,'<?php echo $MatchInfo['Team1'];?>','<?php echo $MatchInfo['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team2Logo']);?>','<?php echo $MatchInfo['Odds'];?>',<?php echo $MatchInfo['TeamOdds'];?>)" <?php } ?> data-original-title="" title="" style="width: inherit;padding-left: 5px;padding-right: 5px;">5</div></a>
						*/
						?>
					</div>

				<?php }
				else{
					echo '<span class="pull-right btn-xs btn-grey">กำลังรอข้อมูล</span>';
				}
			}
			else
			{?>
				<div class="pull-right <?php echo $ClassColorByMatchStatus[$MatchInfo['MatchStatus']];?> btn-xs "><?php
				if($MatchInfo['MatchStatus']=='Fin'){
					echo 'จบการแข่งขัน';
				}
				else if($MatchInfo['MatchStatus']=='Canc'){
					echo 'ยกเลิกการแข่งขัน';
				}
				else if($MatchInfo['MatchStatus']=='Post'){
					echo 'เลื่อนการแข่งขัน';
				}
				else if(	
					($MatchInfo['MatchStatus']=='1 HF')	||
					($MatchInfo['MatchStatus']=='2 HF')	||
					($MatchInfo['MatchStatus']=='H/T')	||
					($MatchInfo['MatchStatus']=='Pen')	||
					($MatchInfo['MatchStatus']=='E/T')
				){
					echo 'กำลังทำการแข่งขัน';
				}
				else{ echo 'N/A'; }
				?></div><?php
			}
			?>
          </td>
        </tr>
        <?php }?>
        <?php
        }
        ?>
        
      </tbody>
    </table>
   
   

<script src="http://football.kapook.com/assets/plugins/bootstrap-tour/bootstrap-tour.js"></script>
<link href="http://football.kapook.com/assets/plugins/bootstrap-tour/bootstrap-tour.css" rel="stylesheet">

<style type="">.modal.in {
   display:block;
}</style>

<script type="text/javascript">
    // Instance the tour
    // Instance the tour
var tour = new Tour({
   
   /* onShown: function(tour) {
        var stepElement = getTourElement(tour);
        $(stepElement).after($('.tour-step-background'));
        $(stepElement).after($('.tour-backdrop'));
    }, */
   template: '<div class="popover tour" > <div class="arrow"></div> <h3 class="popover-title">Title of my step</h3> <div class="popover-content"></div><div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-default" data-role="prev">« ก่อนหน้า</button> <button class="btn btn-sm btn-default" data-role="next">ถัดไป »</button>  </div> <button class="btn btn-sm btn-default end-game-tour" style="display:none" data-role="end">จบ</button> </div> </div>', 
    
  steps: [
  {
  element: ".handicap_color:first",
    title: "ทีมต่อ",
    content: 'ทีมต่อจะมีสัญลักษณ์ <i class="fa fa-circle" style="color:#428bca; font-size:0.6em"></i> จุดสีฟ้าอยูที่ชื่อของทีม '
    , placement: "top"                                                                                                                                           
    ,backdrop: true
      
  },
  {
  element: ".handicap_sign:first",
    title: "อัตราต่อรอง",
    content:  'ข้อมูลที่บอกระดับความต่างของทั้งสองทีม และจะนำมาใช้ในการตัดสินเกมทายผลบอล เช่น <pie class="fifty" style="float:none; display:inline-block"></pie> (0.5) คือทีมต่อ ต่อครึ่งลูก '                 
    , placement: "top"                                                                                                                                           
    ,backdrop: true
      
  },{    
    element: "table .ui-group-buttons .btn-success:first",
    title: "ทายบอลเดี่ยว",
    content: 'คลิกเพื่อเลือกทายคู่นี้เป็นบอลเดี่ยว สามารถทายบอลเดี่ยวได้มากสุดวันละ 3 คู่'
    , placement: "top"                                                                                                                                           
    ,backdrop: true    
  },
  {    
    element: "table .ui-group-buttons .btn-primary:first",
    title: "ทายบอลชุด",
    content: 'คลิกเพื่อเลือกคู่นี้ในการทายบอลชุด 1 ชุดต้องเลือกอย่างน้อย 2 คู่ และไม่เกิน 6 คู่ สามารถทายได้วันละ 1 ชุด'
    , placement: "top"                                                                                                                                           
    ,backdrop: true    
  },
  
  {                                              
    element: ".profile-link",
    title: "สรุปผลการทายบอลเมื่อวาน",
    content: 'คลิกเพื่อดูข้อมูลและสรุปแต้มในการทายทั้งหมด'
     ,backdrop: true
    , placement: "top"
  },
  
     {                                            
    element: ".ranking-link",
    title: "การแข่งขันและจัดอันดับ",
    content: 'เก็บแต้มสะสมหาผู้ชนะรายเดือน และสรุปผลหาแชมป์ประจำสัปดาห์ คลิกเพื่อดูอันดับแต้มทั้งหมด'
    ,backdrop: true
    ,placement: "top"
	,onHidden: function() {
		<?php if(uid>0){ ?>
			saveTour();
		<?php }else{ ?>
			var d = new Date();
			d.setTime(d.getTime() + (60*60*1000));
			var expires = "expires="+d.toUTCString();
			document.cookie = "isFinishTour=1; " + expires;
		<?php } ?>
		
	}
	,onShown: function (tour) {$('.end-game-tour').css('display','block'); }
  }
  
]});

/*
// Initialize the tour
tour.init();

// Start the tour
tour.start();   */
//tour.restart();

</script>



</div>
</div>

<?php if($_REQUEST['tbug']!=2){ ?>
<div class="text_center"><a href="<?php echo BASE_HREF; ?>profile#d1" class="btn btn-primary profile-link">+ สรุปผลการทายเมื่อวานคลิก !</a></div>
<?php } ?>



</div>


<!-- Modal -->
<div class="modal fade style-1" id="gameModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">

</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal -->
<div class="modal fade style-2" id="gameModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">

</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script language="JavaScript">

	<?php if(!isset($_COOKIE['isFinishTour'])) { ?>
		document.cookie = "isFinishTour=0;";
	<?php } ?>

	$(document).ready(function(){
		<?php if(uid>0){
			if($_COOKIE['isFinishTour']==0) { ?>
				isTour();
			<?php }else{ ?>
				saveTour();
			<?php } ?>
		<?php }else if($_COOKIE['isFinishTour']==0) { ?>
			useful_modal('<?php echo BASE_HREF; ?>games/help',false);
		<?php } ?>
	});

	function isTour(){
        var Url='http://football.kapook.com/api/gameapi_istour.php';
        $.ajax({
			url: Url,
			type: "POST",
			dataType: "json",
			success: function(data) {
				if(data.code_id==0){
					useful_modal('<?php echo BASE_HREF; ?>games/help',false);	
				}
			}
        });
	}
	
	function saveTour(){
        var Url='http://football.kapook.com/api/gameapi_add_reward.php?type=3';
        $.ajax({
			url: Url,
			type: "POST",
			dataType: "json",
			success: function(data) {
				if(data.code_id==200){
					var d = new Date();
					d.setTime(d.getTime() + (60*60*1000));
					var expires = "expires="+d.toUTCString();
					document.cookie = "isFinishTour=1; " + expires;
				}
			}
        });
	}
        
    function fnPlayScore(mid,t1score,t2score){
        var Url='http://football.kapook.com/games/playscore/insert/'+mid+'/'+t1score+'/'+t2score;
        $.ajax({
                url: Url,
                type: "POST",
                dataType: "json",
                success: function(data) {
                     //aler(data.code_id);
                }
        });
        document.getElementById('aPlayScore'+mid).className 	= 	'btn btn-default btn-xs';
        document.getElementById('aPlayScore'+mid).innerHTML		=	'<i class="fa fa-check"></i> '+t1score+' - '+t2score;
    }
	
	function fnPlaySide(mid,select_team,select_point){
        var Url='http://football.kapook.com/games/playside/insert/'+mid+'/'+select_team+'/'+select_point;
		var TeamName;
        $.ajax({
			url: Url,
			type: "POST",
			dataType: "json",
			success: function(data) {
				if(data.code_id==200){
					document.getElementById('aPlaySide'+mid).className 	= 	'btn btn-default btn-xs';
					document.getElementById('aPlaySide'+mid).innerHTML	=	'<i class="fa fa-check"></i> '+data.data[0].team_win_nameTHShort;
				}else{
					alert(data.message);
				}
			}
        });
	}
	
</script>



 
