<?php if (!defined('MINIZONE')) exit;

class News_model
{
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
		$this->minizone = minizone::getzone();
        $this->mem_lib = $this->minizone->library('memcache_lib'); 
    }
    
    ///////////////////////////////////////////////// Get : Catagory   /////////////////////////////////////////////////
     function getNews($isClear = false){
        $url = "http://footballapi.kapook.com/news/list/1?size=8";
		if($isClear){
			 $url =  $url . "&clear=1" ;
		}
		$result = json_decode(file_get_contents($url),true);  

        return $result;
    }
    function getNewsTop10List($page,$isClear = false){

		$url = "http://footballapi.kapook.com/news/top10/$page?size=8";
		if($isClear){
			 $url =  $url . "&clear=1" ;
		}
		$result = json_decode(file_get_contents($url),true);  

        return $result;
    }
	
	function getNewsTop10($isClear = false){
        $url = "http://footballapi.kapook.com/news/top10/1?size=3";
		if($isClear){
			 $url =  $url . "&clear=1" ;
		}
		$result = json_decode(file_get_contents($url),true);  

        return $result;
    }
    
    function getNewsTopHits($isClear = false){

		$url = "http://footballapi.kapook.com/news/tophit/1?size=5";
		if($isClear){
			 $url =  $url . "&clear=1" ;
		}
		$result = json_decode(file_get_contents($url),true);  

        return $result;

    }
	
    function getNewsList($page,$isClear = false){
        
        $url = "http://footballapi.kapook.com/news/list/$page?size=8";
		if($isClear){
			 $url =  $url . "&clear=1" ;
		}
		$result = json_decode(file_get_contents($url),true);  

        return $result;

    }	
    
    function getNewsSingle($news_ID,$isClear = false) {
        
		$url = "http://footballapi.kapook.com/news/id/$news_ID";
		if($isClear){
			 $url =  $url . "?clear=1" ;
		}
		$result = json_decode(file_get_contents($url),true);  

        return $result;

    }
    
    function getNewsTransfer($page,$isClear = false) {
        
		$url = "http://footballapi.kapook.com/news/country/13/$page";
		if($isClear){
			 $url =  $url . "?clear=1" ;
		}
		$result = json_decode(file_get_contents($url),true);  

        return $result;

    }
    
	function getNewsByTeam($teamID,$page,$isClear = false) {
		//$teamList	=	str_replace(',' , '_', $teamID);
		$url = "http://footballapi.kapook.com/news/team/$teamID/$page";
		if($isClear){
			 $url =  $url . "?clear=1" ;
		}
		$result = json_decode(file_get_contents($url),true);  

        return $result;
    }
	
    function getNewsLeague($country_id,$page,$isClear = false) {
        $url = "http://footballapi.kapook.com/news/country/$country_id/$page";
		if($isClear){
			 $url =  $url . "?clear=1" ;
		}
		$result = json_decode(file_get_contents($url),true);  

        return $result;
    }
	
    function getNewsCup($cup_name,$page,$isClear = false) {

		$url = "http://footballapi.kapook.com/news/tag/$cup_name/$page";
		if($isClear){
			 $url =  $url . "?clear=1" ;
		}
		$result = json_decode(file_get_contents($url),true);  

        return $result;
    }

    function updateNewsView($news_id,$crView) {
        $url = "http://football.kapook.com/cms/upview.php?news_id=".$news_id."&crview=".$crView;
        $result = json_decode(file_get_contents($url),true);
        
        return $result;
    }
	
	// OLD
	/*function updateNewsViewer($news_ID,$user_id = 0) {
		$hour = date('H');
		$url = "http://footballapi.kapook.com/news/update_viewer/$news_ID/$hour";
		if($user_id > 0){
			 $url =  $url . "/" . $user_id ;
		}
		$result = json_decode(file_get_contents($url),true);  
        return $result;
    }*/
	
	function updateNewsViewer($data_news,$user_id = 0) {
		$url = "http://27.254.156.17:9222/viewer/" . date("Y-m-d") . "/";
		
		$query = array();
		$query["timestamp"] 	= date("Y-m-d") . "T" . date("H:i:s");
		$query["country_list"] 	= $data_news["attr"]["extra_kapookfootball_News_Country"];
		$query["team_list"] 	= $data_news["attr"]["kapookfootball_News_CountryOtherKeyword3"];
		$query["news_id"] 		= $data_news["news_id"];
		
		if($user_id > 0){
			$query["user_id"] = intval($user_id);
		}else{
			$query["user_id"] = 0;
		}
		
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $url); 
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt ($ch, $request_type, 1); 
		curl_setopt ($ch, CURLOPT_POSTFIELDS,json_encode($query));
		curl_setopt ($ch, CURLOPT_TIMEOUT, 120);
		$result = 	curl_exec($ch);
		curl_close($ch);
		$result	=	json_decode($result,true);
		return $result;

    }
   
}
/* End of file home_model.php */
/* Location: ./system/model/home_model.php */