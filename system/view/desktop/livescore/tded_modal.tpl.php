<?php
$MatchTmp = $this->MatchTmp;
	$monthtothai['01']['short'] ="ม.ค.";
	$monthtothai['02']['short'] ="ก.พ.";
	$monthtothai['03']['short'] ="มี.ค.";
	$monthtothai['04']['short'] ="เม.ย.";
	$monthtothai['05']['short'] ="พ.ค.";
	$monthtothai['06']['short'] ="มิ.ย.";
	$monthtothai['07']['short'] ="ก.ค.";
	$monthtothai['08']['short'] ="ส.ค.";
	$monthtothai['09']['short'] ="ก.ย.";
	$monthtothai['10']['short'] ="ต.ค.";
	$monthtothai['11']['short'] ="พ.ย.";
	$monthtothai['12']['short'] ="ธ.ค.";
	
	$monthtothai['01']['full'] ="มกราคม";
	$monthtothai['02']['full'] ="กุมภาพันธ์";
	$monthtothai['03']['full'] ="มีนาคม";
	$monthtothai['04']['full'] ="เมษายน";
	$monthtothai['05']['full'] ="พฤษภาคม";
	$monthtothai['06']['full'] ="มิถุนายน";
	$monthtothai['07']['full'] ="กรกฎาคม";
	$monthtothai['08']['full'] ="สิงหาคม";
	$monthtothai['09']['full'] ="กันยายน";
	$monthtothai['10']['full'] ="ตุลาคม";
	$monthtothai['11']['full'] ="พฤศจิกายน";
	$monthtothai['12']['full'] ="ธันวาคม";
	
	$daytothai['Sun'] = "วันอาทิตย์";
	$daytothai['Mon'] = "วันจันทร์";
	$daytothai['Tue'] = "วันอังคาร";
	$daytothai['Wed'] = "วันพุธ";
	$daytothai['Thu'] = "วันพฤหัสบดี";
	$daytothai['Fri'] = "วันศุกร์";
	$daytothai['Sat'] = "วันเสาร์";
	
	$OddsRate["0"] 		= "เสมอ";
	$OddsRate["0.25"] 	= "เสมอควบครึ่ง";
	$OddsRate["0.5"] 	= "ครึ่งลูก";
	$OddsRate["0.75"] 	= "ครึ่งควบลูก";
	$OddsRate["1"] 	= "หนึ่งลูก";
	$OddsRate["1.25"] 	= "ลูกควบลูกครึ่ง";
	$OddsRate["1.5"] 	= "ลูกครึ่ง";
	$OddsRate["1.75"] 	= "ลูกครึ่งควบสองลูก";
	$OddsRate["2"] 	= "สองลูก";
	$OddsRate["2.25"] 	= "สองลูกควบสองลูกครึ่ง";
	$OddsRate["2.5"] 	= "สองลูกครึ่ง";
	$OddsRate["2.75"] 	= "สองลูกครึ่งควบสามลูก";
	$OddsRate["3"] 	= "สามลูก";
	$OddsRate["3.25"] 	= "สามลูกควบสามลูกครึ่ง";
	$OddsRate["3.5"] 	= "สามลูกครึ่ง";
	$OddsRate["3.75"] 	= "สามลูกครึ่งควบสี่ลูก";
	$OddsRate["4"] 	= "สี่ลูก";
	$OddsRate["4.25"] 	= "สี่ลูกควบสี่ลูกครึ่ง";
	$OddsRate["4.5"] 	= "สี่ลูกครึ่ง";
	$OddsRate["4.75"] 	= "สี่ลูกครึ่งควบห้าลูก";
	$OddsRate["5"] 	= "ห้าลูก";
	$OddsRate["5.25"] 	= "ห้าลูกควบห้าลูกครึ่ง";
	$OddsRate["5.5"] 	= "ห้าลูกครึ่ง";
	$OddsRate["5.75"] 	= "ห้าลูกครึ่งควบหกลูก";
	$OddsRate["6"] 	= "หกลูก";
	$OddsRate["6.25"] 	= "หกลูกควบหกลูกครึ่ง";
	$OddsRate["6.5"] 	= "หกลูกครึ่ง";
	$OddsRate["6.75"] 	= "หกลูกครึ่งควบเจ็ดลูก";
	$OddsRate["7"] 	= "เจ็ดลูก";
	$OddsRate["7.25"] 	= "เจ็ดลูกควบเจ็ดลูกครึ่ง";
	$OddsRate["7.5"] 	= "เจ็ดลูกครึ่ง";
	$OddsRate["7.75"] 	= "เจ็ดลูกครึ่งควบแปดลูก";
	$OddsRate["8"] 	= "แปดลูก";
	$OddsRate["8.25"] 	= "แปดลูกควบแปดลูกครึ่ง";
	$OddsRate["8.5"] 	= "แปดลูกครึ่ง";
	$OddsRate["8.75"] 	= "แปดลูกครึ่งควบเก้าลูก";
	$OddsRate["9"] 	= "เก้าลูก";
	$OddsRate["9.25"] 	= "เก้าลูกควบเก้าลูกครึ่ง";
	$OddsRate["9.5"] 	= "เก้าลูกครึ่ง";
	$OddsRate["9.75"] 	= "เก้าลูกครึ่งควบสิบลูก";
	$OddsRate["10"] 	= "สิบลูก";
	
	$ClassColorByMatchStatus['Fin'] = 'btn-grey';
    $ClassColorByMatchStatus['1 HF'] = 'btn-success';
    $ClassColorByMatchStatus['2 HF'] = 'btn-success';
    $ClassColorByMatchStatus['Sched'] = '';
    $ClassColorByMatchStatus['H/T'] = 'btn-warning';
	
	
	if(isset($MatchTmp['count_playside'])){
		$totalplayside		=	$MatchTmp['count_playside'];
		if(isset($MatchTmp['count_playside_team1'])){
			$playside1		=	$MatchTmp['count_playside_team1'];
		}else{
			$playside1		=	0;
		}
	}else{
		$totalplayside		=	0;
		$playside1			=	0;
	}


?>
<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
  <title>ทีเด็ด   Manchester vs United</title>  
</head>
<body>


<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h4 class="modal-title"><i class="fa fa-star"></i> ทีเด็ด   </h4>
</div> 


<!-- /modal-header -->
 <div class="modal-body clearfix " >
 
 
 
 
 
 
 
 
 
 
                  <div class="row">
            <div class="col-md-12">
                     <div class="bs-example">
                     
							<h3>
							<a href="/team-<?php echo strtolower(str_replace(" ","-",$MatchTmp['Team1'])); ?>" class="tooltip_top " title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank">
								<?php echo $this->mem_lib->get('Football2014-Team-NameTH-'.$MatchTmp['Team1KPID']);?>
							</a>
							<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameTH-'.$MatchTmp['Team1KPID']))); ?>" class="tooltip_top " title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank">
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team1Logo']);?>" class="flag" width="64">
							</a>
							<a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
								if($MatchTmp['MatchStatus']=='Fin'){
									echo $MatchTmp['Team1FTScore']; ?> - <?php echo $MatchTmp['Team2FTScore'];
								}else{
									echo "vs";}
								?></a>
							<a href="/team-<?php echo strtolower(str_replace(" ","-",$MatchTmp['Team2'])); ?>" class="tooltip_top " title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank">
								<img onerror="this.src='http://football.kapook.com/uploads/logo/default.png'; " src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $MatchTmp['Team2Logo']);?>" class="flag" width="64">
							</a>
							<a href="/team-<?php echo strtolower(str_replace(" ","-",$MatchTmp['Team2'])); ?>" class="tooltip_top " title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank">
								<?php echo $this->mem_lib->get('Football2014-Team-NameTH-'.$MatchTmp['Team2KPID']);?>
							</a>
       
       
      
                            
                            </h3>
                            
                            <br>
                            
                           <!--<div><span>Chelsea</span> <span class="pull-right">Manchester United</span></div>--> 
                           <div class="row">
                                     <div class="col-md-3">
                                     
								<?php
									$MatchTmp['MatchDate'] = substr($MatchTmp['MatchDateTime'],0,10);
									$MatchTmp['MatchTime'] = substr($MatchTmp['MatchDateTime'],11,5);
									
									$date = explode('-',$MatchTmp['MatchDate']);
									$thaidate = $date[2]." ".$monthtothai[$date[1]]['short']." ".($date[0]+543);
								?>
								<span class="date"><i class="fa fa-calendar"></i> <?php echo $thaidate; ?>, <?php echo $MatchTmp['MatchTime']; ?> น. </span>
								<div class="channel">ช่องถ่ายทอด : <?php
									foreach($MatchTmp['TVLiveList'] as $ChannelKey){?>
										<img src="http://football.kapook.com/uploads/tvlogo/<?php echo $ChannelKey; ?>.png" title="<?php echo $this->tv_lib->getTVText($ChannelKey); ?>">
								<?php } ?>
								</div>
                                
                                     <b>
								<?php
									if($MatchTmp['Odds'] >0){
										echo 'ราคาบอล : ' . $this->mem_lib->get('Football2014-Team-NameTH-'.$MatchTmp['Team'.$MatchTmp['TeamOdds'].'KPID']) . ' ต่อ ' . $OddsRate[(string)$MatchTmp['Odds']]; 
									}else if($MatchTmp['Odds'] ==0){
										echo 'ราคาบอล : ' . $OddsRate[(string)$MatchTmp['Odds']];
									}
								?>
                                     
                                     </b>
                                     </div>
                            
                                                    <div class="col-md-9">
                                                    
                                                                                              <div class="row">
                                                                                                    <div class="col-md-4 text_left"><?php echo $this->mem_lib->get('Football2014-Team-NameTH-'.$MatchTmp['Team1KPID']);?></div>
                                                                                                    <div class="col-md-4 text_center"><b>ประมวลผลสมาชิกทายผลบอล</b></div>
                                                                                                    <div class="col-md-4 text_right"><?php echo $this->mem_lib->get('Football2014-Team-NameTH-'.$MatchTmp['Team2KPID']);?></div>
                                                                                               </div>
                                                                                            
                                                                                                                                                        
                                                                                                                                                        
                                                                                            <?php
                                                                                            if($totalplayside>0){
                                                                                                $tmpPercent		=	round(($playside1/$totalplayside)*100);
                                                                                            ?>
                                                                                                <div class="progress">
                                                                                                    <div class="progress-bar" style="width: <?php echo $tmpPercent; ?>%"><?php echo $tmpPercent; ?>%</div>
                                                                                                    <div class="progress-bar progress-bar-danger" style="width: <?php echo (100-$tmpPercent); ?>%"><?php echo (100-$tmpPercent); ?>%</div>
                                                                                                </div>
                                                                                            <?php
                                                                                            }else{
                                                                                            ?>
                                                                                                <div class="progress">
                                                                                                    <div class="progress-bar progress-bar-warning" style="width: 100%"><?php echo $tmpPercent; ?>กำลังรอข้อมูล</div>
                                                                                                </div>
                                                                                            <?php
                                                                                            }
                                                                                            ?>
                                                                                            
                                                                                           
                                                    </div>
                           
                               </div>
                        

                        
                      </div>
                      <div class="highlight">
                           
                           <div class="row">
                              <div class="col-md-12"><b>บทวิเคราะห์จากทีมงาน</b></div>
                              <div class="col-md-12">								
							     <?php if($MatchTmp['Analysis']) {?>
									<br>
									ทัศนะ : <?php echo $MatchTmp['Analysis']; ?>
									<br>
								<?php } ?>

<div style="color:#428bca; font-weight: bold;"><br>ฟันธง : <?php echo $MatchTmp['Predict']; ?></div>  </div>
                           </div>

                    </div>
            </div>
        </div>
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
            

            
            
            
 </div>
            
     

            
            
            </div>			<!-- /modal-body -->
            <!--<div class="modal-footer">
  
                <button type="button" class="btn btn-success pull-left" data-dismiss="modal" onclick="tour.restart();"><i class="glyphicon glyphicon-play"></i> เริ่มต้นคำแนะนำ สู่ความเป็นเทพ</button> 
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>

            </div>	-->		<!-- /modal-footer -->

</body></html>