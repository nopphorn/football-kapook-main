<div class="container ranking">


 <h1  class="page-header font-display">อันดับคะแนน</h1>
   <div class="row">  
   
   
<div class="social pull-right" style="margin: -70px 0 10px 0;">
                 <div class="share share_size_large share_type_facebook">
                    <span class="share__count">1</span>
                    <a class="share__btn" href="" onclick="fb_share();">Share</a>
                </div>
                 <div class="share share_size_large share_type_twitter">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" onclick="twitter_share();">Tweet</a>
                </div>            
                <div class="share share_size_large share_type_email">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="">E-mail</a>
                </div>                
     </div>


</div>
  
<div class="row">
   <!-- Start LEFT COLUME --> 
  <div class="col-md-12">
  
  
  <!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
	<!-- Daily -->
	<li class="active"><a href="#tab1" role="tab" data-toggle="tab" value="daily">เมื่อคืน</a></li>
	<!-- Month -->
	<li ><a href="#tab1" id="tab_08" role="tab" data-toggle="tab" value="08">ส.ค.15</a></li>
	<li ><a href="#tab1" id="tab_09" role="tab" data-toggle="tab" value="09">ก.ย.15</a></li>
	<li ><a href="#tab1" id="tab_10" role="tab" data-toggle="tab" value="10">ต.ค.15</a></li>
	<li ><a href="#tab1" id="tab_11" role="tab" data-toggle="tab" value="11">พ.ย.15</a></li>
	<li ><a href="#tab1" id="tab_12" role="tab" data-toggle="tab" value="12">ธ.ค.15</a></li>
	<li ><a href="#tab1" id="tab_01" role="tab" data-toggle="tab" value="01">ม.ค.16</a></li>
	<li ><a href="#tab1" id="tab_02" role="tab" data-toggle="tab" value="02">ก.พ.16</a></li>
	<li ><a href="#tab1" id="tab_03" role="tab" data-toggle="tab" value="03">มี.ค.16</a></li>
	<li ><a href="#tab1" id="tab_04" role="tab" data-toggle="tab" value="04">เม.ย.16</a></li>
	<li ><a href="#tab1" id="tab_05" role="tab" data-toggle="tab" value="05">พ.ค.16</a></li>
	<li ><a href="#tab1" id="tab_06" role="tab" data-toggle="tab" value="06">มิ.ย.16</a></li>
	<li ><a href="#tab1" id="tab_07" role="tab" data-toggle="tab" value="07">ก.ค.16</a></li>
	<!-- Year -->
	<li ><a href="#tab1" id="tab_year" role="tab" data-toggle="tab" value="yearly">2015-2016</i></a></li>
	
	<li style="float: right;">
		<select class="form-control" id="year_select">
			<option value="2015">ฤดูกาล 2015-16</option>
			<option value="2014">ฤดูกาล 2014-15</option>
		</select>
	</li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="tab1">
    <table  class="table  table-striped border-1 " id="rankTable">
      <thead>
        <tr>
            <th width="10%">อันดับ</th>
             <th  class="text_center"  width="8%">ผู้เล่น</th>
			 <th  class="text_center"  width="10%"></th>
             <th  class="text_center"  width="16%">ระดับ</th>
			 <th  class="text_center">อัตราความแม่น</th>
             <th  class="text_center">%เกมส์บอลเดี่ยว</th>
             <th  class="text_center">%เกมส์บอลชุด</th>
			 <th  class="text_center">คะแนน</th>
        </tr>
      </thead>
	  <tbody>
	  </tbody>
    </table>
  </div>
  </div> 
</div>
  </div>
</div>

<script src="<?php echo BASE_HREF; ?>assets/plugins/dt/js/jquery.dataTables.min.js"></script> 
<script src="<?php echo BASE_HREF; ?>assets/plugins/dt_bootstrap/js/datatables.js"></script>
<script src="<?php echo BASE_HREF; ?>assets/plugins/datatable/dataTables.tableTools.min.js"></script>  
   
<link href="<?php echo BASE_HREF; ?>assets/plugins/dt_bootstrap/css/datatables.css" rel="stylesheet" />
<script type=""> 
$(document).ready(function(){
	var target = "daily";
	var year = 2015;
	var monthShortTH = ["", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."];
    var table = $('#rankTable').DataTable( {
		"bDestroy": true,
		"processing": true,
        "serverSide": true,
        "language": {
            "url": "<?php echo BASE_HREF; ?>assets/plugins/dt/th_lang.txt"
        },
        "ajax": {
            "url": "http://football.kapook.com/api/gameapi_ranking.php",
            "type": "GET",
            "data": function ( d ) {
			
				if( target == "daily" ){
					d.date = "<?php
					if(date('G')>=6)
					{
						echo date('Y-m-d',strtotime('now -1 day'));
					}
					else
					{
						echo date('Y-m-d',strtotime('now -2 day'));
					}
					?>";
				}else if(target == "yearly"){
					d.year = year;
				}else{
					if(target>=8){
						d.month = year + "-" + target;
					}else{
						d.month = (parseInt(year)+1) + "-" + target;
					}
				}
                d.comma_style = 1;
            }
        },
        "columns": [
            { "data": "ranking" },
            { "data": "photo" },
            { "data": "name" },
            { "data": "position" },
            { "data": "gameA" },
            { "data": "gameB" },
            { "data": "gameC" },
            { "data": "gameD" }
        ] ,
        "dom": '<"top"ip>rt<"bottom"p><"clear">',
        "iDisplayLength": 10,
        "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 1 , 2, 3 ] } ] 
    } );
	
	$('#year_select').change(function() {
		year = parseInt($('#year_select').val());
		for(var i=1 ; i<=12 ; i++){
			if(i<8){
				var tmpyear =  parseInt(year)+1;
				console.log(tmpyear);
				$('#tab_0' + i).text(monthShortTH[i] + (tmpyear%100));
			}else if(i==8){
				$('#tab_0' + i).text(monthShortTH[i] + (year%100));
			}else if(i==9){
				$('#tab_0' + i).text(monthShortTH[i] + (year%100));
			}else{
				$('#tab_' + i).text(monthShortTH[i] + (year%100));
			}
		}
		$("#tab_year").text(year + "-" + (parseInt(year)+1) );
		table.ajax.reload();
	});

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		//e.target // activated tab
		target = $(e.target).attr("value");
		table.ajax.reload();
		//e.relatedTarget // previous tab
  
	})
});
</script>