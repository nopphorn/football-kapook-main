<table  class="table gray table-striped table-bordered ">
	<thead><tr>
		<th>#</th>
		<th>ชื่อทีม</th>
		<th class="tooltip_top"  title="จำนวนนัดที่แข่ง">P</th>
		<th class="tooltip_top"  title="จำนวนคะแนน">PTS</th>
	</tr></thead>
	<tbody>
		<?php
		$j=0;
		foreach( $tmpTable['TeamData'] as $tmpTeam ){
			$tmpTeam['total_match']		=	($tmpTeam['win']+$tmpTeam['draw']+$tmpTeam['lose']); ?>
			<tr>
				<td><?php echo $j+1 ?></td>
				<td><a href="<?php echo $tmpTeam['url']; ?>" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpTeam['logo']); ?>" width="24" height="24"  alt=""/> <?php echo $tmpTeam['name']; ?></a></td>
				<td><?php echo $tmpTeam['total_match']; ?></td>
				<td><?php echo $tmpTeam['point']; ?></td>
			</tr>
			<?php $j++;
		}
		?>
	</tbody>
</table>