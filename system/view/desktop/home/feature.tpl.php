<?php 
	$monthtothai['Jan'] ="มกราคม";
	$monthtothai['Feb'] ="กุมภาพันธ์";
	$monthtothai['Mar'] ="มีนาคม";
	$monthtothai['Apr'] ="เมษายน";
	$monthtothai['May'] ="พฤษภาคม";
	$monthtothai['Jun'] ="มิถุนายน";
	$monthtothai['Jul'] ="กรกฎาคม";
	$monthtothai['Aug'] ="สิงหาคม";
	$monthtothai['Sep'] ="กันยายน";
	$monthtothai['Oct'] ="ตุลาคม";
	$monthtothai['Nov'] ="พฤศจิกายน";
	$monthtothai['Dec'] ="ธันวาคม";
//	echo "<pre>";	var_dump($monthtothai);

	
	$daytothai['Sun'] = "วันอาทิตย์";
	$daytothai['Mon'] = "วันจันทร์";
	$daytothai['Tue'] = "วันอังคาร";
	$daytothai['Wed'] = "วันพุธ";
	$daytothai['Thu'] = "วันพฤหัสบดี";
	$daytothai['Fri'] = "วันศุกร์";
	$daytothai['Sat'] = "วันเสาร์";
	
$NewsArr = $this->NewsListContents; 
$NewsPage = $this->NewsPage;
$NewsPageTotal = $this->NewsPageTotal;
$NewsTopHitsArr = $this->NewsTopHitsContents;


?>
<div class="container news">
  
      <h1  class="page-header font-display">บทความฟุตบอล</h1>
<div class="row">
   <!-- Start LEFT COLUME --> 
  <div class="col-md-8">
  
<!--  <div class="row">
    <div class="col-md-8">-->
    
           <div class="list_harizontal">
           <?php 
		   foreach($NewsArr as $tmpNews){?>
          <div class="media">
              <a class="" href="<?php echo $tmpNews['link']; ?>" target="_blank">
                <img class="media-object pull-left" alt="64x64" src="<?php echo $tmpNews['picture']; ?>" >
                <h3><?php echo $tmpNews['subject']; ?></h3>
                <span class="date"><i class="fa fa-calendar"></i> <?php echo $daytothai[date("D",$tmpNews['CreateDate'])]." ".date("d", $tmpNews['CreateDate'])." ".$monthtothai[date("M", $tmpNews['CreateDate'])]." ".(date("Y", $tmpNews['CreateDate'])+543); ?>   | <i class="fa fa-eye"> <?php echo $tmpNews['Views']; ?></i> </span>
               <p><?php echo $tmpNews['title']; ?></p>
            
              </a>
            </div>
			<?php } 
			
if($NewsPageTotal > 1) {?>
         
<ul class="pagination">
<?php
if($NewsPage<=3){
	$start = 1;
	if($NewsPageTotal<5){
		$stop = $NewsPageTotal;
	}else{
		$stop = 5;
	}
}elseif($NewsPage>=($NewsPageTotal-2)){
	if(($NewsPageTotal-4)<1){
		$start = 1;
	}else{
		$start = $NewsPageTotal-4;
	}
	$stop = $NewsPageTotal;
}else{
	$start = $NewsPage-2;
	$stop = $NewsPage+2;
}

if($NewsPage>1){
	$linkback = "/feature?page=".($NewsPage-1);
	$back = "";
}else{
	$linkback = "";
	$back = "disabled";
}

if($NewsPage<$NewsPageTotal){
	$linknext = "/feature?page=".($NewsPage+1);
	$next = "";
}else{
	$linknext = "";
	$next = "disabled";
}

?>
  <li class="<?php echo $back; ?>"><a href="<?php echo $linkback; ?>">&laquo;</a></li>
 <?php
 for($i=$start;$i<=$stop;$i++){
	if($i==$NewsPage)
		$class = "active";
	else
		$class = "";
 ?>
  <li class="<?php echo $class; ?>"><a href="/feature?page=<?php echo $i; ?>"><?php echo $i; ?> <span class="sr-only">(current)</span></a></li>
<?php } ?>
  <li class="<?php echo $next; ?>"><a href="<?php echo $linknext; ?>">»</a></li>
</ul>
<?php } ?>

  </div>
  

  
  

  
</div>
<!--End Col 8 LEFT VOLUME-->









<!--Start Right Side Colume-->


<!--ข่าวฮิตประจำสัปดาห์ -->
  <div class="col-md-4 aside">
  <?php include dirname(__FILE__)."../../sidebar-news.tpl.php"?>
  </div><!--End right 4 Aside -->

</div>