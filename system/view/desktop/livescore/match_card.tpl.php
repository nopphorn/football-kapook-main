<?php /* var $tmpMatch */ ?>
<div class="col-md-3 match_fin"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้" >
	<div class=" match_a">
		<a href="<?php echo strtolower($tmpMatch['MatchPageURL']); ?>" target="_blank">
			<div class="row">
				<!-- LIVE & Channel Logo -->
				<div class="col-md-12 option"><span class="league-name"><img src="<?php echo BASE_HREF; ?>assets/img/flags/zone/<?php echo $tmpMatch['flag']; ?>.png" alt="" style="width: 24px;"> <?php echo $tmpMatch['leagueName']; ?> </span>
					<?php 
					if(	($tmpMatch['MatchStatus']=='1 HF')	||
						($tmpMatch['MatchStatus']=='2 HF')	||
						($tmpMatch['MatchStatus']=='H/T')	||
						($tmpMatch['MatchStatus']=='E/T')	||
						($tmpMatch['MatchStatus']=='Pen')){
					?>
					<label class="live pull-left blink_me">LIVE</label> 
					<?php } ?>
					<span class="channel pull-right"><?php
						foreach($tmpMatch['TVLiveList'] as $ChannelKey){ ?>
						<img style="height:16px;" src="<?php echo BASE_HREF; ?>uploads/tvlogo/<?php echo $ChannelKey; ?>.png" title="<?php echo $this->tv_lib->getTVText($ChannelKey); ?>"><?php
						} ?>
					</span>
				</div>
				<!-- Logo Team 1 -->
				<div class="col-md-4 "><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team1Logo']); ?>" class="flag" id="TeamLogo<?php echo $tmpMatch['id']; ?>_1_Tab" ></div>
				<!-- Score -->
				<div class="col-md-4 status <?php echo $this->football_lib->getClassNameByStatusArray($tmpMatch['MatchStatus']); ?> divVS<?php echo $tmpMatch['id']; ?>">     
					<?php
					$ETScoreArr = explode('-', $tmpMatch['ETScore']);
					$PNScoreArr = explode('-', $tmpMatch['PNScore']);
					if($tmpMatch['MatchStatus']=='Sched'){?>
						<?php echo $tmpMatch['MatchTime']; ?><br>VS
					<?php }else if($tmpMatch['MatchStatus']=='Fin'){ ?>
						จบเกม<br><?php
						if(( $tmpMatch['PNScore'] == '-' )&&( $tmpMatch['ETScore'] == '-')){ /*90 min fulltime*/
							echo $tmpMatch['Team1FTScore'] . ' - ' . $tmpMatch['Team2FTScore']; 
						}else if(( $tmpMatch['PNScore'] != '-' )&&( $tmpMatch['ETScore'] == '-')){ /*90 min + pen*/
							echo $tmpMatch['Team1FTScore'] . ' - ' . $tmpMatch['Team2FTScore'] . '<br>( ' . $PNScoreArr[0] . ' PK ' . $PNScoreArr[1] . ' )';
						}else if(( $tmpMatch['PNScore'] == '-' )&&( $tmpMatch['ETScore'] != '-')){ /*120 min*/
							echo $ETScoreArr[0] . ' - ' . $ETScoreArr[1];
						}else { /*120 min + pen*/
							echo $ETScoreArr[0] . ' - ' . $ETScoreArr[1] . '<br>( ' . $PNScoreArr[0] . ' PK ' . $PNScoreArr[1] . ' )';
						}
					}else if($tmpMatch['MatchStatus']=='Int'){?>
						หยุด<br>ชั่วคราว
					<?php }else if($tmpMatch['MatchStatus']=='Canc'){?>
						ยกเลิก<br>การแข่งขัน
					<?php }else if($tmpMatch['MatchStatus']=='Post'){?>
						เลื่อน<br>การแข่งขัน
					<?php }else if($tmpMatch['MatchStatus']=='Abd'){?>
						ยกเลิก<br>การแข่งขัน
					<?php }else if($tmpMatch['MatchStatus']=='1 HF'){?> 
						<?php echo $tmpMatch['Minute'];?>'<br><?php echo $tmpMatch['Team1FTScore']; ?> - <?php echo $tmpMatch['Team2FTScore']; ?>
					<?php }else if($tmpMatch['MatchStatus']=='2 HF'){?>  
						<?php echo $tmpMatch['Minute'];?>'<br><?php echo $tmpMatch['Team1FTScore']; ?> - <?php echo $tmpMatch['Team2FTScore']; ?>
					<?php }else if($tmpMatch['MatchStatus']=='E/T'){?>  
						<?php echo $tmpMatch['Minute'];?>'<br><?php echo $ETScoreArr[0]; ?> - <?php echo $ETScoreArr[1]; ?>
					<?php }else if($tmpMatch['MatchStatus']=='Pen'){?>  
						Pen<br><?php
						if($tmpMatch['ETScore'] == '-'){
							echo $tmpMatch['Team1FTScore'] . ' - ' . $tmpMatch['Team2FTScore'] . '<br>( ' . $PNScoreArr[0] . ' PK ' . $PNScoreArr[1] . ' )';
						}else{
							echo $ETScoreArr[0] . ' - ' . $ETScoreArr[1] . '<br>( ' . $PNScoreArr[0] . ' PK ' . $PNScoreArr[1] . ' )';
						} ?>
					<?php }else if($tmpMatch['MatchStatus']=='H/T'){?>  
						H/T <?php echo $tmpMatch['Minute'];?>'<br><?php echo $tmpMatch['Team1FTScore']; ?> - <?php echo $tmpMatch['Team2FTScore']; ?>
					<?php }else{?>
						N/A<br>
					<?php } ?>      
				</div>
				<!-- Logo Team 2 -->
				<div class="col-md-4"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team2Logo']); ?>" class="flag" id="TeamLogo<?php echo $tmpMatch['id']; ?>_2_Tab"></div>
			</div>
			<!-- Team Name & Handicap -->
			<div class="row hilight-color">
				<div class="col-md-4 "><span class="teamname <?php if($tmpMatch['TeamOdds']==1){ echo ' handicap_before'; } ?>"><?php echo $tmpMatch['Team1']; ?></span></div>
				<div class="col-md-4 handicap-text"><?php 
				if($tmpMatch['Odds']== 0){
					echo 'เสมอ';
				}else{
					echo $this->football_lib->getOddsRate((string)$tmpMatch['Odds']);
				} ?>
				</div>
				<div class="col-md-4"><span class="teamname <?php if($tmpMatch['TeamOdds']==2){ echo ' handicap_after'; } ?>"><?php echo $tmpMatch['Team2']; ?></span></div>
			</div>
		</a>
		<!--GAME ZONE-->
		<?php if(($this->isCanPlayGame)&&($tmpMatch['Odds']!=-1)&&($tmpMatch['MatchStatus']=='Sched')){ ?>
		<div class="extra-text game<?php echo $tmpMatch['id']; ?>"> ทายผล |
			<a href="#game-single" role="tab" data-toggle="tab" onclick="AddPlaySideSingle(<?php echo $tmpMatch['id'];?>,'<?php echo $tmpMatch['Team1'];?>','<?php echo $tmpMatch['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team2Logo']);?>','<?php echo $tmpMatch['Odds'];?>',<?php echo $tmpMatch['TeamOdds'];?>)" class="link-play-game single label label-primary">ทายบอลเดี่ยว</a>
			<a href="#dropdown1" role="tab" data-toggle="tab" onclick="AddPlaySideMulti(1,<?php echo $tmpMatch['id'];?>,'<?php echo $tmpMatch['Team1'];?>','<?php echo $tmpMatch['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team2Logo']);?>','<?php echo $tmpMatch['Odds'];?>',<?php echo $tmpMatch['TeamOdds'];?>)" class="link-play-game single label label-success">ทายบอลชุด</a>
		</div>
		<?php }else if( $this->isCanPlayGame == false ){ ?>
		<div class="extra-text game<?php echo $tmpMatch['id']; ?>">
			<a href="<?php echo BASE_HREF; ?>games" class="link-play-game single label label-primary">เล่นเกมทายผลบอล</a>
		</div>
		<?php } ?>
	</div>  
</div>