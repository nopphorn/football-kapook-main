<?php

class Curl_lib 
{

    var $errMSG;
    var $errNO;

    function getCurl($url, $_postFields=array()) 
    {
        $user_agent = "Mozilla/4.0";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_postFields));
        curl_setopt ($ch, CURLOPT_POSTFIELDS, http_build_query($_postFields, '', '&'));
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            $this->errNO = curl_errno($ch);
            $this->errMSG = curl_error($ch);
            return false;
        }
        
        curl_close($ch);
        
        /* -- Return -- */
        return $result;
    }

}
/* End of file curl_lib.php */
/* Location: ./library/curl_lib.php */