<?php

if (!defined('MINIZONE'))
    exit;

class Comment Extends My_con {

///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();

        /* -- Load : Model -- */
        $this->comment_model = $this->minizone->model('comment_model');
//        $this->hilight_lib = $this->minizone->library('hilight_lib');
    }

    function saveReply($comment_id = '') {
        $this->getip_lib = $this->minizone->library('getIP_lib');

        if ($comment_id != '' and Login === 'Login') {
            $log_data['comment_id'] = $comment_id;
            self::log_reply(1,$log_data);
            $this->_header();
            $this->view->Assign('message', 'ชื่อผู้ใช้หรือรหัสผ่านอ่าจไม่ถูกต้อง');
            $this->view->Assign('error_type', 'login');
            $this->view->render('desktop/comment/error.tpl');
            $this->_footer();
            exit;
        }




        if ($comment_id != '' and Login === true) {
            $log_data['comment_id'] = $comment_id;
            
            self::log_reply(2,$log_data);
            $d = $this->comment_model->updateMemberComment($comment_id, $this->getip_lib->getUserIP());


            $this->view->Assign('returnPage', self::getReturnPage() . "#cid{$comment_id}");

            if ($_COOKIE['fb_post_' . $comment_id] == 1) {
                $this->info_lib = $this->minizone->library('info_lib');
                $current_comment = json_decode($this->comment_model->get_id($comment_id), true);
                $this->view->Assign('current_comment', $current_comment);


                if ($current_comment['alldata']['pagetype'] == 'football_news') {
                    $content = $this->info_lib->get_news($current_comment['alldata']['c_id']);
                } else if ($current_comment['alldata']['pagetype'] == 'football_match') {
                    $content = $this->info_lib->get_match($current_comment['alldata']['c_id']);
                }
                $this->view->assign('content_display', $content);
            }


            $this->_header();
            $this->view->render('desktop/comment/reply.tpl');
            $this->_footer();
            exit;
        }

//        $this->getip_lib = $this->minizone->library('getip_lib');
        $data['c_id'] = (int) $_POST['c_id'];
        $data['p_id'] = (int) PORTAL_ID;
        $data['pagetype'] = (string) $_POST['pagetype'];
        $data['answer'] = (string) $_POST['answer'];
        $data['name'] = (string) $_POST['name'];
        $data['kid'] = (int) 0;
        $data['ip'] = (string) $this->getip_lib->getUserIP();
        $data['parent'] = (string) $_POST['parent_comment'];
        $data['ref'] = (string) $_POST['ref_comment'];
        $data['post_type'] = (string) $_POST['post_type'];

//        exit;
        self::setReturnPage($_POST['rtnPage']);

        if (Login === true) {
            $data['kid'] = (int) uid;
            $comment_id = $this->comment_model->saveReply($data);
            if ($comment_id['status_code'] == 210) {
                $this->_header();
                $this->view->Assign('message', $comment_id['error']);
                $this->view->Assign('error_type', 'post fail');
                $this->view->Assign('returnPage', self::getReturnPage());
                $this->view->render('desktop/comment/error.tpl');
                $this->_footer();
                exit;
            }


//            $this->view->Assign('returnPage', $this->hilight_lib->format_link(array('id'=>$_POST['c_id'])));
            $this->view->Assign('returnPage', self::getReturnPage());

            $breadcrumbs[] = array('label' => 'หน้าแรก', 'link' => 'http://hilight.kapook.com');
            $breadcrumbs[] = array('label' => 'บันทึกข้อมูล', 'link' => '');
            $this->_header();
            $this->view->render('desktop/comment/reply.tpl');
            $this->_footer();
            exit;
        }

        $comment_id = $this->comment_model->saveReply($data);

        if ($comment_id['status_code'] == 210) {
            $this->_header();
            $this->view->Assign('message', $comment_id['error']);
            $this->view->Assign('error_type', 'post fail');
            $this->view->Assign('returnPage', self::getReturnPage());
            $this->view->render('desktop/comment/error.tpl');
            $this->_footer();
            exit;
        }

        $this->view->Assign('comment_id', $comment_id['_id']);

        if ($_POST['post_to_facebook'] == 1) {
            setcookie("fb_post_" . strval($comment_id['_id']), 1, time() + 300, "/", 'kapook.com');
        }

        if ($data['parent'] != "0") {
//            self::setReturnPage(BASE_HREF . "view_new/view_reply/{$data['c_id']}/{$data['parent']}");
//            self::setReturnPage(BASE_HREF . "comment/sub_reply/{$data['c_id']}/{$data['parent']}");
            $_parent_comment = json_decode($this->comment_model->get_id($data['parent']), true);
            self::setReturnPage($_POST['rtnPage'] . '-' . $_parent_comment['alldata']['id']);
        }

        if ($_POST['post_type'] == 'kapook') {
            $breadcrumbs[] = array('label' => 'หน้าแรก', 'link' => 'http://hilight.kapook.com');
            $breadcrumbs[] = array('label' => 'บันทึกข้อมูล', 'link' => '');
            $this->view->Assign('breadcrumbs', $breadcrumbs);
            $this->_header();
            $this->view->render('desktop/comment/login.tpl');
            $this->_footer();
            exit;
        }

        if ($_POST['post_type'] == 'facebook') {
            $this->facebook_lib = $this->minizone->library('facebook_lib');
            $callback = 'http://football.kapook.com/comment/saveReply/' . strval($comment_id['_id']);
            $fb_link = $this->facebook_lib->getLoginUrl(array('scope' => 'email,user_birthday,publish_actions', 'redirect_uri' => 'http://signup-demo.kapook.com/connect/checkuser/?callback=' . $callback));
            header('Location: ' . $fb_link);
        }
    }

    function _iconv($arr_txt) {
        foreach ($arr_txt as $k => $v) {
            $rtn[$k] = iconv('tis-620', 'utf-8//IGNORE', $v);
        }
        return $rtn;
    }

    function ads_ifrme($zid = 0) {
//        echo '<script src="http://ads.kapook.com/adshow.php?zid=' . $zid . '"></script>';
        $this->view->render('desktop/comment/ads_dfp.tpl');
    }

    function sub_reply($id, $parent, $type = '') {
        $this->info_lib = $this->minizone->library('info_lib');
        $this->news_model = $this->minizone->model('news_model');

        $NewsTopHitsContents = $this->news_model->getNewsTopHits($_REQUEST['remove_cache'] == 1);
        $this->view->assign('NewsTopHitsContents', $NewsTopHitsContents['data']);

        if ($type == 'news') {
            $content = $this->info_lib->get_news($id);
            $this->view->Assign('subCommentType', $type);
        } else if ($type == 'match') {
            $content = $this->info_lib->get_match($id);
            echo $parent . '<---';
            $this->view->Assign('subCommentType', $type);
        }


        $this->view->assign('content_display', $content);

        $breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = 'ข่าว';
        $breadcrum[1]['link'] = BASE_HREF . "newslist.php";
        $breadcrum[2]['text'] = $content['title'];
        $breadcrum[2]['link'] = BASE_HREF . "news-{$id}";
        $breadcrum[2]['active'] = true;
//        $breadcrum[3]['text'] = 'ความคิดเห็นเรื่อง '.$content['title'];
//        $breadcrum[3]['active'] = true;

        parent::_setBreadcrum($breadcrum);

        $data['seo_title'] = 'ความคิดเห็นเรื่อง ' . $content['seo_title'];
        $data['seo_description'] = 'ความคิดเห็นเรื่อง ' . $content['seo_description'];
        ;
        $data['seo_keywords'] = $content['seo_keywords'];

        $data['fb_title'] = 'ความคิดเห็นเรื่อง ' . $content['fb_title'];
        $data['fb_description'] = $content['fb_description'];
        $data['fb_img'] = $content['fb_img'];
        parent::_setSocial($data);


        $comment = json_decode($this->comment_model->get_by_id($parent), true);
        $this->view->Assign('viewType', 'Sub');
        $this->view->Assign('comment', $comment);

        if ($type == 'match') {
//            print_r($comment);
        }




        $this->_header();
        $page_render = $this->root_view . '/comment/sub_reply.tpl';
        $this->view->render($page_render);
        $this->_footer();
    }

    private function setReturnPage($url = '') {

        if ($url != '') {
            setcookie("rtnPage", $url, time() + 300, "/", 'kapook.com');
            return true;
        }

        if ($_SERVER['HTTP_REFERER']) {
            setcookie("rtnPage", $_SERVER['HTTP_REFERER'], time() + 300, "/", 'kapook.com');
            return true;
        }
        return true;
    }

    private function getReturnPage() {
        if ($_COOKIE['rtnPage'] == '')
            return BASE_HREF;

        return $_COOKIE['rtnPage'];
    }
    
    private function log_reply($type,$log_data) {
        $message = date('d-m-Y H:i')." Type {$type} - comment_id :: ".var_export($log_data,true)."\n";
        @error_log($message, 3, '/var/web/football.kapook.com/html/uploads/logs/reply_error.txt');
    }

}

/* End of file home.php */
/* Location: ./controller/home.php */