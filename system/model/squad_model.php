<?php if (!defined('MINIZONE')) exit;

class Squad_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
		$this->minizone = minizone::getzone();
        $this->mem_lib = $this->minizone->library('memcache_lib');
    }
    
    ///////////////////////////////////////////////// Get : getResultMatch   /////////////////////////////////////////////////
    function getTeamInfo($KPTeamID=null,$isNew = false)
    {
        if ($KPTeamID) {
            if($isNew == true)
				$option		=	'&clear=1';
			else
				$option		=	'';
            
            $url = 'http://football.kapook.com/api/team.php?id=' . $KPTeamID . $option;
            $result = json_decode(file_get_contents($url),true);

            return $result;
        }
        else {
            return false;
        }
    }
	
}
/* End of file league_model.php */
/* Location: ./system/model/league_model.php */
?>