<?php

if (!defined('MINIZONE'))
    exit;

class Ajax Extends My_con {

///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();

        /* -- Load : Model -- */
        $this->comment_model = $this->minizone->model('comment_model');
    }

    function checkLogin() {
        $this->getlogin_lib = $this->minizone->library('getlogin_lib');
        $rtn['status_code'] = 200;
        if (is_bool(Login)) {
            $rtn['result'] = 1;
            $this->view->render_json($rtn);

            exit;
        }

        $rtn['result'] = Login;
        $this->view->render_json($rtn);
    }

    function saveReply() {

        $this->getip_lib = $this->minizone->library('getIP_lib');

        $data['c_id'] = (int) $_POST['c_id'];
        $data['p_id'] = (int) PORTAL_ID;
        $data['answer'] = (string) htmlspecialchars($_POST['answer']);
        $data['name'] = (string) $_POST['name'];
        $data['kid'] = (int) 0;
        $data['ip'] = (string) $this->getip_lib->getUserIP();
        $data['parent'] = (string) $_POST['parent_comment'];
        $data['ref'] = (string) $_POST['ref_comment'];
        $data['pagetype'] = (string) $_POST['pagetype'];
        
        if($data['parent'] == '') {
           $data['parent'] = 0; 
        }
        if($data['ref'] == '') {
           $data['ref'] = 0; 
        }
        


        if (Login === true) {
            $data['kid'] = (int) uid;
            $result = $this->comment_model->saveReply($data);
			$result['POSTDATA'] = $_POST;
            $this->view->render_json($result);
            exit;
        }
    }

}

/* End of file ajax.php */
/* Location: ./controller/ajax.php */