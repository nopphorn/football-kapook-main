<?php

if (!defined('MINIZONE'))
    exit;

class Tournament extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        parent::__construct();
        $this->mem_lib = $this->minizone->library('memcache_lib');
		$this->comp_model = $this->minizone->model('comp_model');
		$this->news_model = $this->minizone->model('news_model');
    }
	
	private function setDataSocial($dataTournament){
		$data['seo_title'] 			= 		$dataTournament['SEOTitle'];
		$data['seo_description'] 	= 		$dataTournament['SEOdescription'];
		$data['seo_keywords'] 		= 		$dataTournament['SEOkeywords'];
		$data['fb_title'] 			= 		$dataTournament['FBtitle'];
		$data['fb_description'] 	= 		$dataTournament['FBdescription'];
		$data['fb_img'] 			= 		$dataTournament['FBimg'];
		parent::_setSocial($data);
	}

    ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
    function index($cupname) {
		
		if(date('G')>=6){
			$today					=	date("Y-m-d");
		}else{
			$today					=	date("Y-m-d", strtotime("-1 day"));
		}
		
		// dataTournament
		$dataTournament 			= 		$this->mem_lib->get('DataTournament-' . $cupname);
		if((!$dataTournament)||($_REQUEST['remove_cache']=='1')){
			$expire					=		3600;
			$dataTournament			=		$this->comp_model->getInfoByName($cupname);
			
			if(empty($dataTournament['nameURL']))
			{
				header( "location: http://football.kapook.com" );
				exit(0);
			}
			
			$this->mem_lib->set('DataTournament-' . $cupname, $dataTournament, MEMCACHE_COMPRESSED, $expire);
		}
		
		// ProgramContents-Today
		if($cupname != 'euro2016'){
			$ProgramContents 					= 		$this->mem_lib->get('ProgramLastTournament-' . $cupname);
			if((!$ProgramContents)||($_REQUEST['remove_cache']=='1')){
				$expire					=		900;
				$ProgramContents		=		$this->comp_model->getProgramByNameToday($cupname,true);
				foreach($ProgramContents['program'] as $tmpMatch){
					$tmpDiff = strtotime($tmpMatch['MatchDateTime']) - strtotime(date("Y-m-d H:i:s"));
					if(	($tmpDiff < 0)	&&
						(
							($tmpMatch['MatchStatus']=='Sched')	||
							($tmpMatch['MatchStatus']=='1 HF')	||
							($tmpMatch['MatchStatus']=='H/T')	||
							($tmpMatch['MatchStatus']=='2 HF')	||
							($tmpMatch['MatchStatus']=='E/T')	||
							($tmpMatch['MatchStatus']=='Pen')
						)
					){
						$expire		=	60;
					}else if(($tmpDiff < $expire) && ($tmpDiff > 0) ){
						$expire		=	$tmpDiff;
					}

				}
				$this->mem_lib->set('ProgramLastTournament-' . $cupname, $ProgramContents, MEMCACHE_COMPRESSED, $expire);
			}
		}else{
			$AllProgram 					= 		$this->mem_lib->get('ProgramAllTournament-Tournament-' . $cupname);
			if((!$AllProgram)||($_REQUEST['remove_cache']=='1')){
				$expire					=		900;
				$AllProgram = $this->comp_model->getProgramAll($cupname);
				
				foreach($AllProgram['program'] as $tmpMatch){
					$tmpDiff = strtotime($tmpMatch['MatchDateTime']) - strtotime(date("Y-m-d H:i:s"));
					if(	($expire > 30)	&&
						(
							($tmpMatch['MatchStatus']=='Sched')	||
							($tmpMatch['MatchStatus']=='1 HF')	||
							($tmpMatch['MatchStatus']=='H/T')	||
							($tmpMatch['MatchStatus']=='2 HF')	||
							($tmpMatch['MatchStatus']=='E/T')	||
							($tmpMatch['MatchStatus']=='Pen')
						)
					){
						$expire		=	30;
					}else if(($tmpDiff < 0) && ($tmpDiff > -7200 )){
						$expire		=	30;
					}else if(($tmpDiff < $expire) && ($tmpDiff > 0)){
						$expire		=	$tmpDiff;
					}
				}
				$this->mem_lib->set('ProgramLastTournament-' . $cupname, $ProgramContents, MEMCACHE_COMPRESSED, $expire);
			}
			$this->view->assign('datetimeformat_lib', $this->minizone->library('datetimeformat_lib'));
			$this->view->assign('AllProgram', $AllProgram['program']);
		}
		
		// TableContents
		$TableContents 			= 		$this->mem_lib->get('TableTournament-' . $cupname);
		if((!$TableContents)||($_REQUEST['remove_cache']=='1')){
			$expire					=		900;
			$TableContents			=		$this->comp_model->getTableByName($cupname);
			$this->mem_lib->set('TableTournament-' . $cupname, $TableContents, MEMCACHE_COMPRESSED, $expire);
		}
		
		// NewsContents-Index
		$NewsContents 			= 		$this->mem_lib->get('NewsIndexTournament-' . $cupname);
		if((!$NewsContents)||($_REQUEST['remove_cache']=='1')){
			$expire					=		900;
			$NewsContents			=		$this->comp_model->getNewsByName($cupname);
			$this->mem_lib->set('NewsIndexTournament-' . $cupname, $NewsContents, MEMCACHE_COMPRESSED, $expire);
		}
		
		// ScorerContents
		$ScorerContents 			= 		$this->mem_lib->get('ScorerTournament-' . $cupname);
		if((!$ScorerContents)||($_REQUEST['remove_cache']=='1')){
			$expire					=		3600;
			$ScorerContents			=		$this->comp_model->getScorerByName($cupname);
			$this->mem_lib->set('ScorerTournament-' . $cupname, $ScorerContents, MEMCACHE_COMPRESSED, $expire);
		}
		
		// StatContents
		$StatContents 				= 		$this->mem_lib->get('StatTournament-' . $cupname);
		if((!$StatContents)||($_REQUEST['remove_cache']=='1')){
			$expire					=		3600;
			$StatContents			=		$this->comp_model->getStatByName($cupname);
			$this->mem_lib->set('StatTournament-' . $cupname, $StatContents, MEMCACHE_COMPRESSED, $expire);
		}
		
		$this->view->assign('InfoCupContents', $dataTournament);
		$this->view->assign('ProgramContents', $ProgramContents);
		$this->view->assign('TableContents', $TableContents['table']);
		$this->view->assign('TableRoundContents', $TableContents['table_round']);
		$this->view->assign('NewsCupContents', $NewsContents['news']);
		$this->view->assign('ScorerContents', $ScorerContents['scorer']);
		$this->view->assign('StatContents', $StatContents['stat_zone']);
		
		$this->view->assign('tv_lib', $this->minizone->library('tv_lib'));
		
		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
		$breadcrum[0]['link'] = BASE_HREF;
		$breadcrum[1]['text'] = $dataTournament['nameTHShort'];
		$breadcrum[1]['active'] = true;
		parent::_setBreadcrum($breadcrum);
		
		$this->setDataSocial($dataTournament);
                
                $meta_category = array(
                    'premierleague' => 'football_premierleague',
                    'laliga' => 'football_laliga',
                    'bundesliga' => 'football_bundesliga',
                    'seriea' => 'football_calcio',
                    'ligue1' => 'football_Ligue',
                    'thaipremierleague' => 'football_thaipremierleague',
                    'championsleague' => 'football_championsleague',
                    'europaleague' => 'football_europaleague',
                    'afcchampionsleague' => 'football_afcchampionsleague',
                    'u23afc2016' => 'football_u23afc2016',
                    'worldcup2018asianqual' => 'football_worldcup2018asianqual',
                    'euro2016' => 'football_euro2016qual',
                    'worldcup2018saqual' => 'football_worldcup2018saqual'
                );
                
                // meta_category
                $this->view->assign('meta_category', $meta_category[$cupname]);
		
		if($cupname == 'euro2016'){         
			$this->_header('tournament/euro2016/header.tpl');
			$this->view->render($this->root_view . '/tournament/euro2016/index.tpl');
			$this->_footer();
		}else{
			$this->_header();
			$this->view->render($this->root_view . '/tournament/index.tpl');
			$this->_footer();
		}
    }
	
	function program($cupname) {
		$Program					=	array();
		$isCache					=	false;
		
		if(date('G')>=6){
			$today					=	date("Y-m-d");
		}else{
			$today					=	date("Y-m-d", strtotime("-1 day"));
		}
		
		$ProgramContents 		= 		$this->mem_lib->get('ProgramTournament-' . $cupname);
		
		if((!$ProgramContents)||($_REQUEST['remove_cache']=='1')){
			$expire					=		900;
			$isCache				=		true;
			$ProgramContents		=		$this->comp_model->getProgramByName($cupname,true);
			if(empty($ProgramContents['nameURL']))
			{
				header( "location: http://football.kapook.com" );
				exit(0);
			}
		}
		
		foreach($ProgramContents['program'] as $tmpMatch){
			$MatchDate		=	$tmpMatch['MatchDateTime'];
				
			if(date('G',strtotime($MatchDate))>=6){
				$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate));	
			}else{
				$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate .' - 1 days'));
			}
			
			if($isCache){
				$tmpDiff = strtotime($MatchDate) - strtotime(date("Y-m-d H:i:s"));
				if(	($tmpDiff < 0)	&&
					(
						($tmpMatch['MatchStatus']=='Sched')	||
						($tmpMatch['MatchStatus']=='1 HF')	||
						($tmpMatch['MatchStatus']=='H/T')	||
						($tmpMatch['MatchStatus']=='2 HF')	||
						($tmpMatch['MatchStatus']=='E/T')	||
						($tmpMatch['MatchStatus']=='Pen')
					)&&
					($expire > 60)
				){
					$expire		=	60;
				}else if(($tmpDiff < $expire) && ($tmpDiff > 0) ){
					$expire		=	$tmpDiff;
				}
			}
				
			$tmpMatch['MatchDate']	=		$tmpDateMatch;
			$tmpMatch['MatchTime']	=		date("H:i", strtotime($MatchDate));
				
			$Mdate = explode('-', $tmpDateMatch);
			$Program[$Mdate[0] . '-' . $Mdate[1]][$Mdate[2]][] = $tmpMatch;
		}
		
		$ProgramByRound = $ProgramContents['programByRound'];
		$TabRoundSelect = -1;
		
		foreach($ProgramByRound as $key_round => $tmpRound){
			if(intval($tmpRound['id'])==$ProgramContents['TabProgram']){
				$TabRoundSelect = intval($tmpRound['id']);
			}
			foreach($tmpRound['Program'] as $key_match => $tmpMatch){
				$MatchDate		=	$tmpMatch['MatchDateTime'];
				
				if(date('G',strtotime($MatchDate))>=6){
					$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate));	
				}else{
					$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate .' - 1 days'));
				}
				
				if($isCache){
					$tmpDiff = strtotime($MatchDate) - strtotime(date("Y-m-d H:i:s"));
					if(	($tmpDiff < 0)	&&
						(
							($tmpMatch['MatchStatus']=='Sched')	||
							($tmpMatch['MatchStatus']=='1 HF')	||
							($tmpMatch['MatchStatus']=='H/T')	||
							($tmpMatch['MatchStatus']=='2 HF')	||
							($tmpMatch['MatchStatus']=='E/T')	||
							($tmpMatch['MatchStatus']=='Pen')
						)&&
						($expire > 60)
					){
						$expire		=	60;
					}else if(($tmpDiff < $expire) && ($tmpDiff > 0) ){
						$expire		=	$tmpDiff;
					}
				}
					
				$tmpMatch['MatchDate']	=		$tmpDateMatch;
				$tmpMatch['MatchTime']	=		date("H:i", strtotime($MatchDate));
				
				$ProgramByRound[$key_round]['Program'][$key_match] = $tmpMatch;
			}
		}
		if($isCache){
			$this->mem_lib->set('ProgramTournament-' . $cupname, $ProgramContents, MEMCACHE_COMPRESSED, $expire);
		}
		
		$this->view->assign('InfoCupContents', $ProgramContents);		
		$this->view->assign('ProgramContents', $Program);
		$this->view->assign('ProgramRoundContents', $ProgramByRound);
		$this->view->assign('TabRoundSelect', $TabRoundSelect);
		
		$this->view->assign('tv_lib', $this->minizone->library('tv_lib'));
		
		if($cupname == 'euro2016'){
			$ProgramContents['SEOTitle']		=	'โปรแกรมยูโร 2016 ผลบอล โปรแกรมบอล ตารางถ่ายทอดสดยูโร 2016';
			$ProgramContents['SEOdescription']	=	'โปรแกรมยูโร 2016 ผลบอลยูโร ผลบอลสด โปรแกรมบอลยูโร ตารางถ่ายทอดสด ยูโร 2016 ฟุตบอลชิงแชมป์แห่งชาติยุโรป 2016 ตารางบอลยูโร';
			$ProgramContents['SEOkeywords']		=	'โปรแกรมยูโร 2016, ผลบอลยูโร 2016, ผลบอลยูโร, ผลบอลสด, โปรแกรมบอลยูโร, ตารางบอลยูโร, ตารางถ่ายทอดบอลยูโร';
			$ProgramContents['FBtitle']			=	'โปรแกรมยูโร 2016 ผลบอลยูโร ผลบอลสด ตารางถ่ายทอดสดยูโร 2016';
			$ProgramContents['FBdescription']	=	'โปรแกรมยูโร 2016 ผลบอลยูโร ผลบอลสด โปรแกรมบอลยูโร ตารางถ่ายทอดสด ยูโร 2016 ฟุตบอลชิงแชมป์แห่งชาติยุโรป 2016 ตารางบอลยูโร';
			//$ProgramContents['FBimg']			=	'';
		}
		
		$this->setDataSocial($ProgramContents);
		
		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
		$breadcrum[0]['link'] = BASE_HREF;
		$breadcrum[1]['text'] = $ProgramContents['nameTHShort'];
		if(strlen($ProgramContents['nameURLRootRoute'])){
			$breadcrum[1]['link'] = $ProgramContents['nameURLRootRoute'];
		}else{
			$breadcrum[1]['link'] = BASE_HREF . 'tournament/' . $ProgramContents['nameURL'];
		}
		$breadcrum[2]['text'] = "ผลบอล-โปรแกรม " . $ProgramContents['nameTHShort'];
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);
                
                $meta_category = array(
                    'premierleague' => 'football_resultprogram',
                    'championsleague' => 'football_championsleagueresult',
                    'europaleague' => 'football_europaleagueresult',
                    'afcchampionsleague' => 'football_afcchampionsleagueresult',
                    'u23afc2016' => 'football_u23afc2016result',
                    'worldcup2018asianqual' => 'football_worldcup2018asianqualresult',
                    'worldcup2018saqual' => 'football_worldcup2018saqualresult',
                    'euro2016' => 'football_euro2016qualresult',
                    'laliga' => 'football_laligaresult',
                    'bundesliga' => 'football_bundesligaresult',
                    'seriea' => 'football_calcioresult',
                    'ligue1' => 'football_Ligueresult',
                    'thaipremierleague' => 'football_thaipremierleagueresult',
                );
                
                // meta_category
                $this->view->assign('meta_category', $meta_category[$cupname]);
		
		if($cupname == 'euro2016'){
			$this->_header('tournament/euro2016/header.tpl');
			$this->view->render($this->root_view . '/tournament/euro2016/program.tpl');
			$this->_footer();
		}else{
			$this->_header();
			$this->view->render($this->root_view . '/tournament/program.tpl');
			$this->_footer();
		}
    }
	
	function table($cupname) {
        //var_dump($this->mem_lib);
		
		$TableContents 			= 		$this->mem_lib->get('TableTournament-' . $cupname);
		if((!$TableContents)||($_REQUEST['remove_cache']=='1')){
			$expire					=		900;
			$TableContents			=		$this->comp_model->getTableByName($cupname);
			if(empty($TableContents['nameURL']))
			{
				header( "location: http://football.kapook.com" );
				exit(0);
			}
			$this->mem_lib->set('TableTournament-' . $cupname, $TableContents, MEMCACHE_COMPRESSED, $expire);
		}
		
		$this->view->assign('InfoCupContents', $TableContents);
		$this->view->assign('TableContents', $TableContents['table']);
		$this->view->assign('TableRoundContents', $TableContents['table_round']);
		
		if($cupname == 'euro2016'){
			$TableContents['SEOTitle']			=	'ตารางคะแนนยูโร 2016 ตารางคะแนนยูโรล่าสุด สรุปตารางคะแนนยูโร 2016';
			$TableContents['SEOdescription']	=	'ตารางคะแนนยูโร 2016 ตารางคะแนนยูโรล่าสุด ตารางคะแนนยูโรทุกกลุ่ม ตารางบอลยูโร 2016 ผลบอลสด อันดับตารางคะแนนยูโร';
			$TableContents['SEOkeywords']		=	'ตารางคะแนนยูโร 2016, ตารางคะแนน, ยูโร 2016, ตารางบอลยูโร, สรุปตารางคะแนนยูโร, ตารางคะแนนยูโรทุกกลุ่ม';
			$TableContents['FBtitle']			=	'ตารางคะแนนยูโร 2016 ตารางคะแนนยูโรล่าสุด สรุปตารางคะแนนยูโร 2016';
			$TableContents['FBdescription']		=	'ตารางคะแนนยูโร 2016 ตารางคะแนนยูโรล่าสุด ตารางคะแนนยูโรทุกกลุ่ม ตารางบอลยูโร 2016 อันดับตารางคะแนนยูโร';
			//$TableContents['FBimg']			=	'';
		}
		
		$this->setDataSocial($TableContents);
		
		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
		$breadcrum[0]['link'] = BASE_HREF;
		$breadcrum[1]['text'] = $TableContents['nameTHShort'];
		if(strlen($TableContents['nameURLRootRoute'])){
			$breadcrum[1]['link'] = $TableContents['nameURLRootRoute'];
		}else{
			$breadcrum[1]['link'] = BASE_HREF . 'tournament/' . $TableContents['nameURL'];
		}
		$breadcrum[2]['text'] = "ตารางคะแนน " . $TableContents['nameTHShort'];
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);
		
                $meta_category = array(
                    'premierleague' => 'football_premiertable',
                );
                
                // meta_category
                $this->view->assign('meta_category', $meta_category[$cupname]);
                
		if($cupname == 'euro2016'){
			$this->_header('tournament/euro2016/header.tpl');
			$this->view->render($this->root_view . '/tournament/euro2016/table.tpl');
			$this->_footer();
		}else{
			$this->_header();
			$this->view->render($this->root_view . '/tournament/table.tpl');
			$this->_footer();
		}

    }
	
	function news($cupname) {
        //var_dump($this->mem_lib);
		
		if(intval($_REQUEST['page']) > 0){
			$page	=	intval($_REQUEST['page']);
		}else{
			$page	=	1;
		}

		// NewsContents-Index
		$NewsContents 			= 		$this->mem_lib->get('NewsIndexTournament-' . $cupname . '-' . $page);
		if((!$NewsContents)||($_REQUEST['remove_cache']=='1')){
			$expire					=		900;
			$NewsContents			=		$this->comp_model->getNewsByName($cupname,$page);
			if(empty($NewsContents['nameURL']))
			{
				header( "location: http://football.kapook.com" );
				exit(0);
			}
			$this->mem_lib->set('NewsIndexTournament-' . $cupname . '-' . $page, $NewsContents, MEMCACHE_COMPRESSED, $expire);
			if($page == 1){
				$this->mem_lib->set('NewsIndexTournament-Latest-' . $cupname, $NewsContents['news']['NewsList'][0], MEMCACHE_COMPRESSED);
			}
		}

		$this->view->assign('InfoCupContents', $NewsContents);
		
		$this->view->assign('NewsListContents', $NewsContents['news']['NewsList']);
        $this->view->assign('NewsPage', $NewsContents['news']['paged']);
        $this->view->assign('NewsPageTotal', $NewsContents['news']['total_pages']);
        $this->view->assign('NewsPageTitle', $NewsContents['nameTHShort']);
		
        $NewsTopHitsContents = $this->news_model->getNewsTopHits($_REQUEST['remove_cache'] == 1);
        $this->view->assign('NewsTopHitsContents', $NewsTopHitsContents['data']);
		
		$this->setDataSocial($NewsContents);
		
		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
		$breadcrum[0]['link'] = BASE_HREF;
		$breadcrum[1]['text'] = $NewsContents['nameTHShort'];
		if(strlen($NewsContents['nameURLRootRoute'])){
			$breadcrum[1]['link'] = $NewsContents['nameURLRootRoute'];
		}else{
			$breadcrum[1]['link'] = BASE_HREF . 'tournament/' . $NewsContents['nameURL'];
		}
		$breadcrum[2]['text'] = "ข่าว  " . $NewsContents['nameTHShort'];
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);
                
	$meta_category = array(
            'premierleague' => 'football_premierleaguenews',
            'laliga' => 'football_laliganews',
            'bundesliga' => 'football_bundesliganews',
            'seriea' => 'football_calcionews',
            'ligue1' => 'football_Liguenews',
            'thaipremierleague' => 'football_thaipremierleaguenews',
            'championsleague' => 'football_championsleague',
            'europaleague' => 'football_europaleague',
            'afcchampionsleague' => 'football_afcchampionsleaguenews',
            'u23afc2016' => 'football_u23afc2016news',
            'worldcup2018asianqual' => 'football_worldcup2018asianqualnews',
            'euro2016' => 'football_euro2016qualnews',
            'worldcup2018saqual' => 'football_worldcup2018saqualnews',
        );
                
        // meta_category
        $this->view->assign('meta_category', $meta_category[$cupname]);
        
        $this->_header();
        $this->view->render($this->root_view . '/home/newslist.tpl');
        $this->_footer();
    }
	
	function topscorer($cupname) {

		$ScorerContents 			= 		$this->mem_lib->get('ScorerTournament-' . $cupname);
		if((!$ScorerContents)||($_REQUEST['remove_cache']=='1')){
			$expire					=		3600;
			$ScorerContents			=		$this->comp_model->getScorerByName($cupname);
			if(empty($ScorerContents['nameURL']))
			{
				header( "location: http://football.kapook.com" );
				exit(0);
			}
			$this->mem_lib->set('ScorerTournament-' . $cupname, $ScorerContents, MEMCACHE_COMPRESSED, $expire);
		}
		
		$this->view->assign('InfoCupContents', $ScorerContents);
		$this->view->assign('ScorerContents', $ScorerContents['scorer']);
		
		if($cupname == 'euro2016'){
			$ScorerContents['SEOTitle']			=	'ดาวซัลโวยูโร 2016 อันดับดาวซัลโวยูโร ดาวยิงยูโร 2016 ดาวยิงสูงสุด';
			$ScorerContents['SEOdescription']	=	'ดาวซัลโวยูโร 2016 อันดับดาวซัลโวยูโร ดาวยิงสูงสุดยูโร ดาวยิงยูโร อันดับดาวยิงบอลยูโร อันดับดาวยิงยูโร 2016';
			$ScorerContents['SEOkeywords']		=	'ดาวซัลโวยูโร 2016, ดาวยิงยูโร, อันดับดาวยิงยูโร, ดาวยิงสูงสุดยูโร';
			$ScorerContents['FBtitle']			=	'ดาวซัลโวยูโร 2016 อันดับดาวยิงยูโร ดาวยิงยูโร 2016 ดาวยิงสูงสุด';
			$ScorerContents['FBdescription']	=	'ดาวซัลโวยูโร 2016 อันดับดาวซัลโวยูโร ดาวยิงสูงสุดยูโร ดาวยิงยูโร อันดับดาวยิงบอลยูโร อันดับดาวยิงยูโร 2016';
			//$ScorerContents['FBimg']			=	'';
		}
		
		$this->setDataSocial($ScorerContents);
		
		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
		$breadcrum[0]['link'] = BASE_HREF;
		$breadcrum[1]['text'] = $ScorerContents['nameTHShort'];
		if(strlen($ScorerContents['nameURLRootRoute'])){
			$breadcrum[1]['link'] = $ScorerContents['nameURLRootRoute'];
		}else{
			$breadcrum[1]['link'] = BASE_HREF . 'tournament/' . $ScorerContents['nameURL'];
		}
		$breadcrum[2]['text'] = "ดาวซัลโว " . $ScorerContents['nameTHShort'];
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);
		
                $meta_category = array(
                    'premierleague' => 'football_premiertopscorer',
                );
                
                // meta_category
                $this->view->assign('meta_category', $meta_category[$cupname]);
                
		if($cupname == 'euro2016'){
			$this->_header('tournament/euro2016/header.tpl');
			$this->view->render($this->root_view . '/tournament/euro2016/scorer.tpl');
			$this->_footer();
		}else{
			$this->_header();
			$this->view->render($this->root_view . '/tournament/scorer.tpl');
			$this->_footer();
		}
    }
}
