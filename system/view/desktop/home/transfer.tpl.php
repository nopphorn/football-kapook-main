<?php 
        $monthtothai['Jan'] ="ม.ค.";
	$monthtothai['Feb'] ="ก.พ.";
	$monthtothai['Mar'] ="มี.ค.";
	$monthtothai['Apr'] ="เม.ย.";
	$monthtothai['May'] ="พ.ค.";
	$monthtothai['Jun'] ="มิ.ย.";
	$monthtothai['Jul'] ="ก.ค.";
	$monthtothai['Aug'] ="ส.ค.";
	$monthtothai['Sep'] ="ก.ย.";
	$monthtothai['Oct'] ="ต.ค.";
	$monthtothai['Nov'] ="พ.ย.";
	$monthtothai['Dec'] ="ธ.ค.";

	$monthtothai['01']['short'] ="ม.ค.";
	$monthtothai['02']['short'] ="ก.พ.";
	$monthtothai['03']['short'] ="มี.ค.";
	$monthtothai['04']['short'] ="เม.ย.";
	$monthtothai['05']['short'] ="พ.ค.";
	$monthtothai['06']['short'] ="มิ.ย.";
	$monthtothai['07']['short'] ="ก.ค.";
	$monthtothai['08']['short'] ="ส.ค.";
	$monthtothai['09']['short'] ="ก.ย.";
	$monthtothai['10']['short'] ="ต.ค.";
	$monthtothai['11']['short'] ="พ.ย.";
	$monthtothai['12']['short'] ="ธ.ค.";
	
	$monthtothai['01']['full'] ="มกราคม";
	$monthtothai['02']['full'] ="กุมภาพันธ์";
	$monthtothai['03']['full'] ="มีนาคม";
	$monthtothai['04']['full'] ="เมษายน";
	$monthtothai['05']['full'] ="พฤษภาคม";
	$monthtothai['06']['full'] ="มิถุนายน";
	$monthtothai['07']['full'] ="กรกฎาคม";
	$monthtothai['08']['full'] ="สิงหาคม";
	$monthtothai['09']['full'] ="กันยายน";
	$monthtothai['10']['full'] ="ตุลาคม";
	$monthtothai['11']['full'] ="พฤศจิกายน";
	$monthtothai['12']['full'] ="ธันวาคม";
        
	$daytothai['Sun'] = "วันอาทิตย์";
	$daytothai['Mon'] = "วันจันทร์";
	$daytothai['Tue'] = "วันอังคาร";
	$daytothai['Wed'] = "วันพุธ";
	$daytothai['Thu'] = "วันพฤหัสบดี";
	$daytothai['Fri'] = "วันศุกร์";
	$daytothai['Sat'] = "วันเสาร์";
	
$NewsTransferArr = $this->NewsTransferContents['data']; 
$NewsPage = $this->NewsTransferContents['paged'];
$NewsPageTotal = $this->NewsTransferContents['total_pages'];
$PlayerMarketArr = $this->PlayerMarketContents;
$ListPage = $PlayerMarketArr['page'];
$TotalListPage = $PlayerMarketArr['page_all'];
?>
<div class="container transfer premiere">
 <h1  class="page-header font-display">ตลาดนักเตะ</h1>
     <div class="row">  
   
   
<div class="social pull-right" style="margin: -70px 0 10px 0;">
                 <div class="share share_size_large share_type_facebook">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" onclick="fb_share();">Share</a>
                </div>
                 <div class="share share_size_large share_type_twitter">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" onclick="twitter_share();">Tweet</a>
                </div>
               
                <div class="share share_size_large share_type_gplus">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" onclick="google_share();">+1</a>
                </div>
                
                <div class="share share_size_large share_type_email">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="">E-mail</a>
                </div>

                
     </div>


</div>
  
<div class="row">
   <!-- Start LEFT COLUME --> 
  <div class="col-md-12">
                 
			<!-- <div align="center"><iframe src='http://video.kapook.com/content/iframe/54058c3a38217a6678000008' frameborder='0' scrolling='no' width='640' height='360' allowFullScreen></iframe></div> -->
                        <br>
  <div class="">

  
<ul class="pagination">
<?php 
if($ListPage<=3){
$start = 1;
$stop = 5;
$linkback = "";
$linknext = "/transfer-market?list_page=".($ListPage+1)."&news_page=".$NewsPage;
$back = "disabled";
$next = "";
}elseif($ListPage>=($TotalListPage-2)){
$start = $TotalListPage-4;
$stop = $TotalListPage;
$linkback = "/transfer-market?list_page=".($ListPage-1)."&news_page=".$NewsPage;
$linknext = "";
$back = "";
$next = "disabled";
}else{
$start = $ListPage-2;
$stop = $ListPage+2;
$linkback = "/transfer-market?list_page=".($ListPage-1)."&news_page=".$NewsPage;
$linknext = "/transfer-market?list_page=".($ListPage+1)."&news_page=".$NewsPage;
$back = "";
$next = "";
}
?>
  <li class="<?php echo $back; ?>"><a href="<?php echo $linkback; ?>">&laquo;</a></li>
 <?php
 for($i=$start;$i<=$stop;$i++){
	if($i==$ListPage)
		$class = "active";
	else
		$class = "";
 ?>
  <li class="<?php echo $class; ?>"><a href="/transfer-market?list_page=<?php echo $i; ?>&news_page=<?php echo $NewsPage; ?>"><?php echo $i; ?> <span class="sr-only">(current)</span></a></li>
<?php } ?>
  <li class="<?php echo $next; ?>"><a href="<?php echo $linknext; ?>">»</a></li>
</ul>

    <table  class="table  table-striped border-1 ">
      <thead>
        <tr>
            <th class="text_center">วันที่</th>
            <th  colspan="2">ชื่อผู้เล่น และทีมเดิม</th>
          <th class="text_center">ไป</th>
           <th>ทีมปัจจุบัน</th>
          <th class="text_center">ราคา</th>

        </tr>
      </thead>
      <tbody>
      
        <?php foreach($PlayerMarketArr['list'] as $Player){?>
          <tr>
          <td class="text_center">
          <?php 
        
          $tmpDateArr = explode('-',$Player['Date']); 
          echo intval($tmpDateArr[2]).' '.$monthtothai[$tmpDateArr[1]]['short'].' '.($tmpDateArr[0]+543);
          
          if($Player['NewsURL']!=''){?>
              
              <a href="<?php echo $Player['NewsURL'];?>" class="tooltip_top" title="คลิกดูรายละเอียดข่าว"><span class="label label-primary"><i class="fa fa-info-circle"></i> ข่าว</span></a>
          <?php }?>
          </td>
           <td><a href=""><img src="<?php
			if(strlen($Player['PlayerData']['img'])){
				echo $Player['PlayerData']['img'];
			}else{
				echo "http://football.kapook.com/uploads/scorer/noface.jpg";
			} ?>" class="img-circle" title="<?php echo $Player['PlayerData']['name']; ?>"></a></td>
          <td><a href="" ><h2 class="font-display"><?php echo $Player['PlayerData']['name']; ?></h2></a>
          <a href=""><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $Player['TeamOutData']['Logo']); ?>" width="24" height="24"  alt=""/>
		  <?php
			if(!empty($Player['TeamOutData']['NameTH'])){
				$strTeam	=	$Player['TeamOutData']['NameTH'];
			}else if(!empty($Player['TeamOutData']['NameTHShort'])){
				$strTeam	=	$Player['TeamOutData']['NameTHShort'];
			}else{
				$strTeam	=	$Player['TeamOutData']['NameEN'];
			} 
			echo $strTeam; ?></a>
          </td>
          <td><i class="fa fa-angle-right"></i></td>
           <td> <a href=""><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $Player['TeamInData']['Logo']); ?>" width="100" height="100"  alt=""/>
		  <?php
			if(!empty($Player['TeamInData']['NameTH'])){
				$strTeam	=	$Player['TeamInData']['NameTH'];
			}else if(!empty($Player['TeamInData']['NameTHShort'])){
				$strTeam	=	$Player['TeamInData']['NameTHShort'];
			}else{
				$strTeam	=	$Player['TeamInData']['NameEN'];
			} 
			echo $strTeam; ?></a>
			</td>
            <td>
             <?php if($Player['Price']==''){ echo '-'; }else{ echo $Player['Price']; }?>&nbsp;
            <span class="unit"><?php echo $Player['PriceText']; ?></span>
            
            </td>
        
        
        </tr>


          <?php
        }
           ?>
        
      </tbody>
    </table> 

    </div> 


   
        
<div class="row subnews">
            <h2 class="font-display"><i class="fa fa-file-text-o"></i> ข่าวซื้อขายนักเตะ</h2> 
            
            
		<?php for($i=0;$i<=7;$i++){ 
                        if(($i%4)==0){ echo '<div class="row">'; }
                    ?>
              <div class="news_block  col-md-3" ><a class="" href="/news-<?php echo $NewsTransferArr[$i]['content_id']; ?>" target="_blank"><img class="media-object" alt="" src="<?php echo $NewsTransferArr[$i]['picture']; ?>" ><span class="date"><i class="fa fa-calendar"></i>  <?php echo "วันที่ ".date("d", $NewsTransferArr[$i]['CreateDate'])." ".$monthtothai[date("M", $NewsTransferArr[$i]['CreateDate'])]." ".(date("Y", $NewsTransferArr[$i]['CreateDate'])+543); ?></span> <?php echo $NewsTransferArr[$i]['subject']; ?> </a></div>
		<?php 
                        if(($i%4)==3){ echo '</div>'; }
                        } ?>

              
              

</div>
     
     <ul class="pagination">
<?php 
if($NewsPage<=3){
$start = 1;
$stop = 5;
$linkback = "";
$linknext = "/transfer-market?list_page=".$ListPage."&news_page=".($NewsPage+1);
$back = "disabled";
$next = "";
}elseif($NewsPage>=($NewsPageTotal-2)){
$start = $NewsPageTotal-4;
$stop = $NewsPageTotal;
$linkback = "/transfer-market?list_page=".$ListPage."&news_page=".($NewsPage-1);
$linknext = "";
$back = "";
$next = "disabled";
}else{
$start = $NewsPage-2;
$stop = $NewsPage+2;
$linkback = "/transfer-market?list_page=".$ListPage."&news_page=".($NewsPage-1);
$linknext = "/transfer-market?list_page=".$ListPage."&news_page=".($NewsPage+1);
$back = "";
$next = "";
}
?>
  <li class="<?php echo $back; ?>"><a href="<?php echo $linkback; ?>">&laquo;</a></li>
 <?php
 for($i=$start;$i<=$stop;$i++){
	if($i==$NewsPage)
		$class = "active";
	else
		$class = "";
 ?>
  <li class="<?php echo $class; ?>"><a href="/transfer-market?list_page=<?php echo $ListPage; ?>&news_page=<?php echo i; ?>"><?php echo $i; ?> <span class="sr-only">(current)</span></a></li>
<?php } ?>
  <li class="<?php echo $next; ?>"><a href="<?php echo $linknext; ?>">»</a></li>
</ul>
  
  </div>

</div>




