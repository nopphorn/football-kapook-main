
<?php

	$monthtothai['01'] ="ม.ค.";
	$monthtothai['02'] ="ก.พ.";
	$monthtothai['03'] ="มี.ค.";
	$monthtothai['04'] ="เม.ย.";
	$monthtothai['05'] ="พ.ค.";
	$monthtothai['06'] ="มิ.ย.";
	$monthtothai['07'] ="ก.ค.";
	$monthtothai['08'] ="ส.ค.";
	$monthtothai['09'] ="ก.ย.";
	$monthtothai['10'] ="ต.ค.";
	$monthtothai['11'] ="พ.ย.";
	$monthtothai['12'] ="ธ.ค.";
	
	$daytothai['0'] = "อาทิตย์";
	$daytothai['1'] = "จันทร์";
	$daytothai['2'] = "อังคาร";
	$daytothai['3'] = "พุธ";
	$daytothai['4'] = "พฤหัสบดี";
	$daytothai['5'] = "ศุกร์";
	$daytothai['6'] = "เสาร์";
	
	$OddsRate["0"] 		= "เสมอ";
	$OddsRate["0.25"] 	= "เสมอควบครึ่ง";
	$OddsRate["0.5"] 	= "ครึ่งลูก";
	$OddsRate["0.75"] 	= "ครึ่งควบลูก";
	$OddsRate["1"] 		= "หนึ่งลูก";
	$OddsRate["1.25"] 	= "ลูกควบลูกครึ่ง";
	$OddsRate["1.5"] 	= "ลูกครึ่ง";
	$OddsRate["1.75"] 	= "ลูกครึ่งควบสองลูก";
	$OddsRate["2"] 		= "สองลูก";
	$OddsRate["2.25"] 	= "สองลูกควบสองลูกครึ่ง";
	$OddsRate["2.5"] 	= "สองลูกครึ่ง";
	$OddsRate["2.75"] 	= "สองลูกครึ่งควบสามลูก";
	$OddsRate["3"] 		= "สามลูก";
	$OddsRate["3.25"] 	= "สามลูกควบสามลูกครึ่ง";
	$OddsRate["3.5"] 	= "สามลูกครึ่ง";
	$OddsRate["3.75"] 	= "สามลูกครึ่งควบสี่ลูก";
	$OddsRate["4"] 		= "สี่ลูก";
	$OddsRate["4.25"] 	= "สี่ลูกควบสี่ลูกครึ่ง";
	$OddsRate["4.5"] 	= "สี่ลูกครึ่ง";
	$OddsRate["4.75"] 	= "สี่ลูกครึ่งควบห้าลูก";
	$OddsRate["5"] 		= "ห้าลูก";
	$OddsRate["5.25"] 	= "ห้าลูกควบห้าลูกครึ่ง";
	$OddsRate["5.5"] 	= "ห้าลูกครึ่ง";
	$OddsRate["5.75"] 	= "ห้าลูกครึ่งควบหกลูก";
	$OddsRate["6"] 		= "หกลูก";
	$OddsRate["6.25"] 	= "หกลูกควบหกลูกครึ่ง";
	$OddsRate["6.5"] 	= "หกลูกครึ่ง";
	$OddsRate["6.75"] 	= "หกลูกครึ่งควบเจ็ดลูก";
	$OddsRate["7"] 		= "เจ็ดลูก";
	$OddsRate["7.25"] 	= "เจ็ดลูกควบเจ็ดลูกครึ่ง";
	$OddsRate["7.5"] 	= "เจ็ดลูกครึ่ง";
	$OddsRate["7.75"] 	= "เจ็ดลูกครึ่งควบแปดลูก";
	$OddsRate["8"] 		= "แปดลูก";
	$OddsRate["8.25"] 	= "แปดลูกควบแปดลูกครึ่ง";
	$OddsRate["8.5"] 	= "แปดลูกครึ่ง";
	$OddsRate["8.75"] 	= "แปดลูกครึ่งควบเก้าลูก";
	$OddsRate["9"] 		= "เก้าลูก";
	$OddsRate["9.25"] 	= "เก้าลูกควบเก้าลูกครึ่ง";
	$OddsRate["9.5"] 	= "เก้าลูกครึ่ง";
	$OddsRate["9.75"] 	= "เก้าลูกครึ่งควบสิบลูก";
	$OddsRate["10"] 	= "สิบลูก";

	$MatchArr = $this->MatchAnalyzeContensByDate;
	$MatchLiveArr = $this->MatchLiveContents;
	$NewsArr = $this->NewsContents;
	$NewsThaiArr =	$this->NewsThaiListContents;
	$NewsTop10Arr = $this->NewsTop10Contents;
	$NewsTopHitsArr = $this->NewsTopHitsContents;
	$TopplaysideList = $this->TopPlaySide['data'];
	$MiniRanking = $this->RankingDaily['data'];
	
	$TotalMatchLive = count($MatchLiveArr);
	for($i=0;$i<$TotalMatchLive;$i++){
		$tmpMatch = $MatchLiveArr[$i];
		$MatchLiveByLeagueIDArr[$tmpMatch['KPLeagueID']][] = $tmpMatch;    
	}
	
	$AllLeagueByOrderArr = $this->LiveScoreContents['KPLeagueIDOrder'];
	ksort($AllLeagueByOrderArr);
	$index=0;
	foreach($AllLeagueByOrderArr as $OrderIndex => $LeagueID){
		if(isset($MatchLiveByLeagueIDArr[$LeagueID])){
			$index+=1; 
			$LeagueLiveByIndexArr[$index]['ID']     = $LeagueID;
			$LeagueLiveByIndexArr[$index]['Name']   = $this->mem_lib->get('Football2014-League-NameTHShort-'.$LeagueID);
			$LeagueLiveByIndexArr[$index]['Zone']  	= $this->LiveScoreContents['KPLeagueZoneID'][$LeagueID];
		}
	}

	$ClassColorByMatchStatus['Fin'] = 'btn-grey';
    $ClassColorByMatchStatus['1 HF'] = 'btn-success';
    $ClassColorByMatchStatus['2 HF'] = 'btn-success';
    $ClassColorByMatchStatus['Sched'] = '';
    $ClassColorByMatchStatus['H/T'] = 'btn-warning';
?>

  <div class="container">
  
  
<div class="row">
	
   <!-- Start LEFT COLUME --> 
  <div class="col-md-8">
  <h2 class="font-display">ข่าว</h2>
  <div id="carousel-featured" class="carousel slide" data-ride="carousel" >
    
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-featured" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-featured" data-slide-to="1"></li>
        <li data-target="#carousel-featured" data-slide-to="2"></li>
		<li data-target="#carousel-featured" data-slide-to="3"></li>
      </ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<?php 
			for($i=0;$i<=3;$i++){
				$News = $NewsArr[$i];
			?>  
				<div class="item <?php if($i==0){echo 'active'; }?>">
					<a href="<?php echo $News['link']; ?>" target="_blank"><img src="<?php echo $News['picture']; ?>" alt="<?php echo $News['subject']; ?>">
				  <div class="carousel-caption">
						<h2 ><?php echo $News['subject']; ?></h2>
						<p><?php echo $News['title']; ?></p>
				  </div>
				  </a>
				</div>
			<?php } ?>
		</div>
		<a class="right carousel-control" href="#carousel-featured" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
    </div>
  
  <div class="row subnews">
        <?php for($i=4;$i<count($NewsArr);$i++){ $News = $NewsArr[$i]; ?>
            <div class="news_block  col-md-3" ><a class="post.php" href="<?php echo $News['link']; ?>" target="_blank"><img class="media-object" alt="<?php echo $News['subject']; ?>" src="<?php echo $News['picture']; ?>" ><span class="date">   <?php
				echo $daytothai[date('w',$News['CreateDate'])] . ', ' . date('j',$News['CreateDate']) . ' ' . $monthtothai[date('m',$News['CreateDate'])] . ' ' . (date('Y',$News['CreateDate'])+543);
			?> </span><?php echo $News['subject']; ?> </a></div>
        <?php } ?>    

 	    <div class="col-md-12 line faa-parent animated-hover" > <a href="<?php echo BASE_HREF; ?>newslist.php"  class="more faa-vertical waves-effect waves-block"><i class="fa fa-plus "></i></a></div>
  </div>
  
  <a href="http://football.kapook.com/suzukicup"><img src="<?php echo BASE_HREF; ?>image-content/banner_aff_frontpage.jpg" ></a>
    <h2 class="font-display"><img src="<?php echo BASE_HREF; ?>assets/img/flags/24/Thailand.png" border="0" width="24" height="24" alt="Football Thai News"> ข่าวบอลไทย</h2>
    
    <div class="row subnews">
		<div class="news_block  col-md-6" ><a class="post.php" href="<?php echo $NewsThaiArr[0]['link']; ?>"><img class="media-object" alt="<?php echo $NewsThaiArr[0]['subject']; ?>" src="<?php echo $NewsThaiArr[0]['picture']; ?>" > <span class="date">  <?php
			echo $daytothai[date('w',$NewsThaiArr[0]['CreateDate'])] . ', ' . date('j',$NewsThaiArr[0]['CreateDate']) . ' ' . $monthtothai[date('m',$NewsThaiArr[0]['CreateDate'])] . ' ' . (date('Y',$NewsThaiArr[0]['CreateDate'])+543);
		?> </span> <h3><?php echo $NewsThaiArr[0]['subject']; ?> </h3></a></div>
		<div class="col-md-6">
			<div class="news_block  col-md-6" ><a class="" href="<?php echo $NewsThaiArr[1]['link']; ?>"><img class="media-object" alt="<?php echo $NewsThaiArr[1]['subject']; ?>" src="<?php echo $NewsThaiArr[1]['picture']; ?>" ><span class="date">  <?php
			echo $daytothai[date('w',$NewsThaiArr[1]['CreateDate'])] . ', ' . date('j',$NewsThaiArr[1]['CreateDate']) . ' ' . $monthtothai[date('m',$NewsThaiArr[1]['CreateDate'])] . ' ' . (date('Y',$NewsThaiArr[1]['CreateDate'])+543);
		?></span><?php echo $NewsThaiArr[1]['subject']; ?> </a></div>
			<div class="news_block  col-md-6" ><a class="" href="<?php echo $NewsThaiArr[2]['link']; ?>"><img class="media-object" alt="<?php echo $NewsThaiArr[2]['subject']; ?>" src="<?php echo $NewsThaiArr[2]['picture']; ?>" ><span class="date">  <?php
			echo $daytothai[date('w',$NewsThaiArr[2]['CreateDate'])] . ', ' . date('j',$NewsThaiArr[2]['CreateDate']) . ' ' . $monthtothai[date('m',$NewsThaiArr[2]['CreateDate'])] . ' ' . (date('Y',$NewsThaiArr[2]['CreateDate'])+543);
		?></span><?php echo $NewsThaiArr[2]['subject']; ?> </a></div>
        <div class="clearfix"></div>
            <div class="news_block  col-md-6" ><a class="" href="<?php echo $NewsThaiArr[1]['link']; ?>"><img class="media-object" alt="<?php echo $NewsThaiArr[3]['subject']; ?>" src="<?php echo $NewsThaiArr[3]['picture']; ?>" ><span class="date">  <?php
			echo $daytothai[date('w',$NewsThaiArr[3]['CreateDate'])] . ', ' . date('j',$NewsThaiArr[3]['CreateDate']) . ' ' . $monthtothai[date('m',$NewsThaiArr[3]['CreateDate'])] . ' ' . (date('Y',$NewsThaiArr[3]['CreateDate'])+543);
		?></span><?php echo $NewsThaiArr[3]['subject']; ?> </a></div>
            <div class="news_block  col-md-6" ><a class="" href="<?php echo $NewsThaiArr[2]['link']; ?>"><img class="media-object" alt="<?php echo $NewsThaiArr[4]['subject']; ?>" src="<?php echo $NewsThaiArr[4]['picture']; ?>" ><span class="date">    <?php
			echo $daytothai[date('w',$NewsThaiArr[4]['CreateDate'])] . ', ' . date('j',$NewsThaiArr[4]['CreateDate']) . ' ' . $monthtothai[date('m',$NewsThaiArr[4]['CreateDate'])] . ' ' . (date('Y',$NewsThaiArr[4]['CreateDate'])+543);
		?></span><?php echo $NewsThaiArr[4]['subject']; ?> </a></div>
		</div>
		<div class="col-md-12 line faa-parent animated-hover" > <a href="<?php echo BASE_HREF; ?>ไทยพรีเมียร์ลีก/news" class="more faa-vertical waves-effect waves-block"><i class="fa fa-plus "></i></a></div>
	</div>
  
  
  	<!--TOP 10-->
    <div class="row top10">
    		<div class=" col-md-12" ><h2 class="font-display">บทความฟุตบอล</h2></div>
  			<div class="news_block  col-md-4" ><a class="" href="<?php echo BASE_HREF; ?>news-<?php echo $NewsTop10Arr[0]['content_id']; ?>" target="_blank"><img class="media-object" alt="" src="<?php echo $NewsTop10Arr[0]['picture']; ?>" ><?php echo $NewsTop10Arr[0]['subject']; ?></a></div>
            <div class="news_block  col-md-4" ><a class="" href="<?php echo BASE_HREF; ?>news-<?php echo $NewsTop10Arr[1]['content_id']; ?>" target="_blank"><img class="media-object" alt="" src="<?php echo $NewsTop10Arr[1]['picture']; ?>" ><?php echo $NewsTop10Arr[1]['subject']; ?></a></div>
            <div class="news_block  col-md-4" ><a class="" href="<?php echo BASE_HREF; ?>news-<?php echo $NewsTop10Arr[2]['content_id']; ?>" target="_blank"><img class="media-object" alt="" src="<?php echo $NewsTop10Arr[2]['picture']; ?>" ><?php echo $NewsTop10Arr[2]['subject']; ?></a></div>
 			<div class="col-md-12 line faa-parent animated-hover" > <a href="/feature" class="more faa-vertical" target="_blank"><i class="fa fa-plus "></i></a></div>
  </div>
  
  <!--วิเคราะห์-->
    <div class="row analytor">
    <br>
    <div class=" col-md-12" ><h2 class="font-display"><i class="fa fa-tachometer"></i> วิเคราะห์บอล &amp; ทีเด็ดฟุตบอล</h2> </div>
    <!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
	<?php
		
		if(date('G') >= 6){
			$date = strtotime(date("Y-m-d"));
			$Yesterday = strtotime(date("Y-m-d",strtotime(' - 1 days')));
		}else{
			$date = strtotime(date("Y-m-d",strtotime(' - 1 days')));
			$Yesterday = strtotime(date("Y-m-d",strtotime(' - 2 days')));
		}
		
		if( ((date('G') >= 6) && (date('G') < 12)) || (!isset($MatchArr["Today"])) ){
			$today_class = '';
			$yesterday_class = ' class="active"';
			$today_tap = 'class="tab-pane"';
			$yesterday_tap = 'class="tab-pane active"';
		}else{
			$today_class = ' class="active"';
			$today_tap = 'class="tab-pane active"';
			$yesterday_tap = 'class="tab-pane"';
			$yesterday_class = '';
		}
	?>
  <li <?php echo $today_class; ?>><a href="#home" role="tab" data-toggle="tab">วิเคราะห์บอลวันนี้ <?php echo date("d",$date)." ".$monthtothai[date("m",$date)]." ".(date("Y",$date)+543); ?></a></li>
  <li <?php echo $yesterday_class; ?>><a href="#profile" role="tab" data-toggle="tab">สรุปผลทีเด็ดเมื่อวานนี้ <?php echo date("d",$Yesterday)." ".$monthtothai[date("m",$Yesterday)]." ".(date("Y",$Yesterday)+543); ?></a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div <?php echo $today_tap; ?> id="home"><table class="table table-striped border-1">
            <thead>
        <tr>
        
        
      <th> เวลา</th><th></th><th></th><th></th><th>ราคาบอล</th><th> ทีเด็ด</th><th>ผล</th>
        </tr>
      </thead>
      <tbody>
	  <?php 
		foreach($MatchArr["Today"] as $MatchTmp){

		?>
        <tr>
          
          <td><?php echo $MatchTmp['MatchTime']; ?></td>
		  <td class="text_right"><a href="<?php echo BASE_HREF; ?>team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team1KPID']))); ?>" class="tooltip_top match <?php if(($MatchTmp['TeamOdds']==0 || $MatchTmp['TeamOdds']==1)&&($MatchTmp['Odds']!=-1)){ echo 'handicap_color'; } ?>" title="" data-original-title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><?php echo $MatchTmp['Team1'];?> <img src="<?php echo $MatchTmp['Team1Logo'];?>" width="24"></span></a></td>
          <td class="text_center"><?php if($MatchTmp['MatchStatus']=='Sched'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">vs</a>
              <?php }else if($MatchTmp['MatchStatus']=='Fin'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="btn btn-grey btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php echo $MatchTmp['Team1FTScore']; ?> - <?php echo $MatchTmp['Team2FTScore']; ?></a>
              <?php }else if($MatchTmp['MatchStatus']=='Canc'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">c - c</a>
              <?php }else if($MatchTmp['MatchStatus']=='Post'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">p - p</a>
              <?php }else{?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">vs</a>
              <?php }?></td>
          <td class="text_left"><a href="<?php echo BASE_HREF; ?>team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team2KPID']))); ?>" class="tooltip_top match <?php if(($MatchTmp['TeamOdds']==0 || $MatchTmp['TeamOdds']==2)&&($MatchTmp['Odds']!=-1)){ echo 'handicap_color'; } ?>" title="" data-original-title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><img src="<?php echo $MatchTmp['Team2Logo'];?>" width="24"><?php echo $MatchTmp['Team2'];?></span></a></td>
          <td><?php 
               if($MatchTmp['Odds']==0){   
                   echo '(เสมอ)';                   
               }else if($MatchTmp['Odds']!=-1){
                   $OddsArr = explode('.',$MatchTmp['Odds']);
                   if($OddsArr[0]>0){
                       for($o=1;$o<=$OddsArr[0];$o++){
                           echo '<pie class="hundreds"></pie>';
                       }
                   }
                   
                   if($OddsArr[1]=='25'){
                       echo '<pie class="twentyfive"></pie>';
                   }else if($OddsArr[1]=='5'){
                       echo '<pie class="fifty"></pie>';
                   }else if($OddsArr[1]=='75'){
                       echo '<pie class="seventyfive"></pie>';
                   }
                   
                   echo ' ('.$MatchTmp['Odds'].')';
               }?>
          </td>
          <td><?php echo $MatchTmp['Predict']; ?></td>
          <td>
		<?php
			if(isset($MatchTmp['IsSelectCollect'])){
				if($MatchTmp['IsSelectCollect']==1)
				{
					echo '<i class="fa fa-check" style="color:green"></i>';
				}
				else if($MatchTmp['IsSelectCollect']==-1)
				{
					echo '<i class="fa fa-close" style="color:red"></i>';
				}
				else
				{
					echo '<i class="fa fa-minus" style="color:gray"></i>';
				}
			}else{
				echo '<i class="fa fa-minus" style="color:gray"></i>';
			}
		?>
		  </td>
        </tr>
		<?php } ?>
      </tbody>
    </table></div>
	  <div <?php echo $yesterday_tap; ?> id="profile"><table class="table table-striped border-1">
      <thead>
        <tr>
        
        
      <th> เวลา</th><th></th><th></th><th></th><th>ราคาบอล</th><th> ทีเด็ด</th><th>ผล</th>
        </tr>
      </thead>
      <tbody>
	  <?php 
		foreach($MatchArr["Yesterday"] as $MatchTmp){

		?>
        <tr>
          
          <td><?php echo $MatchTmp['MatchTime']; ?></td>
		  <td class="text_right"><a href="<?php echo BASE_HREF; ?>team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team1KPID']))); ?>" class="tooltip_top match <?php if(($MatchTmp['TeamOdds']==0 || $MatchTmp['TeamOdds']==1)&&($MatchTmp['Odds']!=-1)){ echo 'handicap_color'; } ?>" title="" data-original-title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><?php echo $MatchTmp['Team1'];?> <img src="<?php echo $MatchTmp['Team1Logo'];?>" width="24"></span></a></td>
		  <td class="text_center"><?php if($MatchTmp['MatchStatus']=='Sched'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">vs</a>
              <?php }else if($MatchTmp['MatchStatus']=='Fin'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="btn btn-grey btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php echo $MatchTmp['Team1FTScore']; ?> - <?php echo $MatchTmp['Team2FTScore']; ?></a>
              <?php }else if($MatchTmp['MatchStatus']=='Canc'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">c - c</a>
              <?php }else if($MatchTmp['MatchStatus']=='Post'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">p - p</a>
              <?php }else{?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">vs</a>
              <?php }?></td>
          <td class="text_left"><a href="<?php echo BASE_HREF; ?>team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team2KPID']))); ?>" class="tooltip_top match <?php if(($MatchTmp['TeamOdds']==0 || $MatchTmp['TeamOdds']==2)&&($MatchTmp['Odds']!=-1)){ echo 'handicap_color'; } ?>" title="" data-original-title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><img src="<?php echo $MatchTmp['Team2Logo'];?>" width="24"><?php echo $MatchTmp['Team2'];?></span></a></td>
          <td><?php 
               if($MatchTmp['Odds']==0){   
                   echo '(เสมอ)';                   
               }else if($MatchTmp['Odds']!=-1){
                   $OddsArr = explode('.',$MatchTmp['Odds']);
                   if($OddsArr[0]>0){
                       for($o=1;$o<=$OddsArr[0];$o++){
                           echo '<pie class="hundreds"></pie>';
                       }
                   }
                   
                   if($OddsArr[1]=='25'){
                       echo '<pie class="twentyfive"></pie>';
                   }else if($OddsArr[1]=='5'){
                       echo '<pie class="fifty"></pie>';
                   }else if($OddsArr[1]=='75'){
                       echo '<pie class="seventyfive"></pie>';
                   }
                   
                   echo ' ('.$MatchTmp['Odds'].')';
               }?>
          </td>
          <td><?php echo $MatchTmp['Predict']; ?></td>
          <td>
		<?php
			if(isset($MatchTmp['IsSelectCollect']))
			{
				if($MatchTmp['IsSelectCollect']==1)
				{
					echo '<i class="fa fa-check" style="color:green"></i>';
				}
				else if($MatchTmp['IsSelectCollect']==-1)
				{
					echo '<i class="fa fa-close" style="color:red"></i>';
				}
				else
				{
					echo '<i class="fa fa-minus" style="color:gray"></i>';
				}
			}
			else
			{
				echo '<i class="fa fa-minus" style="color:gray"></i>';
			}
		?>
		  </td>
        </tr>
		<?php } ?>
      </tbody>
    </table></div>
</div>
    <div class="col-md-12 line faa-parent animated-hover" > <a href="<?php echo BASE_HREF; ?>วิเคราะห์บอล" class="more faa-vertical" target="_blank"><i class="fa fa-plus "></i></a></div>
</div>

<?php if(true){ ?>
<div class="row index-topscore">
	<div class="game-head-sep">
		<a href="<?php echo BASE_HREF; ?>games" class="btn-link">เข้าสู่หน้าเกม <i class="fa fa-arrow-circle-right"></i></a>
		<a href="<?php echo BASE_HREF; ?>games" class="game-logo"><div class="game-logo-season">พรีซีซั่น 2014-15</div></a>
	</div>
	<div class="col-sm-6">
		<h2 class="font-display"><i class="fa fa-trophy" style="color: orange;"></i> แต้มสูงสุดประจำวัน</h2>
		<?php if(isset($MiniRanking[0])) {
			$tmpData		=		$MiniRanking[0]; ?>
			<div class="hilight clearfix">
				<div class="col-sm-5 text_center"><p style="margin-top: 20px; margin-bottom: 0px;"><?php echo $tmpData['photo']; ?></p>
				<?php echo $tmpData['name']; ?>
				<br>
				<a href="<?php echo BASE_HREF; ?>profile/<?php echo $tmpData['name_no_link']; ?>" class="btn btn-default ">ดูประวัติ</a>
				</div>
				<div class="col-sm-3 text_center">
					<div style="display: block; width: 80px; height: 80px; position: relative; margin: 0 auto;">
						<div id="doughnutChart2" class="chart small-80"></div>
					</div>
					บอลเดี่ยว
				</div>
				<div class="col-sm-3  text_center">
					<div style="display: block; width: 100px; height: 100px; position: relative; margin: 0 auto;">
						<div id="doughnutChart" class="chart small-80"></div>
					</div>
					บอลชุด               
				</div>
				<div class="col-sm-7 text_center" style="font-size: 18px;">
					<div class="statistic">
						<span class="point"><?php echo number_format($tmpData['gameD']); ?>  </span>แต้ม  <br>
						<span class="rank">ความแม่น : <?php echo $tmpData['gameA']; ?>  </span>
					</div>
				</div>
																			 
				<span class="position">1</span>
			</div>
																			
			<link rel="stylesheet" type="text/css" href="<?php echo BASE_HREF; ?>assets/plugins/donut/donut.css">
			<script type="text/javascript" src="<?php echo BASE_HREF; ?>assets/plugins/donut/donut.js"></script>
			<script type="">
				$(document).ready(function(){  
					$(function(){
						$("#doughnutChart").drawDoughnutChart([
							<?php if($tmpData['multi_win']>0){?>
								{ title: "เข้าเต็ม",			value : <?php echo intval($tmpData['multi_win']); ?>,			color: "#6DB600" },
							<?php } ?>
							
							<?php if($tmpData['multi_draw']>0){?>
								{ title: "เสมอ",			value : <?php echo intval($tmpData['multi_draw']); ?>,			color: "#CCC" },
							<?php } ?>
							
							<?php if($tmpData['multi_lose']>0){?>
								{ title: "เสีย",			value : <?php echo intval($tmpData['multi_lose']); ?>,			color: "#FF0000" }
							<?php } ?>
						],{total:"<?php echo $tmpData['multi_percent']; ?>"});
					});
					
					$(function(){
						$("#doughnutChart2").drawDoughnutChart([
							<?php if($tmpData['single_win_full']>0){?>
								{ title: "ได้เต็ม",			value : <?php echo intval($tmpData['single_win_full']); ?>,		color: "#6DB600" },
							<?php } ?>
							
							<?php if($tmpData['single_win_half']>0){?>
								{ title: "ได้ครึ่ง",			value : <?php echo intval($tmpData['single_win_half']); ?>,		color: "#b8e54d" },
							<?php } ?>
							
							<?php if($tmpData['single_draw']>0){?>
								{ title: "เสมอ",			value : <?php echo intval($tmpData['single_draw']); ?>,			color: "#CCC" },
							<?php } ?>
							
							<?php if($tmpData['single_lose_half']>0){?>
								{ title: "เสียครึ่ง",			value : <?php echo intval($tmpData['single_lose_half']); ?>,	color: "#fe9900" },
							<?php } ?>
							
							<?php if($tmpData['single_lose_full']>0){?>
								{ title: "เสียเต็ม",			value : <?php echo intval($tmpData['single_lose_full']); ?>,	color: "#FF0000" }
							<?php } ?>
						],{total:"<?php echo $tmpData['single_percent']; ?>"});
					});
				});  
			</script>
		<?php } 
		
		if(count($MiniRanking) > 0){
		
		?>
			<p>
				<table class="table  short-ranking">
					<thead>
						<tr>
							<th>อันดับ  </th> <th></th> <th></th> <th>ความแม่น</th><th>แต้ม</th>
						</tr>
					</thead>
					<tbody>
						<?php
						for($r=2;$r<=4;$r++) {
							if(isset($MiniRanking[($r-1)])) {
								$tmpData		=		$MiniRanking[($r-1)]; ?>
								<tr>
									<td><?php echo $r ?></td>
									<td><?php echo $tmpData['photo']; ?></td>
									<td><?php echo $tmpData['name']; ?></td>
									<td class="text_center"><?php echo $tmpData['gameA']; ?></td>
									<td><?php echo number_format($tmpData['gameD']); ?></td>
								</tr>
								
						<?php  
							}
						}
						if(isset($this->MyInfoScoreMember)){
						?><tr>
							<td><?php echo $this->MyRankingDaily['rank']; ?></td>
							<td><a href="<?php echo BASE_HREF . 'profile/' . $this->MyInfoScoreMember['username']; ?>" class="game-user-avartar img-circle" title="<?php echo $this->MyInfoScoreMember['username']; ?>" style="background: url(<?php echo $this->MyInfoScoreMember['picture']; ?>) no-repeat; background-size: cover;"></a></td>
							<td><a href="<?php echo BASE_HREF . 'profile/' . $this->MyInfoScoreMember['username']; ?>"><?php echo $this->MyInfoScoreMember['username']; ?></a></td>
							<td class="text_center"><?php echo $this->MyRankingDaily['rate']; ?></td>
							<td><?php echo $this->MyRankingDaily['score']; ?></td>
						</tr><?php
						} ?> 
					</tbody>
				</table>
			</p>
			<div class="col-md-12 line faa-parent animated-hover">
				<a href="http://football.kapook.com/games/ranking" class="more faa-vertical" target="_blank">
					<i class="fa fa-plus "></i>
				</a>
			</div>
		<?php }else{ ?>
			<p>
				<table class="table  short-ranking">
					<thead>
						<tr>
							<th colspan="5" style="text-align: center;">รอผลการแข่งขัน</th>
						</tr>
					</thead>
				</table>
			</p>
		<?php } ?>
		<!--<div class="text_center"> <a href="game-ranking.php" class="btn btn-primary ranking-link">+ ดูอันดับทั้งหมดคลิก !</a></div> -->
	</div>
	<div class="col-sm-6"></div>
	<div class="col-sm-6 top-selected-match">
		<h2 class="font-display"><i class="fa fa-flag-checkered" style="color: orange;"></i> แมตช์ที่คนทายมากที่สุดวันนี้</h2>
		<?php
		$isFirst	=	false;
		if(count($TopplaysideList)>0) {
			foreach($TopplaysideList as $matchData){
				if($isFirst==false){
				?>
					<a href="<?php echo $matchData['MatchPageURL']; ?>">
						<div class="row">
							<div class="col-md-5 text_center"><img src="<?php echo $matchData['Team1Logo']; ?>" width="72"><br><span class="teamname"><?php echo $matchData['Team1']; ?></span></div>
							<div class="col-md-2 text_center" style=" font-size: 24px;"><b>vs</b></div>
							<div class="col-md-5 text_center"><img src="<?php echo $matchData['Team2Logo']; ?>" width="72"> <br><span class="teamname"><?php echo $matchData['Team2']; ?></span></div>
						</div>
						<div class="progress">
							<div class="progress-bar" style="width: <?php echo $matchData['count_playside_team1_percent']; ?>%"><?php echo $matchData['count_playside_team1_percent']; ?>%</div>
							<div class="progress-bar progress-bar-danger" style="width: <?php echo $matchData['count_playside_team2_percent']; ?>%"><?php echo $matchData['count_playside_team2_percent']; ?>%</div>
						</div>
					</a>
				<?php
					$isFirst = true;
				}else{
				?>
					<a href="<?php echo $matchData['MatchPageURL']; ?>">
						<div class="row">
							<div class="col-md-5 text_right"><?php echo $matchData['Team1']; ?> <img src="<?php echo $matchData['Team1Logo']; ?>" width="32"><span class="teamname"></span></div>
							<div class="col-md-2 text_center"><b>vs</b></div>
							<div class="col-md-5 text_left"><img src="<?php echo $matchData['Team2Logo']; ?>" width="32"> <span class="teamname"><?php echo $matchData['Team2']; ?></span> </div>
						</div>
						<div class="progress">
							<div class="progress-bar" style="width: <?php echo $matchData['count_playside_team1_percent']; ?>%"><?php echo $matchData['count_playside_team1_percent']; ?>%</div>
							<div class="progress-bar progress-bar-danger" style="width: <?php echo $matchData['count_playside_team2_percent']; ?>%"><?php echo $matchData['count_playside_team2_percent']; ?>%</div>
						</div>
					</a>
				<?php
				}
			}
		}else{ ?>
			<p>
				<table class="table  short-ranking">
					<thead>
						<tr>
							<th colspan="5" style="text-align: center;">ยังไม่มีการทายผล</th>
						</tr>
					</thead>
				</table>
			</p>
		<?php } ?>
	</div>
	<a href="<?php echo BASE_HREF; ?>games"><img src="<?php echo BASE_HREF; ?>assets/img/front_game_bottom.jpg" ></a>    
</div>
<?php } ?>
  
</div>
<!--End Col 8 LEFT VOLUME-->
<!--Start Right Side Colume-->


<!--ข่าวฮิตประจำสัปดาห์ -->
  <div class="col-md-4 aside">
  <?php include dirname(__FILE__)."../../sidebar.tpl.php"; ?>
  </div><!--End right 4 Aside -->

</div>




