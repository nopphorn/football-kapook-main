<?php 
//if ($_GET['relate']) { echo urldecode($_SERVER['REQUEST_URI']); }
switch (urldecode($_SERVER['REQUEST_URI'])) {
    case '/transfer-market' :
        $arr_word = 'arr_word[0]=ข่าวซื้อขายนักเตะ&arr_word[1]=ตลาดซื้อขายนักเตะ&arr_word[2]=นักฟุตบอล';
        break;
    case '/พรีเมียร์ลีก' :
        $arr_word = 'arr_word[0]=พรีเมียร์ลีก&arr_word[1]=ข่าวพรีเมียร์ลีก&arr_word[2]=ไฮไลท์พรีเมียร์ลีก';
        break;
    default : 
        $arr_word = 'arr_word[0]=นักฟุตบอล&arr_word[1]=สโมสรฟุตบอล&arr_word[2]=ไฮไลท์ฟุตบอล';
        break;
}
?>
<script src="http://my.kapook.com/angular/angular_1.3.10.min.js" type="text/javascript"></script>
<script src="http://my.kapook.com/angular/angular.ng-modules.js" type="text/javascript"></script>

<link href="http://my.kapook.com/vdoworld/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="http://my.kapook.com/fonts/display/fontface.css" rel="stylesheet" type="text/css" media="all">
<link href="http://my.kapook.com/vdoworld/vdo_world.css" rel="stylesheet">

<script>
    var url = "http://cms.kapook.com/content_relate/get_json_by_word/world/box_1/video/7?<?php echo $arr_word; ?>&callback=JSON_CALLBACK";
    var worldVDOApp = angular.module('worldVDOApp', []);
    
    worldVDOApp.run(function($rootScope) {
        $rootScope.loading = true;
    });
    
    worldVDOApp.controller('worldVDOListCtrl', function($scope, $rootScope, $http) {
        $http.jsonp(url)
        .success(function(data) {
			for (x in data) {
				var tmpStr = data[x].img;
				if(typeof tmpStr == 'string') {
					if(x==0){
						tmpStr = tmpStr.replace("default", "hqdefault");
					}else{
						tmpStr = tmpStr.replace("default", "mqdefault");
					}
				}
				data[x].img = tmpStr;
			}
            $scope.vdo = data;
            $rootScope.loading = false;
        });
    });
</script>

<article class="int_vdo_world full_w" style="display:block;" ng-app="worldVDOApp" ng-controller="worldVDOListCtrl">
    <header class="f_display" style="background:#00aaff"><i class="icon-play"></i> คลิปที่เกี่ยวข้อง</header>

    <div ng-if="loading">Loading...</div>
    <div ng-if="!loading">
        <ul>
            <li ng-repeat="data in vdo" ng-if="$index <= 6">
                <a ng-href="{{data.url}}" target="_blank">
                    <div>
                        <img ng-src="{{data.img}}" alt="Photo: {{data.title}}"/>
                    </div>
                    <span>{{data.title}}</span>
                </a>
            </li>
        </ul>
    </div>

    <footer><a href="http://world.kapook.com/clip" target="_blank">ดูคลิปวิดีโอทั้งหมดคลิกที่นี่</a></footer>
</article>                        