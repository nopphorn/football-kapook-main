
<!DOCTYPE html>
<html lang="th">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title -->
    <title>ยูโร 2016 ผลบอลสด ผลบอลยูโร โปรแกรมการแข่งขัน ตารางคะแนน</title>

    <!-- SEO -->

    <meta name="description" content="ยูโร 2016 ผลบอลยูโร ผลบอลสด โปรแกรมบอลยูโร ฟุตบอลชิงแชมป์แห่งชาติยุโรป 2016 เช็กผลบอล วิเคราะห์บอลยูโร ดูตารางถ่ายทอดสดบอลยูโร" />
    <meta name="keywords" content="ยูโร 2016, ผลบอลยูโร, ผลบอลสด, โปรแกรมบอลยูโร, ตารางบอลยูโร, ฟุตบอลชิงแชมป์แห่งชาติยุโรป 2016, วิเคราะห์บอลยูโร,ตารางถ่ายทอดบอลยูโร" />
    <meta name="author" content="กระปุกดอทคอม เว็บแรกที่คุณเลือก">
    <link rel="shortcut icon" href="http://football.kapook.com/favicon.ico">

    <!-- SMO -->

    <link rel="image_src" type="image/jpeg" href="http://football.kapook.com/uploads/comp/og/22.jpg" />
    <!--    <meta property="fb:app_id" content="306795119462">
        <meta property="fb:admins" content="100000352102435" />-->

    <meta property="fb:app_id" content="306795119462">
    <meta property="fb:admins" content="592216870" />
    <meta property="fb:admins" content="100000517048862" />
    <meta property="fb:admins" content="100000607392589" />
    <meta property="fb:admins" content="100000087878814" />
    <meta property="fb:admins" content="1140102705" />


    <meta property="og:type" content="website" />
    <meta property="og:type" content="article">
    <meta property="article:author" content="https://www.facebook.com/kapookdotcom">
    <meta property="og:title" content="ยูโร 2016 ผลบอลสด โปรแกรมการแข่งขัน ตารางคะแนน" />
    <meta property="og:description" content="ยูโร 2016 ผลบอลยูโร ตารางบอล ฟุตบอลชิงแชมป์แห่งชาติยุโรป 2016 เช็กผลบอล วิเคราะห์บอลยูโร โปรแกรมถ่ายทอดสด" />
    <meta property="og:image" content="http://football.kapook.com/uploads/comp/og/22.jpg" />
    <meta property="og:url" content="http://football.kapook.com/tournament/euro2016" />
    <meta property="og:site_name" content="kapook" />

    <meta name="twitter:title" content="ยูโร 2016 ผลบอลสด โปรแกรมการแข่งขัน ตารางคะแนน">
    <meta name="twitter:description" content="ยูโร 2016 ผลบอลยูโร ตารางบอล ฟุตบอลชิงแชมป์แห่งชาติยุโรป 2016 เช็กผลบอล วิเคราะห์บอลยูโร โปรแกรมถ่ายทอดสด" />
    <meta name="twitter:image:src" content="http://football.kapook.com/uploads/comp/og/22.jpg">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@kapookdotcom">
    <meta name="twitter:creator" content="@kapookdotcom">
    <meta name="twitter:domain" content="kapook.com">


    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="default_kapook/style.css" rel="stylesheet" type="text/css" media="all">

    <!--Style-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <script src="assets/js/vendor/jquery-1.11.0.min.js"></script>
    <!-- This code snippet is to be placed in the <head> -->



    <script type="text/javascript">
        var zmt_mtag;
        function zd_get_placements(){
            zmt_mtag = zmt_get_tag(3326,"543519");
            p543519_1 = zmt_mtag.zmt_get_placement("zt_543519_1", "543519", "1" , "78" , "18" , "2" ,"970", "250");
            p543519_2 = zmt_mtag.zmt_get_placement("zt_543519_2", "543519", "2" , "78" , "9" , "2" ,"300", "250");
            p543519_3 = zmt_mtag.zmt_get_placement("zt_543519_3", "543519", "3" , "78" , "24" , "2" ,"300", "250");
            zmt_mtag.zmt_set_async();
            zmt_mtag.zmt_load(zmt_mtag);
        }
    </script>
    <script type="text/javascript" src="http://s.f1.impact-ad.jp/client/xp1/fmos.js" async ></script>

</head>
<body class="tm-euro2016">
<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-36103152-15', 'kapook.com');
    ga('send', 'pageview');
</script>


<nav id="topbar" style=" position:relative">
    <ul>
        <li class="menu left"><a href="http://www.kapook.com" class="topnav"><span class="icon-navicon"></span>เว็บแรกที่คุณเลือก</a></li>


        <li class="login right" style="position: relative;"><a href="" class="btnlogin"><span class="icon-lock"></span>เข้าระบบ</a>
            <form id="form_login_box" class="loginbox" method="post" action="http://signup-demo.kapook.com/connect/kapook">
                <input type="hidden" name="apikey"  value="cxsw3ed21qaz">
                <input type="hidden" name="callback" value="http://football.kapook.com/tournament/euro2016">
                <fieldset>
                    <legend>เข้าระบบด้วย User Kapook</legend>
                    <label>Email</label>
                    <input name="username" type="text" class="input-login">
                    <br>
                    <label>Password</label>
                    <input name="password" type="password" class="input-login">
                    <br>
                    <input name="submit" type="submit" class="btn-login" value="เข้าระบบ">
                </fieldset>
                <p class="connectFB"><a href="http://signup-demo.kapook.com/connect/facebook?callback=http://football.kapook.com/tournament/euro2016"><span class="icon-facebook"></span> เข้าระบบด้วย Facebook</a></p>
            </form>
        </li>






        <li class="signin right"><a href="http://signup-demo.kapook.com/?apikey=cxsw3ed21qaz&callback=http://football.kapook.com/tournament/euro2016"><span class="icon-user-plus"></span>สมัครสมาชิก</a></li>

        <li class="fav right"><a href="http://today.kapook.com/howto" target="_blank"><span class="icon-location"></span>ตั้งเป็นหน้าแรก</a></li>
        <li class="right">

            <script type="text/javascript" language="javascript1.1">page = "football";</script>

            <!-- New Truehits 02-09-57 -->
            <div id="truehits_div"></div>
            <script type="text/javascript">
                (function() {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = "http://hits.truehits.in.th/dataa/a0000034.js";
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();
            </script>
            <!-- \\New Truehits 02-09-57 -->

        </li>
    </ul>
    <ol class="topupmenu">
        <li><a href="http://headline.kapook.com" target="_blank" title="ข่าวด่วน">ข่าวด่วน</a></li>
        <li><a href="http://sn.kapook.com" target="_blank" title="ข่าวสั้น">ข่าวสั้น</a></li>
        <li><a href="http://women.kapook.com/star" target="_blank" title="ข่าวดารา">ข่าวดารา</a></li>
        <li><a href="http://tv.kapook.com" target="_blank" title="ดูทีวี">ดูทีวี</a></li>
        <li><a href="http://drama.kapook.com" target="_blank" title="ละคร">ละคร</a></li>
        <li><a href="http://movie.kapook.com" target="_blank" title="หนังใหม่">หนังใหม่</a></li>
        <li><a href="http://musicstation.kapook.com" target="_blank" title="ฟังเพลง">ฟังเพลง</a></li>
        <li><a href="http://radio.kapook.com" target="_blank" title="ฟังวิทยุออนไลน์">ฟังวิทยุออนไลน์</a></li>
        <li><a href="http://gamecenter.kapook.com" target="_blank" title="เกม">เกม</a></li>
        <li><a href="http://gamecenter.kapook.com/thaichess" target="_blank" title="หมากรุกไทย">หมากรุกไทย</a></li>
        <li><a href="http://gamecenter.kapook.com/checker" target="_blank" title="แชทหมากฮอส">แชทหมากฮอส</a></li>
        <li><a href="http://glitter.kapook.com" target="_blank" title="Glitter">Glitter</a></li>
        <li><a href="http://lottery.kapook.com" target="_blank" title="ตรวจหวย">ตรวจหวย</a></li>
        <li><a href="http://women.kapook.com" target="_blank" title="ผู้หญิง">ผู้หญิง</a></li>
        <li><a href="http://wedding.kapook.com" target="_blank" title="แต่งงาน">แต่งงาน</a></li>
        <li><a href="http://baby.kapook.com" target="_blank" title="แม่และเด็ก">แม่และเด็ก</a></li>
        <li><a href="http://horoscope.kapook.com" target="_blank" title="ดูดวง">ดูดวง</a></li>
        <li><a href="http://dream.kapook.com" target="_blank" title="ทำนายฝัน">ทำนายฝัน</a></li>
        <li><a href="http://health.kapook.com" target="_blank" title="สุขภาพ">สุขภาพ</a></li>
        <li><a href="http://pet.kapook.com" target="_blank" title="สัตว์เลี้ยง">สัตว์เลี้ยง</a></li>
        <li><a href="http://men.kapook.com" target="_blank" title="ผู้ชาย">ผู้ชาย</a></li>
        <li><a href="http://football.kapook.com" target="_blank" title="ผลบอล">ผลบอล</a></li>
        <li><a href="http://home.kapook.com" target="_blank" title="บ้านและการตกแต่ง">บ้านและการตกแต่ง</a></li>
        <li><a href="http://travel.kapook.com" target="_blank" title="ท่องเที่ยว">ท่องเที่ยว</a></li>
        <li><a href="http://guide.kapook.com" target="_blank" title="แวะชิมแวะพัก">แวะชิมแวะพัก</a></li>
        <li><a href="http://poem.kapook.com" target="_blank" title="กลอน">กลอน</a></li>
        <li><a href="http://icare.kapook.com" target="_blank" title="iCare">iCare</a></li>
        <li><a href="http://education.kapook.com" target="_blank" title="การศึกษา">การศึกษา</a></li>
        <li><a href="http://dict.kapook.com" target="_blank" title="dictionary">dictionary</a></li>
        <li><a href="http://speedtest.kapook.com" target="_blank" title="เช็คความเร็วเน็ต">เช็คความเร็วเน็ต</a></li>
        <li><a href="http://iphone.kapook.com" target="_blank" title="iPhone">iPhone</a></li>
        <li><a href="http://facebook.kapook.com" target="_blank" title="Facebook">Facebook</a></li>
        <li><a href="http://twitter.kapook.com" target="_blank" title="Twitter">Twitter</a></li>
        <li><a href="http://instagram.kapook.com/star" target="_blank" title="instagram ดารา">instagram ดารา</a></li>
        <li><a href="http://instagram.kapook.com" target="_blank" title="อินสตาแกรม">อินสตาแกรม</a></li>
    </ol>

</nav>
<div class="header-container " >
    <header class="wrapper clearfix container">
        <div  class="logo"><a class="kapook" href="http://www.kapook.com/"></a> <a  class="football" href="http://football.kapook.com/"><span class="head">FOOTBALL</span><span class="des">เว็บฟุตบอลประจำวันของคุณ</span></a></div>
    </header>
</div>
<div class="nav-container" >
    <div class="nav-wrap">
        <div class="nav-line">
            <nav class="navbar navbar-default main" >

                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="http://football.kapook.com/"><i class="fa fa-home"></i> ฟุตบอล</a>

                    <a href="http://football.kapook.com/modal/Remote-Modal-login.php?url=http%3A%2F%2Ffootball.kapook.com%2F" class="pull-right visible-xs-inline-block nav-bar-link" data-toggle="modal" data-target="#useful-modal"  ><i class="fa fa-user"></i> Login </a>


                </div>




                <!-- /.nav-collapse -->
            </nav>
        </div>
        <div class="top-bar">
            <div class="container">
                <ol class="breadcrumb pull-left">
                    <li><a href="http://football.kapook.com/">หน้าแรกฟุตบอล</a></li><li class="active">ยูโร 2016</li>                            </ol>

                <!-- Social media starts -->
                <span class="btn btn-link  btn-sm pull-right btn-gototop"><i class="fa fa-chevron-circle-up"></i> กลับด้านบน</span>


                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- End nav Contiainer-->




<div class="container  ad_banner_desktop">
    <div id="zt_543519_1" style="display:show">
        <script id="zt_543519_1" language="javascript">
            if(typeof zmt_mtag !='undefined' && typeof zmt_mtag.zmt_render_placement !='undefined')
            {
                zmt_mtag.zmt_render_placement(p543519_1);
            }
        </script>
    </div>
</div>


</div>
</div>
</div>

<script language="JavaScript">
    $('.link-play-game').on('click', function(event){
        event.stopPropagation();
        event.preventDefault();
    });
</script>

<link rel="stylesheet" href="assets/css/bracketz.css">
<script src="assets/js/bracketz.js"></script>



<div class="container league euro2016">


    <h1  class="page-header font-display">ฟุตบอลชิงแชมป์แห่งชาติยุโรป 2016</h1>


    <div class="social pull-right" style="margin-top: -70px; ">
        <div class="share  share_type_facebook">
            <span class="share__count">0</span>
            <a class="share__btn">Share</a>
        </div>
        <div class="share  share_type_twitter">
            <a class="share__btn" href="" onclick="twitter_share();">Tweet</a>
        </div>

        <div class="share share_size_large share_type_gplus">
            <span class="share__count">0</span>
            <a class="share__btn" href="" onclick="google_share();">+1</a>
        </div>

        <div class="share share_size_large share_type_email">
            <span class="share__count">0</span>
            <a class="share__btn" href="">E-mail</a>
        </div>


    </div>




    <div class="row">
        <ul class="navbar-tournament">
            <li><a href="/tournament/euro2016">หน้าแรกฟุตบอลชิงแชมป์แห่งชาติยุโรป 2016</a></li>
            <li><a href='/tournament/euro2016/result-program'>ผลบอล & โปรแกรม </a></li>
            <li><a href='/tournament/euro2016/table'>ตารางคะแนน</a></li>
            <li><a href='/tournament/euro2016/topscorer'>ดาวซัลโว</a></li>
        </ul>





    </div>







    <style>
        .handicap_after:after {
            font-family: FontAwesome;
            content: " \f111";
            display: inline;
            color: #428bca;
        }
        .handicap_before:before {
            font-family: FontAwesome;
            content: "\f111  ";
            display: inline;
            color: #428bca;
        }
    </style>
    <!-- Live Ex Bar-->
    <script type="text/javascript">
        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
            }
            return "";
        }
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        }
        var cname = getCookie("uid")+"view";
        var exdays = 365;
        $(document).ready(function(){
            $("#show").click(function showpage(){
                $("#table1").hide();
                $("#page1").show();
                var cvalue = "show";
                setCookie(cname, cvalue, exdays);
            });

            $("#hide").click(function hidepage(){
                $("#table1").show();
                $("#page1").hide();
                var cvalue = "hide";
                setCookie(cname, cvalue, exdays);
            });
        });
    </script>
    <div class="container scoreboard-x swiper-x insetShadow" >

        <!-- Slider main container -->
        <div class="swiper-container ">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide">


                    <div class="head"> <span id="spLabelDate">วันที่ 10 มิถุนายน 2559</span></div>

                    <table class="table  boder-1" id="table1"  >
                        <tbody style="text-align: -webkit-auto;font-size: 1.1em;">
                        <tr>
                            <td>02:00</td>
                            <td class="text_right"><a href="http://football.kapook.com/team-france" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname ">ฝรั่งเศส <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/FRANCE.png" width="24"></span></a></td>
                            <td class="text_center">
                                <a href="" class="btn  btn-xs tooltip_top" target="_blank" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้" id="aVS(147)">
                                    vs
                                </a>
                            </td>
                            <td class="text_left"><a href="http://football.kapook.com/team-romania" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname "><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/ROMANIA.png" width="24"> โรมาเนีย</span></a></td>
                            <td>เสมอ</td>
                            <td class="channel"> <span class="channel pull-right">								</span> </td>

                            <td> </td>
                        </tr>
                        <tr>
                            <td>02:00</td>
                            <td class="text_right"><a href="http://football.kapook.com/team-france" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname ">ฝรั่งเศส <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/FRANCE.png" width="24"></span></a></td>
                            <td class="text_center">
                                <a href="" class="btn  btn-xs tooltip_top" target="_blank" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้" id="aVS(147)">
                                    vs
                                </a>
                            </td>
                            <td class="text_left"><a href="http://football.kapook.com/team-romania" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname "><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/ROMANIA.png" width="24"> โรมาเนีย</span></a></td>
                            <td>เสมอ</td>
                            <td class="channel"> <span class="channel pull-right">								</span> </td>

                            <td> </td>
                        </tr>
                        <tr>
                            <td>02:00</td>
                            <td class="text_right"><a href="http://football.kapook.com/team-france" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname ">ฝรั่งเศส <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/FRANCE.png" width="24"></span></a></td>
                            <td class="text_center">
                                <a href="" class="btn  btn-xs tooltip_top" target="_blank" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้" id="aVS(147)">
                                    vs
                                </a>
                            </td>
                            <td class="text_left"><a href="http://football.kapook.com/team-romania" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname "><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/ROMANIA.png" width="24"> โรมาเนีย</span></a></td>
                            <td>เสมอ</td>
                            <td class="channel"> <span class="channel pull-right">								</span> </td>

                            <td> </td>
                        </tr>
                        </tbody>
                    </table></div>
                <div class="swiper-slide ">

                    <div class="head"> <span id="spLabelDate">วันที่ 10 มิถุนายน 2559</span></div>

                    <table class="table  boder-1" id="table1"  >
                        <tbody style="text-align: -webkit-auto;font-size: 1.1em;">
                        <tr>
                            <td>02:00</td>
                            <td class="text_right"><a href="http://football.kapook.com/team-france" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname ">ฝรั่งเศส <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/FRANCE.png" width="24"></span></a></td>
                            <td class="text_center">
                                <a href="" class="btn  btn-xs tooltip_top" target="_blank" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้" id="aVS(147)">
                                    vs
                                </a>
                            </td>
                            <td class="text_left"><a href="http://football.kapook.com/team-romania" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname "><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/ROMANIA.png" width="24"> โรมาเนีย</span></a></td>
                            <td>เสมอ</td>
                            <td class="channel"> <span class="channel pull-right">								</span> </td>

                            <td> </td>
                        </tr>
                        <tr>
                            <td>02:00</td>
                            <td class="text_right"><a href="http://football.kapook.com/team-france" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname ">ฝรั่งเศส <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/FRANCE.png" width="24"></span></a></td>
                            <td class="text_center">
                                <a href="" class="btn  btn-xs tooltip_top" target="_blank" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้" id="aVS(147)">
                                    vs
                                </a>
                            </td>
                            <td class="text_left"><a href="http://football.kapook.com/team-romania" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname "><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/ROMANIA.png" width="24"> โรมาเนีย</span></a></td>
                            <td>เสมอ</td>
                            <td class="channel"> <span class="channel pull-right">								</span> </td>

                            <td> </td>
                        </tr>
                        <tr>
                            <td>02:00</td>
                            <td class="text_right"><a href="http://football.kapook.com/team-france" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname ">ฝรั่งเศส <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/FRANCE.png" width="24"></span></a></td>
                            <td class="text_center">
                                <a href="" class="btn  btn-xs tooltip_top" target="_blank" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้" id="aVS(147)">
                                    vs
                                </a>
                            </td>
                            <td class="text_left"><a href="http://football.kapook.com/team-romania" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname "><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/ROMANIA.png" width="24"> โรมาเนีย</span></a></td>
                            <td>เสมอ</td>
                            <td class="channel"> <span class="channel pull-right">								</span> </td>

                            <td> </td>
                        </tr>
                        </tbody>
                    </table></div>

            </div>

            <!-- If we need navigation buttons -->
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>

        </div>


        <script src="assets/js/swiper/js/swiper.min.js"></script>
        <link rel="stylesheet" href="assets/js/swiper/css/swiper.css">
        <script>

            var mySwiper = new Swiper('.swiper-container', {
                speed: 400,
                spaceBetween: 100,
                nextButton: document.querySelector(".swiper-button-next") ,
                prevButton: document.querySelector(".swiper-button-prev"),
            });

        </script>



    </div>



    <div class="row">
        <!-- Start LEFT COLUME -->
        <div class="col-md-12">


            <div class="row ">
                <!--vdo world-->
                <script src="http://my.kapook.com/angular/angular_1.3.10.min.js" type="text/javascript"></script>
                <script src="http://my.kapook.com/angular/angular.ng-modules.js" type="text/javascript"></script>

                <link href="http://my.kapook.com/vdoworld/style.css" rel="stylesheet" type="text/css" media="all" />
                <link href="http://my.kapook.com/fonts/display/fontface.css" rel="stylesheet" type="text/css" media="all">
                <link href="http://my.kapook.com/vdoworld/vdo_world.css" rel="stylesheet">

                <script>
                    var url = "http://cms.kapook.com/content_relate/get_json_by_word/world/box_1/video/12?arr_word[0]=นักฟุตบอล&arr_word[1]=สโมสรฟุตบอล&arr_word[2]=ไฮไลท์ฟุตบอล&callback=JSON_CALLBACK";
                    var worldVDOApp = angular.module('worldVDOApp', []);

                    worldVDOApp.run(function($rootScope) {
                        $rootScope.loading = true;
                    });

                    worldVDOApp.controller('worldVDOListCtrl', function($scope, $rootScope, $http) {
                        $http.jsonp(url)
                            .success(function(data) {
                                $scope.vdo = data;
                                $rootScope.loading = false;
                            });
                    });
                </script>

                <article class="int_vdo_world full_w" style="display:block;" ng-app="worldVDOApp" ng-controller="worldVDOListCtrl">
                    <header class="f_display" style="background:#00aaff"><i class="icon-play"></i> คลิปที่เกี่ยวข้อง</header>

                    <div ng-if="loading">Loading...</div>
                    <div ng-if="!loading">
                        <ul>
                            <li ng-repeat="data in vdo" ng-if="$index <= 11">
                                <a ng-href="{{data.url}}" target="_blank">
                                    <div>
                                        <img ng-src="{{data.img}}" alt="Photo: {{data.title}}"/>
                                    </div>
                                    <span>{{data.title}}</span>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <footer><a href="http://world.kapook.com/clip" target="_blank">ดูคลิปวิดีโอทั้งหมดคลิกที่นี่</a></footer>
                </article>

                <div id="carousel-featured" class="carousel slide col-md-8" data-ride="carousel" style="margin: 10px 0;">

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <a href="http://football.kapook.com/news-24442" target="_blank"><img src="http://football.kapook.com/cms/upload/large/466/24442-news-7849.jpg" alt="...">
                                <div class="carousel-caption">
                                    <h2 >ยุ่งแล้ว เลสเตอร์ !! อิตาลีเล็งดึง รานิเอรี่ คุมทีมชาติหลังจบ ยูโร 2016</h2>
                                    <p>ประธานสหพันธ์ฟุตบอลอิตาลี เตรียมดึง เคลาดิโอ รานิเอรี่ จากอ้อมอก เลสเตอร์ กลับบ้านเกิดไปคุมทีมชาติลุยศึก ฟุตบอลโลก 2018</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>




                <div class="col-md-4" data-ride="carousel" style="margin: 10px 0;">
                    <div class="banner">
                        <script type="text/javascript" src="http://ads.kapook.com/banner/adshow.php?zid=146"></script>

                    </div>
                </div>



                <div class="news ">
                    <div class="col-md-12">
                        <h2 class="font-display" style="margin-top: 0;">ข่าวยูโร 2016</h2>
                        <div class="row subnews">
                            <div class="news_block  col-md-3 col-sm-3 col-xs-6">
                                <a class="" href="http://football.kapook.com/news-24189" target="_blank">
                                    <img class="media-object" alt="" src="http://football.kapook.com/cms/upload/large/213/24189-news-9461.jpg">อันโตนิโอ คอนเต้.. แฟน เชลซี รู้จักเขาดีหรือยัง ?					</a>
                            </div>
                            <div class="news_block  col-md-3 col-sm-3 col-xs-6">
                                <a class="" href="http://football.kapook.com/news-24185" target="_blank">
                                    <img class="media-object" alt="" src="http://football.kapook.com/cms/upload/large/209/24185-news-1581.jpg">เชลซี ประกาศแต่งตั้ง อันโตนิโอ คอนเต้ คุมทีมหลัง ยูโร 2016					</a>
                            </div>
                            <div class="news_block  col-md-3 col-sm-3 col-xs-6">
                                <a class="" href="http://football.kapook.com/match-111225-italy-spain" target="_blank">
                                    <img class="media-object" alt="" src="http://football.kapook.com/cms/upload/large/133/24109-news-2595.jpg">อิตาลี 1-1 สเปน : กระชับมิตรทีมชาติ					</a>
                            </div>
                            <div class="news_block  col-md-3 col-sm-3 col-xs-6">
                                <a class="" href="http://football.kapook.com/news-24099" target="_blank">
                                    <img class="media-object" alt="" src="http://football.kapook.com/cms/upload/large/123/24099-news-8081.jpg">ยูโร 2016 อาจปิดสนามเตะไม่มีแฟนบอลหลังเหตุระเบิดเบลเยียม					</a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 line faa-parent animated-hover"> <a href="/tournament/euro2016/news" class="more faa-vertical waves-effect waves-block" target="_blank"><i class="fa fa-plus "></i></a></div>
                        </div>
                    </div>  <!--End news -->


                </div>
            </div>


            <div class="stat-block insetShadow">
                <div class="block-title">สถิติยูโร 2016</div>
                <div class="row">
                    <div class="col-md-4 block-overall">
                        <span>Overall</span>
                        <div class="row">
                            <div class="col-xs-4">
                                <span>เกม</span>
                                <span>11</span>
                            </div>
                            <div class="col-xs-4">
                                <span>ประตู</span>
                                <span>32</span>
                            </div>
                            <div class="col-xs-4">
                                <span>ประตูเฉลี่ย</span>
                                <span>2.879</span>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 block-team"><span>Team</span>

                        <div class="row">
                            <div class="col-xs-6">
                                <span>ยิงประตูมากสุด</span>
                                <span class="team">
                                    <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" >
                                    อังกฤษ
                                </span>
                                <span class="score">20</span>
                            </div>
                            <div class="col-xs-6">
                                <span>เสียประตูมากสุด</span>
                                <span class="team">
                                    <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" >
                                    สเปน
                                </span>
                                <span class="score">11</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 block-topscorer"><span>Top scorer</span>
                        <div class="row">
                            <div class="col-xs-5">
                                <span class="top-player">
                                    <img src="http://football.kapook.com/uploads/player/212.jpg">
                                    <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/FRANCE.png">
                                    <name>บาสเตียน ชไวน์สไตเกอร์</name>
                                </span>
                                <span class="score">20</span>
                            </div>
                            <div class="col-xs-7">
                                <span class="player">
                                    <span class="score">18</span>
                                    <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/FRANCE.png">
                                    <name>บาสเตียน ชไวน์สไตเกอร์</name>

                                </span>
                                <span class="player">
                                     <span class="score">16</span>
                                    <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/FRANCE.png">
                                    <name>บาสเตียน ชไวน์สไตเกอร์</name>

                                </span>
                                <span class="player">
                                     <span class="score">14</span>
                                    <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/FRANCE.png">
                                    <name>บาสเตียน ชไวน์สไตเกอร์</name>

                                </span>
                            </div>
                        </div>

                    </div>




                </div>
            </div>


            <h1 class="font-display" style="margin-top: 0px;"><i class="fa fa-sort-amount-desc"></i>ตารางคะแนน</h1>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs tab-i" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-i-1" aria-controls="home" role="tab" data-toggle="tab">รอบน็อกเอาต์</a></li>
                    <li role="presentation"><a href="#tab-i-2" aria-controls="profile" role="tab" data-toggle="tab">รอบแบ่งกลุ่ม</a></li>
                    <li role="presentation"><a href="#tab-i-3" aria-controls="messages" role="tab" data-toggle="tab">ดาวซัลโว</a></li>
                    <li role="presentation"><a href="#tab-i-4" aria-controls="settings" role="tab" data-toggle="tab"></a></li>
                </ul>

                <!-- Tab panes -->

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab-i-1">
                        <div class="col-md-12">
                            <!--    ตารางคะแนนลีกส์ต่างๆ     -->
                            <div class="table_standing">
                                <h2 class="font-display" style="margin-top: 0;"></h2>								<div class="brackets_container">
                                    <table>
                                        <!--rounds container-->
                                        <thead><tr>
                                            <th style="text-align: center;">
                                                <span>รอบ 16 ทีม</span>
                                            </th>
                                            <th style="text-align: center;">
                                                <span>รอบ 8 ทีม</span>
                                            </th>
                                            <th style="text-align: center;">
                                                <span>รอบรองชนะเลิศ</span>
                                            </th>
                                            <th style="text-align: center;">
                                                <span>รอบชิงชนะเลิศ</span>
                                            </th>
                                            <th style="text-align: center;">
                                                <span>รอบรองชนะเลิศ</span>
                                            </th>
                                            <th style="text-align: center;">
                                                <span>รอบ 8 ทีม</span>
                                            </th>
                                            <th style="text-align: center;">
                                                <span>รอบ 16 ทีม</span>
                                            </th>
                                        </tr></thead>
                                        <!--matches container-->
                                        <tbody><tr id="playground">
                                            <!-- Round of 16 -->
                                            <td class="round_column r_16">
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top winner" data-team-id="-2">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>รองแชมป์กลุ่ม A</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-3">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>รองแชมป์กลุ่ม C</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-4">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>แชมป์กลุ่ม D</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-5">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>อันดับ 3 กลุ่ม B/E/F</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-6">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>แชมป์กลุ่ม B</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-7">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>อันดับ 3 กลุ่ม A/C/D</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-8">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>แชมป์กลุ่ม F</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-9">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>รองแชมป์กลุ่ม E</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <!-- Round of 8 -->
                                            <td class="round_column r_8">
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-10">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span></span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-11">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span></span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-12">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span></span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-13">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span></span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <!-- Round of 4 -->
                                            <td class="round_column r_4">
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-14">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span></span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-15">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span></span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <!-- Final & 3rd -->
                                            <td class="round_column r_2 final">
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_dtls" style="bottom: 95px;" >
					<span>
											</span>
                                                        </div>
                                                        <div class="m_dtls" style="bottom: -10px;">
                                                            <span><b>รอบชิงชนะเลิศ</b></span>
                                                        </div>
                                                        <div class="m_segment m_top loser" data-team-id="-16">
					<span>
						<a href="">
                            <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                            <span></span>
                        </a>
						<strong>0</strong>
					</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-17">
					<span>
						<a href="">
                            <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                            <span></span>
                        </a>
						<strong>0</strong>
					</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <!-- Round of 4 -->
                                            <td class="round_column r_4 reversed">
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-18">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span></span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-19">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span></span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <!-- Round of 8 -->
                                            <td class="round_column r_8 reversed">
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-20">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span></span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-21">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span></span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-22">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span></span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-23">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span></span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <!-- Round of 16 -->
                                            <td class="round_column r_16 reversed">
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-24">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>แชมป์กลุ่ม C</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-25">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>อันดับ 3 กลุ่ม A/B/F</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-26">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>แชมป์กลุ่ม E</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-27">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>รองแชมป์กลุ่ม D</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-28">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>แชมป์กลุ่ม A</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-29">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>อันดับ 3 กลุ่ม C/D/E</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mtch_container">
                                                    <div class="match_unit">
                                                        <div class="m_segment m_top loser" data-team-id="-30">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
                                <span>รองแชมป์กลุ่ม B</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_segment m_botm loser" data-team-id="-31">
						<span>
							<a href="">
                                <img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.pnground_column r_8" style="height: 25px;"/>
                                <span>รองแชมป์กลุ่ม F</span>
                            </a>
							<strong>0</strong>
						</span>
                                                        </div>
                                                        <div class="m_dtls">
						<span>
													</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 line faa-parent animated-hover" > <a href="/tournament/euro2016/table" class="more faa-vertical" target="_blank"><i class="fa fa-plus "></i></a></div>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-i-2">
                        <div class="col-md-12">
                            <!--    ตารางคะแนนลีกส์ต่างๆ     -->
                            <div class="table_standing">
                                <h2 class="font-display" style="margin-top: 0;">กลุ่ม A</h2>								<div class="col-md-12">
                                    <table  class="table gray table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ชื่อทีม</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่แข่ง">P</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่ชนะ">W</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่เสมอ">D</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่แพ้">L</th>
                                            <th class="tooltip_top"  title="จำนวนประตูที่ยิงได้">GF</th>
                                            <th class="tooltip_top"  title="จำนวนประตูที่เสีย">GA</th>
                                            <th class="tooltip_top"  title="จำนวนผลต่างประตู">GD</th>
                                            <th class="tooltip_top"  title="จำนวนคะแนน">PTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><a href="http://football.kapook.com/team-albania" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/ALBANIA.png" width="24" height="24"  alt=""/> แอลเบเนีย</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td><a href="http://football.kapook.com/team-france" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/FRANCE.png" width="24" height="24"  alt=""/> ฝรั่งเศส</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td><a href="http://football.kapook.com/team-romania" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/ROMANIA.png" width="24" height="24"  alt=""/> โรมาเนีย</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td><a href="http://football.kapook.com/team-switzerland" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" width="24" height="24"  alt=""/> สวิตเซอร์แลนด์</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <h2 class="font-display" style="margin-top: 0;">กลุ่ม B</h2>								<div class="col-md-12">
                                    <table  class="table gray table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ชื่อทีม</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่แข่ง">P</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่ชนะ">W</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่เสมอ">D</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่แพ้">L</th>
                                            <th class="tooltip_top"  title="จำนวนประตูที่ยิงได้">GF</th>
                                            <th class="tooltip_top"  title="จำนวนประตูที่เสีย">GA</th>
                                            <th class="tooltip_top"  title="จำนวนผลต่างประตู">GD</th>
                                            <th class="tooltip_top"  title="จำนวนคะแนน">PTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><a href="http://football.kapook.com/team-england" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/ENGLAND.png" width="24" height="24"  alt=""/> อังกฤษ</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td><a href="http://football.kapook.com/team-russia" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/RUSSIA.png" width="24" height="24"  alt=""/> รัสเซีย</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td><a href="http://football.kapook.com/team-slovakia" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/SLOVAKIA.png" width="24" height="24"  alt=""/> สโลวะเกีย</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td><a href="http://football.kapook.com/team-wales" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" width="24" height="24"  alt=""/> เวลส์</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <h2 class="font-display" style="margin-top: 0;">กลุ่ม C</h2>								<div class="col-md-12">
                                    <table  class="table gray table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ชื่อทีม</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่แข่ง">P</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่ชนะ">W</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่เสมอ">D</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่แพ้">L</th>
                                            <th class="tooltip_top"  title="จำนวนประตูที่ยิงได้">GF</th>
                                            <th class="tooltip_top"  title="จำนวนประตูที่เสีย">GA</th>
                                            <th class="tooltip_top"  title="จำนวนผลต่างประตู">GD</th>
                                            <th class="tooltip_top"  title="จำนวนคะแนน">PTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><a href="http://football.kapook.com/team-germany" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/GERMANY.png" width="24" height="24"  alt=""/> เยอรมนี</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td><a href="http://football.kapook.com/team-northern-ireland" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/NORTHERN-IRELAND.png" width="24" height="24"  alt=""/> ไอร์แลนด์เหนือ</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td><a href="http://football.kapook.com/team-poland" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/POLAND.png" width="24" height="24"  alt=""/> โปแลนด์</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td><a href="http://football.kapook.com/team-ukraine" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/UKRAINE.png" width="24" height="24"  alt=""/> ยูเครน</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <h2 class="font-display" style="margin-top: 0;">กลุ่ม D</h2>								<div class="col-md-12">
                                    <table  class="table gray table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ชื่อทีม</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่แข่ง">P</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่ชนะ">W</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่เสมอ">D</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่แพ้">L</th>
                                            <th class="tooltip_top"  title="จำนวนประตูที่ยิงได้">GF</th>
                                            <th class="tooltip_top"  title="จำนวนประตูที่เสีย">GA</th>
                                            <th class="tooltip_top"  title="จำนวนผลต่างประตู">GD</th>
                                            <th class="tooltip_top"  title="จำนวนคะแนน">PTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><a href="http://football.kapook.com/team-croatia" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/CROATIA.png" width="24" height="24"  alt=""/> โครเอเชีย</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td><a href="http://football.kapook.com/team-czech-republic" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/CZECH-REPUBLIC.png" width="24" height="24"  alt=""/> สาธารณรัฐเช็ก</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td><a href="http://football.kapook.com/team-spain" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/SPAIN.png" width="24" height="24"  alt=""/> สเปน</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td><a href="http://football.kapook.com/team-turkey" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/TURKEY.png" width="24" height="24"  alt=""/> ตุรกี</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <h2 class="font-display" style="margin-top: 0;">กลุ่ม E</h2>								<div class="col-md-12">
                                    <table  class="table gray table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ชื่อทีม</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่แข่ง">P</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่ชนะ">W</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่เสมอ">D</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่แพ้">L</th>
                                            <th class="tooltip_top"  title="จำนวนประตูที่ยิงได้">GF</th>
                                            <th class="tooltip_top"  title="จำนวนประตูที่เสีย">GA</th>
                                            <th class="tooltip_top"  title="จำนวนผลต่างประตู">GD</th>
                                            <th class="tooltip_top"  title="จำนวนคะแนน">PTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><a href="http://football.kapook.com/team-belgium" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/BELGIUM.png" width="24" height="24"  alt=""/> เบลเยียม</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td><a href="http://football.kapook.com/team-italy" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/ITALY.png" width="24" height="24"  alt=""/> อิตาลี</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td><a href="http://football.kapook.com/team-rep.-of-ireland" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/REP.-OF-IRELAND.png" width="24" height="24"  alt=""/> สาธารณรัฐไอร์แลนด์</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td><a href="http://football.kapook.com/team-sweden" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/SWEDEN.png" width="24" height="24"  alt=""/> สวีเดน</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <h2 class="font-display" style="margin-top: 0;">กลุ่ม F</h2>								<div class="col-md-12">
                                    <table  class="table gray table-striped table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ชื่อทีม</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่แข่ง">P</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่ชนะ">W</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่เสมอ">D</th>
                                            <th class="tooltip_top"  title="จำนวนนัดที่แพ้">L</th>
                                            <th class="tooltip_top"  title="จำนวนประตูที่ยิงได้">GF</th>
                                            <th class="tooltip_top"  title="จำนวนประตูที่เสีย">GA</th>
                                            <th class="tooltip_top"  title="จำนวนผลต่างประตู">GD</th>
                                            <th class="tooltip_top"  title="จำนวนคะแนน">PTS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><a href="http://football.kapook.com/team-austria" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" width="24" height="24"  alt=""/> ออสเตรีย</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td><a href="http://football.kapook.com/team-hungary" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/HUNGARY.png" width="24" height="24"  alt=""/> ฮังการี</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td><a href="http://football.kapook.com/team-iceland" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" width="24" height="24"  alt=""/> ไอซ์แลนด์</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td><a href="http://football.kapook.com/team-portugal" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/PORTUGAL.png" width="24" height="24"  alt=""/> โปรตุเกส</a></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div></div>
                    <div role="tabpanel" class="tab-pane" id="tab-i-3">            <!--    ตารางดาวซัลโว     -->
                        <div class="top_scorer">
                            <table  class="table gray table-striped ">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ชื่อผู้เล่น</th>
                                    <th>G</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="3">กำลังรอข้อมูล</td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="col-md-12 line faa-parent animated-hover" > <a href="/tournament/euro2016/topscorer" class="more faa-vertical" target="_blank"><i class="fa fa-plus "></i></a></div>
                        </div></div>
                    <div role="tabpanel" class="tab-pane" id="tab-i-4">...</div>
                </div>





            <!--End Col 8 LEFT VOLUME-->
        </div>
    </div></div>
</div>


<div class="container" style="margin-top:50px"><div class="alert alert-warning text_center" role="alert"><i class="fa fa-envelope-square"></i> สอบถาม ติชม และ ส่งคำเสนอแนะของท่านมาที่ <a href="mailto:football@kapook.com">football@kapook.com</a></div></div>
<div class="container" style="text-align:center; color:#999; position:relative">
    <div style="margin-top: 10px;margin-bottom: -30px;"> เว็บไซต์  www.kapook.com ไม่มี และ ไม่สนับสนุน การเล่นพนันฟุตบอล และการพนันรูปแบบอื่นๆทุกชนิด
        ทีมงานหวังเพียงให้เยาวชน สนใจมุ่งเน้นด้านกีฬาเป็นสำคัญ </div>
</div>
<footer class="footer">
    <article id="footer">
        <ul class="sitemap">
            <li class="social-me"><strong>กระปุกดอทคอม<br><a href="http://www.kapook.com" target="_blank">www.kapook.com</a></strong>
                <p><a href="https://www.facebook.com/kapookdotcom" target="_blank" title="facebook kapook"><span class="icon-facebook"></span></a> <a href="https://www.twitter.com/kapookdotcom" target="_blank" title="twitter kapook"><span class="icon-twitter"></span></a> <a href="http://instagram.com/kapookdotcom" target="_blank" title="instagram kapook"><span class="fa fa-instagram"></span></a><br>
                    <a href="http://www.youtube.com/user/kapookdotcom" target="_blank" title="youtube kapook"><span class="fa fa-youtube"></span></a> <a href="https://plus.google.com/110009272535926187298/posts" target="_blank" title="google plus kapook"><span class="icon-googleplus"></span></a> <a href="http://www.kapook.com/feed" target="_blank" title="kapook feed"><span class="fa fa-rss"></span></a></p></li>
            <li><strong>เกี่ยวกับเรา:</strong><a href="http://www.kapook.com" target="_blank">หน้าแรกกระปุก</a><a href="http://job.kapook.com/" target="_blank">ร่วมงานกับเรา</a><a href="http://www.kapook.com/advert" target="_blank">ติดต่อโฆษณา</a><a href="mailto:webmaster@kapook.com" target="_blank">แนะนำติชม</a></li>
            <li><strong>บริการฮิต:</strong><a href="http://news.kapook.com/" target="_blank">ข่าว</a><a href="http://lottery.kapook.com/" target="_blank">ตรวจหวย</a><a href="http://star.kapook.com/" target="_blank">ข่าวบันเทิง</a><a href="http://horoscope.kapook.com/" target="_blank">ดูดวง</a><a href="http://world.kapook.com/clip" target="_blank">คลิป</a><a href="http://world.kapook.com/photo" target="_blank">รูปภาพ</a></li>
            <li><strong>บริการมือถือ:</strong><a href="http://m.kapook.com/app" target="_blank">Kapook App</a><a href="http://m.kapook.com/hotline" target="_blank"> แอพสายด่วน</a><a href="http://greetcard.kapook.com/" target="_blank">Greet Card</a><a href="http://alert.kapook.com/" target="_blank">kapook Alert</a></li>
            <li class="subcategory"><strong>หมวดหมู่:</strong><a href="http://football.kapook.com/" target="_blank" title="ฟุตบอล">ฟุตบอล</a><a href="http://football.kapook.com/result.php" target="_blank" title="ผลบอล">ผลบอล</a><a href="http://football.kapook.com/livescore.php" target="_blank" title="ผลบอลสด">ผลบอลสด</a><a href="http://football.kapook.com/program.php" target="_blank" title="ตารางบอล">ตารางบอล</a><a href="http://football.kapook.com/%E0%B8%A7%E0%B8%B4%E0%B9%80%E0%B8%84%E0%B8%A3%E0%B8%B2%E0%B8%B0%E0%B8%AB%E0%B9%8C%E0%B8%9A%E0%B8%AD%E0%B8%A5" target="_blank" title="วิเคราะห์บอล">วิเคราะห์บอล</a><a href="http://football.kapook.com/%E0%B8%9E%E0%B8%A3%E0%B8%B5%E0%B9%80%E0%B8%A1%E0%B8%B5%E0%B8%A2%E0%B8%A3%E0%B9%8C%E0%B8%A5%E0%B8%B5%E0%B8%81" target="_blank" title="พรีเมียร์ลีก">พรีเมียร์ลีก</a><a href="http://football.kapook.com/newslist.php" target="_blank" title="ข่าว">ข่าว</a><a href="http://football.kapook.com/transfer-market" target="_blank" title="ตลาดนักเตะ">ตลาดนักเตะ</a></li>
        </ul>
    </article>
</footer>

<!-- Modal -->
<div class="modal fade "  id="useful-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            ...
        </div>
    </div>
</div>



<div class="mobile-left-menu-bg"></div>
<div class="mobile-left-menu">
    <span class="mobile-left-menu-close"></span>
    <div class="">
        <a href="http://football.kapook.com" >
            <h4>
                Football
                <br>
                <small>kapook.com</small>
            </h4>
        </a>
        <ul >

            <li ><a href="http://football.kapook.com/"><i class="fa fa-home"></i> หน้าแรกฟุตบอล</a></li>




            <li>

                <a href="http://football.kapook.com/result.php"><i class="fa fa-futbol-o"></i> ผลบอล <label class="badge blink_me">Hot</label></a>
                <a href="#" data-toggle="collapse" data-target="#toggleSub-1"  class="plus collapsed"></a>

                <div class="collapse" id="toggleSub-1" style="height: 0px;">

                    <ul class="">
                        <li class="dropdown-header">League</li>
                        <li><a href="http://football.kapook.com/พรีเมียร์ลีก/result-program" title="ผลบอลพรีเมียร์ลีก">พรีเมียร์ลีก</a></li>
                        <li><a href="http://football.kapook.com/ลาลีกา/result-program" title="ผลบอลลาลีกา">ลาลีกา</a></li>
                        <li><a href="http://football.kapook.com/บุนเดสลีกา/result-program" title="บอลวันนี้">บุนเดสลีกา</a></li>
                        <li><a href="http://football.kapook.com/เซเรียอา/result-program" title="ผลบอลเซเรียอา">เซเรียอา</a></li>
                        <li><a href="http://football.kapook.com/ลีกเอิง/result-program" title="ผลบอลลีกเอิง">ลีกเอิง</a></li>
                        <li><a href="http://football.kapook.com/ไทยพรีเมียร์ลีก/result-program" title="ผลบอลไทยพรีเมียร์ลีก">ไทยพรีเมียร์ลีก</a></li>
                    </ul>

                    <ul class="">
                        <li class="dropdown-header">Cups</li>
                        <li><a href="http://football.kapook.com/championsleague/result-program" title="ผลบอลยูฟา แชมเปียนส์ ลีก">ยูฟา แชมเปียนส์ ลีก</a></li>
                        <li><a href="http://football.kapook.com/europaleague/result-program" title="ผลบอลยูฟา ยูโรป้า ลีก">ยูฟา ยูโรป้า ลีก</a></li>
                        <li><a href="http://football.kapook.com/tournament/afcchampionsleague/result-program" title="ผลบอลเอเอฟซี แชมเปี้ยนส์ ลีก">เอเอฟซี แชมเปี้ยนส์ ลีก</a></li>
                        <li><a name="http://football.kapook.com/euro/program-result" title="ผลบอลเอฟเอ คัพ">เอฟเอ คัพ</a></li>
                        <li><a name="http://football.kapook.com/worldcup2018/program-result" title="ผลบอลแคปิตอล วัน คัพ">แคปิตอล วัน คัพ</a></li>
                    </ul>

                    <ul class="">
                        <li class="dropdown-header">Tournaments</li>
                        <li><a href="http://football.kapook.com/tournament/u23afc2016/result-program">ฟุตบอล ยู-23 ชิงแชมป์เอเชีย 2016 </a></li>
                        <li><a href="http://football.kapook.com/tournament/worldcup2018asianqual/result-program">ผลบอลฟุตบอลโลก 2018 รอบคัดเลือก โซนเอเชีย</a></li>
                        <li><a href="http://football.kapook.com/tournament/worldcup2018saqual/result-program"> ผลบอลฟุตบอลโลก 2018 รอบคัดเลือก โซนอเมริกาใต้  </a></li>
                        <li><a href="http://football.kapook.com/tournament/euro2016qual/result-program" title="ผลบอลยูโร 2016">ผลบอลยูโร 2016</a></li>
                    </ul>

                    <ul class="">
                        <li class="dropdown-header">Teams</li>
                        <li><a href="http://football.kapook.com/team-manchester-utd/program-result" title="ผลบอลแมนฯ ยูไนเต็ด">แมนฯ ยูไนเต็ด</a></li>
                        <li><a href="http://football.kapook.com/team-liverpool/program-result" title="ผลบอลลิเวอร์พูล">ลิเวอร์พูล</a></li>
                        <li><a href="http://football.kapook.com/team-chelsea/program-result" title="ผลบอลเชลซี">เชลซี</a></li>
                        <li><a href="http://football.kapook.com/team-arsenal/program-result" title="ผลบอลอาร์เซน่อล">อาร์เซน่อล</a></li>
                        <li><a href="http://football.kapook.com/team-manchester-city/program-result" title="ผลบอลแมนเชสเตอร์ ซิตี้">แมนเชสเตอร์ ซิตี้</a></li>
                        <li><a href="http://football.kapook.com/team-real-madrid/program-result" title="ผลบอลเรอัล มาดริด">เรอัล มาดริด</a></li>
                        <li><a href="http://football.kapook.com/team-barcelona/program-result" title="ผลบอลบาร์เซโลน่า">บาร์เซโลน่า</a></li>
                        <li><a href="http://football.kapook.com/team-bayern-munich/program-result" title="ผลบอลบาเยิร์น มิวนิค">บาเยิร์น มิวนิค</a></li>
                        <li><a href="http://football.kapook.com/team-ac-milan/program-result" title="ผลบอลเอซี มิลาน">เอซี มิลาน</a></li>
                    </ul>



                </div>
            </li>


            <li ><a href="http://football.kapook.com/livescore.php"><i class="fa fa-futbol-o"></i> ผลบอลสด</a></li>

            <li><a href="http://football.kapook.com/program.php"><i class="fa fa-calendar"></i> ตารางบอล</a></li>

            <li><a href="http://football.kapook.com/วิเคราะห์บอล"><i class="fa fa-line-chart"></i> วิเคราะห์บอล</a></li>


            <li><a href="http://football.kapook.com/%E0%B8%9E%E0%B8%A3%E0%B8%B5%E0%B9%80%E0%B8%A1%E0%B8%B5%E0%B8%A2%E0%B8%A3%E0%B9%8C%E0%B8%A5%E0%B8%B5%E0%B8%81"><i class="fa fa-star"></i> พรีเมียร์ลีก </a></li>




            <!--    <li><a href="http://football.kapook.com/tournament/u23afc2016"><i class="fa fa-star"></i> ยู-23 ชิงแชมป์เอเชีย 2016</a></li>
               <li><a href="http://football.kapook.com/match-98931-saudi-arabia-(u23)-thailand-(u23)"> <span style="font-family: arial, verdana, tahoma">∟</span><span class="badge blink_me">วันนี้</span> ซาอุ vs ไทย : 23.30 น.</a></li>

    -->
            <li><a href="http://football.kapook.com/newslist.php"><i class="fa fa-newspaper-o"></i> ข่าวบอลวันนี้</a></li>
            <li><a href="http://football.kapook.com/transfer-market"><i class="fa fa-users"></i> ตลาดนักเตะ</a></li>



            <li><a href="http://football.kapook.com/games"><i class="fa fa-gamepad"></i> เกมทายผลบอล</a></li>

        </ul>
    </div><!--/.nav-collapse -->




</div>




</div>



</div>






<script language="JavaScript">
    var liveMatchGame_Single		=	new Array();
    var liveMatchGame_Multi			=	new Array();
    var liveMatchGame_TimeInterval 	= 	new Array();
</script>


<script language="JavaScript">
    var LiveMatchID 				= 	"0";
    var nextInterval				=	-1;
    var isInterval					=	false;
    var LiveScoreObj;

</script>

<!-- Load Cahce livescore -->

<!-- Notify -->
<script language="JavaScript">
    var stack_bottomright = {"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25};
    function show_stack_bottomright(type,str) {
        var typeNotify = 0;
        var opts = {
            title: "Over Here",
            text: "Check me out. I'm in a different stack.",
            addclass: "stack-bottomright",
            stack: stack_bottomright,mouse_reset: false
        };
        switch (type) {
            /*--------------------------------------------Game---------------------------------------------*/
            // success : Get a first login
            case 'success_first_login':
                opts.title = "Welcome";
                opts.text = 'ได้รับแต้มจากการเข้าสู่ฟุตบอลกระปุกครั้งแรก 1000 แต้ม';
                opts.type = "success";
                typeNotify	=	1;
                break;
            // success : Get a monthly login
            case 'success_login_monthly':
                opts.title = "Welcome";
                opts.text = 'คุณได้รับแต้มจากการเข้าสู่ฟุตบอลกระปุกเดือนนี้ 2000 แต้ม';
                opts.type = "success";
                typeNotify	=	2;
                break;
            // success : Get a daliy login
            case 'success_login_daily':
                opts.title = "Welcome";
                opts.text = 'คุณได้รับแต้มจากการเข้าสู่ฟุตบอลกระปุกวันนี้ 200 แต้ม';
                opts.type = "success";
                typeNotify	=	3;
                break;

            // success : Data insert.
            case 'success_playside':
                opts.title 	= "ทํารายการเรียบร้อย";
                opts.text	= "ข้อมูลการทายผลบอลถูกบันทึกเรียบร้อยแล้ว";
                opts.type 	= "success";
                typeNotify	=	4;
                break;

            // Error : Point Not Select
            case 'error_point_notselect':
                opts.title 	= "ใส่แต้มไม่ถูกต้อง";
                opts.text 	= "คุณใส่แต้มไม่ถูกต้อง ต้องใช้ 1 แต้มขึ้นไปในการทายผลบอล";
                opts.type 	= "error";
                typeNotify	=	5;
                break;

            // Error : Teamwin Not Select
            case 'error_teamwin_notselect':
                opts.title	= "ข้อมูลไม่ถูกต้อง";
                opts.text 	= "คุณยังไม่ได้เลือกทีม";
                opts.type 	= "error";
                typeNotify	=	6;
                break;

            // Error : less select match
            case 'error_multi_less_team_select':
                opts.title 	= "ข้อมูลไม่ถูกต้อง";
                opts.text 	= "คุณยังเลือกคู่ไม่ครบ เลือกอย่างน้อย " + tmpLimitMultiPlay_MatchMin + " คู่สำหรับการทายผลบอล 1 ชุด";
                opts.type 	= "error";
                typeNotify	=	7;
                break;

            // Error : Single Quota is full
            case 'error_full_quota_single':
                opts.title 	= "ข้อมูลไม่ถูกต้อง";
                opts.text 	= "คุณทายผลบอลเดี่ยวครบ " + tmpLimitSinglePlay + " คู่แล้ว";
                opts.type 	= "error";
                typeNotify	=	8;
                break;

            // Error : Multi Quota is full
            case 'error_full_quota_multi':
                opts.title 	= "ข้อมูลไม่ถูกต้อง";
                opts.text 	= "คุณเลือกคู่ที่จะทายสำหรับชุดนี้ครบแล้ว";
                opts.type 	= "error";
                typeNotify	=	9;
                break;

            // Error : This Set is played.
            case 'error_set_multi_played':
                opts.title 	= "ข้อมูลไม่ถูกต้อง";
                opts.text 	= "คุณทายผลบอลชุดที่ " + str + " ไปแล้ว";
                opts.type 	= "error";
                typeNotify	=	10;
                break;

            case 'error_point_notenough':
                opts.title 	= "ใส่แต้มไม่ถูกต้อง";
                opts.text 	= "คุณใส่แต้มเกินกว่าที่มีอยู่ แต้มที่สามารถเล่นได้คือ " + str + " แต้ม";
                opts.type 	= "error";
                typeNotify	=	11;
                break;

            case 'error_point_notnumber':
                opts.title 	= "ใส่แต้มไม่ถูกต้อง";
                opts.text 	= "คุณต้องใส่แต้มเป็นตัวเลขจำนวนเต็มเท่านั้นนะ";
                opts.type 	= "error";
                typeNotify	=	12;
                break;
            case 'error_login':
                opts.title 	= "Log in รับคะแนนฟรีทันที";
                opts.text 	= "คุณยังไม่ได้ log In .. Log In เข้าระบบรับแต้มเพื่อเล่นเกมทายผลบอลฟรี <span onclick=\"goToLogin();\" ><div class=\"btn btn-success btn-xs tooltip_top btn_no_login\">คลิก</div></span>";
                opts.type 	= "info";
                typeNotify	=	13;
                break;
            /*--------------------------------------------Game---------------------------------------------*/
            // success : Get a first login
            case 'result':
                opts.title = "ยอดเยี่ยม";
                opts.text = str;
                opts.type = "success";
                typeNotify	=	14;
                break;

            // Info : LiveScore [waiting]
            case 'liveScore_waiting':
                opts.title = "Next Match !!";
                opts.text = str;
                opts.type = "notice";
                typeNotify	=	15;
                break;

            // Info : LiveScore [info]
            case 'liveScore_info':
                opts.title = "Live Score Update !!";
                opts.text = str;
                opts.type = "info";
                typeNotify	=	16;
                break;

            // Info : LiveScore [Finish]
            case 'liveScore_finish':
                opts.title = "Full Time !!";
                opts.text = str;
                opts.type = "info";
                typeNotify	=	17;
                break;

            case 'game_good':
                opts.title = "สรุปคะแนนทายผลบอล";
                opts.text = str;
                opts.type = "success";
                typeNotify	=	18;
                break;

            case 'game_bad':
                opts.title = "สรุปคะแนนทายผลบอล";
                opts.text = str;
                opts.type = "error";
                typeNotify	=	19;
                break;

            case 'game_draw':
                opts.title = "สรุปคะแนนทายผลบอล";
                opts.text = str;
                opts.type = "info";
                typeNotify	=	20;
                break;

            /*--------------------------------------------Other---------------------------------------------*/
            // Error : With text
            case 'error_m':
                opts.title 	= "พบความผิดพลาด";
                opts.text 	= str;
                opts.type 	= "error";
                typeNotify	=	21;
                break;

            // success tour
            case 'success_tour':
                opts.title 	= "ยินดีด้วย";
                opts.text 	= "คุณได้คะแนนหลังจากรู้วิธีการ X แต้ม";
                opts.type 	= "success";
                typeNotify	=	22;
                break;
        }

        new PNotify(opts);


    }

    function goToLogin(){
        $("html, body").animate({scrollTop: 0}, "slow", function() {
            if ($(".loginbox").height() == 0) {
                $("a.btnlogin").trigger('click');
            }
        });
    }

    function getCookieByName(str){
        var ca 			= 	document.cookie.split(';');
        var dataCookie	=	"";

        var name = str + "=";
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ')
                c = c.substring(1);
            if (c.indexOf(name) != -1){
                dataCookie = c.substring(name.length,c.length);
                break;
            }
        }

        return dataCookie;
    }

    $(document).ready(function(){
        show_stack_bottomright('error_login','');
    });

</script>

<script src="http://football.kapook.com/assets/js/bootstrap.min.js"></script>
<script src="http://football.kapook.com/assets/js/pace.min.js"></script>
<script src="http://football.kapook.com/assets/js/waves.js"></script>


<!--Plugins-->
<!--<script src="http://football.kapook.com/assets/plugins/dt/js/jquery.dataTables.min.js"></script> -->
<script src="http://football.kapook.com/assets/plugins/Chart.min.js"></script>
<script src="http://football.kapook.com/assets/plugins/waypoints.min.js"></script>

<script src="http://football.kapook.com/assets/plugins/jquery.touchSwipe.min.js"></script>
<script src="http://my.kapook.com/javascript/jsViews/jsviews.min.js"></script>

<!--Custom-->
<script src="assets/js/main.js"></script>
<script src="http://football.kapook.com/assets/js/main_pong.js"></script>

<script src="http://football.kapook.com/assets/plugins/jquery.mousewheel.min.js"></script>
<link href="http://football.kapook.com/assets/plugins/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" />
<script src="http://football.kapook.com/assets/plugins/mCustomScrollbar/jquery.mCustomScrollbar.js"></script>
<!--<script src="http://football.kapook.com/assets/js/main_pong.js"></script>-->



<script src="http://football.kapook.com/default_kapook/js/mainpage.js"></script>

<link href="http://football.kapook.com/assets/plugins/pnotify/pnotify.custom.min.css" rel="stylesheet" />

<script src="http://football.kapook.com/assets/plugins/pnotify/pnotify.custom.min.js"></script>

<script src="http://football.kapook.com/assets/plugins/jquery.touchSwipe.min.js"></script>

<link href="http://football.kapook.com/assets/plugins/Lexxus-jq-timeTo/timeTo.css" rel="stylesheet" />
<script src="http://football.kapook.com/assets/plugins/Lexxus-jq-timeTo/jquery.timeTo.min.js"></script>


<!-- Add Sound -->
<script type="text/javascript" src="http://football.kapook.com/inc/jquery.mb.audio.js"></script>
<script type="text/javascript">

    /*
     * DEFINE SOUNDS
     */
    $.mbAudio.sounds = {

        backgroundSprite: {
            id: "backgroundSprite",
            ogg: "sounds/Goal.ogg",
            mp3: "sounds/Goal.mp3",
            //example of sprite
            sprite:{
                intro     : {id: "intro", start: 80, end: 116.975, loop: true},
                levelIntro: {id: "levelIntro", start: 60, end: 77.470, loop: true},
                tellStory: {id: "tellStory", start: 80, end: 116.975, loop: true},
                level1    : {id: "level1", start: 5, end: 13, loop: true},
                level2    : {id: "level2", start: 40, end: 56, loop: true},
                level3    : {id: "level3", start: 120, end: 136.030, loop: true}
            }
        },

        effectSprite: {
            id: "effectSprite",
            ogg: "sounds/Goal.ogg",
            mp3: "sounds/Goal.mp3",
            //example of sprite
            sprite:{
                streak: {id: "streak", start: 0, end: 1.3, loop: 3},
                great : {id: "great", start: 5, end: 8, loop: false},
                divine: {id: "divine", start: 10, end: 11.6, loop: false},
                wow   : {id: "wow", start: 15, end: 20, loop: false},
                levelIntro    : {id: "levelIntro", start: 20, end: 25, loop: false},
                levelCompleted: {id: "levelCompleted", start: 25, end: 30, loop: false},
                subLevelLost: {id: "subLevelLost", start: 35, end: 38.1, loop: false},
                subLevelWon : {id: "subLevelWon", start: 30, end: 31.9, loop: false},
                gameWon : {id: "gameWon", start: 30, end: 31.9, loop: false}
            }
        }
    };


    function audioIsReady(){

        setTimeout(function(){
            $('#buttons').fadeIn();
            $("#loading").hide();

            if(isStandAlone || !isDevice)
                $.mbAudio.play('backgroundSprite',"level2");

        },3000);

    }

    $(document).on("initAudio", function () {

        //otherwise sound is initialized on the first tap loosing time and UX
        $.mbAudio.play('effectSprite', audioIsReady);


    });


    function fnPlayGoalSound(){
        var SoundStatus = getCookie('SoundStatus');
        if(SoundStatus!=-1){
            $.mbAudio.play('effectSprite', audioIsReady);
        }
    }


    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    function changeSoundCookie(){
        var SoundStatus = getCookie('SoundStatus');

        if((SoundStatus=='') || (SoundStatus==1)){
            setCookie('SoundStatus', -1, 365);
        }else{
            setCookie('SoundStatus', 1, 365);
        }
    }

</script>

<!-- 150806 edit by pachara -->
<!-- end -->

<!-- Footer CHA 2015-02-11 14:15 -->
</body>
</html>
