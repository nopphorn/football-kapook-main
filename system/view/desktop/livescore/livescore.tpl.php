<?php 
$allow_id = array(43,94,363252,478011,786992,797014,856305,930736,36305);
$userid = uid;
$Disable = false;
if($_REQUEST['tbug']=='debug'){
	echo uid;
	exit;
}
$AllMatch1 = $this->ProgramContents1;
$TotalMatch1 = $AllMatch1['TotalMatch'];

for($i=1;$i<=$TotalMatch1;$i++){
    $AllMatchByLeague1[$AllMatch1[$i]['KPLeagueID']][] = $AllMatch1[$i];
}

$AllMatchByLeague[1] = $AllMatchByLeague1;


	$monthtothai['01']['short'] ="ม.ค.";
	$monthtothai['02']['short'] ="ก.พ.";
	$monthtothai['03']['short'] ="มี.ค.";
	$monthtothai['04']['short'] ="เม.ย.";
	$monthtothai['05']['short'] ="พ.ค.";
	$monthtothai['06']['short'] ="มิ.ย.";
	$monthtothai['07']['short'] ="ก.ค.";
	$monthtothai['08']['short'] ="ส.ค.";
	$monthtothai['09']['short'] ="ก.ย.";
	$monthtothai['10']['short'] ="ต.ค.";
	$monthtothai['11']['short'] ="พ.ย.";
	$monthtothai['12']['short'] ="ธ.ค.";
	
	$monthtothai['01']['full'] ="มกราคม";
	$monthtothai['02']['full'] ="กุมภาพันธ์";
	$monthtothai['03']['full'] ="มีนาคม";
	$monthtothai['04']['full'] ="เมษายน";
	$monthtothai['05']['full'] ="พฤษภาคม";
	$monthtothai['06']['full'] ="มิถุนายน";
	$monthtothai['07']['full'] ="กรกฎาคม";
	$monthtothai['08']['full'] ="สิงหาคม";
	$monthtothai['09']['full'] ="กันยายน";
	$monthtothai['10']['full'] ="ตุลาคม";
	$monthtothai['11']['full'] ="พฤศจิกายน";
	$monthtothai['12']['full'] ="ธันวาคม";
	
	$daytothai['Sun'] = "วันอาทิตย์";
	$daytothai['Mon'] = "วันจันทร์";
	$daytothai['Tue'] = "วันอังคาร";
	$daytothai['Wed'] = "วันพุธ";
	$daytothai['Thu'] = "วันพฤหัสบดี";
	$daytothai['Fri'] = "วันศุกร์";
	$daytothai['Sat'] = "วันเสาร์";

	$OddsRate["0.25"] 	= "เสมอควบครึ่ง";
	$OddsRate["0.5"] 	= "ครึ่งลูก";
	$OddsRate["0.75"] 	= "ครึ่งควบลูก";
	$OddsRate["1"] 	= "หนึ่งลูก";
	$OddsRate["1.25"] 	= "ลูกควบลูกครึ่ง";
	$OddsRate["1.5"] 	= "ลูกครึ่ง";
	$OddsRate["1.75"] 	= "ลูกครึ่งควบสองลูก";
	$OddsRate["2"] 	= "สองลูก";
	$OddsRate["2.25"] 	= "สองลูกควบสองลูกครึ่ง";
	$OddsRate["2.5"] 	= "สองลูกครึ่ง";
	$OddsRate["2.75"] 	= "สองลูกครึ่งควบสามลูก";
	$OddsRate["3"] 	= "สามลูก";
	$OddsRate["3.25"] 	= "สามลูกควบสามลูกครึ่ง";
	$OddsRate["3.5"] 	= "สามลูกครึ่ง";
	$OddsRate["3.75"] 	= "สามลูกครึ่งควบสี่ลูก";
	$OddsRate["4"] 	= "สี่ลูก";
	$OddsRate["4.25"] 	= "สี่ลูกควบสี่ลูกครึ่ง";
	$OddsRate["4.5"] 	= "สี่ลูกครึ่ง";
	$OddsRate["4.75"] 	= "สี่ลูกครึ่งควบห้าลูก";
	$OddsRate["5"] 	= "ห้าลูก";
	$OddsRate["5.25"] 	= "ห้าลูกควบห้าลูกครึ่ง";
	$OddsRate["5.5"] 	= "ห้าลูกครึ่ง";
	$OddsRate["5.75"] 	= "ห้าลูกครึ่งควบหกลูก";
	$OddsRate["6"] 	= "หกลูก";
	$OddsRate["6.25"] 	= "หกลูกควบหกลูกครึ่ง";
	$OddsRate["6.5"] 	= "หกลูกครึ่ง";
	$OddsRate["6.75"] 	= "หกลูกครึ่งควบเจ็ดลูก";
	$OddsRate["7"] 	= "เจ็ดลูก";
	$OddsRate["7.25"] 	= "เจ็ดลูกควบเจ็ดลูกครึ่ง";
	$OddsRate["7.5"] 	= "เจ็ดลูกครึ่ง";
	$OddsRate["7.75"] 	= "เจ็ดลูกครึ่งควบแปดลูก";
	$OddsRate["8"] 	= "แปดลูก";
	$OddsRate["8.25"] 	= "แปดลูกควบแปดลูกครึ่ง";
	$OddsRate["8.5"] 	= "แปดลูกครึ่ง";
	$OddsRate["8.75"] 	= "แปดลูกครึ่งควบเก้าลูก";
	$OddsRate["9"] 	= "เก้าลูก";
	$OddsRate["9.25"] 	= "เก้าลูกควบเก้าลูกครึ่ง";
	$OddsRate["9.5"] 	= "เก้าลูกครึ่ง";
	$OddsRate["9.75"] 	= "เก้าลูกครึ่งควบสิบลูก";
	$OddsRate["10"] 	= "สิบลูก";
    
        $DateProgram[5]=date("Y-m-d");
        $DateProgram[4]=date("Y-m-d",strtotime($date . ' - 1 day'));
        $DateProgram[3]=date("Y-m-d",strtotime($date . ' - 2 day'));
        $DateProgram[2]=date("Y-m-d",strtotime($date . ' - 3 day'));
        $DateProgram[1]=date("Y-m-d",strtotime($date . ' - 4 day'));
        
        $ClassColorByMatchStatus['Fin'] = 'btn-grey';
        $ClassColorByMatchStatus['1 HF'] = 'btn-success';
        $ClassColorByMatchStatus['2 HF'] = 'btn-success';
        $ClassColorByMatchStatus['Sched'] = '';
        $ClassColorByMatchStatus['H/T'] = 'btn-warning';
      

?>
  <div class="container analyze">
  
   <div class="page-header-option">
            <p>
            <?php echo $daytothai[date("D")]." ".date("d")." ".$monthtothai[date("m")]['full']." ".(date("Y")+543); ?> &nbsp; &nbsp;
          
          </p>
</div> 
<h1 class="page-header font-display">ผลบอลสด</h1>   
    
<style>
	@media only screen and (min-width : 768px) {  iframe { width:640px !important; height:360px !important ; margin:auto; display:block} }
	@media only screen and (min-width : 481px) and (max-width : 768px) { iframe { width:100% !important; height:360px !important ; margin:auto;display:block } }
	@media only screen and (min-width : 321px) and (max-width : 480px) { iframe { width:100% !important; height:205px !important ; margin:auto;display:block } }
	@media only screen and (max-width : 320px) { iframe { width:100% !important; height:151px !important; margin:auto;display:block } }
</style>

<div class="row">
	
   <!-- Start LEFT COLUME --> 
  <div class="col-md-9">

  
<?php
	
	if ( in_array($userid,$allow_id) && ($_SERVER['REQUEST_METHOD'] == 'POST') && ($_POST['do']=="updateclip")) {
		//var_dump($_POST);
		$this->mem_lib->set('Football2014-Clip-LiveScore-URL',$_POST['clip_livescore_url']);

		echo "Clip Updated";
	}
		if($this->mem_lib->get('Football2014-Clip-LiveScore-URL')==''){
			$Disable = true;
		}else{
			$Disable = false;
		}
	if(!$Disable){
?>
	<iframe src='<?php if(!$this->mem_lib->get('Football2014-Clip-LiveScore-URL')){ ?>http://video.kapook.com/content/iframe/546194d638217a2341000000<?php }else{ echo $this->mem_lib->get('Football2014-Clip-LiveScore-URL'); } ?>'  frameborder='0' scrolling='no' allowFullScreen></iframe>

<?php
	  }
		
  if(in_array($userid,$allow_id)){
  ?>
	<form action="" method="post">
	<legend>URL of Clip :</legend>
	<input type="hidden" name="do" id="do" value="updateclip">
	<input type="text" name="clip_livescore_url" size="60" id="clip_livescore_url" value="<?php echo $this->mem_lib->get('Football2014-Clip-LiveScore-URL'); ?>">
	<input type="submit" value="Update clip">
	</form>
  <?php
  }
  ?>
  
  <h2 class="font-display"></h2>


<!-- Tab panes -->
<div class="tab-content">
  <?php 
	$i=1;
	$tmpDateArr = explode('-',$DateProgram[$i]);             
	//$AllLiveMatchID = '0'; //use $this->AllLiveMatchID
	//$Counter['Live'] = 0; //use $this->CounterMatch['Live']
	//$Counter['Sched'] = 0; //use $this->CounterMatch['Sched']
	//$NextLiveDate = ''; //user $this->$NextLiveDate
	$LiveStatusArray = array('Sched','1 HF','2 HF','E/T','Pen');

  ?>
  <div class="tab-pane active" id="d<?php echo $i;?>">

      <table class="table  responsive match-game boder-1">

      <tbody>
      <?php 
      
      ksort($AllMatch1['KPLeagueIDOrder']);
      
      //foreach($AllMatchByLeague[$i] as $League=>$AllMatchInLeague){ 
      
      foreach($AllMatch1['KPLeagueIDOrder'] as $Order=>$League){    
          $AllMatchInLeague = $AllMatchByLeague[$i][$League];
      ?>
        <tr>
          <td colspan="6" class="table_tr_head"><!--<a href="" class="tooltip_top" title="<?php echo $this->mem_lib->get('Football2014-League-NameTH-'.$League);?>" target="_blank">--> ผลบอลสด livescore <?php echo $this->mem_lib->get('Football2014-League-NameTHShort-'.$League);?><!--</a>--></td>
        </tr>
        
        <?php foreach( $AllMatchInLeague as $MatchInfo ){
				
				$MatchDate = $MatchInfo['MatchDate'].' '.$MatchInfo['MatchTime'].':00';
				$this->LiveMatchListTime[$MatchInfo['id']]		=	$MatchDate;

                if(in_array($MatchInfo['MatchStatus'],$LiveStatusArray)){
                    $this->AllLiveMatchID.=','.$MatchInfo['id']; 
                    if($MatchInfo['MatchStatus']=='Sched'){
                        $this->CounterMatch['Sched'] += 1;
                        if(($this->NextLiveDate=='') || ($MatchDate<$this->NextLiveDate)){                      
                            $this->NextLiveDate = $MatchDate;
                        }
                        
                    }else{
                        $this->CounterMatch['Live'] += 1;
                    }
                }
            
            ?>
                
        <tr>
          <td><?php echo $MatchInfo['MatchTime']; ?></td>
          <td class="text_right"><a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchInfo['Team1KPID']))); ?>" class="tooltip_top match" title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank"><span><?php echo $MatchInfo['Team1']; ?> <img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team1Logo']); ?>"  width="24"></span></a></td>
          <td class="text_center" id="tdVS<?php echo $MatchInfo['id']; ?>">
              
              <a href="<?php echo strtolower($MatchInfo['MatchPageURL']); ?>" target="_blank"   class="btn <?php echo $ClassColorByMatchStatus[$MatchInfo['MatchStatus']];?> btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"  id="aVS<?php echo $MatchInfo['id']; ?>">
                    <?php if($MatchInfo['MatchStatus']=='Sched'){?>
                          vs
                    <?php }else if($MatchInfo['MatchStatus']=='Fin'){?>
                          <?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?>
                    <?php }else if($MatchInfo['MatchStatus']=='Canc'){?>
                          c-c
                    <?php }else if($MatchInfo['MatchStatus']=='Post'){?>
                          p-p
                    <?php }else if($MatchInfo['MatchStatus']=='1 HF'){?> 
                        <?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?><br>
                        1 HF <?php echo $MatchInfo['Minute'];?>'
                    <?php }else if($MatchInfo['MatchStatus']=='2 HF'){?>  
                        <?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?><br>
                        2 HF <?php echo $MatchInfo['Minute'];?>'   
                    <?php }else if($MatchInfo['MatchStatus']=='H/T'){?>  
                          <?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?><br>
                          H/T
                    <?php }else if($MatchInfo['MatchStatus']=='Pen'){?>  
                          <?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?><br>
                          Pen ( <?php echo $MatchInfo['PNScore']; ?> )
                    <?php }else if($MatchInfo['MatchStatus']=='E/T'){?>  
                          <?php echo $MatchInfo['Team1FTScore']; ?> - <?php echo $MatchInfo['Team2FTScore']; ?><br>
                          E/T ( <?php echo $MatchInfo['ETScore']; ?> )
                    <?php }else{?>
                          N/A
                    <?php }?>
              </a>
              
          </td>
          <td class="text_left"><a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchInfo['Team2KPID']))); ?>" class="tooltip_top match" title="คลิกเพื่อเข้าดูรายละเอียดทีม" target="_blank"><span><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchInfo['Team2Logo']); ?>"  width="24" > <?php echo $MatchInfo['Team2']; ?></span></a></td>

           <td class="channel"><?php foreach($MatchInfo['TVLiveList'] as $ChannelKey){?><img src="http://football.kapook.com/uploads/tvlogo/<?php echo $ChannelKey;?>.png" title="<?php echo $this->tv_lib->getTVText($ChannelKey); ?>"><?php }?></td>
           <td> <a href="<?php echo strtolower($MatchInfo['MatchPageURL']); ?>" target="_blank" class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><i class="fa fa-bar-chart-o"></i></a> </td>
        </tr>  
        
        <?php }?>
        
      <?php }?>  
      </tbody>
    </table>
 
 </div>
    
 
</div>
  
  
  
  
  
  
  
  
  
  
     

  
  
</div>
<!--End Col 8 LEFT VOLUME-->









<!--Start Right Side Colume-->
<!--ข่าวฮิตประจำสัปดาห์ -->
  <div class="col-md-3 aside">
  
  
        <div class="sidebar-social">
                 <div class="share share_size_large share_type_facebook">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" >Share</a>
                </div>
                 <div class="share share_size_large share_type_twitter">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" >Tweet</a>
                </div>
               
                <div class="share share_size_large share_type_gplus">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="" >+1</a>
                </div>
                
                <div class="share share_size_large share_type_email">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="">E-mail</a>
                </div>
                
                

                
     </div>
  
  
  
      <div class="sidebar_margin" ><h2 class="font-display">ผลบอล</h2>

        <ul class="nav ">
        <li><a href="/พรีเมียร์ลีก/result-program" target="_blank">ผลบอล พรีเมียร์ลีก อังกฤษ</a></li>
        <li><a href="/ลาลีกา/result-program" target="_blank">ผลบอล ลา ลีกา สเปน</a></li>
        <li><a href="/บุนนเดสลีกา/result-program" target="_blank"> ผลบอล บุนเดสลีกา เยอรมนี</a></li>
        <li><a href="/เซเรียอา/result-program" target="_blank">ผลบอล เซเรีย อา อิตาลี</a></li>
        <li><a href="/ลีกเอิง/result-program" target="_blank">ผลบอล ลีกเอิง ฝรั่งเศส</a></li>
        <li><a href="/ไทยพรีเมียร์ลีก/result-program" target="_blank">ผลบอล ไทยพรีเมียร์ลีก</a></li>
        <hr>
		<li><a href="<?php echo BASE_HREF; ?>team-manchester-utd" title="ผลบอล แมนฯ ยูไนเต็ด">ผลบอล แมนฯ ยูไนเต็ด</a></li>
		<li><a href="<?php echo BASE_HREF; ?>team-liverpool" title="ผลบอล ลิเวอร์พูล">ผลบอล ลิเวอร์พูล</a></li>
		<li><a href="<?php echo BASE_HREF; ?>team-chelsea" title="ผลบอล เชลซี">ผลบอล เชลซี</a></li>
		<li><a href="<?php echo BASE_HREF; ?>team-arsenal" title="ผลบอล อาร์เซน่อล">ผลบอล อาร์เซน่อล</a></li>
		<li><a href="<?php echo BASE_HREF; ?>team-manchester-city" title="ผลบอล แมนเชสเตอร์ ซิตี้">ผลบอล แมนเชสเตอร์ ซิตี้</a></li>
		<li><a href="<?php echo BASE_HREF; ?>team-real-madrid" title="ผลบอล เรอัล มาดริด">ผลบอล เรอัล มาดริด</a></li>
		<li><a href="<?php echo BASE_HREF; ?>team-barcelona" title="ผลบอล บาร์เซโลน่า">ผลบอล บาร์เซโลน่า</a></li>
		<li><a href="<?php echo BASE_HREF; ?>team-bayern-munich" title="ผลบอล บาเยิร์น มิวนิค">ผลบอล บาเยิร์น มิวนิค</a></li>
		<li><a href="<?php echo BASE_HREF; ?>team-ac-milan" title="ผลบอล เอซี มิลาน">ผลบอล เอซี มิลาน</a></li>
        </ul>


</div>



    
  
  
  
  
  </div><!--End right 4 Aside -->
  
  
  
  
</div>