<?php if (!defined('MINIZONE')) exit;

class Squad extends My_con 
{
    var $view; 
    var $minizone; 
 
    
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() 
    {

        parent::__construct();

        $this->squad_model = $this->minizone->model('squad_model');
        if(!isset($this->livescore_model)){
			$this->livescore_model = $this->minizone->model('livescore_model');
		}
		$this->news_model = $this->minizone->model('news_model');
    }
    
   ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
    function index($teamNameEN) {
		if($_REQUEST['tbug']==1){
			echo "ORI : ".$teamNameEN;
			$teamID = $this->mem_lib->get('Football2014-Team-IDByName-'.$teamNameEN);
			echo "</br>ORI ID : ".$teamID;
			
			echo "</br>UPPER : ".strtoupper($teamNameEN);
			$teamIDUP = $this->mem_lib->get('Football2014-Team-IDByName-'.$teamNameEN);
			echo "</br>UPPER ID : ".$teamIDUP;
			
			var_dump($this->mem_lib);
			exit;
		}
		$teamID = $this->mem_lib->get('Football2014-Team-IDByName-'.$teamNameEN);
		if(!$teamID){ header("Location: http://football.kapook.com");}
		if($_REQUEST['remove_cache']==1){
			$teamInfo = $this->squad_model->getTeamInfo($teamID,true);
		}else{
			$teamInfo = $this->squad_model->getTeamInfo($teamID);
		}
		
		if(!empty($teamInfo['DuplicateList'])){
			$list_all	=	implode( ',' , $teamInfo['DuplicateList'] );
			$list_all	=	implode( ',' , array($list_all,$teamID) );
		}else{
			$list_all	=	$teamID;
		}
		
		//$from_date = date("Y-m-d",strtotime(date("Y-m-d H:i:s")." - 180 days"));
		$from_date = "2014-01-01";
        $to_date = date("Y-m-d",strtotime(date("Y-m-d H:i:s")." + 15 days"));
		$teamMatch = $this->mem_lib->get('Football2014-MatchAll-'.$teamID);
		if((!$teamMatch)||($_REQUEST['remove_cache']==1)){
			$teamMatch = $this->livescore_model->getMatchByTeam($list_all,1000,$from_date,$to_date);
			$this->mem_lib->set('Football2014-MatchAll-'.$teamID,$teamMatch, MEMCACHE_COMPRESSED, 1800);
		}
		$teamNewsArr = $this->news_model->getNewsByTeam($list_all,1,$_REQUEST['remove_cache'] == 1);
		for($i=0;$i<8;$i++){
			$teamNews[$i]=$teamNewsArr['data'][$i];
		}
		$j=0;
		for($i=$teamMatch['TotalMatch'];$i>=1;$i--){
			//echo 	"</br> date : ".$teamMatch[$i]['MatchDate'];
			if(strtotime($teamMatch[$i]['MatchTime']) < strtotime("06:00:00") ){
					$teamMatch[$i]['MatchDate'] = date("Y-m-d", strtotime($teamMatch[$i]['MatchDate']." - 1 days") );
			}
			$Mdate = explode('-',$teamMatch[$i]['MatchDate']);
			if($teamMatch[$i]['MatchDate']!=$teamMatch[$i-1]['MatchDate']){
				$j=0;
			}
			$Program[$Mdate[0].'-'.$Mdate[1]][$Mdate[2]]['totalmatch']= ++$j;
			$Program[$Mdate[0].'-'.$Mdate[1]][$Mdate[2]][] = $teamMatch[$i];
		
			if($teamMatch[$i]['MatchStatus']=="Sched" ){
				$teamNextMatch[] = $teamMatch[$i];
			}
			
		}
		$teaml = $this->mem_lib->get('Football2014-Team-NameTH-'.$teamID);
		$teamNameTH = $this->mem_lib->get('Football2014-Team-NameTHShort-'.$teamID);
		$teamNameEN = str_replace("-"," ",$teamNameEN);
		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = $teamNameTH;
		$breadcrum[1]['active'] = true;
        parent::_setBreadcrum($breadcrum);
		
		$data['seo_title'] = "$teamNameTH ผลบอล ข่าว$teamNameTH";
		$data['seo_description'] = "$teamNameTH ผลบอล$teamNameTH ข่าว$teamNameTH ตารางแข่ง$teamNameTH ดู สรุปผลบอล$teamNameTH สถิติย้อนหลัง ข่าว" . $teamNameTH . "ซื้อนักเตะ ";
		$data['seo_keywords'] = "$teamNameTH, ผลบอล$teamNameTH, ข่าว$teamNameTH, ตารางแข่ง$teamNameTH, ข่าว" . $teamNameTH . "ซื้อนักเตะ, $teaml";

		$data['fb_title'] = "$teamNameTH ข่าว$teamNameTH ผลบอล ข่าว$teaml";
		$data['fb_description'] = "ผลบอล$teamNameTH ข่าว$teamNameTH โปรแกรมการแข่งขัน ดูตารางแข่ง $teaml สรุปผลบอล$teamNameTH คลิกเลย";
		
		parent::_setSocial($data);
		$this->view->assign('teamName',$teamNameEN);
		$this->view->assign('teamInfo',$teamInfo);
		$this->view->assign('teamNameTH',$teamNameTH);
		$this->view->assign('teamAllMatch',$Program);
		$this->view->assign('teamNextMatch',$teamNextMatch);
		$this->view->assign('teamNews',$teamNews);
		
		$this->view->assign('tv_lib', $this->minizone->library('tv_lib'));
		
        $this->_header();
        $this->view->render($this->root_view.'/squad/squad.tpl');
        $this->_footer();
    }
	
}