<?php

if (!defined('MINIZONE'))
    exit;

class Home extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {

        parent::__construct();

		if(!isset($this->games_model)){
			$this->games_model = $this->minizone->model('games_model');
		}
		if(!isset($this->livescore_model)){
			$this->livescore_model = $this->minizone->model('livescore_model');
		}
		
        $this->news_model = $this->minizone->model('news_model');
		$this->comp_model = $this->minizone->model('comp_model');
		
    }

    ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
    function index() {
        //var_dump($this->mem_lib);
		/*-------------REDIS----------------*/
		$redis_server = '192.168.1.78-6379-9';
		$redis = $this->minizone->library('Predis');
		$redis->setConfig($redis_server);
		/*----------------------------------*/

        $this->view->assign('mem_lib', $this->mem_lib);
		
		if(intval(date('G'))<12){
			$LoadDate 						= 		date('Y-m-d',strtotime("-1 days"));
			if((intval(date('G'))>=6)&&(intval(date('G'))<12)){
				$expire 					= 		345600;
			}else{
				$expire 					= 		60;
			}
		}else{
			$LoadDate 						= 		date('Y-m-d');
			$expire 						= 		60;
		}
		$LiveScoreContents_MC 				= 		$this->mem_lib->get('Football2014-LiveScore-'.$LoadDate);
		
        if ($LiveScoreContents_MC && ($_REQUEST['remove_cache'] != 1)) {
            $LiveScoreContents = $LiveScoreContents_MC;
			//echo $LiveScoreContents['cache_time'];
		} else {
			$LiveScoreContents = $this->livescore_model->getProgramByDate($LoadDate);
			$LiveScoreContents['cache_time'] = $expire;
            $this->mem_lib->set('Football2014-LiveScore-'.$LoadDate, $LiveScoreContents, MEMCACHE_COMPRESSED, $expire);
        }
        $this->view->assign('LiveScoreContents', $LiveScoreContents);

        if ($_REQUEST['debug'] == 1) {
            var_dump($LiveScoreContents);
        }

		//---------------------------------Match of League---------------------------------//
		$ProgramByLeague = array();
		//$listLeagueName = array('premierleague','laliga','bundesliga','seriea','ligue1','thaipremierleague','championsleague');
		$listLeagueName = array('euro2016','internationalfriendly','clubfriendly','thaipremierleague');
		$indexLeague = 1;
		foreach($listLeagueName as $strLeague){
			$ProgramByLeague[$indexLeague] 	= 		$this->mem_lib->get('ProgramTournament-' . $strLeague . '-' . $LoadDate);
			if((!$ProgramByLeague[$indexLeague])||($_REQUEST['remove_cache']=='1')){
				$ProgramByLeague[$indexLeague]		=		$this->comp_model->getProgramByNameDate($strLeague,$LoadDate,true);
				$this->mem_lib->set('ProgramTournament-' . $strLeague . '-' . $LoadDate, $ProgramByLeague[$indexLeague], MEMCACHE_COMPRESSED, $expire);
			}
			$indexLeague++;
		}
		$this->view->assign('ProgramByLeague', $ProgramByLeague);
		//---------------------------------------------------------------------------------//
		
		$NewsContents = json_decode($redis->get("newslist_8_1_news_id"),true);
		if((!$NewsContents)||($_REQUEST['remove_cache']==1)){
			$NewsContents = $this->news_model->getNews($_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsContents', $NewsContents['data']);

		$NewsThaiLeagueContents_MC = $this->mem_lib->get('Football2014-ThaiLeagueNews');
		if((!$NewsThaiLeagueContents_MC)||($_REQUEST['remove_cache'] == 1)){
			$NewsThaiLeagueContents = $this->news_model->getNewsLeague(8, 1, $_REQUEST['remove_cache'] == 1);
			$expire = (3600 * 3);
			$this->mem_lib->set('Football2014-ThaiLeagueNews', $NewsThaiLeagueContents, MEMCACHE_COMPRESSED, $expire);
		}else{
			$NewsThaiLeagueContents		=		$NewsThaiLeagueContents_MC;
		}
		$this->view->assign('NewsThaiListContents', $NewsThaiLeagueContents['data']);
		
		$NewsTopHitsContents = json_decode($redis->get("newstophit_5_1"),true);
		if((!$NewsTopHitsContents)||($_REQUEST['remove_cache']==1)){
			 $NewsTopHitsContents = $this->news_model->getNewsTopHits($_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsTopHitsContents', $NewsTopHitsContents['data']);
		
		$NewsTop10Contents = json_decode($redis->get("newstop10_3_1_news_id"),true);
		if((!$NewsTop10Contents)||($_REQUEST['remove_cache']==1)){
			$NewsTop10Contents = $this->news_model->getNewsTop10($_REQUEST['remove_cache'] == 1);
		}
        $this->view->assign('NewsTop10Contents', $NewsTop10Contents['data']);

        //$this->mem_lib->delete('Football2014-MatchAnalysis');
		if(date('G')>=6){
			$dateAnalyze	=	date("Y-m-d");
		}else{
			$dateAnalyze	=	date("Y-m-d",strtotime('-1 day'));
		}
        $MatchAnalysis_MC = $this->mem_lib->get('Football2014-MatchAnalysis-' . $dateAnalyze );
        if ($MatchAnalysis_MC && ($_REQUEST['remove_cache'] != 1)) {
            $MatchAnalyzeContensByDate = $MatchAnalysis_MC;
		} else {
            $MatchContents = $this->livescore_model->getMatchAnalysis($dateAnalyze);
            for ($i = 1; $i <= $MatchContents['TotalMatch']; $i++) {
				$MatchDateTime = $MatchContents[$i]['MatchDate'] . ' ' . $MatchContents[$i]['MatchTime'] . ':00';
				if ($MatchDateTime < $dateAnalyze . " 06:00:00") {
					$MatchAnalyzeContensByDate['Yesterday'][] = $MatchContents[$i];
				} else {
					$MatchAnalyzeContensByDate['Today'][] = $MatchContents[$i];
				}
            }

            $expire = (3600 * 1);
            $this->mem_lib->set('Football2014-MatchAnalysis-' . $dateAnalyze , $MatchAnalyzeContensByDate, MEMCACHE_COMPRESSED, $expire);
        }
		
        if ($_REQUEST['tbug'] == 3) {
            var_dump($MatchAnalyzeContensByDate);
            exit;
        }
		
		if ($_REQUEST['tbug'] == 1) {
            var_dump($LiveScoreContents);
            exit;
        }
        
		$LiveScoreTVLive_MC 	= 		$this->mem_lib->get('Football2014-LiveScore-' . $dateAnalyze);
		if((!$LiveScoreTVLive_MC)||($_REQUEST['remove_cache'] == 1)){
			$expire = 600;
			$LiveScoreTVLive_MC = $this->livescore_model->getProgramByDate($dateAnalyze);
			$this->mem_lib->set('Football2014-LiveScore-' . $dateAnalyze, $LiveScoreTVLive_MC, MEMCACHE_COMPRESSED, $expire);
		}else{
			if($_REQUEST['tbug'] == 4){
				var_dump($LiveScoreTVLive_MC);
			}
		}
		
		for ($i = 1; $i <= $LiveScoreTVLive_MC['TotalMatch']; $i++) {
			if (count($LiveScoreTVLive_MC[$i]['TVLiveList']) > 0) {
				$MatchLiveContens[] = $LiveScoreTVLive_MC[$i];
			}
		}
		
		$AllLeagueLiveByOrderArr	=	$LiveScoreTVLive_MC['KPLeagueIDOrder'];
		$AllLeagueLiveZone			=	$LiveScoreTVLive_MC['KPLeagueZoneID'];
		
        if ($_REQUEST['debug'] == 1) {
            echo $MatchDateTime; // count($MatchAnalyzeContensByDate['Today']);// "<pre> data : "; var_dump($MatchAnalyzeContensByDate); echo "</pre>"; 
            var_dump($MatchContents);
            exit();
        }
		
		$TopPlaySide_MC 	= 	$this->mem_lib->get('Football2014-TopPlaySide' );
		if(($TopPlaySide_MC)&&($_REQUEST['remove_cache'] != 1)){
			$TopPlaySide		=		$TopPlaySide_MC;
		}else{
			$TopPlaySide		=		$this->games_model->getTopPlayside(true);
            $this->mem_lib->set('Football2014-TopPlaySide' , $TopPlaySide, MEMCACHE_COMPRESSED, 300);
		}
		
		if(date('G')>=6){
			$attrDay			=	date('Y-m-d',strtotime('now -1 day'));
		}else{
			$attrDay			=	date('Y-m-d',strtotime('now -2 day'));
		}
		$Ranking_MC 	= 	$this->mem_lib->get( 'Football2014-MiniRanking' );
		if(($Ranking_MC)&&($_REQUEST['remove_cache'] != 1)){
			$RankingDaily		=		$Ranking_MC;
		}else{
			$RankingDaily		=		$this->games_model->getRanking('date', $attrDay, 'total_point', 'desc', 0, 5, true, true);
            $this->mem_lib->set('Football2014-MiniRanking' , $RankingDaily, MEMCACHE_COMPRESSED, 1800);
		}
		
		/*if(uid==856305){
			echo '<pre>';
			var_dump($RankingDaily);
			echo '</pre>';
		}*/
		
		
		// My Ranking
		$uid	=	$this->login_lib->checkCookie();
		if($uid)
		{
			$InfoMember		=	$this->login_lib->get_KMember(array( 'userid' =>  $uid));
			$MyInfoScoreMember['username']	=	$InfoMember->username;
			$MyInfoScoreMember['picture']	=	$InfoMember->picture;
			
			$MyRankingDaily 	= 	$this->mem_lib->get('Football2014-MyRankingDaily-' . $uid );
			if((!$MyRankingDaily)||($_REQUEST['remove_cache'] != 1)){
				$MyRankingDaily		=	$this->games_model->getMyRanking($uid,'date', $attrDay);
				$this->mem_lib->set('Football2014-MyRankingDaily-' . $uid , $MyRankingDaily, MEMCACHE_COMPRESSED, 60);
			}

			$this->view->assign('MyInfoScoreMember', $MyInfoScoreMember);
			$this->view->assign('MyRankingDaily', $MyRankingDaily);

		}

        $this->view->assign('MatchAnalyzeContensByDate', $MatchAnalyzeContensByDate);
        $this->view->assign('MatchLiveContents', $MatchLiveContens);
		$this->view->assign('AllLeagueLiveByOrderArr', $AllLeagueLiveByOrderArr);
		$this->view->assign('AllLeagueLiveZone', $AllLeagueLiveZone);
        $this->view->assign('TopPlaySide', $TopPlaySide);
		$this->view->assign('RankingDaily', $RankingDaily);
		
		$this->view->assign('tv_lib', $this->minizone->library('tv_lib'));
		$this->view->assign('datetimeformat_lib', $this->minizone->library('datetimeformat_lib'));
		
        $data['seo_title'] = 'ผลบอล ผลบอลสด บอลวันนี้ วิเคราะห์บอล ข่าว ฟุตบอล คลิกเลย';
        $data['seo_description'] = 'ผลบอล ผลบอลสด ตารางบอล บอลวันนี้ เช็คผลบอล ผลบอลเมื่อคืน livescore โปรแกรมบอล วิเคราะห์บอล ทีเด็ด แม่นๆ ข่าว ฟุตบอล ข่าวซื้อขายนักเตะ ดู ตารางถ่ายทอดสด คลิปบอล พรีเมียร์ลีก ผลบอลไทย ตารางคะแนน อันดับดาวซัลโว';
        $data['seo_keywords'] = 'ผลบอล, ผลบอลเมื่อคืน, ผลบอลวันนี้, ผลบอลล่าสุด, เช็คผลบอล, ผลบอลสด, livescore, วิเคราะห์บอล, วิเคราะห์บอลวันนี้, ทีเด็ด, ราคาบอล, ตารางบอล, ตารางบอลวันนี้, โปรแกรมบอลพรุ่งนี้, ตารางบอลพรุ่งนี้, โปรแกรมบอลล่วงหน้า, ดูบอล, ข่าว, ข่าวฟุตบอล, ข่าวบอล, ข่าวแมนยู, ข่าวลิเวอร์พูล, พรีเมียร์ลีก, ผลบอลพรีเมียร์ลีก, ตารางคะแนน, ไทยพรีเมียร์ลีก';

        $data['fb_title'] = 'ผลบอล บอลวันนี้ ผลบอลสด วิเคราะห์บอล รวม ข่าว ฟุตบอล';
        $data['fb_description'] = 'ผลบอล ผลบอลสด ตารางบอล เช็คผลบอลเมื่อคืน วิเคราะห์บอล ทีเด็ด แม่นๆ ข่าวฟุตบอล พรีเมียร์ลีก ผลบอลไทย ตารางคะแนน';
        $data['fb_img'] = 'http://football.kapook.com/assets/img/og-football-new.jpg';
		$data['fb_img'] = 'http://football.kapook.com/assets/og/2014-11/page/og-football-new.jpg';
        parent::_setSocial($data);

        parent::_setBreadcrum();

		$this->_header('newheader.tpl');
		$this->view->render($this->root_view . '/home/index.tpl');
		$this->_footer();
    }

    function new2() {
//            $this->_header();
        $this->view->render($this->root_view . '/home/index.tpl');
        $this->_footer();
    }

    function debug() {
        //$getNews_result_MC = $this->mem_lib->get('Football2014-getNews_result');
        echo 'getNews_result_MC : <br>';
        var_dump($getNews_result_MC);
    }
	
	function debug_user_cache( $userid = null ) {
		if(!$userid){
			$GameData_MC = $this->mem_lib->get('Football2014-GameData-' . uid);
		}else{
			$GameData_MC = $this->mem_lib->get('Football2014-GameData-' . $userid);
		}
        echo 'GameData_MC : <br><pre>';
        var_dump($GameData_MC);
		echo '</pre>';
    }
	
	function delete_cache() {
		$this->mem_lib->delete('Football2014-GameData-' . uid);
        echo 'Delete GameData_MC : <br><pre>';
    }
	
	function delete_cache_match() {
		/*for($i=53166;$i<=57842;$i++){
			$this->mem_lib->delete('Football2014-MatchContents-' . $i);
		}*/
        echo 'Delete Match_MC : <br><pre>';
    }
}
