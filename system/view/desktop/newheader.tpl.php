<?php
include "header.tpl.php";

	$LiveScoreContents = $this->LiveScoreContents;

	$AllLeagueArr = $LiveScoreContents['KPLeagueID'];
	$AllLeagueByOrderArr = $LiveScoreContents['KPLeagueIDOrder'];
	$AllLeagueZone = $LiveScoreContents['KPLeagueZoneID'];

	ksort($AllLeagueByOrderArr);

	$index=0;
	foreach($AllLeagueByOrderArr as $OrderIndex => $LeagueID){
		$index+=1; 
		$LeagueByIndexArr[$index]['ID']     = $LeagueID;
		$LeagueByIndexArr[$index]['Name']   = $LiveScoreContents['KPLeagueNameTHShort'][$LeagueID];
		$LeagueByIndexArr[$index]['Zone']   = $AllLeagueZone[$LeagueID];
	}
	$TotalLeague = $index;

	$TotalMatch = $LiveScoreContents['TotalMatch'];
	for($i=1;$i<=$TotalMatch;$i++){
		$tmpMatch = $LiveScoreContents[$i];
		$MathByLeagueIDArr[$tmpMatch['KPLeagueID']][] = $tmpMatch;    
	}

	if(date('G')<12){
		$tmpDateShow 	= 	strtotime(date("Y-m-d 06:00:00",strtotime(' - 1 days')));
		$DateShow 		= 	$this->datetimeformat_lib->thai_full($tmpDateShow,false);
		$textWithDate	=	'เมื่อวาน';
	}else{
		$DateShow 		= 	$this->datetimeformat_lib->thai_full('',false);
		$textWithDate	=	'วันนี้';
	}

	if(!isset($_COOKIE[$_COOKIE["uid"]."view"])){
		$_COOKIE[$_COOKIE["uid"]."view"]	=	'hide';
	}
	
	//$listLeagueFlag = array(-1,76,92,33,90,63,7,9);
	$listLeagueFlag = array(-1,9,3,3,7);
	
?>


<script type="text/javascript">
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}
var cname = getCookie("uid")+"view";
var exdays = 365;
$(document).ready(function(){
	$("#show").click(function showpage(){
		$("#table1").hide();
		$("#table2").hide();
		$("#table3").hide();
		$("#table4").hide();
		$("#table5").hide();
		$("#table6").hide();
		$("#table7").hide();
		$("#table8").hide();
		$("#page1").show();
		$("#page2").show();
		$("#page3").show();
		$("#page4").show();
		$("#page5").show();
		$("#page6").show();
		$("#page7").show();
		$("#page8").show();
		var cvalue = "show";
		setCookie(cname, cvalue, exdays);
	});
	
	$("#hide").click(function hidepage(){
		$("#table1").show();
		$("#table2").show();
		$("#table3").show();
		$("#table4").show();
		$("#table5").show();
		$("#table6").show();
		$("#table7").show();
		$("#table8").show();
		$("#page1").hide();
		$("#page2").hide();
		$("#page3").hide();
		$("#page4").hide();
		$("#page5").hide();
		$("#page6").hide();
		$("#page7").hide();
		$("#page8").hide();	
		var cvalue = "hide";
		setCookie(cname, cvalue, exdays);
	});
});

</script>

<style>
.handicap_after:after {
    font-family: FontAwesome;
    content: " \f111";
    display: inline;
	color: #428bca;
}
.handicap_before:before {
    font-family: FontAwesome;
    content: "\f111  ";
    display: inline;
	color: #428bca;
}
</style>

	<!-- Live Ex Bar-->
	<div class="container scoreboard-x" >

		<div class="pull-left">
			<div class="head"><?php echo $textWithDate; ?> <?php echo $DateShow; ?>
				<div class="view-mode">
					<div class="btn-group" data-toggle="buttons">
					<?php $cookies = $_COOKIE["uid"]."view"; if(($_COOKIE[$cookies] != "hide")){ ?>
						<label class="btn btn-default active" id="show"><input type="radio" name="options" id="option1" checked> <i class="fa fa-th"></i></label>
						<label class="btn btn-default" id="hide"><input type="radio" name="options" id="option2"> <i class="fa fa-th-list"></i></label>
					<?php }else{ ?>
						<label class="btn btn-default" id="show"><input type="radio" name="options" id="option1"> <i class="fa fa-th"></i></label>
						<label class="btn btn-default active" id="hide"><input type="radio" name="options" id="option2" checked> <i class="fa fa-th-list"></i></label>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<!-- Nav tabs -->    
		<ul class="nav nav-tabs hidden-sm hidden-xs" role="tablist">
			<?php /* OPEN SEASON LEAGUE
			<li class="active"><a href="#tab8" role="tab" data-toggle="tab"> ทั้งหมด</a></li>
			<li><a href="#tab7" role="tab" data-toggle="tab">แชมเปียนส์ลีก</a></li>
			<li><a href="#tab6" role="tab" data-toggle="tab">ไทยลีก</a></li>
			<li><a href="#tab5" role="tab" data-toggle="tab">ลีกเอิง</a></li>
			<li><a href="#tab4" role="tab" data-toggle="tab">เซเรียอา</a></li>
			<li><a href="#tab3" role="tab" data-toggle="tab">บุนเดสลีกา</a></li>
			<li><a href="#tab2" role="tab" data-toggle="tab">ลาลีกา</a></li>
			<li><a href="#tab1" role="tab" data-toggle="tab">พรีเมียร์ลีก</a></li>
			*/ ?>
			<li class="active"><a href="#tab5" role="tab" data-toggle="tab"> ทั้งหมด</a></li>
			<li><a href="#tab4" role="tab" data-toggle="tab">ไทยลีก</a></li>
			<li><a href="#tab3" role="tab" data-toggle="tab">อุ่นเครื่องสโมสร</a></li>
			<li><a href="#tab2" role="tab" data-toggle="tab">อุ่นเครื่องทีมชาติ</a></li>
			<li><a href="#tab1" role="tab" data-toggle="tab">ยูโร 2016</a></li>
		</ul>
		
		
		<div class="selectLeague visible-xs-block visible-sm-block" style="margin: 50px 0 20px 0">
			<select class="form-control " >
				<?php /* OPEN SEASON LEAGUE
				<option  value="tab8">ทั้งหมด</option>
				<option  value="tab7">แชมเปียนส์ลีก</option>
				<option  value="tab6">ไทยลีก</option>
				<option  value="tab5">ลีกเอิง</option>
				<option  value="tab4">เซเรียอา</option>
				<option  value="tab3">บุนเดสลีกา</option>
				<option  value="tab2">ลาลีกา</option>
				<option  value="tab1">พรีเมียร์ลีก</option>
				*/ ?>
				<option  value="tab5">ทั้งหมด</option>
				<option  value="tab4">ไทยลีก</option>
				<option  value="tab3">อุ่นเครื่องสโมสร</option>
				<option  value="tab2">อุ่นเครื่องทีมชาติ</option>
				<option  value="tab1">ยูโร 2016</option>
			</select>
		</div>
             <script>

                 $(document).ready(function() {

                     $('.selectLeague select').on('change', function (e) {
                       //  e.target // newly activated tab
                       //  e.relatedTarget // previous active tab
                        //console.log(this.value);
                         //this.value
                        // $('#tab0').tab('show');

                         $('.nav-tabs a[href="#' + this.value + '"]').tab('show');



                     })

                 });


             </script>
<!-- Tab panes -->
	<div class="tab-content">
		<?php
		$key_all	=	0;
		foreach($this->ProgramByLeague as $key => $listMatch) { $key_all = ($key+1); ?>
		<div class="tab-pane" id="tab<?php echo $key; ?>">
			<div id="page<?php echo $key; ?>" class="active" <?php if(($_COOKIE[$cookies] != "hide")){ ?> style="" <?php }else{  ?> style="display: none;" <?php } ?>>
			<div id="carousel-live" class="top-league" >
				<div class="row <?php if(count($listMatch['program']) == 0) { echo 'no-match'; } ?>">
					<?php
					if(count($listMatch['program']) == 0 ){ echo $listMatch['nameTH']; ?>  ไม่มีการแข่งขัน<?php }
					else{
						foreach($listMatch['program'] as $tmpMatch){
							$tmpMatch['flag']			=	$listLeagueFlag[$key];
							$tmpMatch['leagueName']		=	$listMatch['nameTHShort'];
							include INCLUDE_VIEW_ROOTPATH . 'livescore/match_card.tpl.php';
						}
					} ?>
				</div> 
			</div>
			</div>
			<table class="table responsive match-game  boder-1" id="table<?php echo $key; ?>" <?php if(($_COOKIE[$cookies] != "hide")){ ?> style="display: none;" <?php }else{  ?> style="" <?php } ?>>
				<thead>
					<?php if(count($listMatch['program']) > 0 ){ ?>
						<tr> <td colspan="7"><a href="<?php $listMatch['nameURLRootRoute']; ?>" target="_blank"><h4 style="margin: auto;"><img src="<?php echo BASE_HREF; ?>assets/img/flags/zone/<?php echo $listLeagueFlag[$key]; ?>.png" alt="" style="width: 24px;"> <?php echo $listMatch['nameTHShort']; ?></h4></a></td></tr>
					<?php 
					}
					?>
				</thead>
				<tbody>
					<?php if(count($listMatch['program']) == 0 ){ ?>
						<tr><td class="no-match"><?php echo $listMatch['nameTHShort']; ?>  ไม่มีการแข่งขัน</td></tr>
					<?php }
					foreach($listMatch['program'] as $tmpMatch){ ?>
						<tr>
							<td><?php echo $tmpMatch['MatchTime']; ?></td>
							<td class="text_right"><a href="<?php echo strtolower($tmpMatch['MatchPageURL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><span class="teamname <?php if($tmpMatch['TeamOdds']==1){ echo ' handicap_before'; } ?>"><?php echo $tmpMatch['Team1']; ?> <img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" width="24" id="TeamLogo<?php echo $tmpMatch['id']; ?>_1_Table"></span></a></td>
							<td class="text_center"><?php include INCLUDE_VIEW_ROOTPATH . 'livescore/match_boxscore.tpl.php'; ?></td>
							<td class="text_left"><a href="<?php echo strtolower($tmpMatch['MatchPageURL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><span class="teamname <?php if($tmpMatch['TeamOdds']==2){ echo ' handicap_after'; } ?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" width="24"  id="TeamLogo<?php echo $tmpMatch['id']; ?>_2_Table"> <?php echo $tmpMatch['Team2']; ?></span></a></td>
							<td><?php
								if($tmpMatch['Odds']== 0){
									echo 'เสมอ';
								}else{
									echo $this->football_lib->getOddsRatePie((string)$tmpMatch['Odds']);
								} ?>
							</td>
							<td class="channel"><span class="channel pull-right"><?php
							foreach($tmpMatch['TVLiveList'] as $ChannelKey){
								?> <img style="height:16px;" src="<?php echo BASE_HREF; ?>uploads/tvlogo/<?php echo $ChannelKey; ?>.png" title="<?php echo $this->tv_lib->getTVText($ChannelKey); ?>"><?php
							} ?>
							</span></td>
						  
							<td>  <?php if(($this->isCanPlayGame)&&($tmpMatch['Odds']!=-1)&&($tmpMatch['MatchStatus']=='Sched')){ ?>
								<div class="extra-text game<?php echo $tmpMatch['id']; ?>">
								ทายผล |
								<a href="#game-single" role="tab" data-toggle="tab" onclick="AddPlaySideSingle(<?php echo $tmpMatch['id'];?>,'<?php echo $tmpMatch['Team1'];?>','<?php echo $tmpMatch['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team2Logo']);?>','<?php echo $tmpMatch['Odds'];?>',<?php echo $tmpMatch['TeamOdds'];?>)" class="link-play-game single label label-primary">ทายบอลเดี่ยว</a>
								<a href="#dropdown1" role="tab" data-toggle="tab" onclick="AddPlaySideMulti(1,<?php echo $tmpMatch['id'];?>,'<?php echo $tmpMatch['Team1'];?>','<?php echo $tmpMatch['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team2Logo']);?>','<?php echo $tmpMatch['Odds'];?>',<?php echo $tmpMatch['TeamOdds'];?>)" class="link-play-game single label label-success">ทายบอลชุด</a>
								</div>
							<?php } else if( $this->isCanPlayGame == false ){
								?>
								<div class="extra-text game<?php echo $tmpMatch['id']; ?>">
									<a href="<?php echo BASE_HREF; ?>games" class="link-play-game single label label-primary">เล่นเกมทายผลบอล</a>
								</div>
								<?php
							} ?>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<?php } ?>
		<div class="tab-pane active" id="tab<?php echo $key_all; ?>" >
			<div id="page8" <?php if(($_COOKIE[$cookies] != "hide")){ ?> style="" <?php }else{  ?> style="display: none;" <?php } ?>>
				<div id="carousel-live" class="top-league">
					<div class="row" >
					<?php
					for($index=1;$index<=$TotalLeague;$index++){
						$tmpAllMatch = $MathByLeagueIDArr[$LeagueByIndexArr[$index]['ID']];
						foreach($tmpAllMatch as $tmpMatch){
							$MatchDate = $tmpMatch['MatchDate'].' '.$tmpMatch['MatchTime'].':00';
							$this->LiveMatchListTime[$tmpMatch['id']]		=	$MatchDate;
							if($this->football_lib->isLiveMatch($tmpMatch['MatchStatus'])){
								$this->AllLiveMatchID.=','.$tmpMatch['id'];
							}
							
							$tmpMatch['flag']			=	$LeagueByIndexArr[$index]['Zone'];
							$tmpMatch['leagueName']		=	$LeagueByIndexArr[$index]['Name'];
							include INCLUDE_VIEW_ROOTPATH . 'livescore/match_card.tpl.php';
						} 
					} ?>
					</div>
				</div>
			</div>
			<table class="table responsive match-game boder-1" id="table<?php echo $key_all; ?>" <?php if(($_COOKIE[$cookies] != "hide")){ ?> style="display: none;" <?php }else{  ?> style="" <?php } ?>>
				<?php for($index=1;$index<=$TotalLeague;$index++){ ?>
					<thead><tr> <td colspan="7"><a name="#"><h4 style="margin-top: auto;"><img src="<?php echo BASE_HREF; ?>assets/img/flags/zone/<?php echo $LeagueByIndexArr[$index]['Zone']; ?>.png" alt="" style="width: 24px;"> <?php echo $LeagueByIndexArr[$index]['Name']; ?></h4></a></td></tr>
					</thead>
					<tbody>
					<?php
						$tmpAllMatch = $MathByLeagueIDArr[$LeagueByIndexArr[$index]['ID']];
						foreach($tmpAllMatch as $tmpMatch){           
					?>
					<tr>
						<td><?php echo $tmpMatch['MatchTime']; ?></td>
						<td class="text_right"><a href="<?php echo strtolower($tmpMatch['MatchPageURL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><span class="teamname <?php if($tmpMatch['TeamOdds']==1){ echo ' handicap_before'; } ?>"><?php echo $tmpMatch['Team1']; ?> <img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" width="24" id="TeamLogo<?php echo $tmpMatch['id']; ?>_1_Table"></span></a></td>
						<td class="text_center"><?php include INCLUDE_VIEW_ROOTPATH . 'livescore/match_boxscore.tpl.php'; ?></td>
						<td class="text_left"><a href="<?php echo strtolower($tmpMatch['MatchPageURL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><span class="teamname <?php if($tmpMatch['TeamOdds']==2){ echo ' handicap_after'; } ?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" width="24" id="TeamLogo<?php echo $tmpMatch['id']; ?>_2_Table"> <?php echo $tmpMatch['Team2']; ?></span></a></td>
						<td><?php if($tmpMatch['Odds']== 0){
							echo 'เสมอ'; 
						}else{
							echo $this->football_lib->getOddsRatePie((string)$tmpMatch['Odds']);
						}
						
						if($tmpMatch['Predict']){?>
                           <a href="http://football.kapook.com/livescore/analyze_match/<?php echo $tmpMatch['id']; ?>" data-toggle="modal" data-target="#useful-modal"   class="label label-success "><i class="fa fa-star"></i> ทีเด็ด</a>
						<?php } ?> </td>
						<td class="channel"><span class="channel pull-right"><?php
							foreach($tmpMatch['TVLiveList'] as $ChannelKey){
								?> <img style="height:16px;" src="<?php echo BASE_HREF; ?>uploads/tvlogo/<?php echo $ChannelKey; ?>.png" title="<?php echo $this->tv_lib->getTVText($ChannelKey); ?>"><?php
							} ?>
						</span> </td>
					  
						<td>
							<?php if(($this->isCanPlayGame)&&($tmpMatch['Odds']!=-1)&&($tmpMatch['MatchStatus']=='Sched')){ ?>
							<div class="extra-text game<?php echo $tmpMatch['id']; ?>">
								ทายผล |
								<a href="#game-single" role="tab" data-toggle="tab" onclick="AddPlaySideSingle(<?php echo $tmpMatch['id'];?>,'<?php echo $tmpMatch['Team1'];?>','<?php echo $tmpMatch['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team2Logo']);?>','<?php echo $tmpMatch['Odds'];?>',<?php echo $tmpMatch['TeamOdds'];?>)" class="link-play-game single label label-primary">ทายบอลเดี่ยว</a>
								<a href="#dropdown1" role="tab" data-toggle="tab" onclick="AddPlaySideMulti(1,<?php echo $tmpMatch['id'];?>,'<?php echo $tmpMatch['Team1'];?>','<?php echo $tmpMatch['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team2Logo']);?>','<?php echo $tmpMatch['Odds'];?>',<?php echo $tmpMatch['TeamOdds'];?>)" class="link-play-game single label label-success">ทายบอลชุด</a>
							</div>
							<?php } else if( $this->isCanPlayGame == false ){
							?>
							<div class="extra-text game<?php echo $tmpMatch['id']; ?>">
								<a href="<?php echo BASE_HREF; ?>games" class="link-play-game single label label-primary">เล่นเกมทายผลบอล</a>
							</div>
							<?php } ?>
						</td>
					</tr>
					<?php } ?>
					</tbody>
				<?php } ?>
			</table>
		</div>			
	</div>
</div>