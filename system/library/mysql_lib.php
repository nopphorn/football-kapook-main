<?php

class Mysql_lib 
{
    var $connectionId;
    var $error;
    var $host;
    var $username;
    var $password;
    var $database;
    var $resultId;
    var $sql;
    var $query_count;
    var $query_time_total;
    var $connect_time;
    var $key;
    var $encode;
    var $lastsql;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct($key='default') 
    {
        $config = minizone::getzone()->config('mysql',$key);
        $this->host = $config['host'];
        $this->username = $config['user'];
        $this->password = $config['pass'];
        $this->database = $config['db'];
        $this->encode = $config['encode'];
        $this->key = $key;

        return;
    }

    ///////////////////////////////////////////////// Connect /////////////////////////////////////////////////
    function Connect() 
    {
        $time_start = array_sum(explode(' ', microtime()));
        
        if (!$this->connectionId = @mysql_connect($this->host, $this->username, $this->password)) {
            $this->dbError('Connect');
            return false;
        }
        
        if (!@mysql_select_db($this->database)) {
            $this->dbError('SelectDB');
            return false;
        }
        
        $this->connect_time += (array_sum(explode(' ', microtime())) - $time_start);
        $this->Execute('SET NAMES '.$this->encode);
        
        return true;
    }

    ///////////////////////////////////////////////// Execute /////////////////////////////////////////////////
    function Execute($sql, $inputarr=false)
    {
        return $this->do_query($sql, $inputarr);
    }

    ///////////////////////////////////////////////// Query /////////////////////////////////////////////////
    function do_query($sql, $inputarr=false)
    {
        if (!$this->connectionId)
            if (!$this->Connect()) return;
            
        if ($inputarr && is_array($inputarr)) {
            $sqlarr = explode('?', $sql);
            
            if (!is_array(reset($inputarr)))
                $inputarr = array($inputarr);
            
            foreach ($inputarr as $arr) {
                $sql = '';
                $i = 0;
                
                foreach ($arr as $v) {
                    $sql .= $sqlarr[$i];
                    
                    switch (gettype($v)) {
                        case 'string':
                            $sql .= $this->qstr($v);
                            break;
                        case 'double':
                            $sql .= str_replace(',', '.', $v);
                            break;
                        case 'boolean':
                            $sql .= $v ? 1 : 0;
                            break;
                        default:
                            if ($v === null) $sql .= 'NULL';
                            else $sql .= $v;
                    }
                    
                    $i += 1;
                }
                
                $sql .= $sqlarr[$i];
                if ($i + 1 != sizeof($sqlarr)) {
                    $this->dbError("Value in Array for Query");
                    return;
                }
                
                $this->sql = $sql;
                $time_start = array_sum(explode(' ', microtime()));
                $this->query_count++;
                $resultId = @mysql_query($this->sql, $this->connectionId);
                $this->query_time_total += (array_sum(explode(' ', microtime())) - $time_start);
            }
        }
        else {
            $this->sql = $sql;
            $time_start = array_sum(explode(' ', microtime()));
            $this->query_count++;
            $resultId = @mysql_query($this->sql, $this->connectionId);
            $this->query_time_total += (array_sum(explode(' ', microtime())) - $time_start);
        }
        
        if (!$resultId) {
            $this->lastsql = $this->sql;
            $this->dbError($this->sql);
            
            return;
        }
        
        return $resultId;
    }

    ///////////////////////////////////////////////// Qstr /////////////////////////////////////////////////
    function qstr($string)
    {
        return "'".mysql_real_escape_string($string)."'";
    }

    ///////////////////////////////////////////////// Fetch /////////////////////////////////////////////////
    function Fetch() 
    {
        if ($row = @mysql_fetch_array($this->resultId)) {
            while (list($key, $val) = each($row)) {
                $row[$key] = stripslashes($val);
            }
            
            $row['db'] = $this->key;
            
            return $row;
        }
    }

    ///////////////////////////////////////////////// Insert_ID /////////////////////////////////////////////////
    function Insert_ID()
    {
        return @mysql_insert_id($this->connectionId);
    }

    ///////////////////////////////////////////////// GetOne /////////////////////////////////////////////////
    function GetOne($sql, $inputarr = false)
    {
        if (strpos(strtolower($sql), ' limit ') === false)
            $sql .= ' limit 0, 1';
        
        if ($this->resultId = $this->Execute($sql, $inputarr)) {
            $a = $this->Fetch();
            
            return $a[0];
        }
    }

    ///////////////////////////////////////////////// GetRow /////////////////////////////////////////////////
    function GetRow($sql, $inputarr = false)
    {
        if (strpos(strtolower($sql), ' limit ') === false)
            $sql .= ' limit 0, 1';
        
        if ($this->resultId = $this->Execute($sql, $inputarr)) {
            return $this->Fetch();
        }
    }

    ///////////////////////////////////////////////// GetAll /////////////////////////////////////////////////
    function GetAll($sql, $inputarr = false)
    {
        if ($this->resultId = $this->Execute($sql, $inputarr)) {
            $b = array();
            
            while ($a = $this->Fetch())
                $b[] = $a;
            
            return $b;
        }
        
        return;
    }

    ///////////////////////////////////////////////// Close /////////////////////////////////////////////////
    function Close() 
    {
        @mysql_close($this->connectionId);
        $this->connectionId = false;
    }

    ///////////////////////////////////////////////// dbError /////////////////////////////////////////////////
    function dbError($e = '')
    {
        $this->error = "Function: " . $e . "  [" . @mysql_errno($this->connectionId) . " - " . @mysql_error($this->connectionId) . "] [sql: " . $this->sql . " , last sql: " . $this->lastsql . "]";
        echo "<br><b>mySQL Error!</b> " . $this->error . "<br> on " . $this->host . "(" . $this->key . ")";
    }

    ///////////////////////////////////////////////// Count /////////////////////////////////////////////////
    function Count() 
    {
        return number_format($this->query_count);
    }

    ///////////////////////////////////////////////// Time /////////////////////////////////////////////////
    function Time($connect = false)
    {
        return number_format(($connect ? $this->connect_time : $this->query_time_total), 4);
    }

}
/* End of file mysql_lib.php */
/* Location: ./system/library/mysql_lib.php */