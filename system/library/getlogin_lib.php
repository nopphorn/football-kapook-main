<?php

define('API_KEY', 'cxsw3ed21qaz');
define('API_SECRET', 'x3ed1qaz2wsc');

//define('HTTP_REQUEST_URL', 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

class Getlogin_lib {

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() { //member new kapook
        $chk_id = $this->checkCookie();
        if ($chk_id > 0) {
            $_profile = $this->get_KMember(array('userid' => $chk_id));

            define('Login', true);
            define('uid', $_COOKIE['uid']);
            define('Name', $_profile->username);
            define('Avatar', $_profile->picture);
//            define('Avatar', $_profile->avatar['m']);
        } else {
            $this->_uid = '';
        }
    }

    ///////////////////////////////////////////////// Get : Profile (Mini by curl) /////////////////////////////////////////////////
    function getminicurl($action, $user) {
        $user_agent = 'Mozilla/4.0';
        $url = "http://api_beta.kapook.com/apigetmem.php?action={$action}&{$user}";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "id={$_id}");

        $result = curl_exec($ch);

        curl_close($ch);

        return $data = unserialize($result);
    }

    ///////////////////////////////////////////////// Check : Cookie /////////////////////////////////////////////////
    function checkCookie() {
        /* -- Hash key ห้ามเปลี่ยน -- */
        $hash = 'kapook_sudyod';

        if ($_COOKIE['uid'] && $_COOKIE['is_login']) {
            /* -- เช็คความถูกต้องของ Cookie -- */
            if (md5($_COOKIE['uid'] . $hash) == $_COOKIE['is_login']) {
                $kid = $_COOKIE['uid'];

                setcookie("uid", $_COOKIE['uid'], time() + 172800, "/", ".kapook.com");
                setcookie("is_login", $_COOKIE['is_login'], time() + 172800, "/", ".kapook.com");

                /* -- ดึงค่า member จาก userid -- */
                return $kid;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    ///////////////////////////////////////////////// Get : Kmember /////////////////////////////////////////////////
    function get_KMember() {
        $arg = func_get_arg(0);
        $kid = $arg['userid'];
        $kusername = $arg['username'];

        if ($kid || $kusername) {
            $opts['http']['method'] = 'GET';
            $opts['http']['header'] = "Accept-language: en\r\n" . "Authorization: Basic " . base64_encode(API_KEY . ':' . API_SECRET . '') . "\r\n";
            $context = stream_context_create($opts);

            if (version_compare(PHP_VERSION, '5.2.0', '<'))
                $type = 'json';
            else
                $type = 'json';

//            $request='profile.gender,profile.birthday,social.facebook.token,social.facebook.id,social.facebook.link';
//            $request='profile.firstname,profile.lastname';
            if ($request) {
                $request = "&request={$request}";
            }

            if ($kusername)
                $p = file_get_contents('http://kapi.kapook.com/profile/member/username/' . $kusername);
            else
                $p = file_get_contents('http://kapi.kapook.com/profile/member/userid/' . $kid);

            $_tmp = json_decode($p, true);
            $_tmp['data']['email'] = $_tmp['data']['profile']['email'];
            $p = json_encode($_tmp['data']);
            
            if ($p) {
                if ($type == 'json') {
                    return json_decode($p);
                } else {
                    $xml = simplexml_load_string($p);
                    return $xml;
                }
            } else
                return false;
        } else
            return false;
    }

}
/* End of file getlogin_lib.php */
/* Location: ./system/library/getlogin_lib.php */