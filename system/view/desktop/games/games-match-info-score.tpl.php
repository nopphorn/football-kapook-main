<?php
$MonthArr['01'] = 'ม.ค.';
$MonthArr['02'] = 'ก.พ.';
$MonthArr['03'] = 'มี.ค.';
$MonthArr['04'] = 'เม.ย.';
$MonthArr['05'] = 'พ.ค.';
$MonthArr['06'] = 'มิ.ย.';
$MonthArr['07'] = 'ก.ค.';
$MonthArr['08'] = 'ส.ค.';
$MonthArr['09'] = 'ก.ย.';
$MonthArr['10'] = 'ต.ค.';
$MonthArr['11'] = 'พ.ย.';
$MonthArr['12'] = 'ธ.ค.';

$OddsRate["0.25"] 	= "เสมอควบครึ่ง";
	$OddsRate["0.5"] 	= "ครึ่งลูก";
	$OddsRate["0.75"] 	= "ครึ่งควบลูก";
	$OddsRate["1"] 	= "หนึ่งลูก";
	$OddsRate["1.25"] 	= "ลูกควบลูกครึ่ง";
	$OddsRate["1.5"] 	= "ลูกครึ่ง";
	$OddsRate["1.75"] 	= "ลูกครึ่งควบสองลูก";
	$OddsRate["2"] 	= "สองลูก";
	$OddsRate["2.25"] 	= "สองลูกควบสองลูกครึ่ง";
	$OddsRate["2.5"] 	= "สองลูกครึ่ง";
	$OddsRate["2.75"] 	= "สองลูกครึ่งควบสามลูก";
	$OddsRate["3"] 	= "สามลูก";
	$OddsRate["3.25"] 	= "สามลูกควบสามลูกครึ่ง";
	$OddsRate["3.5"] 	= "สามลูกครึ่ง";
	$OddsRate["3.75"] 	= "สามลูกครึ่งควบสี่ลูก";
	$OddsRate["4"] 	= "สี่ลูก";
	$OddsRate["4.25"] 	= "สี่ลูกควบสี่ลูกครึ่ง";
	$OddsRate["4.5"] 	= "สี่ลูกครึ่ง";
	$OddsRate["4.75"] 	= "สี่ลูกครึ่งควบห้าลูก";
	$OddsRate["5"] 	= "ห้าลูก";
	$OddsRate["5.25"] 	= "ห้าลูกควบห้าลูกครึ่ง";
	$OddsRate["5.5"] 	= "ห้าลูกครึ่ง";
	$OddsRate["5.75"] 	= "ห้าลูกครึ่งควบหกลูก";
	$OddsRate["6"] 	= "หกลูก";
	$OddsRate["6.25"] 	= "หกลูกควบหกลูกครึ่ง";
	$OddsRate["6.5"] 	= "หกลูกครึ่ง";
	$OddsRate["6.75"] 	= "หกลูกครึ่งควบเจ็ดลูก";
	$OddsRate["7"] 	= "เจ็ดลูก";
	$OddsRate["7.25"] 	= "เจ็ดลูกควบเจ็ดลูกครึ่ง";
	$OddsRate["7.5"] 	= "เจ็ดลูกครึ่ง";
	$OddsRate["7.75"] 	= "เจ็ดลูกครึ่งควบแปดลูก";
	$OddsRate["8"] 	= "แปดลูก";
	$OddsRate["8.25"] 	= "แปดลูกควบแปดลูกครึ่ง";
	$OddsRate["8.5"] 	= "แปดลูกครึ่ง";
	$OddsRate["8.75"] 	= "แปดลูกครึ่งควบเก้าลูก";
	$OddsRate["9"] 	= "เก้าลูก";
	$OddsRate["9.25"] 	= "เก้าลูกควบเก้าลูกครึ่ง";
	$OddsRate["9.5"] 	= "เก้าลูกครึ่ง";
	$OddsRate["9.75"] 	= "เก้าลูกครึ่งควบสิบลูก";
	$OddsRate["10"] 	= "สิบลูก";


$MatchContents = $this->MatchContents;
$MatchInfo = $MatchContents['MatchInfo'];

$LeagueName = $this->mem_lib->get('Football2014-League-NameTHShort-'.$MatchInfo['KPLeagueID']);
$Team1Name = $this->mem_lib->get('Football2014-Team-NameTHShort-'.$MatchInfo['Team1KPID']);
$Team2Name = $this->mem_lib->get('Football2014-Team-NameTHShort-'.$MatchInfo['Team2KPID']);

$MatchDateTmp = explode('-',substr($MatchInfo['MatchDateTime'],0,10));
$MatchTimeTmp = explode(':',substr($MatchInfo['MatchDateTime'],11,8));

$MatchDate  = intval($MatchDateTmp[2]).' '.$MonthArr[$MatchDateTmp[1]].' '.intval($MatchDateTmp[0]-1957);
$MatchTime  = substr($MatchInfo['MatchDateTime'],11,5);
$DateShow   = intval(date("d")).' '.$MonthArr[date('m')].' '.intval(date('Y')-1957);
?>




<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
  <title>Remote Modal</title>  
</head>
<body>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h4 class="modal-title">ทายผล </h4>
            </div>			<!-- /modal-header -->
            <div class="modal-body modal-body-game">
                
              <div class="alert alert-warning fade in" role="alert" id="divAlert" style="display:none">
                  <button type="button" class="close" onclick="document.getElementById('divAlert').style.display='none'; "><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <strong> ขออภัย!</strong> คุณยังไม่ได้ใส่ผลสกอร์
              </div>
              <div class="row">

                <div class="col-md-4 col-xs-4">
                
                
                        <div  class="btn-radio"> 
                        <div class="flag-no-effect" style="cursor:default;"><img src="<?php echo $MatchInfo['Team1Logo'];?>" ></div>
                        <div class="teamname"><?php echo $Team1Name; ?></div> 
                        </div>
                        <input type="checkbox" id="left-item" class="hidden">
                         
                        
                  </div>
                <div class="col-md-4 col-xs-4">

                <div class="program hidden-xs"><?php echo $LeagueName; ?></div>
                  
                  <a href="<?php echo $MatchInfo['MatchPageURL']; ?>" class="score tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">vs</a>

                   <div class="date hidden-xs"><?php echo $MatchDate; ?>, <?php echo $MatchTime; ?> น.</div>
                   <!--<div class="handicap hidden-xs">แมนฯ ยูไนเต็ด ต่อ ลูกควบลูกครึ่ง</div>-->

                </div>
                <div class="col-md-4 col-xs-4">
                            <div  class="btn-radio">
                                <div class="flag-no-effect" style="cursor:default;"><img src="<?php echo $MatchInfo['Team2Logo'];?>" ></div>
                            
                            <div class="teamname"><?php echo $Team2Name; ?></div>
                             </div>
                             <input type="checkbox" id="left-item" class="hidden">
                           
                  </div>
                  
                  

</div>
            
                  <br>
                  <div class="row">
                          <div class="col-md-4 col-xs-4">         
                              <select class="form-control" id="score1">
                                  <option value="-1">ทายประตู</option>
                                  <option value="0">0</option>
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                                  <option value="6">6</option>
                                  <option value="7">7</option>
                                  <option value="8">8</option>
                                  <option value="9">9</option>

                                    </select> 
                          </div>
                          <div class="col-md-4 col-xs-4">         
                          </div>
                          <div class="col-md-4 col-xs-4">         
                              <select class="form-control" id="score2">
                                  <option value="-1">ทายประตู</option>
                                  <option value="0">0</option>
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                                  <option value="6">6</option>
                                  <option value="7">7</option>
                                  <option value="8">8</option>
                                  <option value="9">9</option>

                                    </select> 
                          </div>
                  </div>
                  

                
                
            </div>
            
     

            
            
            </div>			<!-- /modal-body -->
            <div class="modal-footer">
            

            <!--<a href="league-match.php" class="btn btn-default pull-left" target="_blank" >รายละเอียดการแข่งขัน</a> -->
                <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <button id="loading-example-btn" type="button" class="btn btn-primary" data-loading-text="กำลังบันทึก..." onclick="
                    if(document.getElementById('score1').value<0){ document.getElementById('divAlert').style.display=''; return false; }
                    if(document.getElementById('score2').value<0){ document.getElementById('divAlert').style.display=''; return false; }
                    
                    fnPlayScore(<?php echo $MatchInfo['id']; ?>,document.getElementById('score1').value,document.getElementById('score2').value);
                    $('#gameModal').modal('hide');

                    ">บันทึก</button>
            </div>			<!-- /modal-footer -->

</body></html>