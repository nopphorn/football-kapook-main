<?php
define('APPPATH', 'system/');

class router 
{
    var $config;
    var $routes = array();
    var $error_routes = array();
    var $class = '';
    var $method = 'index';
    var $directory = '';
    var $default_controller;
    
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() 
    {
        $this->minizone = minizone::getzone();
    }
    
    ///////////////////////////////////////////////// Set : Routing /////////////////////////////////////////////////
    function _set_routing() 
    {
        include('system/config/routes.php');

        $this->routes = (!isset($route) OR !is_array($route)) ? array() : $route;
        unset($route);
        
        $this->default_controller = (!isset($this->routes['default_controller']) OR $this->routes['default_controller'] == '') ? FALSE : strtolower($this->routes['default_controller']);

        /* -- Set : Controller (Default) -- */ 
        $uri = $this->minizone->pathsURI();
        
        
        if ($uri[0] == '') {
            return $this->_set_default_controller();
        }
        
        $this->_parse_routes();
    }

    ///////////////////////////////////////////////// Set : Default Controller /////////////////////////////////////////////////
    function _set_default_controller() 
    {
        if ($this->default_controller === FALSE) {
            echo 'show_error_Line' . __LINE__;
        }
        
        if (strpos($this->default_controller, '/') !== FALSE) {
            $x = explode('/', $this->default_controller);

            $this->set_class($x[0]);
            $this->set_method($x[1]);
            $this->_set_request($x);
        } else {
            $this->set_class($this->default_controller);
            $this->set_method('index');
            $this->_set_request(array($this->default_controller, 'index'));
        }
    }
    
    ///////////////////////////////////////////////// Set : Request /////////////////////////////////////////////////
    function _set_request($segments = array()) 
    {
        $segments = $this->_validate_request($segments);
        
        if (count($segments) == 0) {
            return $this->_set_default_controller();
        }

        $this->set_class($segments[0]);

        if (isset($segments[1])) {
            $this->set_method($segments[1]);
        } else {
            $segments[1] = 'index';
        }

        $this->rsegments = $segments;
    }

    ///////////////////////////////////////////////// Validate : Request /////////////////////////////////////////////////
    function _validate_request($segments) 
    {
        if (count($segments) == 0) {
            return $segments;
        }
        
        if (file_exists(APPPATH . 'controller/' . $segments[0] . '.php')) {
            return $segments;
        }
        
        if (is_dir(APPPATH . 'controller/' . $segments[0])) {
            $this->set_directory($segments[0]);
            $segments = array_slice($segments, 1);

            if (count($segments) > 0) {
                if (!file_exists(APPPATH . 'controller/' . $this->fetch_directory() . $segments[0] . '.php')) {
                    if (!empty($this->routes['404_override'])) {
                        $x = explode('/', $this->routes['404_override']);

                        $this->set_directory('');
                        $this->set_class($x[0]);
                        $this->set_method(isset($x[1]) ? $x[1] : 'index');

                        return $x;
                    } else {
                        echo '404-1';
                        exit;
                    }
                }
            } else {
                if (strpos($this->default_controller, '/') !== FALSE) {
                    $x = explode('/', $this->default_controller);

                    $this->set_class($x[0]);
                    $this->set_method($x[1]);
                } else {
                    $this->set_class($this->default_controller);
                    $this->set_method('index');
                }

                if (!file_exists(APPPATH . 'controller/' . $this->fetch_directory() . $this->default_controller . '.php')) {
                    $this->directory = '';
                    return array();
                }
            }
            
            return $segments;
        }

        if (!empty($this->routes['404_override'])) {
            $x = explode('/', $this->routes['404_override']);

            $this->set_class($x[0]);
            $this->set_method(isset($x[1]) ? $x[1] : 'index');

            return $x;
        }
        
        var_dump($segments);

        echo '404-2';
        
        
        
        exit;
    }

    ///////////////////////////////////////////////// Set : Parse Routes /////////////////////////////////////////////////
    function _parse_routes() 
    {
        $uri = implode('/', $this->minizone->pathsURI());

        if (isset($this->routes[$uri])) {
            return $this->_set_request(explode('/', $this->routes[$uri]));
        }

        foreach ($this->routes as $key => $val) {
            $i++;
            
            $key = str_replace(':any', '.+', str_replace(':num', '[0-9]+', $key));
            
            if (preg_match('#^' . $key . '$#', $uri)) {
                if (strpos($val, '$') !== FALSE AND strpos($key, '(') !== FALSE) {
                    $val = preg_replace('#^' . $key . '$#', $val, $uri);
                }

                return $this->_set_request(explode('/', $val));
            }
        }

        $this->_set_request($this->minizone->pathsURI());
    }

    ///////////////////////////////////////////////// Set : Class /////////////////////////////////////////////////
    function set_class($class) 
    {
        $this->class = str_replace(array('/', '.'), '', $class);
    }

    ///////////////////////////////////////////////// Set : Fetch Class /////////////////////////////////////////////////
    function fetch_class() 
    {
        return $this->class;
    }

    ///////////////////////////////////////////////// Set : Method /////////////////////////////////////////////////
    function set_method($method) 
    {
        $this->method = $method;
    }

    ///////////////////////////////////////////////// Set : Method /////////////////////////////////////////////////
    function fetch_method() 
    {
        if ($this->method == $this->fetch_class()) {
            return 'index';
        }

        return $this->method;
    }

    ///////////////////////////////////////////////// Set : Directory /////////////////////////////////////////////////
    function set_directory($dir) 
    {
        $this->directory = str_replace(array('/', '.'), '', $dir) . '/';
    }

    ///////////////////////////////////////////////// Set : Directory /////////////////////////////////////////////////
    function fetch_directory() 
    {
        return $this->directory;
    }

    ///////////////////////////////////////////////// Set : Overrides /////////////////////////////////////////////////
    function _set_overrides($routing) 
    {
        if (!is_array($routing)) {
            return;
        }

        if (isset($routing['directory'])) {
            $this->set_directory($routing['directory']);
        }

        if (isset($routing['controller']) AND $routing['controller'] != '') {
            $this->set_class($routing['controller']);
        }

        if (isset($routing['function'])) {
            $routing['function'] = ($routing['function'] == '') ? 'index' : $routing['function'];
            $this->set_method($routing['function']);
        }
    }

}
/* End of file router.php */
/* Location: ./system/core/router.php */