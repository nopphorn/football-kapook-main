<?php if (!defined('MINIZONE')) exit;

class LiveScore_model
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
		$this->minizone = minizone::getzone();
    }
	
	function file_get_curl($url){
		$data_post	=	parse_url($url, PHP_URL_PATH);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		//curl_setopt($ch, CURLOPT_COOKIE, "uid={$_COOKIE['uid']};is_login={$_COOKIE['is_login']}");
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$this->errNO = curl_errno($ch);
			$this->errMSG = curl_error($ch);
			return false;
		}
		curl_close($ch);
		return $result;
	}
 
    function getLiveScore(){
        $url = "http://football.kapook.com/api/livescore.php";
		$result		=	$this->file_get_curl($url);
		return json_decode($result,true);
    }
    
 
    function getLiveScoreMatch($id){

        $url = "http://football.kapook.com/api/match.php?id=".$id;
		$result		=	$this->file_get_curl($url);
		return json_decode($result,true);
    }    
    function getMatchAnalysis($date = ''){

        $url = "http://football.kapook.com/api/livescore.php?type=analysis";
		if($date!=''){
			$url = $url . '&date=' . $date;
		}
		$result		=	$this->file_get_curl($url);
		return json_decode($result,true);
    } 
 
    function getMatchByTeam($team_id,$page_size='',$start_date,$end_date){

        $url = "http://football.kapook.com/api/livescore.php?team_id=".$team_id."&page_size=".$page_size."&from_date=".$start_date."&to_date=".$end_date;
		if($_REQUEST['remove_cache']==1)
			echo $url . '<br>';
		$result		=	$this->file_get_curl($url);
		return json_decode($result,true);
    } 
    
    function getProgram(){

        $url = "http://football.kapook.com/api/livescore.php?type=program";
        $result		=	$this->file_get_curl($url);
		return json_decode($result,true);
    }    
    function getProgramByDate($date){
        
        $url = "http://football.kapook.com/api/livescore.php?date=".$date;
		$result		=	$this->file_get_curl($url);
		return json_decode($result,true);
        
    }   
    
    function getProgramByLeague($league){
        
        $url = "http://football.kapook.com/api/livescore.php?type=league&page_size=1000&league=".$league;
		$result		=	$this->file_get_curl($url);
		return json_decode($result,true);
        
    }   
    
    function getProgramByLeagueBetweenDate($league,$date_from,$date_to){        
        $url = "http://football.kapook.com/api/livescore.php?type=league&page_size=1000&league=".$league."&from_date=".$date_from."&to_date=".$date_to;
		$result		=	$this->file_get_curl($url);
		return json_decode($result,true);       
    }   
        
    function getProgramByLeagueFin($league,$date=''){        
        $url = "http://football.kapook.com/api/livescore.php?type=league_fin&page_size=1000&league=".$league."&date=".$date;
		$result		=	$this->file_get_curl($url);
		return json_decode($result,true);
        
    }   
        
    function getProgramByLeagueSched($league){        
        $url = "http://football.kapook.com/api/livescore.php?type=league_sched&page_size=1000&league=".$league;
		$result		=	$this->file_get_curl($url);
		return json_decode($result,true);
        
    }  
    function getPlayerMarket($page = 1,$size = 10){        
        $url 		= 	"http://football.kapook.com/api/adminfootball/api/market/?type=json&size=" . $size . "&page=" . $page;
		$result		=	$this->file_get_curl($url);
		return json_decode($result,true);
    }
}
/* End of file home_model.php */
/* Location: ./system/model/home_model.php */
