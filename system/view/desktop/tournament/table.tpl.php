<link rel="stylesheet" href="<?php echo BASE_HREF; ?>assets/css/bracketz.css">
<script src="<?php echo BASE_HREF; ?>assets/js/bracketz.js"></script>

<style>
.match_unit.highlight{
    padding: 0px 0px;
    margin-bottom: 0px;
    border-radius: 0px;
	border:none;
	background-color: #F0F0F0;
}
.m_segment.highlight{
    padding: 0px 0px;
    margin-bottom: 0px;
    border-radius: 0px;
	border:none;
	background-color: #F0F0F0;
	border-right: solid 2px;
}
</style>

<?php

function getMonth($arg){
	$monthtothai['01'] ="ม.ค.";	
	$monthtothai['02'] ="ก.พ.";
	$monthtothai['03'] ="มี.ค.";
	$monthtothai['04'] ="เม.ย.";
	$monthtothai['05'] ="พ.ค.";
	$monthtothai['06'] ="มิ.ย.";
	$monthtothai['07'] ="ก.ค.";
	$monthtothai['08'] ="ส.ค.";
	$monthtothai['09'] ="ก.ย.";
	$monthtothai['10'] ="ต.ค.";
	$monthtothai['11'] ="พ.ย.";
	$monthtothai['12'] ="ธ.ค.";
	
	return $monthtothai[$arg];
}

function getDay($arg){
	$daytothai['Sun'] = "อา.";
	$daytothai['Mon'] = "จ.";
	$daytothai['Tue'] = "อ.";
	$daytothai['Wed'] = "พ.";
	$daytothai['Thu'] = "พฤ.";
	$daytothai['Fri'] = "ศ.";
	$daytothai['Sat'] = "ส.";
	
	return $daytothai[$arg];
}

function final_print($tmpIdTeam,$finalMatch,$thirdMatch = null){ ?>
	<td class="round_column r_2 final">
		<div class="mtch_container">
			<div class="match_unit">
				<div class="m_dtls" <?php
					if(count($finalMatch['MatchData'])==2){
						?>style="bottom: 125px;"<?php 
					}else{
						?>style="bottom: 95px;"<?php
					} ?> >
					<span>
						<?php
						foreach($finalMatch['MatchData'] as $key => $tmpSubMatch ){
							if($key >= 1){
								echo '<br/>';
							}
							if(count($finalMatch['MatchData'])==2){
								if($key == 0){
									echo "นัดแรก, ".getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
								}else{
									echo "นัดสอง, ".getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
								}
							}else{
								echo getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
							}
							?>
							<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
							if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
								vs<?php
							}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
								<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
							}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
								c-c<?php
							}else if($tmpSubMatch['MatchStatus']=='Post'){?>
								p-p<?php
							}else{ ?>
								N/A<?php
							} ?>
							</a>
							<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
						<?php } ?>
					</span>
				</div>
				<div class="m_dtls" style="bottom: -10px;">
					<span><b>รอบชิงชนะเลิศ</b></span>
				</div>
				<div class="m_segment m_top <?php
					if($finalMatch['TeamWinner'] == 1) { echo 'winner'; }
					else { echo 'loser'; }
					?>" data-team-id="<?php
					if($finalMatch['Team1KPID'] > 0) { echo $finalMatch['Team1KPID']; }
					else{ echo $tmpIdTeam; $tmpIdTeam--; }
				?>">
					<span>
						<a href="<?php echo $finalMatch['Team1URL']; ?>">
							<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $finalMatch['Team1Logo']))){ ?>
							<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $finalMatch['Team1Logo']); ?>" style="height: 25px;"/>
							<?php }else{ ?>
							<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
							<?php } ?>
							<span><?php echo $finalMatch['Team1']; ?></span>
						</a>
						<strong><?php echo $finalMatch['Team1FTScore']; ?></strong>
					</span>
				</div>
				<div class="m_segment m_botm <?php
					if($finalMatch['TeamWinner'] == 2) { echo 'winner'; }
					else { echo 'loser'; }
					?>" data-team-id="<?php
					if($finalMatch['Team2KPID'] > 0) { echo $finalMatch['Team2KPID']; }
					else{ echo $tmpIdTeam; $tmpIdTeam--; }
				?>">
					<span>
						<a href="<?php echo $finalMatch['Team2URL']; ?>">
							<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $finalMatch['Team2Logo']))){ ?>
							<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $finalMatch['Team2Logo']); ?>" style="height: 25px;"/>
							<?php }else{ ?>
							<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
							<?php } ?>
							<span><?php echo $finalMatch['Team2']; ?></span>
						</a>
						<strong><?php echo $finalMatch['Team2FTScore']; ?></strong>
					</span>
				</div>
			</div>
		</div>
		<?php if($thirdMatch!=null){ ?>
		<div class="mtch_container third_position">
			<div class="match_unit">
				<div class="m_dtls" style="bottom: 90px;">
					<span><b>รอบชิงอันดับที่ 3</b></span>
				</div>
				<div class="m_dtls" <?php
					if(count($thirdMatch['MatchData'])==2){
						?>style="bottom: -45px;"<?php 
					}else{
						?>style="bottom: -25px;"<?php
					} ?> >
					<span>
						<?php
						foreach($thirdMatch['MatchData'] as $key => $tmpSubMatch ){
							if($key >= 1){
								echo '<br/>';
							}
							if(count($thirdMatch['MatchData'])==2){
								if($key == 0){
									echo "นัดแรก, ".getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
								}else{
									echo "นัดสอง, ".getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
								}
							}else{
								echo getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
							}
							?>
							<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
							if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
								vs<?php
							}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
								<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
							}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
								c-c<?php
							}else if($tmpSubMatch['MatchStatus']=='Post'){?>
								p-p<?php
							}else{ ?>
								N/A<?php
							} ?>
							</a>
							<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
						<?php } ?>
					</span>
				</div>
				<div class="m_segment m_top <?php
					if($thirdMatch['TeamWinner'] == 1) { echo 'winner'; }
					else { echo 'loser'; }
					?>" data-team-id="<?php
					if($thirdMatch['Team1KPID'] > 0) { echo $thirdMatch['Team1KPID']; }
					else{ echo $tmpIdTeam; $tmpIdTeam--; }
				?>">
					<span>
						<a href="<?php echo $thirdMatch['Team1URL']; ?>">
							<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $thirdMatch['Team1Logo']))){ ?>
							<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $thirdMatch['Team1Logo']); ?>" style="height: 25px;"/>
							<?php }else{ ?>
							<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
							<?php } ?>
							<span><?php echo $thirdMatch['Team1']; ?></span>
						</a>
						<strong><?php echo $thirdMatch['Team1FTScore']; ?></strong>
					</span>
				</div>
				<div class="m_segment m_botm <?php
					if($thirdMatch['TeamWinner'] == 2) { echo 'winner'; }
					else { echo 'loser'; }
					?>" data-team-id="<?php
					if($thirdMatch['Team2KPID'] > 0) { echo $thirdMatch['Team2KPID']; }
					else{ echo $tmpIdTeam; $tmpIdTeam--; }
				?>">
					<span>
						<a href="<?php echo $thirdMatch['Team2URL']; ?>">
							<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $thirdMatch['Team2Logo']))){ ?>
							<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $thirdMatch['Team2Logo']); ?>" style="height: 25px;"/>
							<?php }else{ ?>
							<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
							<?php } ?>
							<span><?php echo $thirdMatch['Team2']; ?></span>
						</a>
						<strong><?php echo $thirdMatch['Team2FTScore']; ?></strong>
					</span>
				</div>
			</div>
		</div>
		<?php } ?>
	</td>
<?php
	return $tmpIdTeam;
}

function round_print($dataRound,$class_round,$size_match,$offset_match,$tmpIdTeam){ ?>
	<td class="round_column <?php echo $class_round; ?>">
		<?php for( $i=$offset_match ; $i<($offset_match+$size_match) ; $i++) {
			$tmpMatch = $dataRound[$i];
			?>
			<div class="mtch_container">
				<div class="match_unit">
					<div class="m_segment m_top <?php
						if($tmpMatch['TeamWinner'] == 1) { echo 'winner'; }
						else { echo 'loser'; }
						?>" data-team-id="<?php
						if($tmpMatch['Team1KPID'] > 0) { echo $tmpMatch['Team1KPID']; }
						else{ echo $tmpIdTeam; $tmpIdTeam--; }
					?>">
						<span>
							<a href="<?php echo $tmpMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/logo/default.png" style="height: 25px;"/>
								<?php } ?>
								<span><?php echo $tmpMatch['Team1']; ?></span>
							</a>
							<strong><?php echo $tmpMatch['Team1FTScore']; ?></strong>
						</span>
					</div>
					<div class="m_segment m_botm <?php
						if($tmpMatch['TeamWinner'] == 2) { echo 'winner'; }
						else { echo 'loser'; }
						?>" data-team-id="<?php
						if($tmpMatch['Team2KPID'] > 0) { echo $tmpMatch['Team2KPID']; }
						else{ echo $tmpIdTeam; $tmpIdTeam--; }
					?>">
						<span>
							<a href="<?php echo $tmpMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/logo/default.png" style="height: 25px;"/>
								<?php } ?>
								<span><?php echo $tmpMatch['Team2']; ?></span>
							</a>
							<strong><?php echo $tmpMatch['Team2FTScore']; ?></strong>
						</span>
					</div>
					<div class="m_dtls">
						<span>
							<?php
							foreach($tmpMatch['MatchData'] as $key => $tmpSubMatch ){
								if($key >= 1){
									echo '<br/>';
								}
								if(count($tmpMatch['MatchData'])==2){
									if($key == 0){
										echo "นัดแรก, ".getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
									}else{
										echo "นัดสอง, ".getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
									}
								}else{
									echo getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
								}
								?>
								<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
									<?php } ?>
								</a>
								<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
								if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
									vs<?php
								}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
									<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
								}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
									c-c<?php
								}else if($tmpSubMatch['MatchStatus']=='Post'){?>
									p-p<?php
								}else{ ?>
									N/A<?php
								} ?>
								</a>
								<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
									<?php } ?>
								</a>
							<?php } ?>
						</span>
					</div>
				</div>
			</div>
		<?php } ?>
	</td>
<?php
	return $tmpIdTeam;
}
?>

<div class="container league ">
	<?php
	
	$allow_id = array(43,94,363252,478011,786992,797014,856305,930736,36305,775500);
	$userid = uid;
	if($_REQUEST['tbug']=='debug'){
		echo uid;
		exit;
	}
	
	$cupname 		= 	$this->InfoCupContents['nameTH'];
	$cupshortname 	= 	$this->InfoCupContents['nameTHShort'];
	$cuppathname	=	$this->InfoCupContents['nameURL'];
	
    include dirname(__FILE__) . "/header.tpl.php";
	
	if ( in_array($userid,$allow_id) && ($_SERVER['REQUEST_METHOD'] == 'POST') && ($_POST['do']=="updateclip")) {
		//var_dump($_POST);
		$this->mem_lib->set('Football2014-Clip-League-URL',$_POST['clip_league_url']);
		echo "Clip Updated";
	}
	
    ?>


  
<div class="row">
   <!-- Start LEFT COLUME --> 
	<div class="col-md-8">
		<style>
			@media only screen and (min-width : 768px) {  #video_premium { width:640px !important; height:360px !important ; margin:auto; display:block} }
			@media only screen and (min-width : 481px) and (max-width : 768px) { #video_premium { width:100% !important; height:360px !important ; margin:auto;display:block } }
			@media only screen and (min-width : 321px) and (max-width : 480px) { #video_premium { width:100% !important; height:205px !important ; margin:auto;display:block } }
			@media only screen and (max-width : 320px) { #video_premium { width:100% !important; height:151px !important; margin:auto;display:block } }
		</style>
		<?php if($cuppathname == premierleague){ ?>
			<iframe id="video_premium" style="margin-bottom: 1%;" src='<?php if(!$this->mem_lib->get('Football2014-Clip-League-URL')){ ?>http://video.kapook.com/content/iframe/546194d638217a2341000000<?php }else{ echo $this->mem_lib->get('Football2014-Clip-League-URL'); } ?>'  frameborder='0' scrolling='no' width='640'  height='360' allowFullScreen></iframe>
			<?php
			if(in_array($userid,$allow_id)){
			?>
				<form action="" method="post">
					<legend>URL of Clip :</legend>
					<input type="hidden" name="do" id="do" value="updateclip">
					<input type="text" name="clip_league_url" size="60" id="clip_league_url" value="<?php echo $this->mem_lib->get('Football2014-Clip-League-URL'); ?>">
					<input type="submit" value="Update clip">
				</form>
			<?php
			}
		}
		?>
		<div class="news ">
			<?php if(count($this->TableContents)) { ?>
			<h1 class="font-display"><i class="fa fa-sort-amount-desc"></i> ตารางคะแนน</h1>
			<div class="col-md-12">
				<!--    ตารางคะแนนลีกส์ต่างๆ     -->
				<div class="table_standing">
				<?php
					foreach( $this->TableContents as $tmpTable ){ ?>
						<h2 class="font-display" style="margin-top: 0;"><?php echo $tmpTable['name'];?></h2><?php
						if(($tmpTable['type']==1)||($tmpTable['type']==2)){ ?>
							<div class="col-md-12">
								<table  class="table gray table-striped table-bordered ">
									<thead>
										<tr>
											<th>#</th>
											<th>ชื่อทีม</th>
											<th class="tooltip_top"  title="จำนวนนัดที่แข่ง">P</th>
											<th class="tooltip_top"  title="จำนวนนัดที่ชนะ">W</th>
											<th class="tooltip_top"  title="จำนวนนัดที่เสมอ">D</th>
											<th class="tooltip_top"  title="จำนวนนัดที่แพ้">L</th>
											<th class="tooltip_top"  title="จำนวนประตูที่ยิงได้">GF</th>
											<th class="tooltip_top"  title="จำนวนประตูที่เสีย">GA</th>
											<th class="tooltip_top"  title="จำนวนผลต่างประตู">GD</th>
											<th class="tooltip_top"  title="จำนวนคะแนน">PTS</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i=0;
										foreach( $tmpTable['TeamData'] as $tmpTeam ){
											$tmpTeam['total_match']		=	($tmpTeam['win']+$tmpTeam['draw']+$tmpTeam['lose']);
											?>
											<tr>
												<td><?php echo $i+1 ?></td>
												<td><a href="<?php echo $tmpTeam['url']; ?>" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpTeam['logo']); ?>" width="24" height="24"  alt=""/> <?php echo $tmpTeam['name']; ?></a></td>
												<td><?php echo $tmpTeam['total_match']; ?></td>
												<td><?php echo $tmpTeam['win']; ?></td>
												<td><?php echo $tmpTeam['draw']; ?></td>
												<td><?php echo $tmpTeam['lose']; ?></td>
												<td><?php echo $tmpTeam['goal_for']; ?></td>
												<td><?php echo $tmpTeam['goal_against']; ?></td>
												<td><?php echo $tmpTeam['goal_dif']; ?></td>
												<td><?php echo $tmpTeam['point']; ?></td>
											</tr>
										<?php
											$i++;
										}
										?>
									</tbody>
								</table>
							</div>
						<?php }elseif($tmpTable['type']==3){ ?>
							<div class="brackets_container">
								<table>
									<!--rounds container-->
									<thead><tr>
										<?php if(count($tmpTable['RoundData'])>=4){ ?>
										<th>
											<span>รอบ 16 ทีม</span>
										</th>
										<?php }
										if(count($tmpTable['RoundData'])>=3){ ?>
										<th>
											<span>รอบ 8 ทีม</span>
										</th>
										<?php }
										if(count($tmpTable['RoundData'])>=2){ ?>
										<th>
											<span>รอบรองชนะเลิศ</span>
										</th>
										<?php }
										if(count($tmpTable['RoundData'])>=1){ ?>
										<th>
											<span>รอบชิงชนะเลิศ</span>
										</th>
										<?php }
										if(count($tmpTable['RoundData'])>=2){ ?>
										<th>
											<span>รอบรองชนะเลิศ</span>
										</th>
										<?php }
										if(count($tmpTable['RoundData'])>=3){ ?>
										<th>
											<span>รอบ 8 ทีม</span>
										</th>
										<?php }
										if(count($tmpTable['RoundData'])>=4){ ?>
										<th>
											<span>รอบ 16 ทีม</span>
										</th>
										<?php } ?>
									</tr></thead>
									<!--matches container-->
									<?php
										$index_round 	= 	0;
										$tmpIdTeam		=	-2;
									?>
									<tbody><tr id="playground">
										<!-- Round of 16 -->
										<?php if(count($tmpTable['RoundData'])>=4){
											$tmpRound = $tmpTable['RoundData'][$index_round];
											$tmpIdTeam = round_print($tmpRound,"r_16",4,0,$tmpIdTeam);
											$index_round++;
										} ?>
										<!-- Round of 8 -->
										<?php if(count($tmpTable['RoundData'])>=3){
											$tmpRound = $tmpTable['RoundData'][$index_round];
											$tmpIdTeam = round_print($tmpRound,"r_8",2,0,$tmpIdTeam);
											$index_round++;
										} ?>
										<!-- Round of 4 -->
										<?php if(count($tmpTable['RoundData'])>=2){
											$tmpRound = $tmpTable['RoundData'][$index_round];
											$tmpIdTeam = round_print($tmpRound,"r_4",1,0,$tmpIdTeam);
											$index_round++;
										} ?>
										<!-- Final & 3rd -->
										<?php if(count($tmpTable['RoundData'])>=1){
											$tmpRound = $tmpTable['RoundData'][$index_round];
											if(isset($tmpRound[1])){
												$tmpIdTeam = final_print($tmpIdTeam,$tmpRound[0],$tmpRound[1]);
											}else{
												$tmpIdTeam = final_print($tmpIdTeam,$tmpRound[0]);
											}
											$index_round--;
										} ?>
										<!-- Round of 4 -->
										<?php if(count($tmpTable['RoundData'])>=2){
											$tmpRound = $tmpTable['RoundData'][$index_round];
											$tmpIdTeam = round_print($tmpRound,"r_4 reversed",1,1,$tmpIdTeam);
											$index_round--;
										} ?>
										<!-- Round of 8 -->
										<?php if(count($tmpTable['RoundData'])>=3){
											$tmpRound = $tmpTable['RoundData'][$index_round];
											$tmpIdTeam = round_print($tmpRound,"r_8 reversed",2,2,$tmpIdTeam);
											$index_round--;
										} ?>
										<!-- Round of 16 -->
										<?php if(count($tmpTable['RoundData'])>=4){
											$tmpRound = $tmpTable['RoundData'][$index_round];
											$tmpIdTeam = round_print($tmpRound,"r_16 reversed",4,4,$tmpIdTeam);
											//$index_round--;
										} ?>
									</tr></tbody>
								</table>
							</div>
						<?php }
					}
					?>
				</div>
			</div>
			<?php } ?>
			
			<?php foreach($this->TableRoundContents as $tmpRound){ ?>
				<h1 class="font-display"><i class="fa fa-sort-amount-desc"></i> ตารางคะแนน <?php echo $tmpRound['name']; ?></h1>
				<div class="col-md-12">
					<!--    ตารางคะแนนลีกส์ต่างๆ     -->
					<div class="table_standing">
					<?php
						foreach( $tmpRound['table'] as $tmpTable ){ ?>
							<h2 class="font-display" style="margin-top: 0;"><?php echo $tmpTable['name'];?></h2>
							<div class="col-md-12">
								<?php if(($tmpTable['type']==1)||($tmpTable['type']==2)){ ?>
								<table  class="table gray table-striped table-bordered ">
									<thead>
										<tr>
											<th>#</th>
											<th>ชื่อทีม</th>
											<th class="tooltip_top"  title="จำนวนนัดที่แข่ง">P</th>
											<th class="tooltip_top"  title="จำนวนนัดที่ชนะ">W</th>
											<th class="tooltip_top"  title="จำนวนนัดที่เสมอ">D</th>
											<th class="tooltip_top"  title="จำนวนนัดที่แพ้">L</th>
											<th class="tooltip_top"  title="จำนวนประตูที่ยิงได้">GF</th>
											<th class="tooltip_top"  title="จำนวนประตูที่เสีย">GA</th>
											<th class="tooltip_top"  title="จำนวนผลต่างประตู">GD</th>
											<th class="tooltip_top"  title="จำนวนคะแนน">PTS</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i=0;
										foreach( $tmpTable['TeamData'] as $tmpTeam ){
											$tmpTeam['total_match']		=	($tmpTeam['win']+$tmpTeam['draw']+$tmpTeam['lose']);
											?>
											<tr>
												<td><?php echo $i+1 ?></td>
												<td><a href="<?php echo $tmpTeam['url']; ?>" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpTeam['logo']); ?>" width="24" height="24"  alt=""/> <?php echo $tmpTeam['name']; ?></a></td>
												<td><?php echo $tmpTeam['total_match']; ?></td>
												<td><?php echo $tmpTeam['win']; ?></td>
												<td><?php echo $tmpTeam['draw']; ?></td>
												<td><?php echo $tmpTeam['lose']; ?></td>
												<td><?php echo $tmpTeam['goal_for']; ?></td>
												<td><?php echo $tmpTeam['goal_against']; ?></td>
												<td><?php echo $tmpTeam['goal_dif']; ?></td>
												<td><?php echo $tmpTeam['point']; ?></td>
											</tr>
										<?php
											$i++;
										}
										?>
									</tbody>
								</table>
								<?php }else{ ?>
								<div class="brackets_container">
									<table>
										<!--rounds container-->
										<thead><tr>
											<?php if(count($tmpTable['RoundData'])>=4){ ?>
											<th>
												<span>รอบ 16 ทีม</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=3){ ?>
											<th>
												<span>รอบ 8 ทีม</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=2){ ?>
											<th>
												<span>รอบรองชนะเลิศ</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=1){ ?>
											<th>
												<span>รอบชิงชนะเลิศ</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=2){ ?>
											<th>
												<span>รอบรองชนะเลิศ</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=3){ ?>
											<th>
												<span>รอบ 8 ทีม</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=4){ ?>
											<th>
												<span>รอบ 16 ทีม</span>
											</th>
											<?php } ?>
										</tr></thead>
										<!--matches container-->
										<?php
											$index_round 	= 	0;
											$tmpIdTeam		=	-2;
										?>
										<tbody><tr id="playground">
											<!-- Round of 16 -->
											<?php if(count($tmpTable['RoundData'])>=4){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_16",4,0,$tmpIdTeam);
												$index_round++;
											} ?>
											<!-- Round of 8 -->
											<?php if(count($tmpTable['RoundData'])>=3){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_8",2,0,$tmpIdTeam);
												$index_round++;
											} ?>
											<!-- Round of 4 -->
											<?php if(count($tmpTable['RoundData'])>=2){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_4",1,0,$tmpIdTeam);
												$index_round++;
											} ?>
											<!-- Final & 3rd -->
											<?php if(count($tmpTable['RoundData'])>=1){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												if(isset($tmpRound[1])){
													$tmpIdTeam = final_print($tmpIdTeam,$tmpRound[0],$tmpRound[1]);
												}else{
													$tmpIdTeam = final_print($tmpIdTeam,$tmpRound[0]);
												}
												$index_round--;
											} ?>
											<!-- Round of 4 -->
											<?php if(count($tmpTable['RoundData'])>=2){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_4 reversed",1,1,$tmpIdTeam);
												$index_round--;
											} ?>
											<!-- Round of 8 -->
											<?php if(count($tmpTable['RoundData'])>=3){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_8 reversed",2,2,$tmpIdTeam);
												$index_round--;
											} ?>
											<!-- Round of 16 -->
											<?php if(count($tmpTable['RoundData'])>=4){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_16 reversed",4,4,$tmpIdTeam);
												//$index_round--;
											} ?>
										</tr></tbody>
									</table>
								</div>
								<?php } ?>
							</div> 
						<?php
						}
						?>
					</div>
				</div>
			<?php } ?>
			
		</div>
	</div>
<!--End Col 8 LEFT VOLUME-->
	<div class="col-md-4 aside">
		<div class="sidebar_margin" >
		  <h2 class="font-display"><i class="fa fa-list-alt"></i> ตารางคะแนน</h2>
		  <ul class="nav ">
			<li><a href="/พรีเมียร์ลีก/table" class="tooltip_top" title="คลิกดูรายละเอียด" target="_blank"> ตารางคะแนนพรีเมียร์ลีก</a></li>
			<li><a href="/ลาลีกา/table" class="tooltip_top" title="คลิกดูรายละเอียด" target="_blank"> ตารางคะแนนลาลีกา</a></li>
			<li><a href="/บุนเดสลีกา/table" class="tooltip_top" title="คลิกดูรายละเอียด" target="_blank"> ตารางคะแนนบุนเดสลีกา</a></li>
			<li><a href="/เซเรียอา/table" class="tooltip_top" title="คลิกดูรายละเอียด" target="_blank"> ตารางคะแนนเซเรียอา</a></li>
			<li><a href="/ลีกเอิง/table" class="tooltip_top" title="คลิกดูรายละเอียด" target="_blank"> ตารางคะแนนลีกเอิง</a></li>
			<li><a href="/ไทยพรีเมียร์ลีก/table" class="tooltip_top" title="คลิกดูรายละเอียด" target="_blank"> ตารางคะแนนไทยพรีเมียร์ลีก</a></li>
			<li><a href="/suzukicup/table" class="tooltip_top" title="คลิกดูรายละเอียด" target="_blank"> ตารางคะแนนเอเอฟเอฟ ซูซูกิ คัพ 2014</a></li>
		  </ul>
		</div>
	</div>
</div>
</div></div>
