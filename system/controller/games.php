<?php

if (!defined('MINIZONE'))
    exit;

class Games extends My_con {

    var $view;
    var $minizone;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {

        parent::__construct();

		if(!isset($this->games_model)){
			$this->games_model = $this->minizone->model('games_model');
		}
		if(!isset($this->livescore_model)){
			$this->livescore_model = $this->minizone->model('livescore_model');
		}
    }

    ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
    function index() {
		/*if((!$this->isCanPlayGame)&&($_REQUEST['tbug']!=2)) {
			echo 'ยังไม่เปิดใช้บริการ';
			exit;
		}*/
		session_start();

		if($_REQUEST['remove_cache']==1){
			$isClear	=	true;
		}else{
			$isClear	=	false;
		}
		$this->view->assign('mem_lib', $this->mem_lib);
		
		if(intval(date('G'))<6){
			$DateProgram[1]		= 	date("Y-m-d",strtotime("-1 days"));
			$attrMonth			=	date('Y-m',strtotime("-1 days"));
			$attrDay			=	date("Y-m-d",strtotime("-2 days"));
		}else{
			$DateProgram[1] 	= 	date("Y-m-d");
			$attrMonth			=	date('Y-m');
			$attrDay			=	date("Y-m-d",strtotime("-1 days"));
		}
		
		$ProgramContents1 =	$this->mem_lib->get('Football2014-LiveScore-' . $DateProgram[1]);
		
		if((!$ProgramContents1)||($_REQUEST['remove_cache']==1)){
			$expire		=		600;
			$ProgramContents1 = $this->livescore_model->getProgramByDate($DateProgram[1]);
			$ProgramContents1['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $DateProgram[1], $ProgramContents1, MEMCACHE_COMPRESSED, $expire);
		}

		$start_date['day']		=	1;
		$start_date['month']	=	8;
		
		$attrYear				=	'2014';
			
		// Top 3 Ranking
		$RankingDaily	=	$this->games_model->getRanking('date', $attrDay, 'total_point', 'desc', 0, 3, $isClear);
		$RankingMonth	=	$this->games_model->getRanking('month', $attrMonth, 'total_point', 'desc', 0, 3, $isClear);
		$RankingYear	=	$this->games_model->getRanking('year', $attrYear, 'total_point', 'desc', 0, 3, $isClear);
			
		// My Ranking
		$uid	=	$this->login_lib->checkCookie();
		if($uid)
		{
			$InfoMember		=	$this->login_lib->get_KMember(array( 'userid' =>  $uid));
			$MyInfoScoreMember['username']	=	$InfoMember->username;
			$MyInfoScoreMember['picture']	=	$InfoMember->picture;
			
			$MyRankingDaily 	= 	$this->mem_lib->get('Football2014-MyRankingDaily-' . $uid );
			if((!$MyRankingDaily)||($_REQUEST['remove_cache'] != 1)){
				$MyRankingDaily		=	$this->games_model->getMyRanking($uid,'date', $attrDay);
				$this->mem_lib->set('Football2014-MyRankingDaily-' . $uid , $MyRankingDaily, MEMCACHE_COMPRESSED, 3600);
			}
			
			$MyRankingMonth 	= 	$this->mem_lib->get('Football2014-MyRankingMonth-' . $uid );
			if((!$MyRankingMonth)||($_REQUEST['remove_cache'] != 1)){
				$MyRankingMonth		=	$this->games_model->getMyRanking($uid,'month', $attrMonth);
				$this->mem_lib->set('Football2014-MyRankingMonth-' . $uid , $MyRankingMonth, MEMCACHE_COMPRESSED, 3600);
			}
			
			$MyRankingYear 		= 	$this->mem_lib->get('Football2014-MyRankingYear-' . $uid );
			if((!$MyRankingYear)||($_REQUEST['remove_cache'] != 1)){
				$MyRankingYear		=	$this->games_model->getMyRanking($uid,'year', $attrYear);
				$this->mem_lib->set('Football2014-MyRankingYear-' . $uid , $MyRankingYear, MEMCACHE_COMPRESSED, 3600);
			}
				
			$this->view->assign('MyInfoScoreMember', $MyInfoScoreMember);
			$this->view->assign('MyRankingDaily', $MyRankingDaily);
			$this->view->assign('MyRankingMonth', $MyRankingMonth);
			$this->view->assign('MyRankingYear', $MyRankingYear);

		}
			
		$this->view->assign('RankingDaily', $RankingDaily);
		$this->view->assign('RankingMonth', $RankingMonth);
		$this->view->assign('RankingYear', $RankingYear);
			
		$this->view->assign('ProgramContents1', $ProgramContents1);
		/*$this->view->assign('infoGameScore', $infoGameScore);
		$this->view->assign('infoGameSide', $infoGameSide);*/

		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
		$breadcrum[0]['link'] = BASE_HREF;
		$breadcrum[1]['text'] = 'เกมทายผลบอล';
		$breadcrum[1]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$data['seo_title'] = "เกมทายผลบอล กระปุกทายผลบอล เกมทายบอลเดี่ยว ทายบอลชุด";
		$data['seo_description'] = "เกมทายผลบอล ทายผลบอลเดี่ยว ทายผลบอลชุด ทายผลบอลโลก เกมทายผลบอลเก็บคะแนน จัดอันดับ เทพวางบอล";
		$data['seo_keywords'] = "ทายผลบอล, วางบอล, เกมทายผลบอล, ทายบอลเดี่ยว, ทายบอลชุด, เทพวางบอล";

		$data['fb_title'] = "กระปุกทายผลบอล เกมทายบอลเดี่ยว ทายบอลชุด";
		$data['fb_description'] = " เกมทายผลบอล ทายผลบอลเดี่ยว ทายผลบอลชุด ทายผลบอลเก็บคะแนน ทายผลบอลชิงรางวัล";
		$data['fb_img'] = 'http://football.kapook.com/assets/og/2014-11/game/poster.jpg';

		parent::_setSocial($data);

		$this->_header();
		$this->view->render($this->root_view . '/games/games-match-list.tpl');
		$this->_footer();
    }
	
	function ranking($page)
	{
		/*if(!$this->isCanPlayGame) {
			echo 'ยังไม่เปิดใช้บริการ';
			exit;
		}*/
		$this->view->assign('mem_lib', $this->mem_lib);
		
		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
		$breadcrum[0]['link'] = BASE_HREF;
		$breadcrum[1]['text'] = 'เกมทายผลบอล';
		$breadcrum[1]['link'] = BASE_HREF."games";
		$breadcrum[2]['text'] = 'อันดับคะแนน';
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$data['seo_title'] = "เกมทายผลบอล กระปุกทายผลบอล เกมทายบอลเดี่ยว ทายบอลชุด";
		$data['seo_description'] = "เกมทายผลบอล ทายผลบอลเดี่ยว ทายผลบอลชุด ทายผลบอลโลก เกมทายผลบอลเก็บคะแนน จัดอันดับ เทพวางบอล";
		$data['seo_keywords'] = "ทายผลบอล, วางบอล, เกมทายผลบอล, ทายบอลเดี่ยว, ทายบอลชุด, เทพวางบอล";

		$data['fb_title'] = "กระปุกทายผลบอล เกมทายบอลเดี่ยว ทายบอลชุด";
		$data['fb_description'] = " เกมทายผลบอล ทายผลบอลเดี่ยว ทายผลบอลชุด ทายผลบอลเก็บคะแนน ทายผลบอลชิงรางวัล";
		$data['fb_img'] = 'http://football.kapook.com/assets/og/2014-11/game/poster.jpg';

		parent::_setSocial($data);

		if($_REQUEST['remove_cache']==1){
			$this->view->assign('isClear', true);
		}else{
			$this->view->assign('isClear', false);
		}
		
		$this->_header();
		$this->view->render($this->root_view . '/games/game-ranking.tpl');
		$this->_footer();
		
	}
    
    function playscore($saveOrDelete,$idMatch,$scoreTeam1,$scoreTeam2){
		//echo $scoreTeam1 . ' ' . $scoreTeam2;
		if(strtolower($saveOrDelete)=='insert')
			$returnData = $this->games_model->insertPlayScore($idMatch,$scoreTeam1,$scoreTeam2);
		echo json_encode($returnData);
    }
    
    function playside($saveOrDelete,$idMatch,$select_team = null,$select_point = null){
		if(strtolower($saveOrDelete)=='insert')
			$returnData = $this->games_model->insertPlaySide($idMatch,$select_team,$select_point);
		echo json_encode($returnData);
    }
	
    function playsidemulti($saveOrDelete,$setNo,$point){
		if(strtolower($saveOrDelete)=='insert'){
			$returnData = $this->games_model->insertPlaySideMulti($setNo,$point);
		}
		echo json_encode($returnData);
    }
	
	function confirm( $type , $arg_1 , $arg_2 , $arg_3 = null){
		if($type == 'single'){
			$dataSubmit	=	array(
				'type'			=>		'single',
				'match_id'		=>		$arg_1,
				'selectSide'	=>		$arg_2,
				'point'			=>		$arg_3
			);
			
			/*$tmpMatch		=	$this->livescore_model->getLiveScoreMatch($arg_1);
			$strListTeam	=	$tmpMatch['MatchInfo']['Team1KPID'] . ',' . $tmpMatch['MatchInfo']['Team2KPID'];*/
			
		}else if($type == 'multi'){
			$dataSubmit	=	array(
				'type'			=>		'multi',
				'set_id'		=>		$arg_1,
				'point'			=>		$arg_2,
			);
			
			/*$arrGameTmp		=	explode('|',$_COOKIE['SET' . $arg_1]);
			$sizeOfPlay		=	count($arrGameTmp);

			$strListTeam	=	null;

			for($x=0;$x<$sizeOfPlay;$x++){
				$tmpGame		=	explode(':',$arrGameTmp[$x]);
				$tmpMatch		=	$this->livescore_model->getLiveScoreMatch($tmpGame[0]);
				if($strListTeam==null){
					$strListTeam	=	$tmpMatch['MatchInfo']['Team1KPID'] . ',' . $tmpMatch['MatchInfo']['Team2KPID'];
				}else{
					$strListTeam	=	$strListTeam . ',' . $tmpMatch['MatchInfo']['Team1KPID'] . ',' . $tmpMatch['MatchInfo']['Team2KPID'];
				}
			}*/
		}
		
		//$NewsMatchContents = $this->news_model->getNewsByTeam($strListTeam,1);
		
		$this->view->assign('dataSubmit', $dataSubmit);
		//$this->view->assign('NewsMatchContents', $NewsMatchContents);
		$this->view->render($this->root_view . '/games/game-modal-confirm.tpl');
	}
    
    function match($id,$play_type){
        
        $MatchContents 		= 	$this->livescore_model->getLiveScoreMatch($id);
        
        $this->view->assign('MatchContents', $MatchContents);
        if($play_type=='playscore'){
            $this->view->render($this->root_view . '/games/games-match-info-score.tpl');
        }
    }
	
    function help(){

		$this->view->render($this->root_view . '/games/games-help.tpl');
    }
	
    function helprule(){
		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
		$breadcrum[0]['link'] = BASE_HREF;
		$breadcrum[1]['text'] = 'เกมทายผลบอล';
		$breadcrum[1]['link'] = BASE_HREF."/games";
		$breadcrum[2]['text'] = ' กติกาการเล่น & คำถาม?';
		$breadcrum[2]['active'] = true;
		parent::_setBreadcrum($breadcrum);

		$data['seo_title'] = "เกมทายผลบอล กระปุกทายผลบอล เกมทายบอลเดี่ยว ทายบอลชุด";
		$data['seo_description'] = "เกมทายผลบอล ทายผลบอลเดี่ยว ทายผลบอลชุด ทายผลบอลโลก เกมทายผลบอลเก็บคะแนน จัดอันดับ เทพวางบอล";
		$data['seo_keywords'] = "ทายผลบอล, วางบอล, เกมทายผลบอล, ทายบอลเดี่ยว, ทายบอลชุด, เทพวางบอล";

		$data['fb_title'] = "กระปุกทายผลบอล เกมทายบอลเดี่ยว ทายบอลชุด";
		$data['fb_description'] = " เกมทายผลบอล ทายผลบอลเดี่ยว ทายผลบอลชุด ทายผลบอลเก็บคะแนน ทายผลบอลชิงรางวัล";
		$data['fb_img'] = '';

		parent::_setSocial($data);
		$this->_header();
		$this->view->render($this->root_view . '/games/game-help-full.tpl');
		$this->_footer();
    }
    
}
