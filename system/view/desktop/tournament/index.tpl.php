<link rel="stylesheet" href="<?php echo BASE_HREF; ?>assets/css/bracketz.css">
<script src="<?php echo BASE_HREF; ?>assets/js/bracketz.js"></script>

<style>
.match_unit.highlight{
    padding: 0px 0px;
    margin-bottom: 0px;
    border-radius: 0px;
	border:none;
	background-color: #F0F0F0;
	
}
.m_segment.highlight{
    padding: 0px 0px;
    margin-bottom: 0px;
    border-radius: 0px;
	border:none;
	background-color: #F0F0F0;
	border-right: solid 2px;
}
</style>

<?php

function getMonth($arg){
	$monthtothai['01'] ="ม.ค.";	
	$monthtothai['02'] ="ก.พ.";
	$monthtothai['03'] ="มี.ค.";
	$monthtothai['04'] ="เม.ย.";
	$monthtothai['05'] ="พ.ค.";
	$monthtothai['06'] ="มิ.ย.";
	$monthtothai['07'] ="ก.ค.";
	$monthtothai['08'] ="ส.ค.";
	$monthtothai['09'] ="ก.ย.";
	$monthtothai['10'] ="ต.ค.";
	$monthtothai['11'] ="พ.ย.";
	$monthtothai['12'] ="ธ.ค.";
	
	return $monthtothai[$arg];
}

function getDay($arg){
	$daytothai['Sun'] = "อา.";
	$daytothai['Mon'] = "จ.";
	$daytothai['Tue'] = "อ.";
	$daytothai['Wed'] = "พ.";
	$daytothai['Thu'] = "พฤ.";
	$daytothai['Fri'] = "ศ.";
	$daytothai['Sat'] = "ส.";
	
	return $daytothai[$arg];
}

function final_print($tmpIdTeam,$finalMatch,$thirdMatch = null){ ?>
	<td class="round_column r_2 final">
		<div class="mtch_container">
			<div class="match_unit">
				<div class="m_dtls" <?php
					if(count($finalMatch['MatchData'])==2){
						?>style="bottom: 125px;"<?php 
					}else{
						?>style="bottom: 95px;"<?php
					} ?> >
					<span>
						<?php
						foreach($finalMatch['MatchData'] as $key => $tmpSubMatch ){
							if($key >= 1){
								echo '<br/>';
							}
							if(count($finalMatch['MatchData'])==2){
								if($key == 0){
									echo "นัดแรก, ".getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
								}else{
									echo "นัดสอง, ".getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
								}
							}else{
								echo getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
							}
							?>
							<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
							if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
								vs<?php
							}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
								<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
							}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
								c-c<?php
							}else if($tmpSubMatch['MatchStatus']=='Post'){?>
								p-p<?php
							}else{ ?>
								N/A<?php
							} ?>
							</a>
							<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
						<?php } ?>
					</span>
				</div>
				<div class="m_dtls" style="bottom: -10px;">
					<span><b>รอบชิงชนะเลิศ</b></span>
				</div>
				<div class="m_segment m_top <?php
					if($finalMatch['TeamWinner'] == 1) { echo 'winner'; }
					else { echo 'loser'; }
					?>" data-team-id="<?php
					if($finalMatch['Team1KPID'] > 0) { echo $finalMatch['Team1KPID']; }
					else{ echo $tmpIdTeam; $tmpIdTeam--; }
				?>">
					<span>
						<a href="<?php echo $finalMatch['Team1URL']; ?>">
							<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $finalMatch['Team1Logo']))){ ?>
							<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $finalMatch['Team1Logo']); ?>" style="height: 25px;"/>
							<?php }else{ ?>
							<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
							<?php } ?>
							<span><?php echo $finalMatch['Team1']; ?></span>
						</a>
						<strong><?php echo $finalMatch['Team1FTScore']; ?></strong>
					</span>
				</div>
				<div class="m_segment m_botm <?php
					if($finalMatch['TeamWinner'] == 2) { echo 'winner'; }
					else { echo 'loser'; }
					?>" data-team-id="<?php
					if($finalMatch['Team2KPID'] > 0) { echo $finalMatch['Team2KPID']; }
					else{ echo $tmpIdTeam; $tmpIdTeam--; }
				?>">
					<span>
						<a href="<?php echo $finalMatch['Team2URL']; ?>">
							<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $finalMatch['Team2Logo']))){ ?>
							<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $finalMatch['Team2Logo']); ?>" style="height: 25px;"/>
							<?php }else{ ?>
							<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
							<?php } ?>
							<span><?php echo $finalMatch['Team2']; ?></span>
						</a>
						<strong><?php echo $finalMatch['Team2FTScore']; ?></strong>
					</span>
				</div>
			</div>
		</div>
		<?php if($thirdMatch!=null){ ?>
		<div class="mtch_container third_position">
			<div class="match_unit">
				<div class="m_dtls" style="bottom: 90px;">
					<span><b>รอบชิงอันดับที่ 3</b></span>
				</div>
				<div class="m_dtls" <?php
					if(count($thirdMatch['MatchData'])==2){
						?>style="bottom: -45px;"<?php 
					}else{
						?>style="bottom: -25px;"<?php
					} ?> >
					<span>
						<?php
						foreach($thirdMatch['MatchData'] as $key => $tmpSubMatch ){
							if($key >= 1){
								echo '<br/>';
							}
							if(count($thirdMatch['MatchData'])==2){
								if($key == 0){
									echo "นัดแรก, ".getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
								}else{
									echo "นัดสอง, ".getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
								}
							}else{
								echo getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
							}
							?>
							<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
							<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
							if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
								vs<?php
							}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
								<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
							}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
								c-c<?php
							}else if($tmpSubMatch['MatchStatus']=='Post'){?>
								p-p<?php
							}else{ ?>
								N/A<?php
							} ?>
							</a>
							<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
								<?php } ?>
							</a>
						<?php } ?>
					</span>
				</div>
				<div class="m_segment m_top <?php
					if($thirdMatch['TeamWinner'] == 1) { echo 'winner'; }
					else { echo 'loser'; }
					?>" data-team-id="<?php
					if($thirdMatch['Team1KPID'] > 0) { echo $thirdMatch['Team1KPID']; }
					else{ echo $tmpIdTeam; $tmpIdTeam--; }
				?>">
					<span>
						<a href="<?php echo $thirdMatch['Team1URL']; ?>">
							<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $thirdMatch['Team1Logo']))){ ?>
							<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $thirdMatch['Team1Logo']); ?>" style="height: 25px;"/>
							<?php }else{ ?>
							<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
							<?php } ?>
							<span><?php echo $thirdMatch['Team1']; ?></span>
						</a>
						<strong><?php echo $thirdMatch['Team1FTScore']; ?></strong>
					</span>
				</div>
				<div class="m_segment m_botm <?php
					if($thirdMatch['TeamWinner'] == 2) { echo 'winner'; }
					else { echo 'loser'; }
					?>" data-team-id="<?php
					if($thirdMatch['Team2KPID'] > 0) { echo $thirdMatch['Team2KPID']; }
					else{ echo $tmpIdTeam; $tmpIdTeam--; }
				?>">
					<span>
						<a href="<?php echo $thirdMatch['Team2URL']; ?>">
							<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $thirdMatch['Team2Logo']))){ ?>
							<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $thirdMatch['Team2Logo']); ?>" style="height: 25px;"/>
							<?php }else{ ?>
							<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" style="height: 25px;"/>
							<?php } ?>
							<span><?php echo $thirdMatch['Team2']; ?></span>
						</a>
						<strong><?php echo $thirdMatch['Team2FTScore']; ?></strong>
					</span>
				</div>
			</div>
		</div>
		<?php } ?>
	</td>
<?php
	return $tmpIdTeam;
}

function round_print($dataRound,$class_round,$size_match,$offset_match,$tmpIdTeam){ ?>
	<td class="round_column <?php echo $class_round; ?>">
		<?php for( $i=$offset_match ; $i<($offset_match+$size_match) ; $i++) {
			$tmpMatch = $dataRound[$i];
			?>
			<div class="mtch_container">
				<div class="match_unit">
					<div class="m_segment m_top <?php
						if($tmpMatch['TeamWinner'] == 1) { echo 'winner'; }
						else { echo 'loser'; }
						?>" data-team-id="<?php
						if($tmpMatch['Team1KPID'] > 0) { echo $tmpMatch['Team1KPID']; }
						else{ echo $tmpIdTeam; $tmpIdTeam--; }
					?>">
						<span>
							<a href="<?php echo $tmpMatch['Team1URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/logo/default.png" style="height: 25px;"/>
								<?php } ?>
								<span><?php echo $tmpMatch['Team1']; ?></span>
							</a>
							<strong><?php echo $tmpMatch['Team1FTScore']; ?></strong>
						</span>
					</div>
					<div class="m_segment m_botm <?php
						if($tmpMatch['TeamWinner'] == 2) { echo 'winner'; }
						else { echo 'loser'; }
						?>" data-team-id="<?php
						if($tmpMatch['Team2KPID'] > 0) { echo $tmpMatch['Team2KPID']; }
						else{ echo $tmpIdTeam; $tmpIdTeam--; }
					?>">
						<span>
							<a href="<?php echo $tmpMatch['Team2URL']; ?>">
								<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']))){ ?>
								<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" style="height: 25px;"/>
								<?php }else{ ?>
								<img src="http://fb.thaibuffer.com/r/24/h/logo/default.png" style="height: 25px;"/>
								<?php } ?>
								<span><?php echo $tmpMatch['Team2']; ?></span>
							</a>
							<strong><?php echo $tmpMatch['Team2FTScore']; ?></strong>
						</span>
					</div>
					<div class="m_dtls">
						<span>
							<?php
							foreach($tmpMatch['MatchData'] as $key => $tmpSubMatch ){
								if($key >= 1){
									echo '<br/>';
								}
								if(count($tmpMatch['MatchData'])==2){
									if($key == 0){
										echo "นัดแรก, ".getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
									}else{
										echo "นัดสอง, ".getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
									}
								}else{
									echo getDay(date("D",strtotime($tmpSubMatch['MatchDateTime'])))." ".date("d",strtotime($tmpSubMatch['MatchDateTime']))." ".getMonth(date("m",strtotime($tmpSubMatch['MatchDateTime'])))." ".((date("Y",strtotime($tmpSubMatch['MatchDateTime']))+543)%100)."<br/>";
								}
								?>
								<a href="<?php echo $tmpSubMatch['Team1URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team1Logo']); ?>" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team1']; ?>" style="height: 25px;"/>
									<?php } ?>
								</a>
								<a href="<?php echo strtolower($tmpSubMatch['MatchURL']); ?>" class="btn btn-grey btn-xs tooltip_top" title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php
								if($tmpSubMatch['MatchStatus']=='Sched'){ ?>
									vs<?php
								}else if($tmpSubMatch['MatchStatus']=='Fin'){ ?>
									<?php echo $tmpSubMatch['Team1FTScore']; ?> - <?php echo $tmpSubMatch['Team2FTScore']; ?><?php
								}else if($tmpSubMatch['MatchStatus']=='Canc'){?>
									c-c<?php
								}else if($tmpSubMatch['MatchStatus']=='Post'){?>
									p-p<?php
								}else{ ?>
									N/A<?php
								} ?>
								</a>
								<a href="<?php echo $tmpSubMatch['Team2URL']; ?>">
									<?php if(strlen(str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']))){ ?>
									<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpSubMatch['Team2Logo']); ?>" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
									<?php }else{ ?>
									<img src="http://fb.thaibuffer.com/r/24/h/uploads/logo/default.png" alt="<?php echo $tmpSubMatch['Team2']; ?>" style="height: 25px;"/>
									<?php } ?>
								</a>
							<?php } ?>
						</span>
					</div>
				</div>
			</div>
		<?php } ?>
	</td>
<?php
	return $tmpIdTeam;
}
?>

<div class="container league ">
	<?php
	
	$NewsCupArr = $this->NewsCupContents['NewsList'];
	
    $ProgramContents 	= 		$this->ProgramContents;
    $TotalMatch 		= 		count($ProgramContents['program']);
	$tmpAllMatch		=		$ProgramContents['program'];
    
    $monthtothai['Jan'] = "มกราคม";
    $monthtothai['Feb'] = "กุมภาพันธ์";
    $monthtothai['Mar'] = "มีนาคม";
    $monthtothai['Apr'] = "เมษายน";
    $monthtothai['May'] = "พฤษภาคม";
    $monthtothai['Jun'] = "มิถุนายน";
    $monthtothai['Jul'] = "กรกฎาคม";
    $monthtothai['Aug'] = "สิงหาคม";
    $monthtothai['Sep'] = "กันยายน";
    $monthtothai['Oct'] = "ตุลาคม";
    $monthtothai['Nov'] = "พฤศจิกายน";
    $monthtothai['Dec'] = "ธันวาคม";
    
    $monthtothaiByNo['01'] = "มกราคม";
    $monthtothaiByNo['02'] = "กุมภาพันธ์";
    $monthtothaiByNo['03'] = "มีนาคม";
    $monthtothaiByNo['04'] = "เมษายน";
    $monthtothaiByNo['05'] = "พฤษภาคม";
    $monthtothaiByNo['06'] = "มิถุนายน";
    $monthtothaiByNo['07'] = "กรกฎาคม";
    $monthtothaiByNo['08'] = "สิงหาคม";
    $monthtothaiByNo['09'] = "กันยายน";
    $monthtothaiByNo['10'] = "ตุลาคม";
    $monthtothaiByNo['11'] = "พฤศจิกายน";
    $monthtothaiByNo['12'] = "ธันวาคม";
    
    $daytothai['Sun'] = "วันอาทิตย์";
    $daytothai['Mon'] = "วันจันทร์";
    $daytothai['Tue'] = "วันอังคาร";
    $daytothai['Wed'] = "วันพุธ";
    $daytothai['Thu'] = "วันพฤหัสบดี";
    $daytothai['Fri'] = "วันศุกร์";
    $daytothai['Sat'] = "วันเสาร์";
	
    $MonthArr['01'] = 'ม.ค.';
    $MonthArr['02'] = 'ก.พ.';
    $MonthArr['03'] = 'มี.ค.';
    $MonthArr['04'] = 'เม.ย.';
    $MonthArr['05'] = 'พ.ค.';
    $MonthArr['06'] = 'มิ.ย.';
    $MonthArr['07'] = 'ก.ค.';
    $MonthArr['08'] = 'ส.ค.';
    $MonthArr['09'] = 'ก.ย.';
    $MonthArr['10'] = 'ต.ค.';
    $MonthArr['11'] = 'พ.ย.';
    $MonthArr['12'] = 'ธ.ค.';

	$OddsRate["0.25"] 	= "เสมอควบครึ่ง";
	$OddsRate["0.5"] 	= "ครึ่งลูก";
	$OddsRate["0.75"] 	= "ครึ่งควบลูก";
	$OddsRate["1"] 		= "หนึ่งลูก";
	$OddsRate["1.25"] 	= "ลูกควบลูกครึ่ง";
	$OddsRate["1.5"] 	= "ลูกครึ่ง";
	$OddsRate["1.75"] 	= "ลูกครึ่งควบสองลูก";
	$OddsRate["2"] 		= "สองลูก";
	$OddsRate["2.25"] 	= "สองลูกควบสองลูกครึ่ง";
	$OddsRate["2.5"] 	= "สองลูกครึ่ง";
	$OddsRate["2.75"] 	= "สองลูกครึ่งควบสามลูก";
	$OddsRate["3"] 		= "สามลูก";
	$OddsRate["3.25"] 	= "สามลูกควบสามลูกครึ่ง";
	$OddsRate["3.5"] 	= "สามลูกครึ่ง";
	$OddsRate["3.75"] 	= "สามลูกครึ่งควบสี่ลูก";
	$OddsRate["4"] 		= "สี่ลูก";
	$OddsRate["4.25"] 	= "สี่ลูกควบสี่ลูกครึ่ง";
	$OddsRate["4.5"] 	= "สี่ลูกครึ่ง";
	$OddsRate["4.75"] 	= "สี่ลูกครึ่งควบห้าลูก";
	$OddsRate["5"] 		= "ห้าลูก";
	$OddsRate["5.25"] 	= "ห้าลูกควบห้าลูกครึ่ง";
	$OddsRate["5.5"] 	= "ห้าลูกครึ่ง";
	$OddsRate["5.75"] 	= "ห้าลูกครึ่งควบหกลูก";
	$OddsRate["6"] 		= "หกลูก";
	$OddsRate["6.25"] 	= "หกลูกควบหกลูกครึ่ง";
	$OddsRate["6.5"] 	= "หกลูกครึ่ง";
	$OddsRate["6.75"] 	= "หกลูกครึ่งควบเจ็ดลูก";
	$OddsRate["7"] 		= "เจ็ดลูก";
	$OddsRate["7.25"] 	= "เจ็ดลูกควบเจ็ดลูกครึ่ง";
	$OddsRate["7.5"] 	= "เจ็ดลูกครึ่ง";
	$OddsRate["7.75"] 	= "เจ็ดลูกครึ่งควบแปดลูก";
	$OddsRate["8"] 		= "แปดลูก";
	$OddsRate["8.25"] 	= "แปดลูกควบแปดลูกครึ่ง";
	$OddsRate["8.5"] 	= "แปดลูกครึ่ง";
	$OddsRate["8.75"] 	= "แปดลูกครึ่งควบเก้าลูก";
	$OddsRate["9"] 		= "เก้าลูก";
	$OddsRate["9.25"] 	= "เก้าลูกควบเก้าลูกครึ่ง";
	$OddsRate["9.5"] 	= "เก้าลูกครึ่ง";
	$OddsRate["9.75"] 	= "เก้าลูกครึ่งควบสิบลูก";
	$OddsRate["10"] 	= "สิบลูก";
	
    $OddsRatePie["0.25"] 	= '( 0.25 )<pie class="twentyfive"></pie>';
	$OddsRatePie["0.5"] 	= '( 0.50 )<pie class="fifty"></pie>';
	$OddsRatePie["0.75"] 	= '( 0.75 )<pie class="seventyfive"></pie>';
	$OddsRatePie["1"] 		= '( 1.00 )<pie class="hundreds"></pie>';
	$OddsRatePie["1.25"] 	= '( 1.25 )<pie class="hundreds"></pie><pie class="twentyfive"></pie>';
	$OddsRatePie["1.5"] 	= '( 1.50 )<pie class="hundreds"></pie><pie class="fifty"></pie>';
	$OddsRatePie["1.75"] 	= '( 1.75 )<pie class="hundreds"></pie><pie class="seventyfive"></pie>';
	$OddsRatePie["2"] 		= '( 2.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie>';
	$OddsRatePie["2.25"] 	= '( 2.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>';
	$OddsRatePie["2.5"] 	= '( 2.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>';
	$OddsRatePie["2.75"] 	= '( 2.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>';
	$OddsRatePie["3"] 		= '( 3.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>';
	$OddsRatePie["3.25"] 	= '( 3.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>';
	$OddsRatePie["3.5"] 	= '( 3.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>';
	$OddsRatePie["3.75"] 	= '( 3.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>';
	$OddsRatePie["4"] 		= '( 4.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>';
	$OddsRatePie["4.25"] 	= '( 4.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>';
	$OddsRatePie["4.5"] 	= '( 4.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>';
	$OddsRatePie["4.75"] 	= '( 4.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>';
	$OddsRatePie["5"] 		= '( 5.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>';
	$OddsRatePie["5.25"] 	= '( 5.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>';
	$OddsRatePie["5.5"] 	= '( 5.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>';
	$OddsRatePie["5.75"] 	= '( 5.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>';
	$OddsRatePie["6"] 		= '( 6.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>';
	$OddsRatePie["6.25"] 	= '( 6.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>';
	$OddsRatePie["6.5"] 	= '( 6.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>';
	$OddsRatePie["6.75"] 	= '( 6.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>';
	$OddsRatePie["7"] 		= '( 7.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>';
	$OddsRatePie["7.25"] 	= '( 7.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>';
	$OddsRatePie["7.5"] 	= '( 7.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>';
	$OddsRatePie["7.75"] 	= '( 7.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>';
	$OddsRatePie["8"] 	    = '( 8.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>';
	$OddsRatePie["8.25"] 	= '( 8.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>';
	$OddsRatePie["8.5"] 	= '( 8.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>';
	$OddsRatePie["8.75"] 	= '( 8.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>';
	$OddsRatePie["9"] 		= '( 9.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>';
	$OddsRatePie["9.25"] 	= '( 9.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>';
	$OddsRatePie["9.5"] 	= '( 9.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>';
	$OddsRatePie["9.75"] 	= '( 9.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>';
	$OddsRatePie["10"] 		= '( 10.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>';
	
	
    $DateShow = intval(date("d")).' '.$MonthArr[date('m')].' '.intval(date('Y')+543);
	$cupname 		= 	$this->InfoCupContents['nameTH'];
	$cupshortname 	= 	$this->InfoCupContents['nameTHShort'];
	$cuppathname	=	$this->InfoCupContents['nameURL'];
	
	if(date('G')>=6){
		$todayDate		=	date('Y-m-d');
	}else{
		$todayDate		=	date("Y-m-d", strtotime("-1 day"));
	}
	
	$LiveStatusArray = array('Sched','1 HF','2 HF','H/T','E/T','Pen');
	
    include dirname(__FILE__) . "/header.tpl.php";
    ?>
	<style>
	.handicap_after:after {
		font-family: FontAwesome;
		content: " \f111";
		display: inline;
		color: #428bca;
	}
	.handicap_before:before {
		font-family: FontAwesome;
		content: "\f111  ";
		display: inline;
		color: #428bca;
	}
	</style>
	<!-- Live Ex Bar-->
	<script type="text/javascript">
		function getCookie(cname) {
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for(var i=0; i<ca.length; i++) {
				console.log(ca[i]);
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1);
				if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
			}
			return "";
		}
		function setCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays*24*60*60*1000));
			var expires = "expires="+d.toUTCString();
			document.cookie = cname + "=" + cvalue + "; " + expires;
		}
		var cname = getCookie("uid")+"view";
		console.log(cname);
		var exdays = 365;
		$(document).ready(function(){
			$("#show").click(function showpage(){
				$("#table1").hide();
				$("#page1").show();
				var cvalue = "show";
				setCookie(cname, cvalue, exdays);
			});
			
			$("#hide").click(function hidepage(){
				$("#table1").show();
				$("#page1").hide();
				var cvalue = "hide";
				setCookie(cname, cvalue, exdays);
			});
		});
	</script>
	<div class="container scoreboard-x" >
		<div class="pull-left">
			<div class="head"> <span id="spLabelDate"><?php if(isset($ProgramContents['Today'])) { ?>วันที่ <?php
				echo date("j", strtotime($ProgramContents['Today']));
				?> <?php
				echo $monthtothaiByNo[date("m", strtotime($ProgramContents['Today']))];
				?> <?php
				echo (date("Y", strtotime($ProgramContents['Today'])) + 543);
				} ?></span>
				<div class="view-mode">
					<div class="btn-group" data-toggle="buttons">
					<?php $cookies = $_COOKIE["uid"]."view"; if(($_COOKIE[$cookies] != "hide")){ ?>
						<label class="btn btn-default active" id="show"><input type="radio" name="options" id="option1" checked> <i class="fa fa-th"></i></label>
						<label class="btn btn-default" id="hide"><input type="radio" name="options" id="option2"> <i class="fa fa-th-list"></i></label>
					<?php }else{ ?>
						<label class="btn btn-default" id="show"><input type="radio" name="options" id="option1"> <i class="fa fa-th"></i></label>
						<label class="btn btn-default active" id="hide"><input type="radio" name="options" id="option2" checked> <i class="fa fa-th-list"></i></label>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<!-- Nav tabs -->    
		<ul class="nav nav-tabs" role="tablist">
			<li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo $cupshortname; ?></a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<div id="page1" class="active" <?php if(($_COOKIE[$cookies] != "hide")){ ?> style="" <?php }else{  ?> style="display: none;" <?php } ?>>
			<div id="carousel-live" class="top-league" style="padding: 10px;">
				<div class="row <?php if(count($tmpAllMatch) == 0) { echo 'no-match'; } ?>">
					<?php
					if(count($tmpAllMatch) == 0 ){ ?>
						<?php echo $cupshortname;?>  ไม่มีการแข่งขัน<?php } else {
						if(intval(date('G'))<6){
							$TodayDate	=	date('Y-m-d',strtotime("-1 days"));
						}else{
							$TodayDate	=	date('Y-m-d');
						}
						foreach($tmpAllMatch as $tmpMatch){
							//$MatchDate = $tmpMatch['MatchDate'].' '.$tmpMatch['MatchTime'].':00';
							$tmpMatch['MatchTime'] = date('H:i', strtotime($tmpMatch['MatchDateTime']));
							$MatchDate = $tmpMatch['MatchDateTime'];
							$this->LiveMatchListTime[$tmpMatch['id']]		=	$MatchDate;
							if($this->football_lib->isLiveMatch($tmpMatch['MatchStatus'])){
								$this->AllLiveMatchID.=','.$tmpMatch['id'];
							}
							include INCLUDE_VIEW_ROOTPATH . 'livescore/match_card_nonameleague.tpl.php';
						}
					}
					?>
				</div> 
			</div>
			</div>
				<table class="table  boder-1" id="table1" <?php if(($_COOKIE[$cookies] != "hide")){ ?> style="display: none;" <?php }else{  ?> style="" <?php } ?>>
					<tbody style="text-align: -webkit-auto;font-size: 1.1em;">
					<?php
						if(count($tmpAllMatch) == 0 ){ ?>
						<tr>
							<td class="no-match"><?php echo $cupshortname;?>  ไม่มีการแข่งขัน</td>
						</tr>
						<?php 
						}
						foreach($tmpAllMatch as $tmpMatch){

							$MatchDate		=	$tmpMatch['MatchDateTime'];
							if(date('G',strtotime($MatchDate))>=6){
								$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate));
							}else{
								$tmpDateMatch		=		date("Y-m-d", strtotime($MatchDate .' - 1 days'));
							}
							
							$tmpTimeMatch			=		date("H:i", strtotime($MatchDate));
							?>
							<tr>
								<td><?php echo $tmpTimeMatch; ?></td>
								<td class="text_right"><a href="<?php echo strtolower($tmpMatch['Team1URL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname <?php if($tmpMatch['TeamOdds']==1){ echo ' handicap_before'; } ?>"><?php echo $tmpMatch['Team1']; ?> <img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team1Logo']); ?>" width="24"></span></a></td>
								<td class="text_center"><?php include INCLUDE_VIEW_ROOTPATH . 'livescore/match_boxscore.tpl.php'; ?></td>
								<td class="text_left"><a href="<?php echo strtolower($tmpMatch['Team2URL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดทีมนี้"><span class="teamname <?php if($tmpMatch['TeamOdds']==2){ echo ' handicap_after'; } ?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpMatch['Team2Logo']); ?>" width="24"> <?php echo $tmpMatch['Team2']; ?></span></a></td>
								<td><?php if($tmpMatch['Odds']== 0){
										echo 'เสมอ';
									}else{
										echo $OddsRatePie[(string)$tmpMatch['Odds']];
								} ?></td>
								<td class="channel"> <span class="channel pull-right"><?php
									if(isset($tmpMatch['TVLiveList'])){
										foreach($tmpMatch['TVLiveList'] as $ChannelKey){
											?> <img style="height:16px;" src="<?php echo BASE_HREF; ?>uploads/tvlogo/<?php echo $ChannelKey; ?>.png" title="<?php echo $this->tv_lib->getTVText($ChannelKey); ?>"><?php
										} 
									}?>
								</span> </td>
							  
								<td> <?php
									if(($this->isCanPlayGame)&&(isset($tmpMatch['Odds']))&&($tmpMatch['Odds']!=-1)&&($tmpMatch['MatchStatus']=='Sched')&&($tmpDateMatch==$todayDate)){ ?>
										<div class="extra-text game<?php echo $tmpMatch['id']; ?>"> ทายผล | <a href="#game-single" role="tab" data-toggle="tab" onclick="AddPlaySideSingle(<?php echo $tmpMatch['id'];?>,'<?php echo $tmpMatch['Team1'];?>','<?php echo $tmpMatch['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team2Logo']);?>','<?php echo $tmpMatch['Odds'];?>',<?php echo $tmpMatch['TeamOdds'];?>)" class="link-play-game single label label-primary">ทายบอลเดี่ยว</a>
										<a href="#dropdown1" role="tab" data-toggle="tab" onclick="AddPlaySideMulti(1,<?php echo $tmpMatch['id'];?>,'<?php echo $tmpMatch['Team1'];?>','<?php echo $tmpMatch['Team2'];?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team1Logo']);?>','<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team2Logo']);?>','<?php echo $tmpMatch['Odds'];?>',<?php echo $tmpMatch['TeamOdds'];?>)" class="link-play-game single label label-success">ทายบอลชุด</a>
										</div>
									<?php } else if( ($tmpMatch['Odds']!=-1)&&(isset($tmpMatch['Odds']))&&($tmpMatch['MatchStatus']=='Sched')&&($tmpDateMatch==$todayDate) ){
										?>
										<div class="extra-text game<?php echo $tmpMatch['id']; ?>"><a href="<?php echo BASE_HREF; ?>games" class="link-play-game single label label-primary">เล่นเกมส์ทายผลบอล</a></div>
										<?php
									}
								?></td>
							</tr>
					<?php
						}
					?>
					</tbody>
				</table>
		</div>	
		</div>		
	</div>


  
<div class="row">
   <!-- Start LEFT COLUME --> 
  <div class="col-md-12">


	<div class="row ">
		<!--vdo world-->	 
			<?php include dirname(__FILE__) . "../../list_content_relate_full.tpl.php" ?>	
		<!--/vdo world-->
		<div id="carousel-featured" class="carousel slide col-md-8" data-ride="carousel" style="margin: 10px 0;">

		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<div class="item active">
				<a href="<?php echo $NewsCupArr[0]['link']; ?>" target="_blank"><img src="<?php echo $NewsCupArr[0]['picture']; ?>" alt="...">
					<div class="carousel-caption">
						<h2 ><?php echo $NewsCupArr[0]['subject']; ?></h2>
						<p><?php echo $NewsCupArr[0]['title']; ?></p>
					</div>
				</a>
			</div>
		</div>
    </div>
    
    
    <div class="col-md-4" data-ride="carousel" style="margin: 10px 0;">
		<div class="banner">
			<?php include dirname(__FILE__)."../../ads.php"; ?>
		</div>
	</div>    
    
    
    
<div class="news "> 
    <div class="col-md-12">
    <h2 class="font-display" style="margin-top: 0;"><i class="fa fa-file-text" ></i> ข่าว<?php echo $cupshortname;?></h2>
		<div class="row subnews">
			<?php for ($i = 1; $i <= 4; $i++) { ?>
				<div class="news_block  col-md-3">
					<a class="" href="<?php echo $NewsCupArr[$i]['link']; ?>" target="_blank">
						<img class="media-object" alt="" src="<?php echo $NewsCupArr[$i]['picture']; ?>"><?php echo $NewsCupArr[$i]['subject']; ?>
					</a>
				</div>
			<?php }
			
			if(strlen($this->InfoCupContents['nameURLRootRoute'])) { ?>
			<div class="col-md-12 line faa-parent animated-hover"> <a href="<?php echo $this->InfoCupContents['nameURLRootRoute']; ?>/news" class="more faa-vertical waves-effect waves-block" target="_blank"><i class="fa fa-plus "></i></a></div>
			<?php } else { ?>
			<div class="col-md-12 line faa-parent animated-hover"> <a href="/tournament/<?php echo $cuppathname; ?>/news" class="more faa-vertical waves-effect waves-block" target="_blank"><i class="fa fa-plus "></i></a></div>
			<?php } ?>
		</div>
	</div>  <!--End news -->
	
	<div class="col-md-9">
		
		<div class="news ">
			<?php if(count($this->TableContents)) { ?>
				<h1 class="font-display" style="margin-top: 0px;"><i class="fa fa-sort-amount-desc"></i>ตารางคะแนน</h1>
				<div class="col-md-12">
					<!--    ตารางคะแนนลีกส์ต่างๆ     -->
					<div class="table_standing">
					<?php
						foreach( $this->TableContents as $tmpTable ){ ?>
							<h2 class="font-display" style="margin-top: 0;"><?php echo $tmpTable['name'];?></h2><?php
							if(($tmpTable['type']==1)||($tmpTable['type']==2)){ ?>
								<div class="col-md-12">
									<table  class="table gray table-striped table-bordered ">
										<thead>
											<tr>
												<th>#</th>
												<th>ชื่อทีม</th>
												<th class="tooltip_top"  title="จำนวนนัดที่แข่ง">P</th>
												<th class="tooltip_top"  title="จำนวนนัดที่ชนะ">W</th>
												<th class="tooltip_top"  title="จำนวนนัดที่เสมอ">D</th>
												<th class="tooltip_top"  title="จำนวนนัดที่แพ้">L</th>
												<th class="tooltip_top"  title="จำนวนประตูที่ยิงได้">GF</th>
												<th class="tooltip_top"  title="จำนวนประตูที่เสีย">GA</th>
												<th class="tooltip_top"  title="จำนวนผลต่างประตู">GD</th>
												<th class="tooltip_top"  title="จำนวนคะแนน">PTS</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$i=0;
											foreach( $tmpTable['TeamData'] as $tmpTeam ){
												$tmpTeam['total_match']		=	($tmpTeam['win']+$tmpTeam['draw']+$tmpTeam['lose']);
												?>
												<tr>
													<td><?php echo $i+1 ?></td>
													<td><a href="<?php echo $tmpTeam['url']; ?>" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpTeam['logo']); ?>" width="24" height="24"  alt=""/> <?php echo $tmpTeam['name']; ?></a></td>
													<td><?php echo $tmpTeam['total_match']; ?></td>
													<td><?php echo $tmpTeam['win']; ?></td>
													<td><?php echo $tmpTeam['draw']; ?></td>
													<td><?php echo $tmpTeam['lose']; ?></td>
													<td><?php echo $tmpTeam['goal_for']; ?></td>
													<td><?php echo $tmpTeam['goal_against']; ?></td>
													<td><?php echo $tmpTeam['goal_dif']; ?></td>
													<td><?php echo $tmpTeam['point']; ?></td>
												</tr>
											<?php
												$i++;
											}
											?>
										</tbody>
									</table>
								</div> 
							<?php }elseif($tmpTable['type']==3){ ?>
								<div class="brackets_container">
									<table>
										<!--rounds container-->
										<thead><tr>
											<?php if(count($tmpTable['RoundData'])>=4){ ?>
											<th style="text-align: center;">
												<span>รอบ 16 ทีม</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=3){ ?>
											<th style="text-align: center;">
												<span>รอบ 8 ทีม</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=2){ ?>
											<th style="text-align: center;">
												<span>รอบรองชนะเลิศ</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=1){ ?>
											<th style="text-align: center;">
												<span>รอบชิงชนะเลิศ</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=2){ ?>
											<th style="text-align: center;">
												<span>รอบรองชนะเลิศ</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=3){ ?>
											<th style="text-align: center;">
												<span>รอบ 8 ทีม</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=4){ ?>
											<th style="text-align: center;">
												<span>รอบ 16 ทีม</span>
											</th>
											<?php } ?>
										</tr></thead>
										<!--matches container-->
										<?php
											$index_round 	= 	0;
											$tmpIdTeam		=	-2;
										?>
										<tbody><tr id="playground">
											<!-- Round of 16 -->
											<?php if(count($tmpTable['RoundData'])>=4){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_16",4,0,$tmpIdTeam);
												$index_round++;
											} ?>
											<!-- Round of 8 -->
											<?php if(count($tmpTable['RoundData'])>=3){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_8",2,0,$tmpIdTeam);
												$index_round++;
											} ?>
											<!-- Round of 4 -->
											<?php if(count($tmpTable['RoundData'])>=2){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_4",1,0,$tmpIdTeam);
												$index_round++;
											} ?>
											<!-- Final & 3rd -->
											<?php if(count($tmpTable['RoundData'])>=1){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												if(isset($tmpRound[1])){
													$tmpIdTeam = final_print($tmpIdTeam,$tmpRound[0],$tmpRound[1]);
												}else{
													$tmpIdTeam = final_print($tmpIdTeam,$tmpRound[0]);
												}
												$index_round--;
											} ?>
											<!-- Round of 4 -->
											<?php if(count($tmpTable['RoundData'])>=2){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_4 reversed",1,1,$tmpIdTeam);
												$index_round--;
											} ?>
											<!-- Round of 8 -->
											<?php if(count($tmpTable['RoundData'])>=3){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_8 reversed",2,2,$tmpIdTeam);
												$index_round--;
											} ?>
											<!-- Round of 16 -->
											<?php if(count($tmpTable['RoundData'])>=4){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_16 reversed",4,4,$tmpIdTeam);
												//$index_round--;
											} ?>
										</tr></tbody>
									</table>
								</div>
							<?php
							}
						}
						?>
					</div>
				</div>
			<?php } ?>
			
			<?php foreach($this->TableRoundContents as $TmpRound){ ?>
				<h1 class="font-display" style="margin-top: 0px;"><i class="fa fa-sort-amount-desc"></i>ตารางคะแนน <?php echo $TmpRound['name']; ?></h1>
				<div class="col-md-12">
					<!--    ตารางคะแนนลีกส์ต่างๆ     -->
					<div class="table_standing">
					<?php
						foreach( $TmpRound['table'] as $tmpTable ){ ?>
							<h2 class="font-display" style="margin-top: 0;"><?php echo $tmpTable['name'];?></h2><?php
							if(($tmpTable['type']==1)||($tmpTable['type']==2)){ ?>
								<div class="col-md-12">
									<table  class="table gray table-striped table-bordered ">
										<thead>
											<tr>
												<th>#</th>
												<th>ชื่อทีม</th>
												<th class="tooltip_top"  title="จำนวนนัดที่แข่ง">P</th>
												<th class="tooltip_top"  title="จำนวนนัดที่ชนะ">W</th>
												<th class="tooltip_top"  title="จำนวนนัดที่เสมอ">D</th>
												<th class="tooltip_top"  title="จำนวนนัดที่แพ้">L</th>
												<th class="tooltip_top"  title="จำนวนประตูที่ยิงได้">GF</th>
												<th class="tooltip_top"  title="จำนวนประตูที่เสีย">GA</th>
												<th class="tooltip_top"  title="จำนวนผลต่างประตู">GD</th>
												<th class="tooltip_top"  title="จำนวนคะแนน">PTS</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$i=0;
											foreach( $tmpTable['TeamData'] as $tmpTeam ){
												$tmpTeam['total_match']		=	($tmpTeam['win']+$tmpTeam['draw']+$tmpTeam['lose']);
												?>
												<tr>
													<td><?php echo $i+1 ?></td>
													<td><a href="<?php echo $tmpTeam['url']; ?>" class="tooltip_top" title="คลิกดูรายละเอียด"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpTeam['logo']); ?>" width="24" height="24"  alt=""/> <?php echo $tmpTeam['name']; ?></a></td>
													<td><?php echo $tmpTeam['total_match']; ?></td>
													<td><?php echo $tmpTeam['win']; ?></td>
													<td><?php echo $tmpTeam['draw']; ?></td>
													<td><?php echo $tmpTeam['lose']; ?></td>
													<td><?php echo $tmpTeam['goal_for']; ?></td>
													<td><?php echo $tmpTeam['goal_against']; ?></td>
													<td><?php echo $tmpTeam['goal_dif']; ?></td>
													<td><?php echo $tmpTeam['point']; ?></td>
												</tr>
											<?php
												$i++;
											}
											?>
										</tbody>
									</table>
								</div> 
							<?php }elseif($tmpTable['type']==3){ ?>
								<div class="brackets_container">
									<table>
										<!--rounds container-->
										<thead><tr>
											<?php if(count($tmpTable['RoundData'])>=4){ ?>
											<th style="text-align: center;">
												<span>รอบ 16 ทีม</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=3){ ?>
											<th style="text-align: center;">
												<span>รอบ 8 ทีม</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=2){ ?>
											<th style="text-align: center;">
												<span>รอบรองชนะเลิศ</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=1){ ?>
											<th style="text-align: center;">
												<span>รอบชิงชนะเลิศ</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=2){ ?>
											<th style="text-align: center;">
												<span>รอบรองชนะเลิศ</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=3){ ?>
											<th style="text-align: center;">
												<span>รอบ 8 ทีม</span>
											</th>
											<?php }
											if(count($tmpTable['RoundData'])>=4){ ?>
											<th style="text-align: center;">
												<span>รอบ 16 ทีม</span>
											</th>
											<?php } ?>
										</tr></thead>
										<!--matches container-->
										<?php
											$index_round 	= 	0;
											$tmpIdTeam		=	-2;
										?>
										<tbody><tr id="playground">
											<!-- Round of 16 -->
											<?php if(count($tmpTable['RoundData'])>=4){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_16",4,0,$tmpIdTeam);
												$index_round++;
											} ?>
											<!-- Round of 8 -->
											<?php if(count($tmpTable['RoundData'])>=3){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_8",2,0,$tmpIdTeam);
												$index_round++;
											} ?>
											<!-- Round of 4 -->
											<?php if(count($tmpTable['RoundData'])>=2){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_4",1,0,$tmpIdTeam);
												$index_round++;
											} ?>
											<!-- Final & 3rd -->
											<?php if(count($tmpTable['RoundData'])>=1){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												if(isset($tmpRound[1])){
													$tmpIdTeam = final_print($tmpIdTeam,$tmpRound[0],$tmpRound[1]);
												}else{
													$tmpIdTeam = final_print($tmpIdTeam,$tmpRound[0]);
												}
												$index_round--;
											} ?>
											<!-- Round of 4 -->
											<?php if(count($tmpTable['RoundData'])>=2){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_4 reversed",1,1,$tmpIdTeam);
												$index_round--;
											} ?>
											<!-- Round of 8 -->
											<?php if(count($tmpTable['RoundData'])>=3){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_8 reversed",2,2,$tmpIdTeam);
												$index_round--;
											} ?>
											<!-- Round of 16 -->
											<?php if(count($tmpTable['RoundData'])>=4){
												$tmpRound = $tmpTable['RoundData'][$index_round];
												$tmpIdTeam = round_print($tmpRound,"r_16 reversed",4,4,$tmpIdTeam);
												//$index_round--;
											} ?>
										</tr></tbody>
									</table>
								</div>
							<?php
							}
						}
						?>
					</div>
				</div>
			<?php } ?>
		</div>
		<?php if((count($this->TableContents))||(count($this->TableRoundContents))) {
			if(strlen($this->InfoCupContents['nameURLRootRoute'])) { ?>
			<div class="col-md-12 line faa-parent animated-hover" > <a href="<?php echo $this->InfoCupContents['nameURLRootRoute']; ?>/table" class="more faa-vertical" target="_blank"><i class="fa fa-plus "></i></a></div>
			<?php }else{ ?>
			<div class="col-md-12 line faa-parent animated-hover" > <a href="/tournament/<?php echo $cuppathname; ?>/table" class="more faa-vertical" target="_blank"><i class="fa fa-plus "></i></a></div>
			<?php }
		} ?>
	</div>
	<div class="col-md-3">   
	<!--    ตารางดาวซัลโว     -->
		<div class="top_scorer">
			<h2 class="font-display" style="margin-top: 0;"><i class="fa fa-star" ></i>  ดาวซัลโว</h2>
			<table  class="table gray table-striped ">
				<thead>
					<tr>
						<th>#</th>
						<th>ชื่อผู้เล่น</th>
						<th>G</th>
					</tr>
				</thead>
				<tbody>
				<?php
					if($this->ScorerContents != null){
						$i = 1;
						foreach ($this->ScorerContents as $tmpData) {
				?>
							<tr>
								<td><?php echo $i; ?></td>
								<td>
									<a href="<?php echo $tmpData['TeamURL']; ?>" class="tooltip_top" title="คลิกดูรายละเอียด" target="_blank">
										<img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpData['TeamLogo']); ?>" width="24" height="24"  alt=""/>
									</a>
									<?php echo $tmpData['PlayerName']; ?>
								</td>
								<td><?php echo $tmpData['goal']; ?></td>
							</tr>
						<?php
							$i++;
						}
					}else{
						?>
							<tr>
								<td colspan="3">กำลังรอข้อมูล</td>
							</tr>
						<?php
					}?>
				</tbody>
			</table>
			<?php if(strlen($this->InfoCupContents['nameURLRootRoute'])) { ?>
			<div class="col-md-12 line faa-parent animated-hover" > <a href="<?php echo $this->InfoCupContents['nameURLRootRoute']; ?>/topscorer" class="more faa-vertical" target="_blank"><i class="fa fa-plus "></i></a></div>
			<?php }else{ ?>
			<div class="col-md-12 line faa-parent animated-hover" > <a href="/tournament/<?php echo $cuppathname; ?>/topscorer" class="more faa-vertical" target="_blank"><i class="fa fa-plus "></i></a></div>
			<?php } ?>
		</div> 
	</div>
</div>
</div>
<!--End Col 8 LEFT VOLUME-->
</div>
</div></div>
