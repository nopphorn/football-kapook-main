
<?php

	$monthtothai['01']['short'] ="ม.ค.";
	$monthtothai['02']['short'] ="ก.พ.";
	$monthtothai['03']['short'] ="มี.ค.";
	$monthtothai['04']['short'] ="เม.ย.";
	$monthtothai['05']['short'] ="พ.ค.";
	$monthtothai['06']['short'] ="มิ.ย.";
	$monthtothai['07']['short'] ="ก.ค.";
	$monthtothai['08']['short'] ="ส.ค.";
	$monthtothai['09']['short'] ="ก.ย.";
	$monthtothai['10']['short'] ="ต.ค.";
	$monthtothai['11']['short'] ="พ.ย.";
	$monthtothai['12']['short'] ="ธ.ค.";
	
	$OddsRate["0"] 		= "เสมอ";
	$OddsRate["0.25"] 	= "เสมอควบครึ่ง";
	$OddsRate["0.5"] 	= "ครึ่งลูก";
	$OddsRate["0.75"] 	= "ครึ่งควบลูก";
	$OddsRate["1"] 		= "หนึ่งลูก";
	$OddsRate["1.25"] 	= "ลูกควบลูกครึ่ง";
	$OddsRate["1.5"] 	= "ลูกครึ่ง";
	$OddsRate["1.75"] 	= "ลูกครึ่งควบสองลูก";
	$OddsRate["2"] 		= "สองลูก";
	$OddsRate["2.25"] 	= "สองลูกควบสองลูกครึ่ง";
	$OddsRate["2.5"] 	= "สองลูกครึ่ง";
	$OddsRate["2.75"] 	= "สองลูกครึ่งควบสามลูก";
	$OddsRate["3"] 		= "สามลูก";
	$OddsRate["3.25"] 	= "สามลูกควบสามลูกครึ่ง";
	$OddsRate["3.5"] 	= "สามลูกครึ่ง";
	$OddsRate["3.75"] 	= "สามลูกครึ่งควบสี่ลูก";
	$OddsRate["4"] 		= "สี่ลูก";
	$OddsRate["4.25"] 	= "สี่ลูกควบสี่ลูกครึ่ง";
	$OddsRate["4.5"] 	= "สี่ลูกครึ่ง";
	$OddsRate["4.75"] 	= "สี่ลูกครึ่งควบห้าลูก";
	$OddsRate["5"] 		= "ห้าลูก";
	$OddsRate["5.25"] 	= "ห้าลูกควบห้าลูกครึ่ง";
	$OddsRate["5.5"] 	= "ห้าลูกครึ่ง";
	$OddsRate["5.75"] 	= "ห้าลูกครึ่งควบหกลูก";
	$OddsRate["6"] 		= "หกลูก";
	$OddsRate["6.25"] 	= "หกลูกควบหกลูกครึ่ง";
	$OddsRate["6.5"] 	= "หกลูกครึ่ง";
	$OddsRate["6.75"] 	= "หกลูกครึ่งควบเจ็ดลูก";
	$OddsRate["7"] 		= "เจ็ดลูก";
	$OddsRate["7.25"] 	= "เจ็ดลูกควบเจ็ดลูกครึ่ง";
	$OddsRate["7.5"] 	= "เจ็ดลูกครึ่ง";
	$OddsRate["7.75"] 	= "เจ็ดลูกครึ่งควบแปดลูก";
	$OddsRate["8"] 		= "แปดลูก";
	$OddsRate["8.25"] 	= "แปดลูกควบแปดลูกครึ่ง";
	$OddsRate["8.5"] 	= "แปดลูกครึ่ง";
	$OddsRate["8.75"] 	= "แปดลูกครึ่งควบเก้าลูก";
	$OddsRate["9"] 		= "เก้าลูก";
	$OddsRate["9.25"] 	= "เก้าลูกควบเก้าลูกครึ่ง";
	$OddsRate["9.5"] 	= "เก้าลูกครึ่ง";
	$OddsRate["9.75"] 	= "เก้าลูกครึ่งควบสิบลูก";
	$OddsRate["10"] 	= "สิบลูก";

	$MatchArr = $this->MatchAnalyzeContensByDate;
	$MatchLiveArr = $this->MatchLiveContents;
	$NewsArr = $this->NewsContents;
	$NewsThaiArr =	$this->NewsThaiListContents;
	$NewsTop10Arr = $this->NewsTop10Contents;
	$NewsTopHitsArr = $this->NewsTopHitsContents;

	$ClassColorByMatchStatus['Fin'] = 'btn-grey';
    $ClassColorByMatchStatus['1 HF'] = 'btn-success';
    $ClassColorByMatchStatus['2 HF'] = 'btn-success';
    $ClassColorByMatchStatus['Sched'] = '';
    $ClassColorByMatchStatus['H/T'] = 'btn-warning';
?>

  <div class="container">
  
  
<div class="row">
	
   <!-- Start LEFT COLUME --> 
  <div class="col-md-8">
  <h2 class="font-display">ข่าว</h2>
  <div id="carousel-featured" class="carousel slide" data-ride="carousel" >
    
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-featured" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-featured" data-slide-to="1"></li>
        <li data-target="#carousel-featured" data-slide-to="2"></li>
		<li data-target="#carousel-featured" data-slide-to="3"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <?php 
        for($i=0;$i<=3;$i++){ 
            $News = $NewsArr[$i];
        ?>  
            <div class="item <?php if($i==0){echo 'active'; }?>">
                <a href="<?php echo $News['link']; ?>" target="_blank"><img src="<?php echo $News['picture']; ?>" alt="<?php echo $News['subject']; ?>">
              <div class="carousel-caption">
                    <h2 ><?php echo $News['subject']; ?></h2>
                    <p><?php echo $News['title']; ?></p>
              </div>
              </a>
            </div>
        <?php } ?>  

      </div>
	        <a class="right carousel-control" href="#carousel-featured" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a> 

    </div>
  
  <div class="row subnews">
        <?php for($i=4;$i<count($NewsArr);$i++){ $News = $NewsArr[$i]; ?>
            <div class="news_block  col-md-3" ><a class="post.php" href="<?php echo $News['link']; ?>" target="_blank"><img class="media-object" alt="<?php echo $News['subject']; ?>" src="<?php echo $News['picture']; ?>" ><?php echo $News['subject']; ?> </a></div>
        <?php } ?>    

 	    <div class="col-md-12 line faa-parent animated-hover" > <a href="/newslist.php"  class="more faa-vertical waves-effect waves-block"><i class="fa fa-plus "></i></a></div>
  </div>
  
    <h2 class="font-display"><img src="assets/img/flags/24/Thailand.png" border="0" width="24" height="24" alt="Football Thai News"> ข่าวบอลไทย</h2>
    <div class="row subnews">
		<div class="news_block  col-md-5" ><a class="post.php" href="<?php echo $NewsThaiArr[0]['link']; ?>"><img class="media-object" alt="<?php echo $NewsThaiArr[0]['subject']; ?>" src="<?php echo $NewsThaiArr[0]['picture']; ?>" ><?php echo $NewsThaiArr[0]['subject']; ?> </a></div>
		<div class="col-md-7">
			<div class="news_block  col-md-6" ><a class="" href="<?php echo $NewsThaiArr[1]['link']; ?>"><img class="media-object" alt="<?php echo $NewsThaiArr[1]['subject']; ?>" src="<?php echo $NewsThaiArr[1]['picture']; ?>" ><?php echo $NewsThaiArr[1]['subject']; ?> </a></div>
			<div class="news_block  col-md-6" ><a class="" href="<?php echo $NewsThaiArr[2]['link']; ?>"><img class="media-object" alt="<?php echo $NewsThaiArr[2]['subject']; ?>" src="<?php echo $NewsThaiArr[2]['picture']; ?>" ><?php echo $NewsThaiArr[2]['subject']; ?> </a></div>
		</div>
		<div class="col-md-12 line faa-parent animated-hover" > <a href="http://football.kapook.com/ไทยพรีเมียร์ลีก/news" class="more faa-vertical waves-effect waves-block"><i class="fa fa-plus "></i></a></div>
	</div>
  
  
  	<!--TOP 10-->
    <div class="row top10">
    		<div class=" col-md-12" ><h2 class="font-display">บทความฟุตบอล</h2></div>
  			<div class="news_block  col-md-4" ><a class="" href="/news-<?php echo $NewsTop10Arr[0]['content_id']; ?>" target="_blank"><img class="media-object" alt="" src="<?php echo $NewsTop10Arr[0]['picture']; ?>" ><?php echo $NewsTop10Arr[0]['subject']; ?></a></div>
            <div class="news_block  col-md-4" ><a class="" href="/news-<?php echo $NewsTop10Arr[1]['content_id']; ?>" target="_blank"><img class="media-object" alt="" src="<?php echo $NewsTop10Arr[1]['picture']; ?>" ><?php echo $NewsTop10Arr[1]['subject']; ?></a></div>
            <div class="news_block  col-md-4" ><a class="" href="/news-<?php echo $NewsTop10Arr[2]['content_id']; ?>" target="_blank"><img class="media-object" alt="" src="<?php echo $NewsTop10Arr[2]['picture']; ?>" ><?php echo $NewsTop10Arr[2]['subject']; ?></a></div>
 			<div class="col-md-12 line faa-parent animated-hover" > <a href="/feature" class="more faa-vertical" target="_blank"><i class="fa fa-plus "></i></a></div>
  </div>
  
  <!--วิเคราะห์-->
    <div class="row analytor">
    <br>
    <div class=" col-md-12" ><h2 class="font-display"><i class="fa fa-tachometer"></i> วิเคราะห์บอล &amp; ทีเด็ดฟุตบอล</h2> </div>
    <!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
	<?php
		
		if(date('G') >= 6){
			$date = strtotime(date("Y-m-d"));
			$Yesterday = strtotime(date("Y-m-d",strtotime(' - 1 days')));
		}else{
			$date = strtotime(date("Y-m-d",strtotime(' - 1 days')));
			$Yesterday = strtotime(date("Y-m-d",strtotime(' - 2 days')));
		}
		
		if( ((date('G') >= 6) && (date('G') < 12)) || (!isset($MatchArr["Today"])) ){
			$today_class = '';
			$yesterday_class = ' class="active"';
			$today_tap = 'class="tab-pane"';
			$yesterday_tap = 'class="tab-pane active"';
		}else{
			$today_class = ' class="active"';
			$today_tap = 'class="tab-pane active"';
			$yesterday_tap = 'class="tab-pane"';
			$yesterday_class = '';
		}
	?>
  <li<?php echo $today_class; ?>><a href="#home" role="tab" data-toggle="tab">วิเคราะห์บอลวันนี้ <?php echo date("d",$date)." ".$monthtothai[date("m",$date)]['short']." ".(date("Y",$date)+543); ?></a></li>
  <li<?php echo $yesterday_class; ?>><a href="#profile" role="tab" data-toggle="tab">สรุปผลทีเด็ดเมื่อวานนี้ <?php echo date("d",$Yesterday)." ".$monthtothai[date("m",$Yesterday)]['short']." ".(date("Y",$Yesterday)+543); ?></a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div <?php echo $today_tap; ?> id="home"><table class="table table-striped border-1">
            <thead>
        <tr>
        
        
      <th> เวลา</th><th></th><th></th><th></th><th>ราคาบอล</th><th> ทีเด็ด</th><th>ผล</th>
        </tr>
      </thead>
      <tbody>
	  <?php 
		foreach($MatchArr["Today"] as $MatchTmp){

		?>
        <tr>
          
          <td><?php echo $MatchTmp['MatchTime']; ?></td>
		  <td class="text_right"><a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team1KPID']))); ?>" class="tooltip_top match <?php if(($MatchTmp['TeamOdds']==0 || $MatchTmp['TeamOdds']==1)&&($MatchTmp['Odds']!=-1)){ echo 'handicap_color'; } ?>" title="" data-original-title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><?php echo $MatchTmp['Team1'];?> <img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchTmp['Team1Logo']);?>" width="24"></span></a></td>
          <td class="text_center"><?php if($MatchTmp['MatchStatus']=='Sched'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">vs</a>
              <?php }else if($MatchTmp['MatchStatus']=='Fin'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="btn btn-grey btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php echo $MatchTmp['Team1FTScore']; ?> - <?php echo $MatchTmp['Team2FTScore']; ?></a>
              <?php }else if($MatchTmp['MatchStatus']=='Canc'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">c - c</a>
              <?php }else if($MatchTmp['MatchStatus']=='Post'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">p - p</a>
              <?php }else{?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">vs</a>
              <?php }?></td>
          <td class="text_left"><a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team2KPID']))); ?>" class="tooltip_top match <?php if(($MatchTmp['TeamOdds']==0 || $MatchTmp['TeamOdds']==2)&&($MatchTmp['Odds']!=-1)){ echo 'handicap_color'; } ?>" title="" data-original-title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchTmp['Team2Logo']);?>" width="24"><?php echo $MatchTmp['Team2'];?></span></a></td>
          <td><?php 
               if($MatchTmp['Odds']==0){   
                   echo '(เสมอ)';                   
               }else if($MatchTmp['Odds']!=-1){
                   $OddsArr = explode('.',$MatchTmp['Odds']);
                   if($OddsArr[0]>0){
                       for($o=1;$o<=$OddsArr[0];$o++){
                           echo '<pie class="hundreds"></pie>';
                       }
                   }
                   
                   if($OddsArr[1]=='25'){
                       echo '<pie class="twentyfive"></pie>';
                   }else if($OddsArr[1]=='5'){
                       echo '<pie class="fifty"></pie>';
                   }else if($OddsArr[1]=='75'){
                       echo '<pie class="seventyfive"></pie>';
                   }
                   
                   echo ' ('.$MatchTmp['Odds'].')';
               }?>
          </td>
          <td><?php echo $MatchTmp['Predict']; ?></td>
          <td>
		<?php
			if(isset($MatchTmp['IsSelectCollect']))
			{
				if($MatchTmp['IsSelectCollect']==1)
				{
					echo '<i class="fa fa-check" style="color:green"></i>';
				}
				else if($MatchTmp['IsSelectCollect']==-1)
				{
					echo '<i class="fa fa-close" style="color:red"></i>';
				}
				else
				{
					echo '<i class="fa fa-minus" style="color:gray"></i>';
				}
			}
			else
			{
				echo '<i class="fa fa-minus" style="color:gray"></i>';
			}
		?>
		  </td>
        </tr>
		<?php } ?>
      </tbody>
    </table></div>
	  <div <?php echo $yesterday_tap; ?> id="profile"><table class="table table-striped border-1">
      <thead>
        <tr>
        
        
      <th> เวลา</th><th></th><th></th><th></th><th>ราคาบอล</th><th> ทีเด็ด</th><th>ผล</th>
        </tr>
      </thead>
      <tbody>
	  <?php 
		foreach($MatchArr["Yesterday"] as $MatchTmp){

		?>
        <tr>
          
          <td><?php echo $MatchTmp['MatchTime']; ?></td>
		  <td class="text_right"><a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team1KPID']))); ?>" class="tooltip_top match <?php if(($MatchTmp['TeamOdds']==0 || $MatchTmp['TeamOdds']==1)&&($MatchTmp['Odds']!=-1)){ echo 'handicap_color'; } ?>" title="" data-original-title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><?php echo $MatchTmp['Team1'];?> <img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchTmp['Team1Logo']);?>" width="24"></span></a></td>
		  <td class="text_center"><?php if($MatchTmp['MatchStatus']=='Sched'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">vs</a>
              <?php }else if($MatchTmp['MatchStatus']=='Fin'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="btn btn-grey btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php echo $MatchTmp['Team1FTScore']; ?> - <?php echo $MatchTmp['Team2FTScore']; ?></a>
              <?php }else if($MatchTmp['MatchStatus']=='Canc'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">c - c</a>
              <?php }else if($MatchTmp['MatchStatus']=='Post'){?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">p - p</a>
              <?php }else{?>
                    <a href="<?php echo strtolower($MatchTmp['MatchPageURL']); ?>" target="_blank"   class="tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">vs</a>
              <?php }?></td>
          <td class="text_left"><a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$MatchTmp['Team2KPID']))); ?>" class="tooltip_top match <?php if(($MatchTmp['TeamOdds']==0 || $MatchTmp['TeamOdds']==2)&&($MatchTmp['Odds']!=-1)){ echo 'handicap_color'; } ?>" title="" data-original-title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $MatchTmp['Team2Logo']);?>" width="24"><?php echo $MatchTmp['Team2'];?></span></a></td>
          <td><?php 
               if($MatchTmp['Odds']==0){   
                   echo '(เสมอ)';                   
               }else if($MatchTmp['Odds']!=-1){
                   $OddsArr = explode('.',$MatchTmp['Odds']);
                   if($OddsArr[0]>0){
                       for($o=1;$o<=$OddsArr[0];$o++){
                           echo '<pie class="hundreds"></pie>';
                       }
                   }
                   
                   if($OddsArr[1]=='25'){
                       echo '<pie class="twentyfive"></pie>';
                   }else if($OddsArr[1]=='5'){
                       echo '<pie class="fifty"></pie>';
                   }else if($OddsArr[1]=='75'){
                       echo '<pie class="seventyfive"></pie>';
                   }
                   
                   echo ' ('.$MatchTmp['Odds'].')';
               }?>
          </td>
          <td><?php echo $MatchTmp['Predict']; ?></td>
          <td>
		<?php
			if(isset($MatchTmp['IsSelectCollect']))
			{
				if($MatchTmp['IsSelectCollect']==1)
				{
					echo '<i class="fa fa-check" style="color:green"></i>';
				}
				else if($MatchTmp['IsSelectCollect']==-1)
				{
					echo '<i class="fa fa-close" style="color:red"></i>';
				}
				else
				{
					echo '<i class="fa fa-minus" style="color:gray"></i>';
				}
			}
			else
			{
				echo '<i class="fa fa-minus" style="color:gray"></i>';
			}
		?>
		  </td>
        </tr>
		<?php } ?>
      </tbody>
    </table></div>
</div>
    <div class="col-md-12 line faa-parent animated-hover" > <a href="/วิเคราะห์บอล" class="more faa-vertical" target="_blank"><i class="fa fa-plus "></i></a></div>
</div>
  

  
</div>
<!--End Col 8 LEFT VOLUME-->









<!--Start Right Side Colume-->


<!--ข่าวฮิตประจำสัปดาห์ -->
  <div class="col-md-4 aside">
  <?php include dirname(__FILE__)."../../sidebar.tpl.php"; ?>
  </div><!--End right 4 Aside -->

</div>




