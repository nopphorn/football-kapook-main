<?php

class Football_lib {
	
	var $OddsRate	=	array(
		"0.25" 	=> 	"เสมอควบครึ่ง",
		"0.5" 	=> 	"ครึ่งลูก",
		"0.75" 	=> 	"ครึ่งควบลูก",
		"1" 	=> 	"หนึ่งลูก",
		"1.25" 	=> 	"ลูกควบลูกครึ่ง",
		"1.5" 	=> 	"ลูกครึ่ง",
		"1.75" 	=> 	"ลูกครึ่งควบสองลูก",
		"2" 	=> 	"สองลูก",
		"2.25" 	=> 	"สองลูกควบสองลูกครึ่ง",
		"2.5" 	=> 	"สองลูกครึ่ง",
		"2.75" 	=> 	"สองลูกครึ่งควบสามลูก",
		"3" 	=> 	"สามลูก",
		"3.25" 	=> 	"สามลูกควบสามลูกครึ่ง",
		"3.5" 	=> 	"สามลูกครึ่ง",
		"3.75" 	=> 	"สามลูกครึ่งควบสี่ลูก",
		"4" 	=> 	"สี่ลูก",
		"4.25" 	=> 	"สี่ลูกควบสี่ลูกครึ่ง",
		"4.5" 	=> 	"สี่ลูกครึ่ง",
		"4.75" 	=> 	"สี่ลูกครึ่งควบห้าลูก",
		"5" 	=> 	"ห้าลูก",
		"5.25" 	=> 	"ห้าลูกควบห้าลูกครึ่ง",
		"5.5" 	=> 	"ห้าลูกครึ่ง",
		"5.75" 	=> 	"ห้าลูกครึ่งควบหกลูก",
		"6" 	=> 	"หกลูก",
		"6.25" 	=> 	"หกลูกควบหกลูกครึ่ง",
		"6.5" 	=> 	"หกลูกครึ่ง",
		"6.75" 	=> 	"หกลูกครึ่งควบเจ็ดลูก",
		"7" 	=> 	"เจ็ดลูก",
		"7.25" 	=> 	"เจ็ดลูกควบเจ็ดลูกครึ่ง",
		"7.5" 	=> 	"เจ็ดลูกครึ่ง",
		"7.75" 	=> 	"เจ็ดลูกครึ่งควบแปดลูก",
		"8" 	=> 	"แปดลูก",
		"8.25" 	=> 	"แปดลูกควบแปดลูกครึ่ง",
		"8.5" 	=> 	"แปดลูกครึ่ง",
		"8.75" 	=> 	"แปดลูกครึ่งควบเก้าลูก",
		"9" 	=> 	"เก้าลูก",
		"9.25" 	=> 	"เก้าลูกควบเก้าลูกครึ่ง",
		"9.5" 	=> 	"เก้าลูกครึ่ง",
		"9.75" 	=> 	"เก้าลูกครึ่งควบสิบลูก",
		"10" 	=> 	"สิบลูก"
	);
	
	var $OddsRatePie = array(
		"0.25"	=> 	'( 0.25 )<pie class="twentyfive"></pie>',
		"0.5"	=> 	'( 0.50 )<pie class="fifty"></pie>',
		"0.75"	=> 	'( 0.75 )<pie class="seventyfive"></pie>',
		"1"		=> 	'( 1.00 )<pie class="hundreds"></pie>',
		"1.25"	=> 	'( 1.25 )<pie class="hundreds"></pie><pie class="twentyfive"></pie>',
		"1.5"	=> 	'( 1.50 )<pie class="hundreds"></pie><pie class="fifty"></pie>',
		"1.75"	=> 	'( 1.75 )<pie class="hundreds"></pie><pie class="seventyfive"></pie>',
		"2"		=> 	'( 2.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie>',
		"2.25"	=> 	'( 2.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>',
		"2.5"	=> 	'( 2.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>',
		"2.75"	=> 	'( 2.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>',
		"3"		=> 	'( 3.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>',
		"3.25"	=> 	'( 3.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>',
		"3.5"	=> 	'( 3.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>',
		"3.75"	=> 	'( 3.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>',
		"4"		=> 	'( 4.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>',
		"4.25"	=> 	'( 4.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>',
		"4.5"	=> 	'( 4.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>',
		"4.75"	=> 	'( 4.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>',
		"5"		=> 	'( 5.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>',
		"5.25"	=> 	'( 5.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>',
		"5.5"	=> 	'( 5.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>',
		"5.75"	=> 	'( 5.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>',
		"6"		=>	'( 6.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>',
		"6.25"	=> 	'( 6.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>',
		"6.5"	=> 	'( 6.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>',
		"6.75"	=> 	'( 6.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>',
		"7"		=> 	'( 7.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>',
		"7.25"	=> 	'( 7.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>',
		"7.5"	=> 	'( 7.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>',
		"7.75"	=> 	'( 7.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>',
		"8"		=> 	'( 8.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>',
		"8.25"	=> 	'( 8.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>',
		"8.5"	=> 	'( 8.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>',
		"8.75"	=> 	'( 8.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>',
		"9"		=> 	'( 9.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>',
		"9.25"	=> 	'( 9.25 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="twentyfive"></pie>',
		"9.5"	=> 	'( 9.50 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="fifty"></pie>',
		"9.75"	=> 	'( 9.75 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="seventyfive"></pie>',
		"10"	=> 	'( 10.00 )<pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie><pie class="hundreds"></pie>'
	);

	var $LiveStatusArray 			= 	array('Sched','1 HF','2 HF','H/T','E/T','Pen');
	var $ClassNameByStatusArray 	= 	array('Fin'=>'fin','Sched'=>'','1 HF'=>'success','2 HF'=>'success','E/T'=>'success','H/T'=>'success','Pen'=>'success');
	
	var $ClassColorByMatchStatus	=	array(
		'Fin'	=>	'btn-grey',
		'1 HF'	=>	'btn-success',
		'2 HF'	=>	'btn-success',
		'Sched'	=>	'',
		'H/T'	=>	'btn-warning',
		'Fin'	=>	'btn-grey',
		'Canc'	=>	'btn-danger',
		'Post'	=>	'btn-danger',
		'Abd'	=>	'btn-danger'
	);

	function getOddsRate($rate_str = '') {
		if(array_key_exists($rate_str, $this->OddsRate)){
			return $this->OddsRate[$rate_str];
		}else{
			return '';
		}
    }
	
	function getOddsRatePie($rate_str = '') {
		if(array_key_exists($rate_str, $this->OddsRatePie)){
			return $this->OddsRatePie[$rate_str];
		}else{
			return '';
		}
    }
	
	function isLiveMatch($MatchStatus = ''){
		if(in_array($MatchStatus,$this->LiveStatusArray)){
			return true;
		}else{
			return false;
		}
	}
	
	function getClassNameByStatusArray($MatchStatus = ''){
		if(array_key_exists($MatchStatus, $this->ClassNameByStatusArray)){
			return $this->ClassNameByStatusArray[$MatchStatus];
		}else{
			return '';
		}
	}
	
	function getClassColorByMatchStatus($MatchStatus = ''){
		if(array_key_exists($MatchStatus, $this->ClassColorByMatchStatus)){
			return $this->ClassColorByMatchStatus[$MatchStatus];
		}else{
			return '';
		}
	}

}