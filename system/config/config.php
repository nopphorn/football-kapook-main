<?php if (!defined('MINIZONE')) exit;

$config['path'] = '/';
$config['domain'] = 'kapook.com';
$config['cachetime'] = '1';
$config['debug'] = 0;
$config['theme'] = '';
$config['portal_id'] = 29;

/* -- Images -- */
$config['imgTypes'] = array('image/gif', 'image/jpg', 'image/pjpeg', 'image/jpeg');

?>