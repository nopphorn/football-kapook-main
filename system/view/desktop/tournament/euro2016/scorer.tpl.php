<div class="container league euro2016">
<?php  

$cupname 		= 	$this->InfoCupContents['nameTH'];
$cupshortname 	= 	$this->InfoCupContents['nameTHShort'];
$cuppathname	=	$this->InfoCupContents['nameURL'];
	
include dirname(__FILE__) . "/menubar.tpl.php";
 ?>

  
<div class="row">
  <div class="col-md-12">

  <div class="top-scorer">
    <h2 class="font-display" ><i class="fa fa-star" ></i>  ดาวซัลโว<?php echo $cupname; ?> </h2>
    <table  class="table  table-striped border-1 ">
      <thead>
        <tr>
            <th class="text_center">#</th>
            <th>ชื่อผู้เล่น</th>
			<th></th>
			<th class="text_center">ประตู</th>
      
        </tr>
      </thead>
      <tbody>
      
        <?php
			if(count($this->ScorerContents)>0){
				$i = 1;
				foreach ($this->ScorerContents as $tmpData) { ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><a name=""><img src="<?php echo $tmpData['PlayerImg']; ?>" class="img-circle" title="<?php echo $tmpData['PlayerName']; ?>"></a></td>
						<td><a name="" class="tooltip_top" title="คลิกดูรายละเอียด" target="_blank"><h2 class="font-display"><?php echo $tmpData['PlayerName']; ?></h2></a>
							<a href="<?php echo $tmpData['TeamURL']; ?>" class="tooltip_top" title="คลิกดูรายละเอียด" target="_blank"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $tmpData['TeamLogo']); ?>" width="24" height="24"  alt=""/><?php echo $tmpData['TeamName']; ?></a>
						</td>
						<td><?php echo $tmpData['goal']; ?></td>
					</tr>


				<?php
					$i++;
				}
			}else{
				?>
					<tr>
						<td colspan="4" >กำลังรอข้อมูล</td>
					</tr>
				<?php
			}
           ?>
        
      </tbody>
    </table>
    </div> 
     <br><br>
</div>
</div>



