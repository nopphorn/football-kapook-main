<?php

class template
{

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct()
    {
        return $this;
    }

    ///////////////////////////////////////////////// Path /////////////////////////////////////////////////
    function path($path = null)
    {
        $this->dir = (is_null($path) ? MODULES . SUB . '/' . (defined('MY_MODULE') ? MY_MODULE : '') : $path) . '/templates/';
//        $this->dir = 'system/view/';
        
        if (!is_null($path)) $this->dir = $path;
    }

    ///////////////////////////////////////////////// Assign /////////////////////////////////////////////////
    function assign($spec)
    {
        if (is_string($spec)) {
            $this->$spec = @func_get_arg(1);
        }
        elseif (is_array($spec)) {
            foreach ($spec as $key => $val) {
                $this->$key = $val;
            }
        }
    }

    ///////////////////////////////////////////////// Assign : By Ref /////////////////////////////////////////////////
    function assign_by_ref($key, &$value)
    {
        $this->$key = &$value;
    }

    ///////////////////////////////////////////////// Render /////////////////////////////////////////////////
    function render($file) 
    {
        echo $this->fetch($file);
    }

    ///////////////////////////////////////////////// Fetch /////////////////////////////////////////////////
    function fetch($file) 
    {
        ob_start();
        include ($this->dir.$file.'.php');
        
        return ob_get_clean();
    }

    ///////////////////////////////////////////////// Redirect /////////////////////////////////////////////////
    function redirect($uri, $header = true) 
    {
        if ($_COOKIE['dev'] == 1) {
            echo '<br><i>Dev mode > </i><a href="' . $uri . '"><i>Click to redirect</i></a>';
            exit;
        }
//        header('Location: '.$uri);
        if ($header === true) {
            header('Connection: Keep-Alive');
//            header('Connection: Close');
            header('Location: ' . $uri);
        } else {
            echo '<meta http-equiv="refresh" content="' . (int) $header . ';URL=' . $uri . '">';
        }
        
        exit;
    }
    
    function render_json($_data) {
        header('Content-type: application/json');
        if ($_GET['jsoncallback'] != "")
            echo $_GET['jsoncallback'] . '(' . json_encode($_data) . ');';
        else
            echo json_encode($_data);
    }

}
/* End of file template.php */
/* Location: ./system/core/template.php */