 <?php
	
	$LiveMatchList_single		=		array();
	$LiveMatchList_multi		=		array();

 ?>
 
 <style>
.handicap_after:after {
    font-family: FontAwesome;
    content: " \f111";
    display: inline;
	color: #428bca;
}
.handicap_before:before {
    font-family: FontAwesome;
    content: "\f111  ";
    display: inline;
	color: #428bca;
}
</style>
 
 <div class="mobile-menu" style="padding: 10px;">
 <?php
	//var_dump($this->MyListSinglePlayside);
 ?>
 <span class="sidebar_collap  pull-right">
	<span class="fa fa-angle-left"></span>
 </span>
              <div class="user-info">
              <span class="score myscore"><?php echo number_format($this->MyGameInfo['point_remain'],0); ?> แต้ม</span>
                <a href="<?php echo BASE_HREF . 'profile'; ?>" class="name"><?php echo Name; ?></a><br>
				<div class="rank-badges font-display">
				<?php 
					if($this->MyGameInfo['total_rate_calculate']<3){
						echo '<img src="'. BASE_HREF .'assets/img/rank-badges/rookie.svg" alt="" width="40" height="auto"> เด็กฝึกหัด ';
					}
					else if($this->MyGameInfo['total_rate_calculate']<5){
						echo '<img src="'. BASE_HREF .'assets/img/rank-badges/wonder-kid.svg" alt="" width="40" height="auto"> ดาวรุ่ง ';
					}
					else if($this->MyGameInfo['total_rate_calculate']<8){
						echo '<img src="'. BASE_HREF .'assets/img/rank-badges/pro.svg" alt="" width="40" height="auto"> เซียน  ';
					}
					else{
						echo '<img src="'. BASE_HREF .'assets/img/rank-badges/angle.svg" alt="" width="40" height="auto"> เทพ ';
					}
				?>
				</div>
              </div>
  <div class="content" >
	<a href="http://football.kapook.com/games" class="game-logo"></a>
     <div class="tab darkstrap">
	 <a href="http://football.kapook.com/games" class="btn btn-primary" style="width: 100%; text-align: center;">เริ่มทายผลบอล</a>
	 <h3 class="font-display" style="font-size: 32px;">ทายผลบอล</h3>
    
    <ul id="myGameTab" class="nav nav-tabs" role="tablist">
      <li id="tab_single" class="active"><a href="#game-single" role="tab" data-toggle="tab">บอลเดี่ยว</a></li>
	  <li id="tab_Multi_1"><a href="#dropdown1" role="tab" data-toggle="tab">บอลชุด</a></li>
    </ul>
    
    
    <div id="myGameTabContent" class="tab-content">
		<div class="tab-pane fade in active" id="game-single">
			<div class="card-head active"><span class="number">บอลเดี่ยว</span>
			<span class="point point_single"><?php echo (int)$this->MyListSinglePlayside['point'] ?> แต้ม</span></div>
			<br>
			<?php
			$sizeOfPlay		=	count($this->MyListSinglePlayside['list']);
			if($sizeOfPlay<$this->MyGameInfo['quota_single']){
			?>
				<div class="alert alert-warning fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<div id="notice_single">
						<b><u>คำแนะนำ</u></b><br>
						- กดปุ่ม <p class="label label-success">ทายบอลเดี่ยว</p> ของคู่ที่ต้องการเล่นเกมส์ทายผลบอลเดี่ยว<br>
						- ในหนึ่งวัน สามารถเล่นเกมส์ทายผลบอลเดี่ยวได้สูงสุด <?php echo $this->MyGameInfo['quota_single'];?> คู่<br>
						- <b>เหลือจำนวนคู่ที่สามารถทายผลได้อีก <?php echo ($this->MyGameInfo['quota_single']-$sizeOfPlay);?> คู่</b>
					</div>
				</div>	
			<?php
			}
			?>
			<div id="carousel-live" class="match-box singleplay">
				<?php
				
				for($x=0;$x<$sizeOfPlay;$x++) {
					$tmpGame											=	$this->MyListSinglePlayside['list'][$x];
					$MatchDate 											= 	$tmpGame['MatchDateTime'];
					$this->LiveMatchListTime[$tmpGame['match_id']]		=	$MatchDate;
					
					if($this->football_lib->isLiveMatch($tmpGame['MatchStatus'])){
						$this->AllLiveMatchID.=','.$tmpGame['match_id']; 
						$LiveMatchList_single[$tmpGame['match_id']][]	=	$tmpGame['id'];
					}
				?>
					<div class="match_a clearfix" id="<?php echo $tmpGame['id'];?>">
						<input type="hidden" class="url_match" value="<?php echo $tmpGame['MatchPageURL'];?>">
						<input type="hidden" class="odd_match" value="<?php echo $tmpGame['Odds'];?>">
						<div class="col-md-12 option">
							<?php 
							if(
								($tmpGame['MatchStatus']=='1 HF')	||
								($tmpGame['MatchStatus']=='2 HF')	||
								($tmpGame['MatchStatus']=='H/T')	||
								($tmpGame['MatchStatus']=='E/T')	||
								($tmpGame['MatchStatus']=='Pen')
							){
								?><label class="live pull-left blink_me">LIVE</label> <?php
							}
							?>
						<span class="status pull-right status_time"><?php
							if($tmpGame['MatchStatus']=='Sched'){?>
								<?php echo '';/*substr($tmpGame['MatchDateTime'], 11, 5);*/ ?>
							<?php }else if($tmpGame['MatchStatus']=='Fin'){?>
								จบเกม
							<?php }else if($tmpGame['MatchStatus']=='Int'){?>
								หยุดชั่วคราว
							<?php }else if($tmpGame['MatchStatus']=='Canc'){?>
								ยกเลิก
							<?php }else if($tmpGame['MatchStatus']=='Post'){?>
								เลื่อน
							<?php }else if($tmpGame['MatchStatus']=='Abd'){?>
								ยกเลิก
							<?php }else if($tmpGame['MatchStatus']=='1 HF'){?> 
								<?php echo $tmpGame['Minute'];?>'
							<?php }else if($tmpGame['MatchStatus']=='2 HF'){?>  
								<?php echo $tmpGame['Minute'];?>'
							<?php }else if($tmpGame['MatchStatus']=='E/T'){?>  
								<?php echo $tmpGame['Minute'];?>'
							<?php }else if($tmpGame['MatchStatus']=='Pen'){?>  
								Pen
							<?php }else if($tmpGame['MatchStatus']=='H/T'){?>  
								H/T
							<?php }else{ ?>
								N/A
							<?php }
						?></span></div>
						<div class="row">
							<div class="col-md-4 <?php if($tmpGame['team_win']==1) echo 'active';?>" id="divRadio1_<?php echo $this->MyListSinglePlayside['list'][$x]['id'];?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpGame['Team1Logo']);?>" class="flag"></div>
							<div class="col-md-4 status status_match"> <span class="badge mini_point" <?php
								if($tmpGame['isCalculatePoint']){
									if($tmpGame['point_return']>0){
										?> style="background: #00C02E !important;" <?php
									}else if($tmpGame['point_return']<0){
										?> style="background: #C70000 !important;" <?php
									}else{
										?> style="background: #7a7a7a !important;" <?php
									}
								}
							?> ><?php echo $tmpGame['point'];?> แต้ม</span> <br>
								<span class="mini_score"><?php
									if($tmpGame['MatchStatus']=='Sched'){
										?>VS<?php
									}else if(
										($tmpGame['MatchStatus']=='1 HF')	||
										($tmpGame['MatchStatus']=='2 HF')	||
										($tmpGame['MatchStatus']=='H/T')	||
										($tmpGame['MatchStatus']=='E/T')	||
										($tmpGame['MatchStatus']=='Fin')	||
										($tmpGame['MatchStatus']=='Pen')
									){
										echo $tmpGame['Team1FTScore']; ?>-<?php echo $tmpGame['Team2FTScore'];
									}else{
										?>N/A<?php
									}
									?>
								</span>
							</div>
							<div class="col-md-4 <?php if($tmpGame['team_win']==2) echo 'active';?>" id="divRadio2_<?php echo $this->MyListSinglePlayside['list'][$x]['id'];?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpGame['Team2Logo']);?>" class="flag"></div>
						</div>
						<div class="row hilight-color">
							<div class="col-md-4 "><span class="teamname teamname_home <?php if(($tmpGame['TeamOdds']==1)&&($tmpGame['Odds']> 0)){echo ' handicap_before';}?>">
							<?php
								echo $tmpGame['Team1'];
							?></span></div>
							<div class="col-md-4 handicap-text"><?php
								if($tmpGame['Odds']== 0){
									echo 'เสมอ';
								}else{
									echo $this->football_lib->getOddsRate((string)$tmpGame['Odds']);
								} ?></div>
							<div class="col-md-4"><span class="teamname teamname_away <?php if(($tmpGame['TeamOdds']==2)&&($tmpGame['Odds']> 0)){echo ' handicap_after';}?>">
							<?php
								echo $tmpGame['Team2'];
							?></span></div>
						</div>
						<!--<a href="http://football.kapook.com/games/deletegamebymatchid/<?php/* echo $this->MyListSinglePlayside['list'][$x]['match_id']; */?>" data-toggle="modal" data-target="#gameModal2"><span class="remove"></span></a>-->
					</div>
				<?php
				}
				?>            
			</div>
			<div class="sidebar-notice">
				คะแนนโบนัส<hr style="margin-bottom: 5px; margin-top: auto;">
				- ล็อกอินครั้งแรกของเดือนรับ 2000 แต้ม<br>
				- ล็อกอินแต่ละวันรับ 200 แต้ม
			</div>
		</div>
		<div class="tab-pane fade" id="dropdown1">
			<?php if(isset($this->MyListMultiPlayside[1])){
				$tmpList	=	$this->MyListMultiPlayside[1];
				setcookie('SET1' , '-1', time() + (86400 * 30), '/'); ?>
				<div class="card-head active">
					<span class="number">บอลชุด</span> <span class="point" id="point_multi_1"><?php echo $tmpList['point']; ?> แต้ม</span>
				</div>
				<br>
				<div id="carousel-live" class="match-box multi_play_1">
					<?php
					$sizeOfPlay		=	count($tmpList['list']);
					for($x=0;$x<$sizeOfPlay;$x++) {
					
						$tmpGame										=	$tmpList['list'][$x];
						$MatchDate 										= 	$tmpGame['MatchDateTime'];
						$this->LiveMatchListTime[$tmpGame['match_id']]		=	$MatchDate;
						
						if($this->football_lib->isLiveMatch($tmpGame['MatchStatus'])){
							$this->AllLiveMatchID.=','.$tmpGame['match_id']; 
							$LiveMatchList_multi[$tmpGame['match_id']][]	=	1;
						}
					?>
						<div class="match_a clearfix" id="<?php echo $tmpGame['match_id'];?>">
							<input type="hidden" class="url_match" value="<?php echo $tmpGame['MatchPageURL'];?>">
							<input type="hidden" class="odd_match" value="<?php echo $tmpGame['Odds'];?>">
							<div class="col-md-12 option">
								<?php 
								if(
									($tmpGame['MatchStatus']=='1 HF')	||
									($tmpGame['MatchStatus']=='2 HF')	||
									($tmpGame['MatchStatus']=='H/T')	||
									($tmpGame['MatchStatus']=='E/T')	||
									($tmpGame['MatchStatus']=='Pen')
								){
									?><label class="live pull-left blink_me">LIVE</label> <?php
								}
								?>
								<span class="status pull-right status_time"><?php
									if($tmpGame['MatchStatus']=='Sched'){?>
										<?php echo '';/*substr($tmpGame['MatchDateTime'], 11, 5);*/ ?>
									<?php }else if($tmpGame['MatchStatus']=='Fin'){?>
										จบเกม
									<?php }else if($tmpGame['MatchStatus']=='Int'){?>
										หยุดชั่วคราว
									<?php }else if($tmpGame['MatchStatus']=='Canc'){?>
										ยกเลิก
									<?php }else if($tmpGame['MatchStatus']=='Post'){?>
										เลื่อน
									<?php }else if($tmpGame['MatchStatus']=='Abd'){?>
										ยกเลิก
									<?php }else if($tmpGame['MatchStatus']=='1 HF'){?> 
										<?php echo $tmpGame['Minute'];?>'
									<?php }else if($tmpGame['MatchStatus']=='2 HF'){?>  
										<?php echo $tmpGame['Minute'];?>'
									<?php }else if($tmpGame['MatchStatus']=='E/T'){?>  
										<?php echo $tmpGame['Minute'];?>'
									<?php }else if($tmpGame['MatchStatus']=='Pen'){?>  
										Pen
									<?php }else if($tmpGame['MatchStatus']=='H/T'){?>  
										H/T
									<?php }else{?>
										N/A
									<?php }
								?></span>
							</div>
							<div class="row">
								<div class="col-md-4 <?php if($tmpGame['team_win']==1) echo 'active';?>" id="divRadio1_1_<?php echo $tmpGame['match_id'];?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpGame['Team1Logo']);?>" class="flag"></div>
								<div class="col-md-4 status status_match"><br>
								<span class="mini_score"><?php
									if($tmpGame['MatchStatus']=='Sched'){
										?>VS<?php
									}else if(
										($tmpGame['MatchStatus']=='1 HF')	||
										($tmpGame['MatchStatus']=='2 HF')	||
										($tmpGame['MatchStatus']=='H/T')	||
										($tmpGame['MatchStatus']=='E/T')	||
										($tmpGame['MatchStatus']=='Fin')	||
										($tmpGame['MatchStatus']=='Pen')
									){
										echo $tmpGame['Team1FTScore']; ?>-<?php echo $tmpGame['Team2FTScore'];
									}else{
										?>N/A<?php
									}
									?>
								</span></div>
								<div class="col-md-4 <?php if($tmpGame['team_win']==2) echo 'active';?>" id="divRadio2_1_<?php echo $tmpGame['match_id'];?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpGame['Team2Logo']);?>" class="flag"></div>
							</div>
							<div class="row hilight-color">
								<div class="col-md-4 "><span class="teamname teamname_home <?php if(($tmpGame['TeamOdds']==1)&&($tmpGame['Odds']> 0)){echo ' handicap_before';}?>">
								<?php
									echo $tmpGame['Team1'];
								?></span></div>
								<div class="col-md-4 handicap-text"><?php
									if($tmpGame['Odds']== 0){
										echo 'เสมอ';
									}else{
										echo $this->football_lib->getOddsRate((string)$tmpGame['Odds']);
									} ?></div>
								<div class="col-md-4"><span class="teamname teamname_away <?php if(($tmpGame['TeamOdds']==2)&&($tmpGame['Odds']> 0)){echo ' handicap_after';}?>">
								<?php
									echo $tmpGame['Team2'];
								?></span></div>
							</div>
						</div>
					<?php
					}
					?>  
				</div>
				<?php
			}else{
				if(count($this->arrGameCookie[1])<=0){?>
					<div class="card-head active">
						<span class="number">บอลชุด</span> <span class="point" id="point_multi_1">ยังไม่ทายผล</span>
					</div>
					<br>
					<div class="alert alert-warning fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<div id="notice_multi_1">
							<b><u>คำแนะนำ</u></b><br>
							- กดปุ่ม <p class="label label-primary">ทายบอลชุด</p> ของคู่ที่ต้องการเล่นทายผลบอลชุด<br>
							- ในหนึ่งวัน สามารถเล่นเกมส์ทายผลบอลชุดได้ตั้งแต่ <?php echo $this->MyGameInfo['quota_multi_per_game_min'];?> ถึง <?php echo $this->MyGameInfo['quota_multi_per_game_max'];?> คู่<br>
						</div>
					</div>
					<div id="carousel-live" class="match-box multi_play_1"></div>
					<?php
				}else if(count($this->arrGameCookie[1])<$this->MyGameInfo['quota_multi_per_game_min']){
					$tmpList		=	$this->arrGameCookie[1];
					$tmpStr			=	'';
					$sizeOfPlay		=	count($tmpList);
					for($x=0;$x<$sizeOfPlay;$x++) {
						if(($tmpList[$x]['team_win']!=1)&&($tmpList[$x]['team_win']!=2)){
							$tmpStr		=		'- เลือกทีมที่จะทายผล ให้ครบทุกคู่ โดยการคลื๊กโลโก้ทีมในแต่ละคู่<br>';
							break;
						}
					}
				?>
					<div class="card-head active">
						<span class="number">บอลชุด</span> <span class="point" id="point_multi_1">ยังไม่ทายผล</span>
					</div>
					<br>
					<div class="alert alert-warning fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<div id="notice_multi_1">
							<b><u>คำแนะนำ</u></b><br>
							- กดปุ่ม <p class="label label-primary">ทายบอลชุด</p> ของคู่ที่ต้องการเล่นทายผลบอลชุด<br>
							<?php echo $tmpStr; ?>
							- <b>ต้องเลือกคู่การแข่งขันอีก <?php echo ($this->MyGameInfo['quota_multi_per_game_min']-$sizeOfPlay);?> คู่</b>
						</div>
					</div>
					<div id="carousel-live" class="match-box multi_play_1">
						<?php
						for($x=0;$x<$sizeOfPlay;$x++) {
							$tmpGame	=	$tmpList[$x];
							$tmpMatch	=	$this->arrMatchCookie[$tmpGame['match_id']];
						?>
							<div class="match_a clearfix blink" id="<?php echo $tmpGame['match_id'];?>">
								<input type="hidden" class="url_match" value="<?php echo $tmpGame['MatchPageURL'];?>">
								<input type="hidden" class="odd_match" value="<?php echo $tmpGame['Odds'];?>">
								<div class="col-md-12 option">คลิกโลโก้ทีมเพื่อทายผล</div>
								<div class="row">
									<div
										onclick="select_multi(1,<?php echo $tmpGame['match_id'];?>,1);"
										class="col-md-4 <?php if($tmpGame['team_win']==1) echo 'active';?>"
										id="divRadio1_1_<?php echo $tmpGame['match_id'];?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team1Logo']);?>" class="flag"></div>
									<div class="col-md-4 status status_match"><br><span class="mini_score">vs</span></div>
									<div
										onclick="select_multi(1,<?php echo $tmpGame['match_id'];?>,2);"
										class="col-md-4 <?php if($tmpGame['team_win']==2) echo 'active';?>"
										id="divRadio2_1_<?php echo $tmpGame['match_id'];?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team2Logo']);?>" class="flag"></div>
								</div>
								<div class="row hilight-color">
									<div class="col-md-4 "><span class="teamname teamname_home <?php if(($tmpMatch['TeamOdds']==1)&&($tmpMatch['Odds']> 0)){echo ' handicap_before';}?>">
									<?php
										echo $tmpMatch['Team1'];
									?></span></div>
									<div class="col-md-4 handicap-text"><?php
										if($tmpMatch['Odds']== 0){
											echo 'เสมอ';
										}else{
											echo $this->football_lib->getOddsRate((string)$tmpMatch['Odds']);
										} ?>
									</div>
									<div class="col-md-4"><span class="teamname teamname_away <?php if(($tmpMatch['TeamOdds']==2)&&($tmpMatch['Odds']> 0)){echo ' handicap_after';}?>">
									<?php
										echo $tmpMatch['Team2'];
									?></span></div>
								</div>
								<span class="remove" onclick="remove_multi(1,<?php echo $tmpGame['match_id']; ?>);"></span >
							</div>
						<?php
						}
						?>
					</div>
				<?php
				}else{
					$tmpList		=	$this->arrGameCookie[1];
					$tmpStr			=	'';
					$sizeOfPlay		=	count($tmpList);
					for($x=0;$x<$sizeOfPlay;$x++) {
						if(($tmpList[$x]['team_win']!=1)&&($tmpList[$x]['team_win']!=2)){
							$tmpStr		=		'- เลือกทีมที่จะทายผล ให้ครบทุกคู่ โดยการคลื๊กโลโก้ทีมในแต่ละคู่<br>';
							break;
						}
					}
					if($x==$sizeOfPlay){
						$tmpStr		=		'- กรอกแต้มที่ใช้งาน และกดปุ่ม <div class="btn btn-success" style="padding: initial;">ตกลง</div> เพื่อยืนยันการเล่น<br>';
					}
				?>
					<div class="card-head active">
						<span class="number">บอลชุด</span> <span class="point" id="point_multi_1">ยังไม่ทายผล</span>
					</div>
					<br>
					<div class="alert alert-warning fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<div id="notice_multi_1">
							<b><u>คำแนะนำ</u></b><br>
							<?php if($sizeOfPlay < $this->MyGameInfo['quota_multi_per_game_max']){ ?>
								- กดปุ่ม <p class="label label-primary">ทายบอลชุด</p> ของคู่ที่ต้องการเล่นทายผลบอลชุด<br>
							<?php
							}
							?>
							<?php echo $tmpStr;
							if($sizeOfPlay < $this->MyGameInfo['quota_multi_per_game_max']){
							?>
								- <b>เหลือจำนวนคู่ที่สามารถทายผลได้อีก <?php echo ($this->MyGameInfo['quota_multi_per_game_max']-$sizeOfPlay);?> คู่</b>
							<?php
							}
							?>
						</div>
					</div>
					<div id="carousel-live" class="match-box multi_play_1">
						<?php
						for($x=0;$x<$sizeOfPlay;$x++) {
							$tmpGame	=	$tmpList[$x];
							$tmpMatch	=	$this->arrMatchCookie[$tmpGame['match_id']];
						?>
						<div class="match_a clearfix blink" id="<?php echo $tmpGame['match_id'];?>">
							<input type="hidden" class="url_match" value="<?php echo $tmpGame['MatchPageURL'];?>">
							<input type="hidden" class="odd_match" value="<?php echo $tmpGame['Odds'];?>">
							<div class="col-md-12 option">คลิกโลโก้ทีมเพื่อทายผล</div>
							<div class="row">
								<div
									onclick="select_multi(1,<?php echo $tmpGame['match_id'];?>,1);"
									class="col-md-4 <?php if($tmpGame['team_win']==1) echo 'active';?>"
									id="divRadio1_1_<?php echo $tmpGame['match_id'];?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team1Logo']);?>" class="flag">
								</div>
									<div class="col-md-4 status status_match"><br><span class="mini_score">vs</span></div>
									<div
									onclick="select_multi(1,<?php echo $tmpGame['match_id'];?>,2);"
									class="col-md-4 <?php if($tmpGame['team_win']==2) echo 'active';?>"
									id="divRadio2_1_<?php echo $tmpGame['match_id'];?>"><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/96/h", $tmpMatch['Team2Logo']);?>" class="flag"></div>
							</div>
							<div class="row hilight-color">
								<div class="col-md-4 "><span class="teamname teamname_home <?php if(($tmpMatch['TeamOdds']==1)&&($tmpMatch['Odds']> 0)){echo ' handicap_before';}?>">
								<?php
									echo $tmpMatch['Team1'];
								?></span></div>
								<div class="col-md-4 handicap-text"><?php
									if($tmpMatch['Odds']== 0){
										echo 'เสมอ';
									}else{
										echo $this->football_lib->getOddsRate((string)$tmpMatch['Odds']);
									} ?></div>
								<div class="col-md-4"><span class="teamname teamname_away <?php if(($tmpMatch['TeamOdds']==2)&&($tmpMatch['Odds']> 0)){echo ' handicap_after';}?>">
								<?php
									echo $tmpMatch['Team2'];
								?></span></div>
							</div>
							<span class="remove" onclick="remove_multi(1,<?php echo $tmpGame['match_id']; ?>);"></span >
						</div>
						<?php
						}
						?>
						<div class="form-group point-select">
							<div class="col-xs-7">
								<input type="input" class="form-control " id="tmpSelectPoint_Multi_1" placeholder="คะแนนที่จะลง">
							</div>
							<div  class="col-xs-5 " style="font-size: 16px; line-height: 34px;">
								<button type="button" class="btn btn-success" onclick="InsertNewPlaySideMulti(1);">ตกลง</button>
							</div>                 
						</div>
					</div>
					<?php
				}
			}
			?>
			<div class="sidebar-notice">
				คะแนนโบนัส<hr style="margin-bottom: 5px; margin-top: auto;">
				- ล็อกอินครั้งแรกของเดือนรับ 2000 แต้ม<br>
				- ล็อกอินแต่ละวันรับ 200 แต้ม
			</div>
		</div>
    </div>
  </div><!-- /example -->

   <div style="height: 100px; display: block;"></div>
  </div><!--END Content-->

 
</div>   <!--END-->
<script language="JavaScript">

	var tmpSelectTeam_Single_New=0;
	
	var tmpSelectTeam_Multi=new Array();
	
	tmpSelectTeam_Multi[1]=new Array();
	tmpSelectTeam_Multi[2]=new Array();
	tmpSelectTeam_Multi[3]=new Array();
	tmpSelectTeam_Multi[4]=new Array();
	tmpSelectTeam_Multi[5]=new Array();
	
	var tmpLimitSinglePlay	=	<?php echo $this->MyGameInfo['quota_single'];?>;
	var tmpLimitMultiPlay	=	<?php echo $this->MyGameInfo['quota_multi'];?>;
	
	var tmpLimitMultiPlay_MatchMin	=	<?php echo $this->MyGameInfo['quota_multi_per_game_min'];?>;
	var tmpLimitMultiPlay_MatchMax	=	<?php echo $this->MyGameInfo['quota_multi_per_game_max'];?>;
	
	//var liveMatchGame_Single	=	new Array(); //implement on footer
	<?php
	if(count($LiveMatchList_single)>0){
		foreach($LiveMatchList_single as $mid => $gamelist){
			?>liveMatchGame_Single[<?php echo $mid; ?>] = new Array();
			<?php
			foreach($gamelist as $gameid){
				?>liveMatchGame_Single[<?php echo $mid; ?>].push(<?php echo $gameid;?>);
				<?php
			}
		}
	}
	?>
	
	//var liveMatchGame_Multi		=	new Array(); //implement on footer
	<?php
	if(count($LiveMatchList_multi)>0){
		foreach($LiveMatchList_multi as $mid => $setlist){
			?>liveMatchGame_Multi[<?php echo $mid; ?>] = new Array();
			<?php
			foreach($setlist as $setid){
				?>liveMatchGame_Multi[<?php echo $mid; ?>].push(<?php echo $setid;?>);
				<?php
			}
		}
	}
	?>
	
	//var liveMatchGame_TimeInterval = new Array(); //implement on footer

	initDataFormCookie();
	
	var OddsRate = new Array();
	
	OddsRate["0"] 		= "เสมอ";
	OddsRate["0.25"] 	= "เสมอควบครึ่ง";
	OddsRate["0.5"] 	= "ครึ่งลูก";
	OddsRate["0.75"] 	= "ครึ่งควบลูก";
	OddsRate["1"] 		= "หนึ่งลูก";
	OddsRate["1.25"] 	= "ลูกควบลูกครึ่ง";
	OddsRate["1.5"] 	= "ลูกครึ่ง";
	OddsRate["1.75"] 	= "ลูกครึ่งควบสองลูก";
	OddsRate["2"] 		= "สองลูก";
	OddsRate["2.25"] 	= "สองลูกควบสองลูกครึ่ง";
	OddsRate["2.5"] 	= "สองลูกครึ่ง";
	OddsRate["2.75"] 	= "สองลูกครึ่งควบสามลูก";
	OddsRate["3"] 		= "สามลูก";
	OddsRate["3.25"] 	= "สามลูกควบสามลูกครึ่ง";
	OddsRate["3.5"] 	= "สามลูกครึ่ง";
	OddsRate["3.75"] 	= "สามลูกครึ่งควบสี่ลูก";
	OddsRate["4"] 		= "สี่ลูก";
	OddsRate["4.25"] 	= "สี่ลูกควบสี่ลูกครึ่ง";
	OddsRate["4.5"] 	= "สี่ลูกครึ่ง";
	OddsRate["4.75"] 	= "สี่ลูกครึ่งควบห้าลูก";
	OddsRate["5"] 		= "ห้าลูก";
	OddsRate["5.25"] 	= "ห้าลูกควบห้าลูกครึ่ง";
	OddsRate["5.5"] 	= "ห้าลูกครึ่ง";
	OddsRate["5.75"] 	= "ห้าลูกครึ่งควบหกลูก";
	OddsRate["6"] 		= "หกลูก";
	OddsRate["6.25"] 	= "หกลูกควบหกลูกครึ่ง";
	OddsRate["6.5"] 	= "หกลูกครึ่ง";
	OddsRate["6.75"] 	= "หกลูกครึ่งควบเจ็ดลูก";
	OddsRate["7"] 		= "เจ็ดลูก";
	OddsRate["7.25"] 	= "เจ็ดลูกควบเจ็ดลูกครึ่ง";
	OddsRate["7.5"] 	= "เจ็ดลูกครึ่ง";
	OddsRate["7.75"] 	= "เจ็ดลูกครึ่งควบแปดลูก";
	OddsRate["8"] 		= "แปดลูก";
	OddsRate["8.25"] 	= "แปดลูกควบแปดลูกครึ่ง";
	OddsRate["8.5"] 	= "แปดลูกครึ่ง";
	OddsRate["8.75"] 	= "แปดลูกครึ่งควบเก้าลูก";
	OddsRate["9"] 		= "เก้าลูก";
	OddsRate["9.25"] 	= "เก้าลูกควบเก้าลูกครึ่ง";
	OddsRate["9.5"] 	= "เก้าลูกครึ่ง";
	OddsRate["9.75"] 	= "เก้าลูกครึ่งควบสิบลูก";
	OddsRate["10"] 		= "สิบลูก";
	
	function number_format(number, decimals, dec_point, thousands_sep) {
		//  discuss at: http://phpjs.org/functions/number_format/
		// original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
		// improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		// improved by: davook
		// improved by: Brett Zamir (http://brett-zamir.me)
		// improved by: Brett Zamir (http://brett-zamir.me)
		// improved by: Theriault
		// improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		// bugfixed by: Michael White (http://getsprink.com)
		// bugfixed by: Benjamin Lupton
		// bugfixed by: Allan Jensen (http://www.winternet.no)
		// bugfixed by: Howard Yeend
		// bugfixed by: Diogo Resende
		// bugfixed by: Rival
		// bugfixed by: Brett Zamir (http://brett-zamir.me)
		//  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
		//  revised by: Luke Smith (http://lucassmith.name)
		//    input by: Kheang Hok Chin (http://www.distantia.ca/)
		//    input by: Jay Klehr
		//    input by: Amir Habibi (http://www.residence-mixte.com/)
		//    input by: Amirouche
		//   example 1: number_format(1234.56);
		//   returns 1: '1,235'
		//   example 2: number_format(1234.56, 2, ',', ' ');
		//   returns 2: '1 234,56'
		//   example 3: number_format(1234.5678, 2, '.', '');
		//   returns 3: '1234.57'
		//   example 4: number_format(67, 2, ',', '.');
		//   returns 4: '67,00'
		//   example 5: number_format(1000);
		//   returns 5: '1,000'
		//   example 6: number_format(67.311, 2);
		//   returns 6: '67.31'
		//   example 7: number_format(1000.55, 1);
		//   returns 7: '1,000.6'
		//   example 8: number_format(67000, 5, ',', '.');
		//   returns 8: '67.000,00000'
		//   example 9: number_format(0.9, 0);
		//   returns 9: '1'
		//  example 10: number_format('1.20', 2);
		//  returns 10: '1.20'
		//  example 11: number_format('1.20', 4);
		//  returns 11: '1.2000'
		//  example 12: number_format('1.2000', 3);
		//  returns 12: '1.200'
		//  example 13: number_format('1 000,50', 2, '.', ' ');
		//  returns 13: '100 050.00'
		//  example 14: number_format(1e-8, 8, '.', '');
		//  returns 14: '0.00000001'

		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
		var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function(n, prec) {
			var k = Math.pow(10, prec);
			return '' + (Math.round(n * k) / k).toFixed(prec);
		};
			
		// Fix for IE parseFloat(0.55).toFixed(0) = 0;
		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		}
		if ((s[1] || '').length < prec) {
			s[1] = s[1] || '';
			s[1] += new Array(prec - s[1].length + 1).join('0');
		}
		return s.join(dec);
	}
	
	//init data form cookie
	function initDataFormCookie(){
		var ca 				= 	document.cookie.split(';');
		var dataCookie		=	"";
		var arrDataCookie	=	new Array();

		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ')
				c = c.substring(1);
			tempdata						=	c.split('=');
			arrDataCookie[tempdata[0]]		=	tempdata[1];
		}
		//console.log(tmpLimitMultiPlay);
		for(var i=1; i<=tmpLimitMultiPlay; i++) {
		
			if(typeof arrDataCookie['SET' + i] !== 'undefined'){
				ca 		= 	arrDataCookie['SET' + i].split('|');
				for(var j=0; j<ca.length; j++) {
					var c = ca[j];
					tempdata										=	c.split(':');
					tmpSelectTeam_Multi[i][Number(tempdata[0])]		=	Number(tempdata[1]);
				}
			}
		}
		
		//console.log(tmpSelectTeam_Multi[i]);
	}

	//Script for add game
	function AddPlaySideSingle(id,Team1,Team2,Team1Logo,Team2Logo,Odds,TeamOdds,matchLink){
		
		/*if($('#' + id).length>0){
			EditPlaySideSingle(id);
			return false;
		}*/
		
		$('.nav-tabs a[href="#game-single"]').tab('show');
		
		if($('.newplaysingle').length>0){
			$( '.singleplay' ).children('.newplaysingle').remove();
		}
		
		if( $( '.singleplay' ).children('.match_a').length >= tmpLimitSinglePlay ){
			show_stack_bottomright('error_full_quota_single','');
			return false;
		}
		
		/*
		document.getElementById('tab_Multi').className='dropdown';
		document.getElementById('tab_Multi_1').className='';
		document.getElementById('tab_Multi_2').className='';
		document.getElementById('tab_Multi_3').className='';
		document.getElementById('tab_Multi_4').className='';
		document.getElementById('tab_Multi_5').className='';
		*/
		
		if(clicked==false){
			sidemenu_active();
			clicked = true;
		}
		
		if(TeamOdds==1){
			teamOdds1	=	'handicap_before';
			teamOdds2	=	'';
		}else if(TeamOdds==2){
			teamOdds1	=	'';
			teamOdds2	=	'handicap_after';
		}else{
			teamOdds1	=	'';
			teamOdds2	=	'';
		}
		
		tmpSelectTeam_Single_New=0;
		
		$('.singleplay')
		.append($('<div >').attr('class', 'match_a blink clearfix newplaysingle')
			.append('<input type="hidden" class="url_match" value="' + matchLink + '">')
			.append('<input type="hidden" class="odd_match" value="' + Odds + '">')
			.append($('<div >').attr('class', 'col-md-12 option').text('คลิกโลโก้ทีมเพื่อทายผล'))
			
			.append($('<div >').attr('class', 'row')
				.append($('<div >')
					.attr('class', 'col-md-4')
					.attr('id', 'divRadio1_new')
					.attr('onclick', 'tmpSelectTeam_Single_New=1; document.getElementById(\'divRadio1_new\').className=\'col-md-4 active\'; document.getElementById(\'divRadio2_new\').className=\'col-md-4\';notice_change(3,0);')
					.append('<img src="'+ Team1Logo +'" class="flag">')
				)
				.append($('<div >')
					.attr('class', 'col-md-4 status status_match')
					.append('<br><span class="mini_score">vs</span>')
				)
				.append($('<div >')
					.attr('class', 'col-md-4')
					.attr('id', 'divRadio2_new')
					.attr('onclick', 'tmpSelectTeam_Single_New=2; document.getElementById(\'divRadio2_new\').className=\'col-md-4 active\'; document.getElementById(\'divRadio1_new\').className=\'col-md-4\';notice_change(3,0);')
					.append('<img src="'+ Team2Logo +'" class="flag">')
				)
			)
			.append($('<div >').attr('class', 'row hilight-color')
				.append($('<div >').attr('class', 'col-md-4').append('<span class="teamname teamname_home ' + teamOdds1 + '">'+ Team1 + '</span>'))
				.append($('<div >').attr('class', 'col-md-4 handicap-text').text(OddsRate[Odds]))
				.append($('<div >').attr('class', 'col-md-4').append('<span class="teamname teamname_away ' + teamOdds2 + '">'+ Team2 + '</span>'))
			)
			.append($('<div >').attr('class', 'form-group point-select')
				.append($('<div >').attr('class', 'col-xs-7')
					.append('<input type="input" class="form-control" id="tmpSelectPoint_Single_New" placeholder="คะแนนที่จะลง">')
				)
				.append($('<div >')
					.attr('class', 'col-xs-5')
					.attr('style', 'font-size: 16px; line-height: 34px;')
					.append($('<button >')
						.attr('type', 'button')
						.attr('onclick', 'InsertNewPlaySideSingle(' + id + ');')
						.attr('class', 'btn btn-success')
						.text('ตกลง')
					)
				)
			)
			.append($('<span >')
				.attr('class', 'remove')
				.attr('onclick', 'remove_new_single();')
			)
		);
		$(".content").mCustomScrollbar("scrollTo","last");
		notice_change(2,0);
	}
	
	//Script for add Multi game
	function AddPlaySideMulti(setNo,id,Team1,Team2,Team1Logo,Team2Logo,Odds,TeamOdds,matchLink){
		
		if(clicked==false){
			sidemenu_active();
			clicked = true;
		}
		
		$('.nav-tabs a[href="#dropdown1"]').tab('show');
		
		/*
		for(i=1;i<=5;i++){
			if(i==setNo){
				document.getElementById('tab_Multi_' + i).className='active';
			}else{
				document.getElementById('tab_Multi_' + i).className='';
			}
		}*/
		
		var ca 			= 	document.cookie.split(';');
		var dataCookie	=	"";

		var name = "SET" + setNo + "=";
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ')
				c = c.substring(1);
			if (c.indexOf(name) != -1){
				dataCookie = c.substring(name.length,c.length);
				break;
			}
		}
		
		if(dataCookie=="-1"){
			show_stack_bottomright('error_set_multi_played',setNo);
			return false;
		}
		
		if($('.multi_play_' + setNo).find('#' + id).length>0){
			return false;
		}
		
		if( $( '.multi_play_' + setNo ).children('.match_a').length >= tmpLimitMultiPlay_MatchMax ){
			show_stack_bottomright('error_full_quota_multi','');
			return false;
		}
		
		if(dataCookie.length>0){
			var listMatch 			= 	dataCookie.split('|');
			dataCookie				=	dataCookie + '|' + id + ':' + '0';
		}else{
			var listMatch			=	'';
			dataCookie				=	id + ':' + '0';
		}
		
		document.cookie = "SET" + setNo + "=" + dataCookie + "; path=/";
		
		if(TeamOdds==1){
			teamOdds1	=	'handicap_before';
			teamOdds2	=	'';
		}else if(TeamOdds==2){
			teamOdds1	=	'';
			teamOdds2	=	'handicap_after';
		}else{
			teamOdds1	=	'';
			teamOdds2	=	'';
		}
		
		tmpSelectTeam_Multi[setNo][id]	=	0;
		
		if( $('.multi_play_' + setNo).find('.match_a').length >= tmpLimitMultiPlay_MatchMin ){
			$('.multi_play_' + setNo).find('.point-select')
			.before($('<div >').attr('class', 'match_a clearfix blink').attr('id', id)
				.append('<input type="hidden" class="url_match" value="' + matchLink + '">')
				.append('<input type="hidden" class="odd_match" value="' + Odds + '">')
				.append($('<div >').attr('class', 'col-md-12').text('คลิกโลโก้ทีมเพื่อทายผล'))
				.append($('<div >').attr('class', 'row')
					.append($('<div >')
						.attr('class', 'col-md-4')
						.attr('id', 'divRadio1_'+setNo+'_'+id)
						.attr('onclick', 'select_multi(' + setNo + ',' + id + ',1);')
						.append('<img src="'+ Team1Logo +'" class="flag">')
					)
					.append($('<div >')
						.attr('class', 'col-md-4 status status_match')
						.append('<br><span class="mini_score">vs</span>')
					)
					.append($('<div >')
						.attr('class', 'col-md-4')
						.attr('id', 'divRadio2_'+setNo+'_'+id)
						.attr('onclick', 'select_multi(' + setNo + ',' + id + ',2);')
						.append('<img src="'+ Team2Logo +'" class="flag">')
					)
				)
				.append($('<div >').attr('class', 'row hilight-color')
					.append($('<div >').attr('class', 'col-md-4').append('<span class="teamname teamname_home ' + teamOdds1 + '">'+ Team1 + '</span>'))
					.append($('<div >').attr('class', 'col-md-4 handicap-text').text(OddsRate[Odds]))
					.append($('<div >').attr('class', 'col-md-4').append('<span class="teamname teamname_away ' + teamOdds2 + '">'+ Team2 + '</span>'))
				)
				.append($('<span >')
					.attr('class', 'remove')
					.attr('onclick', 'remove_multi('+setNo+','+id+');')
				)
			);
		}else if(($('.multi_play_' + setNo).find('.match_a').length > 0) && ($('.multi_play_' + setNo).find('.match_a').length < tmpLimitMultiPlay_MatchMin)){
			$('.multi_play_' + setNo)
			.append($('<div >').attr('class', 'match_a clearfix blink').attr('id', id)
				.append('<input type="hidden" class="url_match" value="' + matchLink + '">')
				.append('<input type="hidden" class="odd_match" value="' + Odds + '">')
				.append($('<div >').attr('class', 'col-md-12').text('คลิกโลโก้ทีมเพื่อทายผล'))
				.append($('<div >').attr('class', 'row')
					.append($('<div >')
						.attr('class', 'col-md-4')
						.attr('id', 'divRadio1_'+setNo+'_'+id)
						.attr('onclick', 'select_multi(' + setNo + ',' + id + ',1);')
						.append('<img src="'+ Team1Logo +'" class="flag">')
					)
					.append($('<div >')
						.attr('class', 'col-md-4 status status_match')
						.append('<br><span class="mini_score">vs</span>')
					)
					.append($('<div >')
						.attr('class', 'col-md-4')
						.attr('id', 'divRadio2_'+setNo+'_'+id)
						.attr('onclick', 'select_multi(' + setNo + ',' + id + ',2);')
						.append('<img src="'+ Team2Logo +'" class="flag">')
					)
				)
				.append($('<div >').attr('class', 'row hilight-color')
					.append($('<div >').attr('class', 'col-md-4').append('<span class="teamname teamname_home ' + teamOdds1 + '">'+ Team1 + '</span>'))
					.append($('<div >').attr('class', 'col-md-4 handicap-text').text(OddsRate[Odds]))
					.append($('<div >').attr('class', 'col-md-4').append('<span class="teamname teamname_away ' + teamOdds2 + '">'+ Team2 + '</span>'))
				)
				.append($('<span >')
					.attr('class', 'remove')
					.attr('onclick', 'remove_multi('+setNo+','+id+');')
				)
			);
			
			$('.multi_play_' + setNo)
			.append($('<div >').attr('class', 'form-group point-select')
				.append($('<div >').attr('class', 'col-xs-7')
					.append('<input type="input" class="form-control" id="tmpSelectPoint_Multi_' + setNo + '" placeholder="คะแนนที่จะลง">')
				)
				.append($('<div >')
					.attr('class', 'col-xs-5')
					.attr('style', 'font-size: 16px; line-height: 34px;')
					.append($('<button >')
						.attr('type', 'button')
						.attr('onclick', 'InsertNewPlaySideMulti(' + setNo + ');')
						.attr('class', 'btn btn-success')
						.text('ตกลง')
					)
				)
			);
		}else{
			$('.multi_play_' + setNo)
			.append($('<div >').attr('class', 'match_a clearfix blink').attr('id', id)
				.append('<input type="hidden" class="url_match" value="' + matchLink + '">')
				.append('<input type="hidden" class="odd_match" value="' + Odds + '">')
				.append($('<div >').attr('class', 'col-md-12').text('คลิกโลโก้ทีมเพื่อทายผล'))
				.append($('<div >').attr('class', 'row')
					.append($('<div >')
						.attr('class', 'col-md-4')
						.attr('id', 'divRadio1_'+setNo+'_'+id)
						.attr('onclick', 'select_multi(' + setNo + ',' + id + ',1);')
						.append('<img src="'+ Team1Logo +'" class="flag">')
					)
					.append($('<div >')
						.attr('class', 'col-md-4 status status_match')
						.append('<br> vs ')
					)
					.append($('<div >')
						.attr('class', 'col-md-4')
						.attr('id', 'divRadio2_'+setNo+'_'+id)
						.attr('onclick', 'select_multi(' + setNo + ',' + id + ',2);')
						.append('<img src="'+ Team2Logo +'" class="flag">')
					)
				)
				.append($('<div >').attr('class', 'row hilight-color')
					.append($('<div >').attr('class', 'col-md-4').append('<span class="teamname teamname_home ' + teamOdds1 + '">'+ Team1 + '</span>'))
					.append($('<div >').attr('class', 'col-md-4 handicap-text').text(OddsRate[Odds]))
					.append($('<div >').attr('class', 'col-md-4').append('<span class="teamname teamname_away ' + teamOdds2 + '">'+ Team2 + '</span>'))
				)
				.append($('<span >')
					.attr('class', 'remove')
					.attr('onclick', 'remove_multi('+setNo+','+id+');')
				)
			);
		}
		$(".content").mCustomScrollbar("scrollTo","last");
		notice_change(2,setNo);
	}
	
	function select_multi(setNo,id,team_win){
		if((team_win==1)||(team_win==2)){
			tmpSelectTeam_Multi[setNo][id]=team_win;
		}
		if(team_win==1){
			document.getElementById('divRadio1_' + setNo + '_' + id).className='col-md-4 active';
			document.getElementById('divRadio2_' + setNo + '_' + id).className='col-md-4';
		}else if(team_win==2){
			document.getElementById('divRadio2_' + setNo + '_' + id).className='col-md-4 active';
			document.getElementById('divRadio1_' + setNo + '_' + id).className='col-md-4';
		}
		
		var ca 			= 	document.cookie.split(';');
		var dataCookie	=	"";

		var name = "SET" + setNo + "=";
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ')
				c = c.substring(1);
			if (c.indexOf(name) != -1){
				dataCookie = c.substring(name.length,c.length);
				break;
			}
		}
		
		if((dataCookie!="-1")&&(dataCookie.length>0)){
			var ca 			= 	dataCookie.split('|');
			
			var name = id + ":";
			for(var i=0; i<ca.length; i++) {
				var c = ca[i];
				if (c.indexOf(name) != -1){
					dataCookie		=		dataCookie.replace(c, id + ':' + team_win);
					break;
				}
			}
			if(i==ca.length){
				dataCookie	=	dataCookie + '|' + id + ':' + team_win;
			}
		}else{
			dataCookie		=	id + ':' + team_win;
		}
		
		document.cookie 	= 	"SET" + setNo + "=" + dataCookie + "; path=/;";
		notice_change(2,setNo);

	}
	
	function remove_new_single(){
		$( '.singleplay' ).children('.newplaysingle').remove();
		notice_change(1,0);
	}
	
	function remove_multi(setNo,id){
		var ca 			= 	document.cookie.split(';');
		var dataCookie	=	"";

		var name = "SET" + setNo + "=";
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ')
				c = c.substring(1);
			if (c.indexOf(name) != -1){
				dataCookie = c.substring(name.length,c.length);
				break;
			}
		}
		
		if((dataCookie!="-1")&&(dataCookie.length>0)){
			
			var ca 				= 	dataCookie.split('|');
			dataCookie			=	"";
			
			var name = id + ":";
			for(var i=0; i<ca.length; i++) {
				var c = ca[i];
				if (c.indexOf(name) == -1){
					if(dataCookie.length>0){
						dataCookie	=	dataCookie + '|' + c;
					}else{
						dataCookie	=	c;
					}
				}
			}
		}
		
		document.cookie = "SET" + setNo + "=" + dataCookie + "; path=/;";;
	
		$('.multi_play_' + setNo).children('#' + id).remove();
		delete tmpSelectTeam_Multi[setNo][id];
		//console.log(tmpSelectTeam_Multi[setNo]);
		if($('.multi_play_' + setNo).find('.match_a').length<tmpLimitMultiPlay_MatchMin){
			$('.multi_play_' + setNo).find('.point-select').remove();
		}
		notice_change(2,setNo);
	}
	
	function InsertNewPlaySideSingle(id){
		                
		if(tmpSelectTeam_Single_New==0){
			show_stack_bottomright('error_teamwin_notselect','');
			return false;
		}
		
		if(	(isNaN(document.getElementById('tmpSelectPoint_Single_New').value)) ||
			(parseFloat(document.getElementById('tmpSelectPoint_Single_New').value)!=parseInt(document.getElementById('tmpSelectPoint_Single_New').value))
		){
			document.getElementById('tmpSelectPoint_Single_New').focus();
			show_stack_bottomright('error_point_notnumber','');
			return false;
		}
		
		if(document.getElementById('tmpSelectPoint_Single_New').value==0){
			document.getElementById('tmpSelectPoint_Single_New').focus();
			show_stack_bottomright('error_point_notselect','');
			return false;
		}
		
		if(parseInt($('.myscore').text().replace(/,/g, ""))<document.getElementById('tmpSelectPoint_Single_New').value){
			document.getElementById('tmpSelectPoint_Single_New').focus();
			show_stack_bottomright('error_point_notenough',parseInt($('.myscore').text().replace(/,/g, "")));
			return false;
		}
		
		/*$('#gameModal2').modal({
            keyboard: false
        })*/
		
		useful_modal('<?php echo BASE_HREF; ?>games/confirm/single/' + id + '/' + tmpSelectTeam_Single_New + '/' + document.getElementById('tmpSelectPoint_Single_New').value,false);
	}
	
	function InsertNewPlaySideMulti(setNo){
		if(tmpSelectTeam_Multi[setNo].length < tmpLimitMultiPlay_MatchMin){
			show_stack_bottomright('error_multi_less_team_select','');
			return false;
		}
		
		for (var k in tmpSelectTeam_Multi[setNo]){
			if(tmpSelectTeam_Multi[setNo][k]==0){
				show_stack_bottomright('error_teamwin_notselect','');
				return false;
			}
		}
		
		if(	(isNaN(document.getElementById('tmpSelectPoint_Multi_' + setNo).value)) ||
			(parseFloat(document.getElementById('tmpSelectPoint_Multi_' + setNo).value)!=parseInt(document.getElementById('tmpSelectPoint_Multi_' + setNo).value))
		){
			document.getElementById('tmpSelectPoint_Multi_' + setNo).focus();
			show_stack_bottomright('error_point_notnumber','');
			return false;
		}
		
		if(document.getElementById('tmpSelectPoint_Multi_' + setNo).value==0){
			document.getElementById('tmpSelectPoint_Multi_' + setNo).focus();
			show_stack_bottomright('error_point_notselect','');
			return false;
		}
		
		if(parseInt($('.myscore').text().replace(/,/g, ""))<document.getElementById('tmpSelectPoint_Multi_' + setNo).value){
			document.getElementById('tmpSelectPoint_Multi_' + setNo).focus();
			show_stack_bottomright('error_point_notenough',parseInt( $('.myscore').text().replace(/,/g, "") ));
			return false;
		}
		
		useful_modal('<?php echo BASE_HREF; ?>games/confirm/multi/' + setNo + '/' + document.getElementById('tmpSelectPoint_Multi_' + setNo).value,false);
	}
	
	function fnPlaySide_SM(mid,select_team,select_point){

        var Url='http://football.kapook.com/games/playside/insert/'+mid+'/'+select_team+'/'+select_point;
   
        $.ajax({
			url: Url,
			type: "POST",
			dataType: "json",
			success: function(data) {
				if(data.code_id==200){
					
					$('.newplaysingle').attr('id',data.id);
					$('.newplaysingle').find('.col-md-12').empty();
					$('.newplaysingle').find('.col-md-12').append('<span class="status pull-right status_time"></span>');
					
					$('.newplaysingle').find('.status_match').prepend($('<span >')
						.attr('class', 'badge mini_point')
						.text( data.point + ' แต้ม')
					)
					
					$('#divRadio1_new').removeAttr('onclick');
					$('#divRadio1_new').attr('id','divRadio1_' + data.id);
					$('#divRadio2_new').removeAttr('onclick');
					$('#divRadio2_new').attr('id','divRadio2_' + data.id);
					
					$('.newplaysingle').find('.point-select').remove();
					
					var total_single=0;
					$( '.mini_point' ).each(function( index ) {
						total_single	=	total_single + parseInt($(this).text());
					});
					$('.point_single').text( total_single + ' แต้ม' );
					
					$('.myscore').text( number_format(data.point_remain) + ' แต้ม');
					$('.newplaysingle').find('.remove').remove();
					$('.newplaysingle').removeClass( 'blink newplaysingle' );
					
					notice_change(1,0);
					show_stack_bottomright('success_playside','');
					
					if(typeof liveMatchGame_Single[mid] == 'undefined'){
						liveMatchGame_Single[mid]	=	 new Array();
					}
					liveMatchGame_Single[mid].push(data.id);
					
					if($('#game-confirm').length>0){
						$('#game-confirm').carousel(1);
					}
					
					//console.log(liveMatchGame_Single[mid]);
					
				}else if(data.code_id==401){ //Authention
					show_stack_bottomright('error_login','');
					$("a.btnlogin").trigger('click');
					if(clicked==true){
						sidemenu_inactive();
						clicked = false;
					}
					if($('#game-confirm').length>0){
						$('#game-confirm').carousel(2);
					}
				}else{
					show_stack_bottomright('error_m',data.message);
					if($('#game-confirm').length>0){
						$('#game-confirm').carousel(2);
					}
				}
			}
        });
	}
	
	function fnPlaySideMulti_SM(setNo,point){

        var Url='http://football.kapook.com/games/playsidemulti/insert/' + setNo + '/' + point;
   
        $.ajax({
			url: Url,
			type: "POST",
			dataType: "json",
			success: function(data) {
				if(data.code_id==200){
					
					var ca 			= 	document.cookie.split(';');
					var dataCookie	=	"";
					
					var name = "SET" + setNo + "=";
					for(var i=0; i<ca.length; i++) {
						var c = ca[i];
						while (c.charAt(0)==' ')
							c = c.substring(1);
						if (c.indexOf(name) != -1){
							dataCookie = c.substring(name.length,c.length);
							break;
						}
					}
					
					if((dataCookie!="-1")&&(dataCookie.length>0)){
						var ca 			= 	dataCookie.split('|');

						for(var i=0; i<ca.length; i++) {
							var tmpDataMatch = ca[i].split(':');
							if(typeof liveMatchGame_Multi[parseInt(tmpDataMatch[0])] == 'undefined'){
								liveMatchGame_Multi[parseInt(tmpDataMatch[0])]	=	 new Array();
							}
							liveMatchGame_Multi[parseInt(tmpDataMatch[0])].push(setNo);
						}
					}
					//console.log(liveMatchGame_Multi);

					document.cookie = "SET" + setNo + "=-1; path=/;";
					
					$('.multi_play_'+setNo).find('.remove').remove();
					
					$('#dropdown'+setNo).find('.col-md-12').empty();
					$('#dropdown'+setNo).find('.col-md-12').append('<span class="status pull-right status_time"></span>');
					$('#dropdown'+setNo).find('.point-select').remove();
					$('#point_multi_' + setNo).text( point + ' แต้ม' );
					$('.myscore').text( number_format(data.point_remain) + ' แต้ม');
					$('#dropdown'+setNo).find('.col-md-4').removeAttr('onclick');
					$('#dropdown'+setNo).find('.blink').removeClass( 'blink' );
					
					show_stack_bottomright('success_playside','');
					
					notice_change(1,setNo);
					
					if($('#game-confirm').length>0){
						$('#game-confirm').carousel(1);
					}
					
				}else if(data.code_id==401){ //Authention
					show_stack_bottomright('error_login','');
					$("a.btnlogin").trigger('click');
					if(clicked==true){
						sidemenu_inactive();
						clicked = false;
					}
					if($('#game-confirm').length>0){
						$('#game-confirm').carousel(2);
					}
				}else{
					show_stack_bottomright('error_m',data.message);
					if($('#game-confirm').length>0){
						$('#game-confirm').carousel(2);
					}
				}
			}
        });
	}
	
	function notice_change(state,element){
		if( element == 0 ){
			if( state == 1 ){
				if(tmpLimitSinglePlay == $( '.singleplay' ).children('.match_a').length){
					$('#game-single').children('.alert').remove();
				}else{
					$('#notice_single').html('<b><u>คำแนะนำ</u></b><br>- กดปุ่ม <p class="label label-success">ทายบอลเดี่ยว</p> ของคู่ที่ต้องการเล่นเกมส์ทายผลบอลเดี่ยว<br>- ในหนึ่งวัน สามารถเล่นเกมส์ทายผลบอลเดี่ยวได้สูงสุด ' + tmpLimitSinglePlay + ' คู่<br>- <b>เหลือจำนวนคู่ที่สามารถทายผลได้อีก ' + (tmpLimitSinglePlay-$( '.singleplay' ).children('.match_a').length) + ' คู่</b>');
				}
			}
			else if( state == 2 ){
				$('#notice_single').html('<b><u>คำแนะนำ</u></b><br>- คลิกโลโก้ทีมเพื่อเลือกทีมที่ทายผลบอลเดี่ยว');
			}else if( state == 3 ){
				$('#notice_single').html('<b><u>คำแนะนำ</u></b><br>- กรอกแต้มที่ใช้งาน และกดปุ่ม <div class="btn btn-success" style="padding: initial;">ตกลง</div> เพื่อยืนยันการเล่น');
			}
		}else if( state == 1 ){
			$( '#dropdown' + element ).children('.alert').remove();
		}else{
			var headingText		=		'<b><u>คำแนะนำ</u></b><br>';
			var textList		=		new Array();
			var strNotice		=		'';
			var isSelectAll		=		true;
			
			if( $( '.multi_play_' + element ).children('.match_a').length < tmpLimitMultiPlay_MatchMax ){
				textList.push('- กดปุ่ม <p class="label label-primary">ทายบอลชุด</p> ของคู่ที่ต้องการเล่นทายผลบอลชุด<br>');
			}
			
			if( $( '.multi_play_' + element ).children('.match_a').length <= 0){
				textList.push('- ในหนึ่งวัน สามารถเล่นเกมส์ทายผลบอลชุดได้ตั้งแต่ ' + tmpLimitMultiPlay_MatchMin + ' ถึง ' + tmpLimitMultiPlay_MatchMax + ' คู่<br>');
			}else{
				for (var k in tmpSelectTeam_Multi[element]){
					if(tmpSelectTeam_Multi[element][k]==0){
						isSelectAll		=		false;
						break;
					}
				}
				if((isSelectAll)&&($( '.multi_play_' + element ).children('.match_a').length >= tmpLimitMultiPlay_MatchMin)){
					textList.push('- กรอกแต้มที่ใช้งาน และกดปุ่ม <div class="btn btn-success" style="padding: initial;">ตกลง</div> เพื่อยืนยันการเล่น<br>');
				}else{
					textList.push('- เลือกทีมที่จะทายผล ให้ครบทุกคู่ โดยการคลื๊กโลโก้ทีมในแต่ละคู่<br>');
				}
			}
				
			if(
				( $( '.multi_play_' + element ).children('.match_a').length > 0) &&
				( $( '.multi_play_' + element ).children('.match_a').length < tmpLimitMultiPlay_MatchMin )	
			){
				textList.push('- <b>ต้องเลือกคู่การแข่งขันอีก ' + (tmpLimitMultiPlay_MatchMin - $( '.multi_play_' + element ).children('.match_a').length) + ' คู่</b>');
			}else if(
				( $( '.multi_play_' + element ).children('.match_a').length >= tmpLimitMultiPlay_MatchMin ) &&
				( $( '.multi_play_' + element ).children('.match_a').length < tmpLimitMultiPlay_MatchMax )
			){
				textList.push('- <b>เหลือจำนวนคู่ที่สามารถทายผลได้อีก ' + (tmpLimitMultiPlay_MatchMax - $( '.multi_play_' + element ).children('.match_a').length) + ' คู่</b>');
			}
			
			for (var k in textList){
				strNotice	=	strNotice + textList[k];
			}
			
			$('#notice_multi_' + element).html( headingText + strNotice);
		}
	}
	
	$(document).ready(function(){
	<?php
		if($this->isNoti['Monthly']){
		?>
			show_stack_bottomright('success_login_monthly','');
		<?php
		}
		
		if($this->isNoti['Daily']){
		?>
			show_stack_bottomright('success_login_daily','');
		<?php
		}
	?>
	});
	
	/*$(document).click(function() {
		if(clicked==true){
			sidemenu_inactive();
			clicked = false;
		}
	});

	$(".mobile-menu").click(function(event) {
		return false;
	});
	
	$(".link-play-game").click(function(event) {
		return false;
	});*/
	
</script>


<!-- Modal -->
<div class="modal fade style-2"	id="gameModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">

</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->