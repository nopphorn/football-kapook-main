<?php

class Tv_lib {

	var $arrTV		=		array(
		//----------------TV Analog----------------//
		'tv3' 	=> 	'CH3',
		'tv5'	=>	'CH5',
		'tv7'	=>	'CH7',
		'tv9'	=>	'CH9',
		'tv11'	=>	'NBT',
		'tpbs'	=>	'TPBS',
		//----------------TV Digital----------------//
		'tv5hd'			=>	'Ch1 CH5HD',
		'tv11hd'		=>	'Ch2 NBT',
		'tpbshd'		=>	'Ch3 TPBS HD',
		'tv3family'		=> 	'Ch13 3 FAMILY',
		'mcotfamily'	=> 	'Ch14 MCOT-Family',
		'tnn'			=> 	'Ch16 TNN24',
		'newtv'			=>	'Ch18 NEW-tv',
		'springnews'	=> 	'Ch19 SPRING-NEWS',
		'nation'		=> 	'Ch22 NATION-TV',
		'workpointtv' 	=> 	'Ch23 WORKPOINT-TV',
		'true4u' 		=> 	'Ch24 TRUE4U',
		'gmmchannel' 	=> 	'Ch25 GMM',
		'now' 			=> 	'Ch26 NOW',
		'ch8' 			=> 	'Ch27 8',
		'tv3sd' 		=> 	'Ch28 3 SD',
		'mono' 			=> 	'Ch29 MONO-29',
		'mcothd'		=>	'Ch30 MCOT-HD',
		'one' 			=> 	'Ch31 ONE-HD',
		'thairath' 		=> 	'Ch32 THAIRATH-TV',
		'tv3hd'			=>	'Ch33 3-HD',
		'amarin' 		=> 	'Ch34 AMARIN-TV-HD',
		'pptv' 			=> 	'Ch36 PPTV-HD',
		//----------------beIN----------------//
		'bein'					=>	'beIN-SPORTS',
		'bein1'					=>	'beIN-SPORTS-1',
		'bein2'					=>	'beIN-SPORTS-2',
		//----------------CTH----------------//
		'cthstd1'				=> 	'CTH-STADIUM-1',
		'cthstd2'				=> 	'CTH-STADIUM-2',
		'cthstd3'				=> 	'CTH-STADIUM-3',
		'cthstd4'				=>	'CTH-STADIUM-4',
		'cthstd5'				=> 	'CTH-STADIUM-5',
		'cthstd6'				=> 	'CTH-STADIUM-6',
		'cthstdx' 				=> 	'CTH-STADIUM-X',
		//----------------Fox Sport----------------//
		'foxsport' 				=>	'FOX-Sports-HD',
		'foxsport2' 			=>	'FOX-Sports-2',
		'foxsport3' 			=>	'FOX-Sports-3',
		'foxsportplay' 			=>	'FOX-Sports-Play',
		//----------------GMM----------------//
		'gmmclubchannel' 		=>	'GMMCLUBChanel',
		'gmmfootballplus' 		=>	'GMMFootbalPlus',
		'gmmfootballmax'		=>	'GMMFootbalMax',
		'gmmfootballeuro' 		=>	'GMMFootballEuro',
		'gmmfootballextrahd'	=>	'GMMFootballExtra',
		'gmmsportextreme'		=>	'GMMSPORTEXTREME',
		'gmmsportplus'			=>	'GMMSPORTPLUS',
		'gmmsportextra'			=>	'GMMSPORTEXTRA',
		'gmmsportone'			=>	'GMMSPORTONE',
		//----------------RS----------------//
		'sunchannel'			=>	'SUNCHANELLALIGA',
		'worldcupchannel' 		=>	'World Cup channel',
		//----------------True Vision----------------//
		'tshd1'		=>	'TrueSportHD1',
		'tshd2' 	=>	'TrueSportHD2',
		'tshd3' 	=>	'TrueSportHD3',
		'tshd4' 	=>	'TrueSportHD4',
		'ts1' 		=>	'TrueSport 1',
		'ts2' 		=>	'TrueSport 2',
		'ts3' 		=>	'TrueSport 3',
		'ts4' 		=>	'TrueSport 4',
		'ts5' 		=>	'TrueSport 5',
		'ts6' 		=>	'TrueSport 6',
		'ts7' 		=>	'TrueSport 7',
		'tstennis'	=>	'True-Tennis-HD',
		'hayha'		=>	'Hayha (352)',
		'thaithai'	=>	'ThaiThai (358)',
		'true' 		=>	'truevision'
	);

	function getTVText($tvfile) {

		if(array_key_exists($tvfile, $this->arrTV)){
			return $this->arrTV[$tvfile];
		}else{
			return $tvfile;
		}
		
    }

}