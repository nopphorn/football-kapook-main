<?php

/* -- Check : PHP Version -- */
//define('REQUEST_VERSION', 50201);
//$version = PHP_VERSION;
//if ($version{0} * 10000 + $version{2} * 100 + $version{4} < REQUEST_VERSION) {
//    die('not available in versions prior to 5.3.8 ('.PHP_VERSION.')');
//}

class minizone
{	
    var $object = array();
    var $controller = array();
    var $model = array();
    var $config = array();
    var $library = array();
    
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() 
    {
        $this->render_start = array_sum(explode(' ', microtime()));
    }
    
    ///////////////////////////////////////////////// Destruct /////////////////////////////////////////////////
    function __destruct() 
    {
        $this->render_end = array_sum(explode(' ', microtime()));       
//        $html=ob_get_clean();
//        if(is_object($json=@json_decode($html))){
//              $json->dev=array('mem'=>self::memory(),'render'=>number_format($this->render_end-$this->render_start,4)." sec");
//              echo json_encode($json);
//        }else{
//            echo $html;
//            echo "<!--<br /><font color=#000>".$_SERVER['SERVER_ADDR']." memory : ".self::memory()." &nbsp; Query: ".number_format($this->render_end-$this->render_start,4)." sec"."</font>-->";
//        }
    }
    
    ///////////////////////////////////////////////// Getzone /////////////////////////////////////////////////
    function getzone()
    {
        static $zone;
        
        if (is_null($zone)) {
            require_once(dirname(__FILE__).'/../config/config.php');
            
            $zone = new minizone();
            $zone->config = $config;
        }
        
        /* -- Return -- */
        return $zone;
    }
    
    ///////////////////////////////////////////////// Load /////////////////////////////////////////////////
    function load($class, $name='default')
    {
        $key = "load.{$class}.{$name}";
        
        if (empty($this->object[$key])) {
            $filename = dirname(__FILE__)."/{$class}.php";
            
            if (file_exists($filename)) {
                require_once($filename);
                
                if (class_exists($class)) {
                    $this->object[$key] = new $class($name);
                }
            }
        }
        
        /* -- Return -- */
        return $this->object[$key];
    }
    
    ///////////////////////////////////////////////// Controller /////////////////////////////////////////////////
    function controller($name=null, $folder=null) 
    {
        if (empty($name)) return false;
        
        $key = "controller.{$name}";
        
        if (empty($this->controller[$key])) {
            $filename = dirname(__FILE__)."/../controller/{$folder}{$name}.php";
            
            if (file_exists($filename)) {
                require_once($filename);
                
                if (class_exists($name)) {
                    return $this->controller[$key] = new $name;
                }
            }
        }
        
        elseif (is_object($this->controller[$key])) {
            return $this->controller[$key];
        }
        
        return false;
    }
    
    ///////////////////////////////////////////////// Model /////////////////////////////////////////////////
    function model($class=null) 
    {
        if (empty($class)) return false;
        
        /* -- Split : Folder -- */
        $arr_folder = explode('/', $class);
        
        if (count($arr_folder)==1) {
            $class = $arr_folder[0];
        } else {
            $folder = $arr_folder[0].'/';
            $class = $arr_folder[1];
        }
        
        $key = "model.{$folder}{$class}";
        
        if (empty($this->model[$key])) {
            $filename = dirname(__FILE__)."/../model/{$folder}{$class}.php";
            
            if (file_exists($filename)) {
                require_once ($filename);
                
                $class = "{$class}";
                if (class_exists($class)) {
                    return $this->model[$key] = new $class;
                }
            }
        }
        
        elseif (is_object($this->model[$key])) {
            return $this->model[$key];
        }
        
        return false;
    }
    
    ///////////////////////////////////////////////// View /////////////////////////////////////////////////
    function view()
    {
        $tmp = self::load('template');
        $tmp->template_dir = dirname(__FILE__).'/../view/';
//        $tmp->template_dir = 'system/view/';
        $tmp->path($tmp->template_dir);
        
        /* -- Return -- */
        return $tmp;
    }
    
    ///////////////////////////////////////////////// Route /////////////////////////////////////////////////
    function router()
    {
        $tmp = self::load('router');
        
        /* -- Return -- */
        return $tmp;
    }
    
    ///////////////////////////////////////////////// Library /////////////////////////////////////////////////
    function library($class=null, $name='default') 
    {
        if (empty($class)) return false;
        
        /* -- Split : Folder -- */
        $arr_folder = explode('/', $class);
        
        if (count($arr_folder)==1) {
            $class = $arr_folder[0];
        } else {
            $folder = $arr_folder[0].'/';
            $class = $arr_folder[1];
        }
        
        $key = "model.{$folder}{$class}";
        
        if (empty($this->library[$key])) {
            $filename = dirname(__FILE__)."/../library/{$folder}{$class}.php";
            
            if (file_exists($filename)) {
                require_once($filename);
                
                if (class_exists($class)) {
                    return $this->library[$key] = new $class($name);
                }
            }
        } 
        
        elseif (is_object($this->library[$key])) {
            return $this->library[$key];
        }
        
        return false;
    }
    
    ///////////////////////////////////////////////// Memory /////////////////////////////////////////////////
    function memory()
    {
        if (function_exists('memory_get_usage')) return $this->mksize(memory_get_usage());
    }

    ///////////////////////////////////////////////// Mksize /////////////////////////////////////////////////
    function mksize($bytes)
    {
        if ($bytes < 1000 * 1024) return number_format($bytes / 1024, 2).' KB';
        elseif ($bytes < 1000 * 1048576) return number_format($bytes / 1048576, 2).' MB';
        elseif ($bytes < 1000 * 1073741824) return number_format($bytes / 1073741824, 2).' GB';
        else return number_format($bytes / 1099511627776, 2).' TB';
    }
    
    ///////////////////////////////////////////////// Config /////////////////////////////////////////////////
    function config($name, $key=null) 
    {
        if (empty($name)) return false;
        
        if (!$this->config[$name]) {
            $filename = dirname(__FILE__)."/../config/{$name}.php";
            
            if (file_exists($filename)) {
                require($filename);
                $this->config[$name] = $config;
            }
        }
        
        if (!empty($key))
            return $this->config[$name][$key];
        else
            return $this->config[$name];
    }
    
    function encode_ur(&$item1, $key)
    {
        $item1 = rawurldecode($item1);
    }
    
    ///////////////////////////////////////////////// pathsURI /////////////////////////////////////////////////
    function pathsURI() 
    {
        $uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $_SERVER['SCRIPT_NAME'];
        
        if (strstr($uri, '?')) $uri = substr($uri, 0, strpos($uri, '?'));
        
        $uri = substr($uri, strlen('/')-1, strlen($uri));
        $paths = explode('/', substr(substr($uri, -1)=='/' ? substr($uri, 0, -1) : $uri, strlen('/')));
        array_walk($paths, array($this, 'encode_ur'));
        return $paths;
    }
    
    ///////////////////////////////////////////////// Page : Error /////////////////////////////////////////////////
    function showErrorPage($e=null, $l=null)
    {
        $view = self::view();
        $view->assign('error', $e);
        
        $html = $view->render('404');
        
        if ($l) {
            $view->redirect($l, 30);
        }
    }
    
}
/* End of file minizone.php */
/* Location: ./system/core/minizone.php */