<?php

Class Couchbase_Lib extends Couchbase 
{
    var $minizone;
    var $config;

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct($name='default')
    {
        $this->minizone = minizone::getzone();
        $this->config = $this->minizone->config('couchbase', $name);

        parent::__construct("{$this->config['host']}:{$this->config['port']}", $this->config['user'], $this->config['pass'], $this->config['bucket']);
    }
    
}
/* End of file couchbase_Lib.php */
/* Location: ./system/library/couchbase_Lib.php */