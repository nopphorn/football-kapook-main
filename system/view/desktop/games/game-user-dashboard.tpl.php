<?php
	$monthtothai['01']['short'] ="ม.ค.";
	$monthtothai['02']['short'] ="ก.พ.";
	$monthtothai['03']['short'] ="มี.ค.";
	$monthtothai['04']['short'] ="เม.ย.";
	$monthtothai['05']['short'] ="พ.ค.";
	$monthtothai['06']['short'] ="มิ.ย.";
	$monthtothai['07']['short'] ="ก.ค.";
	$monthtothai['08']['short'] ="ส.ค.";
	$monthtothai['09']['short'] ="ก.ย.";
	$monthtothai['10']['short'] ="ต.ค.";
	$monthtothai['11']['short'] ="พ.ย.";
	$monthtothai['12']['short'] ="ธ.ค.";
	
	$monthtothai['01']['full'] ="มกราคม";
	$monthtothai['02']['full'] ="กุมภาพันธ์";
	$monthtothai['03']['full'] ="มีนาคม";
	$monthtothai['04']['full'] ="เมษายน";
	$monthtothai['05']['full'] ="พฤษภาคม";
	$monthtothai['06']['full'] ="มิถุนายน";
	$monthtothai['07']['full'] ="กรกฎาคม";
	$monthtothai['08']['full'] ="สิงหาคม";
	$monthtothai['09']['full'] ="กันยายน";
	$monthtothai['10']['full'] ="ตุลาคม";
	$monthtothai['11']['full'] ="พฤศจิกายน";
	$monthtothai['12']['full'] ="ธันวาคม";
	
	$daytothai['Sun'] = "วันอาทิตย์";
	$daytothai['Mon'] = "วันจันทร์";
	$daytothai['Tue'] = "วันอังคาร";
	$daytothai['Wed'] = "วันพุธ";
	$daytothai['Thu'] = "วันพฤหัสบดี";
	$daytothai['Fri'] = "วันศุกร์";
	$daytothai['Sat'] = "วันเสาร์";

	$OddsRate["0.25"] 	= "เสมอควบครึ่ง";
	$OddsRate["0.5"] 	= "ครึ่งลูก";
	$OddsRate["0.75"] 	= "ครึ่งควบลูก";
	$OddsRate["1"] 		= "หนึ่งลูก";
	$OddsRate["1.25"] 	= "ลูกควบลูกครึ่ง";
	$OddsRate["1.5"] 	= "ลูกครึ่ง";
	$OddsRate["1.75"] 	= "ลูกครึ่งควบสองลูก";
	$OddsRate["2"] 		= "สองลูก";
	$OddsRate["2.25"] 	= "สองลูกควบสองลูกครึ่ง";
	$OddsRate["2.5"] 	= "สองลูกครึ่ง";
	$OddsRate["2.75"] 	= "สองลูกครึ่งควบสามลูก";
	$OddsRate["3"] 		= "สามลูก";
	$OddsRate["3.25"] 	= "สามลูกควบสามลูกครึ่ง";
	$OddsRate["3.5"] 	= "สามลูกครึ่ง";
	$OddsRate["3.75"] 	= "สามลูกครึ่งควบสี่ลูก";
	$OddsRate["4"] 		= "สี่ลูก";
	$OddsRate["4.25"] 	= "สี่ลูกควบสี่ลูกครึ่ง";
	$OddsRate["4.5"] 	= "สี่ลูกครึ่ง";
	$OddsRate["4.75"] 	= "สี่ลูกครึ่งควบห้าลูก";
	$OddsRate["5"] 		= "ห้าลูก";
	$OddsRate["5.25"] 	= "ห้าลูกควบห้าลูกครึ่ง";
	$OddsRate["5.5"] 	= "ห้าลูกครึ่ง";
	$OddsRate["5.75"] 	= "ห้าลูกครึ่งควบหกลูก";
	$OddsRate["6"] 		= "หกลูก";
	$OddsRate["6.25"] 	= "หกลูกควบหกลูกครึ่ง";
	$OddsRate["6.5"] 	= "หกลูกครึ่ง";
	$OddsRate["6.75"] 	= "หกลูกครึ่งควบเจ็ดลูก";
	$OddsRate["7"] 		= "เจ็ดลูก";
	$OddsRate["7.25"] 	= "เจ็ดลูกควบเจ็ดลูกครึ่ง";
	$OddsRate["7.5"] 	= "เจ็ดลูกครึ่ง";
	$OddsRate["7.75"] 	= "เจ็ดลูกครึ่งควบแปดลูก";
	$OddsRate["8"] 		= "แปดลูก";
	$OddsRate["8.25"] 	= "แปดลูกควบแปดลูกครึ่ง";
	$OddsRate["8.5"] 	= "แปดลูกครึ่ง";
	$OddsRate["8.75"] 	= "แปดลูกครึ่งควบเก้าลูก";
	$OddsRate["9"] 		= "เก้าลูก";
	$OddsRate["9.25"] 	= "เก้าลูกควบเก้าลูกครึ่ง";
	$OddsRate["9.5"] 	= "เก้าลูกครึ่ง";
	$OddsRate["9.75"] 	= "เก้าลูกครึ่งควบสิบลูก";
	$OddsRate["10"] 	= "สิบลูก";
	
	//$rewardType[1]		=	'โบนัสล็อกอินครั้งแรก';
	$rewardType[1]		=	'โบนัสล็อกอินรายเดือน';
	$rewardType[2]		=	'โบนัสล็อกอินรายวัน';
	//$rewardType[3]		=	'โบนัสล็อกอินรายเดือน';
			
	$ClassColorByMatchStatus['Fin'] = 'btn-grey';
	$ClassColorByMatchStatus['1 HF'] = 'btn-success';
	$ClassColorByMatchStatus['2 HF'] = 'btn-success';
	$ClassColorByMatchStatus['Sched'] = '';
	$ClassColorByMatchStatus['H/T'] = 'btn-warning';
?>

<div class="user-cover" style="height: 200px; background:#333 url(<?php echo BASE_HREF; ?>uploads/cover_user/default_team_bg.jpg) ; background-size: cover;">
	<div class="container">
		<div class="pull-right"> <a href="#"><img src="http://football.kapook.com/uploads/logo/default.png" alt="..." class="team-logo" height="200" style="padding: 20px;"></a></div>
	</div>
</div>

<div class="container user-dashboard">
  <div class="row">
    <div class="col-md-3 col-lg-3 text_center " align="center"> 
		<div class="game-user-avartar img-circle" title="<?php echo $this->InfoMember['username']; ?>" style="background: url(<?php echo $this->InfoMember['picture']; ?>) no-repeat; background-size: cover;">
		<?php if(uid==$this->InfoMember['uid']) {
			$pageURL = 'http';
			if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
				$pageURL .= "://";
			if ($_SERVER["SERVER_PORT"] != "80") {
				$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
			} else {
				$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			}
		?>
			<div class="options"><a href="http://signup-demo.kapook.com/account/profile?callback=<?php echo $pageURL; ?>#avatar" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-pencil"></i> แก้ใขรูป</a></div>
		<?php
		}
		?>
		</div>
	
		<h3><?php echo $this->InfoMember['username']; ?></h3>
      <!--div class="options"> <a href="javascript:;" class="btn btn-success "><i class="glyphicon glyphicon-plus"></i> เพิ่มเพื่อน</a> </div-->
    </div>
    <div class=" col-md-9 col-lg-9 ">
      <div class="row">
        <div class="col-md-8">
          <table class="table table-user-information ">
			<thead>
				<tr>
					<td colspan="2" class="warning"><div class="rank-badges font-display"><?php
						if($this->InfoMember['total_rate_calculate']<3){
							echo '<span class="rank-badges-rookie"></span> เด็กฝึกหัด ';
						}
						else if($this->InfoMember['total_rate_calculate']<5){
							echo '<span class="rank-badges-wonder-kid "> </span> ดาวรุ่ง ';
						}
						else if($this->InfoMember['total_rate_calculate']<8){
							echo '<span class="rank-badges-pro "></span> เซียน  ';
						}
						else{
							echo '<span class="rank-badges-angle "></span> เทพ ';
						}
						?><a href="#" class="btn btn-info btn-xs"></div><i class="fa fa-info"></i>
					</td>
				</tr>
			</thead>
            <tbody>
			
			<tr>
                <td colspan="2"> 
                  <div class="row">
					<div class="col-md-4"> 
						<div class="hero-widget well well-sm" style="margin-bottom: 0;">
							<a href="<?php echo BASE_HREF; ?>games/ranking" class="tooltip_top" title="" data-original-title="คลิกเพื่อดูอันดูตารางอันดับ">
								<!--<div class="icon"> <i class="glyphicon glyphicon-user"></i> </div> -->
								<div class="text"> <var><?php echo $this->InfoMember['ranking_month'];?></var>
									<label class="text-muted">อันดับเดือนนี้</label>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-4"> 
						<div class="hero-widget well well-sm" style="margin-bottom: 0;">
							<a href="<?php echo BASE_HREF; ?>games/ranking" class="tooltip_top" title="" data-original-title="คลิกเพื่อดูอันดูตารางอันดับ">
							<!--<div class="icon"> <i class="glyphicon glyphicon-user"></i> </div> -->
								<div class="text"> <var><?php
									if( $this->InfoMember['point_month'] > 1000000){
										echo number_format($this->InfoMember['point_month']/1000000,2) . 'M' ;
									}else{
										echo number_format($this->InfoMember['point_month']);
									} ?></var>
									<label class="text-muted">แต้มเดือนนี้</label>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-4"> 
						<div class="hero-widget well well-sm" style="margin-bottom: 0;">
							<a href="<?php echo BASE_HREF; ?>games/ranking" class="tooltip_top" title="" data-original-title="คลิกเพื่อดูอันดูตารางอันดับ">
								<!--<div class="icon"> <i class="glyphicon glyphicon-user"></i> </div> -->
								<div class="text"> <var><?php echo number_format ( $this->InfoMember['total_rate_calculate'] , 1 ); ?></var>
									<label class="text-muted">ความแม่น</label>
								</div>
							</a>
						</div>
					</div>
                  </div>
				</td>
              </tr>
            </tbody>
          </table>
		  <div class="row">
			<div class="col-md-5 text_center">
				<div style="display: block; width: 180px; height: 180px; position: relative; margin: 0 auto;">
					<div id="doughnutChart2" class="chart"></div>
				</div>สถิติวางบอลเดียวเข้าเต็ม
			</div>  
			<div class="col-md-7 text_center">
				<div style="display: block; width: 220px; height: 220px; position: relative; margin: 0 auto;">
					<div id="doughnutChart" class="chart"></div>
				</div>สถิติวางบอลชุดได้แต้ม
			</div>
          </div>
                 
		  <link rel="stylesheet" type="text/css" href="<?php echo BASE_HREF; ?>assets/plugins/donut/donut.css">
		  <script type="text/javascript" src="<?php echo BASE_HREF; ?>assets/plugins/donut/donut.js"></script>
		  <script type="">
				$(document).ready(function(){  
					$(function(){
						$("#doughnutChart").drawDoughnutChart([
							<?php if($this->InfoMember['total_playside_multi_win']>0){?>
								{ title: "เข้าเต็ม",			value : <?php echo intval($this->InfoMember['total_playside_multi_win']); ?>,			color: "#6DB600" },
							<?php } ?>
							
							<?php if($this->InfoMember['total_playside_multi_draw']>0){?>
								{ title: "เสมอ",			value : <?php echo intval($this->InfoMember['total_playside_multi_draw']); ?>,			color: "#CCC" },
							<?php } ?>
							
							<?php if($this->InfoMember['total_playside_multi_lose_full']>0){?>
								{ title: "เสีย",			value : <?php echo intval($this->InfoMember['total_playside_multi_lose_full']); ?>,			color: "#FF0000" }
							<?php } ?>
						],{total:"<?php echo number_format($this->InfoMember['total_playside_multi_win_percent'],1); ?>%"});
					});
					
					$(function(){
						$("#doughnutChart2").drawDoughnutChart([
							<?php if($this->InfoMember['total_playside_single_win_full']>0){?>
								{ title: "ได้เต็ม",			value : <?php echo intval($this->InfoMember['total_playside_single_win_full']); ?>,		color: "#6DB600" },
							<?php } ?>
							
							<?php if($this->InfoMember['total_playside_single_win_half']>0){?>
								{ title: "ได้ครึ่ง",			value : <?php echo intval($this->InfoMember['total_playside_single_win_half']); ?>,		color: "#b8e54d" },
							<?php } ?>
							
							<?php if($this->InfoMember['total_playside_single_draw']>0){?>
								{ title: "เสมอ",			value : <?php echo intval($this->InfoMember['total_playside_single_draw']); ?>,			color: "#CCC" },
							<?php } ?>
							
							<?php if($this->InfoMember['total_playside_single_lose_half']>0){?>
								{ title: "เสียครึ่ง",			value : <?php echo intval($this->InfoMember['total_playside_single_lose_half']); ?>,	color: "#fe9900" },
							<?php } ?>
							
							<?php if($this->InfoMember['total_playside_single_lose_full']>0){?>
								{ title: "เสียเต็ม",			value : <?php echo intval($this->InfoMember['total_playside_single_lose_full']); ?>,	color: "#FF0000" }
							<?php } ?>
						],{total:"<?php echo number_format($this->InfoMember['total_playside_single_win_percent'],1); ?>%"});
					});
				}); 
		  </script>
        </div>
        <div class="col-md-4">
         <div class="sidebar-social">
                 <div class="share share_size_large share_type_facebook">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="">Share</a>
                </div>
                 <div class="share share_size_large share_type_twitter">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="">Tweet</a>
                </div>
               
                <div class="share share_size_large share_type_gplus">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="">+1</a>
                </div>
                
                <div class="share share_size_large share_type_email">
                    <span class="share__count">0</span>
                    <a class="share__btn" href="">E-mail</a>
                </div>
                
                

                
		</div>
        
        
        <?php /*<div class="trophy">ผลงานและถ้วยรางวัล : <br><span class="label label-warning"><i class="glyphicon glyphicon-tower" ></i> Winner 2008</span> <span class="label label-primary"><i class="fa fa-futbol-o" ></i> 2nd 2010</span> <span class="label label-success"><i class="glyphicon glyphicon-tower"></i> King of Steps 2009</span></div> */ ?>
		
		</div>
      </div>
      <div class="row">
        <!--div class="col-sm-3">
          <div class="hero-widget well well-sm">
            <div class="icon"> <i class="glyphicon glyphicon-user"></i> </div>
            <div class="text"> <var>N/A</var>
              <label class="text-muted">เพื่อน</label>
            </div>
          </div>
        </div-->
        <!--div class="col-sm-3">
          <div class="hero-widget well well-sm">
            <div class="icon"> <i class="glyphicon glyphicon-tags"></i> </div>
            <div class="text"> <var>N/A</var>
              <label class="text-muted">ผู้ติดตามผลงาน</label>
            </div>
          </div>
        </div-->
      </div>
    </div>
  </div>
  
  
	<!--div class="alert alert-danger alert-dismissible fade in" role="alert" style="margin:40px 0px">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>แจ้งให้ทราบ!</strong> จากปัญหาการอัพเดทผลการแข่งขัน และคะแนนทายผลบอลเมื่อวันที่ 15 พ.ค. และ 17 พ.ค. ที่ผ่านมา ทีมงานได้ทำการแก้ไขคะแนนแล้ว หากพบว่าคะแนนของท่านยังไม่ถูกต้อง กรุณาแจ้งมาที่ <a href="mailto:football@kapook.com">football@kapook.com</a> หรือ <a href="http://facebook.com/KapookFootball">facebook.com/KapookFootball</a>
    </div-->

  
	<div class="game-head-sep">
		<a href="<?php echo BASE_HREF; ?>games" class="btn-link">เข้าสู่หน้าเกม <i class="fa fa-arrow-circle-right"></i></a>
		<a href="<?php echo BASE_HREF; ?>games" class="game-logo"><div class="game-logo-season">พรีซีซั่น 2014-15</div></a>
    </div>
	<ul class="nav nav-tabs" role="tablist">
		<?php
			$size=count($this->DateProgram);
			for( $i_tab = $size-1 ; $i_tab >= 0 ; $i_tab-- )
			{
				$tmpDateArr = explode('-',$this->DateProgram[$i_tab]);
				if($i_tab==1)// active in yesterday
					echo '<li class="active"><a href="#d' . $i_tab . '" role="tab" data-toggle="tab">' . intval($tmpDateArr[2]) . ' ' . $monthtothai[$tmpDateArr[1]]['short'] . ' ' . ($tmpDateArr[0]+543) . '</a></li>';
				else
					echo '<li><a href="#d' . $i_tab . '" role="tab" data-toggle="tab">' . intval($tmpDateArr[2]) . ' ' . $monthtothai[$tmpDateArr[1]]['short'] . ' ' . ($tmpDateArr[0]+543) . '</a></li>';
			}
		?>
	</ul>
	
	<div class="tab-content">
		<?php
			$size=count($this->DateProgram);
			for( $i_tab = 0 ; $i_tab < $size ; $i_tab++ )
			{
				$tmpDateArr = explode('-',$this->DateProgram[$i_tab]);
				
				$size_game					=	$this->listGameSide[$i_tab]['total_game'];
				$tmpListGamePlayside		=	$this->listGameSide[$i_tab]['game_info'];
				$tmpListReward				=	$this->listReward[$i_tab]['list'];
				
				// Single Playside
				$tmpSinglePlaySide			=	$tmpListGamePlayside[0];
				$size_single_game			=	count($tmpSinglePlaySide['list']);
				
				// Multi Playside
				$size_multi_game			=	count($tmpListGamePlayside);
				
				// Reward
				$size_reward				=	count($tmpListReward);
				//echo $size_reward;
				$tmpTotalPoint				=	($this->listReward[$i_tab]['total_point']+$tmpSinglePlaySide['point_return']);
				foreach( $tmpListGamePlayside as $tmpKeyPlayside => $tmpPlaySide ){
					if($tmpKeyPlayside!=0){
						$tmpTotalPoint		=	$tmpTotalPoint + $tmpPlaySide['point_return'];
					}
				}
				?>
				<div class="tab-pane <?php if($i_tab==1){ echo 'active'; } ?>" id="d<?php echo $i_tab; ?>" >
					<div class="daily-sumary">รวมแต้มวันนี้ได้ <?php
					if($tmpTotalPoint>0){
						echo '<span  style="color: green;">+ '. number_format($tmpTotalPoint) .'</span>';
					}else if($tmpTotalPoint<0){
						echo '<span  style="color: red;">- '. number_format(abs($tmpTotalPoint)).'</span>';
					}else{
						echo '<span  style="color: gray;">0</span>';
					}
					?> แต้ม</div>
					<h2 class="font-display">โบนัสจากระบบ วันที่ <?php echo intval($tmpDateArr[2]) . ' ' . $monthtothai[$tmpDateArr[1]]['short'] . ' ' . ($tmpDateArr[0]+543); ?></h2>
					<div class="row">
						<div class="col-md-12">
							<!-- Tab panes -->
							<table class="table  boder-1">
								<?php if($size_reward > 0){?>
								<tbody>
									<tr>
										<td  class="table_tr_head " style="width: 90%;">รายการ</td>
										<td class="table_tr_head">แต้ม</td>
									</tr>
									<?php for( $i_game=0 ; $i_game<$size_reward ; $i_game++ ){
										$GameInfo	=	$tmpListReward[$i_game];
										if($GameInfo['type']!=3){
									?>
									<tr>
										<td><i class="fa fa-check " style="color:green"></i> <?php echo $rewardType[$GameInfo['type']]; ?></td>
										<td><?php
											if($GameInfo['value']>0){
												echo '<i class=" " style="color:green"> +'. $GameInfo['value'] .' </i>';
											}else if($GameInfo['value']<0){
												echo '<i class=" " style="color:red"> '. $GameInfo['value'] .' </i>';
											}else{
												echo '<i class=" " style="color:grey"> 0 </i>';
											}
										?></td>
									</tr>
									<?php
										}
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<td align="right">รวมได้แต้มโบนัส</td>
										<td><?php
											if($this->listReward[$i_tab]['total_point']>0){
												echo '<i class=" " style="color:green; font-weight: bold;"> +'.$this->listReward[$i_tab]['total_point'].' </i>';
											}else if($this->listReward[$i_tab]['total_point']<0){
												echo '<i class=" " style="color:red; font-weight: bold;"> '.$this->listReward[$i_tab]['total_point'].' </i>';
											}else{
												echo '<i class=" " style="color:grey; font-weight: bold;"> '.$this->listReward[$i_tab]['total_point'].' </i>';
											}
										?></td>
									</tr>
								</tfoot>
							<?php }else{
							?>
								<tbody>
									<tr>
										<td colspan="7"><center><i class=" " style="color:grey;">ไม่มีโบนัสแต้ม</i></center></td>
									</tr>
								</tbody>
							<?php
							}?>
							</table>
						</div>
					</div>
					<h2 class="font-display">สรุปผล วางบอลเดี่ยว วันที่ <?php echo intval($tmpDateArr[2]) . ' ' . $monthtothai[$tmpDateArr[1]]['short'] . ' ' . ($tmpDateArr[0]+543); ?></h2>
					<div class="row">
						<div class="col-md-12"> 
							<table class="table  boder-1">
								<?php if($size_single_game > 0){?>
								<tbody>
								<tr>
									<td colspan="5" class="table_tr_head">เวลา</td>
									<td class="table_tr_head">คุณเลือก</td>
									<td class="table_tr_head">วาง</td>
									<td class="table_tr_head">ผลวางบอล</td>
									<td class="table_tr_head">ได้</td>
								</tr>
								<?php
								for( $i_game=0 ; $i_game<$size_single_game ; $i_game++ ){
									$GameInfo	=	$tmpSinglePlaySide['list'][$i_game];
								?>
								<tr>
									<td><?php echo substr( $GameInfo['MatchDateTime'] , 11 ,5  ) ?></td>
									<td class="text_right">
										<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$GameInfo['Team1KPID']))); ?>" target="_blank" class="tooltip_top match <?php if(($GameInfo['TeamOdds']==0 || $GameInfo['TeamOdds']==1)&&($GameInfo['Odds']!=-1)){ echo 'handicap_color'; } ?>" title="คลิกเพื่อเข้าดูรายละเอียดทีม">
											<span><?php echo $GameInfo['Team1']; ?> <img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $GameInfo['Team1Logo']); ?>"  width="24"></span>
										</a>
									</td>
									<td class="text_center">
										<?php if($GameInfo['MatchStatus']=='Sched'){?>
											<a href="<?php echo strtolower($GameInfo['MatchPageURL']); ?>" target="_blank"   class="btn btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">vs</a>
										<?php }
										else if($GameInfo['MatchStatus']=='Fin'){?>
											<a href="<?php echo strtolower($GameInfo['MatchPageURL']); ?>" target="_blank"   class="btn btn-grey btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php echo $GameInfo['Team1FTScore']; ?> - <?php echo $GameInfo['Team2FTScore']; ?></a>
										<?php }
										else if($GameInfo['MatchStatus']=='Canc'){?>
											<a href="<?php echo strtolower($GameInfo['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">c - c</a>
										<?php }
										else if($GameInfo['MatchStatus']=='Post'){?>
											<a href="<?php echo strtolower($GameInfo['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">p - p</a>
										<?php }
										else{?>
											<a href="<?php echo strtolower($GameInfo['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"> n/a </a>
										<?php }?>
									</td>
									<td class="text_left">
										<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$GameInfo['Team2KPID']))); ?>" target="_blank" class="tooltip_top match <?php if(($GameInfo['TeamOdds']==0 || $GameInfo['TeamOdds']==2)&&($GameInfo['Odds']!=-1)){ echo 'handicap_color'; } ?>" title="คลิกเพื่อเข้าดูรายละเอียดทีม">
											<span><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $GameInfo['Team2Logo']); ?>"  width="24"> <?php echo $GameInfo['Team2']; ?></span>
										</a>
									</td>
									<td><span class="tooltip_top" title="ราคาต่อรอง"><?php 
										if($GameInfo['Odds']==0){   
											echo '(เสมอ)';                   
										}else if($GameInfo['Odds']!=-1){
											$OddsArr = explode('.',$GameInfo['Odds']);
											if($OddsArr[0]>0){
												for($o=1;$o<=$OddsArr[0];$o++){
													echo '<pie class="hundreds"></pie>';
												}
											}
															   
											if($OddsArr[1]=='25'){
												echo '<pie class="twentyfive"></pie>';
											}else if($OddsArr[1]=='5'){
												echo '<pie class="fifty"></pie>';
											}else if($OddsArr[1]=='75'){
												echo '<pie class="seventyfive"></pie>';
											}
															   
											echo ' ('.$GameInfo['Odds'].')';
															
										}
										?>
										</span>
									</td>
									<td><a href="/team-<?php
										if($GameInfo['team_win']==1){
											echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$GameInfo['Team1KPID'])));
										}else{
											echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$GameInfo['Team2KPID'])));
										}?>" class="tooltip_top match handicap_color" title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><?php
										if($GameInfo['team_win']==1){
											echo '<img src="'.str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $GameInfo['Team1Logo']).'"  width="24"> '.$GameInfo['Team1'];
										}else{
											echo '<img src="'.str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $GameInfo['Team2Logo']).'"  width="24"> '.$GameInfo['Team2'];
										}?>
										 </span></a>
									</td>
									<td><?php
										echo $GameInfo['point'];
									?></td>
									<td><?php
										if($GameInfo['isCorrect']>0){
											echo '<i class="fa fa-check " style="color:green"></i>';
										}else if($GameInfo['isCorrect']<0){
											echo '<i class="fa fa-close " style="color:red"></i>';
										}else{
											echo '<i class="fa fa-minus " style="color:gray"></i>';
										}
									?></td>
									<td><?php
										if($GameInfo['MatchStatus']=='Fin')
										{
											if($GameInfo['point_return']>0){
												echo '<i class=" " style="color:green"> +'. $GameInfo['point_return'] .' </i>';
											}else if($GameInfo['point_return']<0){
												echo '<i class=" " style="color:red"> '. $GameInfo['point_return'] .' </i>';
											}else{
												echo '<i class=" " style="color:grey"> 0 </i>';
											}
										}else if(($GameInfo['MatchStatus']=='Post')||($GameInfo['MatchStatus']=='Canc')){
											echo '<i class="fa fa-minus " style="color:gray">(เลื่อนการแข่งขัน)</i>';
										}else{
											echo '<i style="color:grey">รอผลการแข่งขัน</i>';
										}
									?></td>
								</tr><?php
								}?>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="7"></td>
										<td colspan="1">รวมได้แต้ม</td>
										<td><?php
											if($tmpSinglePlaySide['point_return']>0){
												echo '<i class=" " style="color:green; font-weight: bold;"> +'.$tmpSinglePlaySide['point_return'].' </i>';
											}else if($tmpSinglePlaySide['point_return']<0){
												echo '<i class=" " style="color:red; font-weight: bold;"> '.$tmpSinglePlaySide['point_return'].' </i>';
											}else{
												echo '<i class=" " style="color:grey; font-weight: bold;"> '.$tmpSinglePlaySide['point_return'].' </i>';
											}
										?></td>
									</tr>
								</tfoot>
								<?php }else
									{
									?>
										<tbody>
											<tr>
												<td colspan="7"><center><i class=" " style="color:grey;">ไม่มีการวางบอลเดี่ยว</i></center></td>
											</tr>
										</tbody>
									<?php
									}?>
							</table>
						</div>
					</div>
					
					<?php 
					foreach( $tmpListGamePlayside as $tmpKeyPlayside => $tmpPlaySide ){
						if($tmpKeyPlayside != 0){
							$size_game			=	count($tmpPlaySide['list']);
							?>
							<h2 class="font-display">สรุปผล <?php /*วางบอลชุดที่ <?php echo $tmpKeyPlayside; */?>วางบอลชุด วันที่ <?php echo intval($tmpDateArr[2]) . ' ' . $monthtothai[$tmpDateArr[1]]['short'] . ' ' . ($tmpDateArr[0]+543); ?></h2>
							<div class="row">
								<div class="col-md-12">
									<table class="table  boder-1">
										<?php if($size_game > 0){?>
										<tbody>
										<tr>
											<td colspan="5" class="table_tr_head">เวลา</td>
											<td colspan="2" class="table_tr_head">คุณเลือก</td>
											<td colspan="2"class="table_tr_head">ผลวางบอล</td>
										</tr>
										<?php for( $i_game=0 ; $i_game<$size_game ; $i_game++ ){
											$GameInfo	=	$tmpPlaySide['list'][$i_game];
										?>
										<tr>
											<td><?php echo substr( $GameInfo['MatchDateTime'] , 11 ,5  ) ?></td>
											<td class="text_right">
												<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$GameInfo['Team1KPID']))); ?>" target="_blank" class="tooltip_top match <?php if(($GameInfo['TeamOdds']==0 || $GameInfo['TeamOdds']==1)&&($GameInfo['Odds']!=-1)){ echo 'handicap_color'; } ?>" title="คลิกเพื่อเข้าดูรายละเอียดทีม">
													<span><?php echo $GameInfo['Team1']; ?> <img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $GameInfo['Team1Logo']); ?>"  width="24"></span>
												</a>
											</td>
											<td class="text_center">
												<?php if($GameInfo['MatchStatus']=='Sched'){?>
													<a href="<?php echo strtolower($GameInfo['MatchPageURL']); ?>" target="_blank"   class="btn btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">vs</a>
												<?php }
												else if($GameInfo['MatchStatus']=='Fin'){?>
													<a href="<?php echo strtolower($GameInfo['MatchPageURL']); ?>" target="_blank"   class="btn btn-grey btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"><?php echo $GameInfo['Team1FTScore']; ?> - <?php echo $GameInfo['Team2FTScore']; ?></a>
												<?php }
												else if($GameInfo['MatchStatus']=='Canc'){?>
													<a href="<?php echo strtolower($GameInfo['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">c - c</a>
												<?php }
												else if($GameInfo['MatchStatus']=='Post'){?>
													<a href="<?php echo strtolower($GameInfo['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้">p - p</a>
												<?php }
												else{?>
													<a href="<?php echo strtolower($GameInfo['MatchPageURL']); ?>" target="_blank"   class="btn btn-danger btn-xs tooltip_top"  title="คลิกเพื่อเข้าดูรายละเอียดคู่นี้"> n/a </a>
												<?php }?>
											</td>
											<td class="text_left">
												<a href="/team-<?php echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$GameInfo['Team2KPID']))); ?>" target="_blank" class="tooltip_top match <?php if(($GameInfo['TeamOdds']==0 || $GameInfo['TeamOdds']==2)&&($GameInfo['Odds']!=-1)){ echo 'handicap_color'; } ?>" title="คลิกเพื่อเข้าดูรายละเอียดทีม">
													<span><img src="<?php echo str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $GameInfo['Team2Logo']); ?>"  width="24"> <?php echo $GameInfo['Team2']; ?></span>
												</a>
											</td>
											<td><span class="tooltip_top" title="ราคาต่อรอง"><?php 
												if($GameInfo['Odds']==0){   
													echo '(เสมอ)';                   
												}else if($GameInfo['Odds']!=-1){
													$OddsArr = explode('.',$GameInfo['Odds']);
													if($OddsArr[0]>0){
														for($o=1;$o<=$OddsArr[0];$o++){
															echo '<pie class="hundreds"></pie>';
														}
													}
													if($OddsArr[1]=='25'){
														echo '<pie class="twentyfive"></pie>';
													}else if($OddsArr[1]=='5'){
														echo '<pie class="fifty"></pie>';
													}else if($OddsArr[1]=='75'){
														echo '<pie class="seventyfive"></pie>';
													}
																	   
													echo ' ('.$GameInfo['Odds'].')';
																	
												}
												?>
												</span>
											</td>
											<td colspan="2"><a href="/team-<?php
												if($GameInfo['team_win']==1){
													echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$GameInfo['Team1KPID'])));
												}else{
													echo strtolower(str_replace(" ","-",$this->mem_lib->get('Football2014-Team-NameEN-'.$GameInfo['Team2KPID'])));
												}?>" class="tooltip_top match handicap_color" title="คลิกเพื่อเข้าดูรายละเอียดทีม"><span><?php
												if($GameInfo['team_win']==1){
													echo '<img src="'.str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $GameInfo['Team1Logo']).'"  width="24"> '.$GameInfo['Team1'];
												}else{
													echo '<img src="'.str_replace("football.kapook.com", "fb.thaibuffer.com/r/24/h", $GameInfo['Team2Logo']).'"  width="24"> '.$GameInfo['Team2'];
												}?>
												 </span></a>
											</td>
											<td colspan="2"><?php
												if($GameInfo['MatchStatus']=='Fin')
												{
													if($GameInfo['isCorrect']>0){
														echo '<i class="fa fa-check " style="color:green"></i>';
													}else if($GameInfo['isCorrect']<0){
														echo '<i class="fa fa-close " style="color:red"></i>';
													}else{
														echo '<i class="fa fa-minus " style="color:gray"></i>';
													}
												}else if(($GameInfo['MatchStatus']=='Post')||($GameInfo['MatchStatus']=='Canc')){
													echo '<i class="fa fa-minus " style="color:gray">(เลื่อนการแข่งขัน)</i>';
												}else{
													echo '<i style="color:grey">รอผลการแข่งขัน</i>';
												}
											?></td>
										</tr><?php
										}?>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="5"></td>
												<td colspan="1">วางแต้ม</td>
												<td><?php echo $tmpPlaySide['point']; ?></td>
												<td colspan="1">แต้มที่ได้ <?php
													if($tmpPlaySide['multiple']>1){
														echo '<i class=" " style="color:green; font-weight: bold;">(x'.$tmpPlaySide['multiple'].')</i>';
													}else if($tmpPlaySide['multiple']<1){
														echo '<i class=" " style="color:red; font-weight: bold;">(x'.$tmpPlaySide['multiple'].')</i>';
													}else{
														echo '<i class=" " style="color:grey; font-weight: bold;">(x'.$tmpPlaySide['multiple'].')</i>';
													}
												?></td>
												<td style="text-align: right;"><?php
													echo '<i class=" " style="color:grey; font-weight: bold;"> '.($tmpPlaySide['point_return']+$tmpPlaySide['point']).' </i>';
												?></td>
											</tr>
											<tr>
												<td colspan="7"></td>
												<td colspan="1">สรุปผลการวางบอลชุดนี้</td>
												<td style="text-align: right;"><?php
													if($tmpPlaySide['point_return']>0){
														echo '<i class=" " style="color:green; font-weight: bold;"> +'.$tmpPlaySide['point_return'].' </i>';
													}else if($tmpPlaySide['point_return']<0){
														echo '<i class=" " style="color:red; font-weight: bold;"> '.$tmpPlaySide['point_return'].' </i>';
													}else{
														echo '<i class=" " style="color:grey; font-weight: bold;"> '.$tmpPlaySide['point_return'].' </i>';
													}
												?></td>
											</tr>
										</tfoot>
										<?php }else
											{
											?>
												<tbody>
													<tr>
														<td colspan="7"><center><i class=" " style="color:grey;">ไม่มีการวางบอลเดี่ยว</i></center></td>
													</tr>
												</tbody>
											<?php
											}?>
									</table>
								</div>
							</div>
						<?php }
					} ?>
				</div><?php
			}
		?>
	</div>
	
	<?php 
	$NewsArr = $this->NewsLeagueContents;
	if(sizeof($NewsArr) > 0){ 
	?>
	<div class="row subnews">
				<h2 class="font-display"><i class="fa fa-file-text-o"></i> ข่าวที่น่าสนใจสำหรับคุณ</h2> 
				<?php 
					for($i=0;$i<4;$i++) { ?>
				<div class="news_block  col-md-3" ><a class="" href="<?php echo $NewsArr[$i]['link']?>" target="_blank"><img class="media-object" alt="" src="<?php echo $NewsArr[$i]['picture']?>" ><?php echo $NewsArr[$i]['subject']?> </a></div>
				<?php } ?>

	</div>
	<?php } ?>   
</div>