<?php if (!defined('MINIZONE')) exit;

class Notpage extends My_con 
{
    var $view; 
    var $minizone; 
    
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() 
    {
        parent::__construct();
        
//         $this->authen();
        /* -- Load : Library  -- */
        
       /* -- Load : Model -- */
        
        /* -- ========= View ========= -- */
        
    }
    
   ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
    function index() {
       header('HTTP/1.1 404 Not Found');
        /* -- Load : Template (s) -- */
        $msg['type'] = '1'; // 404
        $msg['title'] = 'ขออภัยค่ะ';
        $msg['msg'] = 'ไม่พบหน้าเว็บไซต์ที่คุณต้องการ <br> กรุณาตรวจสอบ URL ของหน้าเว็บไซต์ว่าถูกต้องหรือไม่ ';
        $msg['desc'] = '';
        $msg['img'] = 'after_post404.gif';
        
        $this->_blank($msg, FALSE, 'header_control.tpl');
                
	
    }
	

    
	
}

