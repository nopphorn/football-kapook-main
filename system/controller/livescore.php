<?php if (!defined('MINIZONE')) exit;

class Livescore extends My_con 
{
    var $view; 
    var $minizone; 
    
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() 
    {

        parent::__construct();

        $this->news_model = $this->minizone->model('news_model');
		$this->squad_model = $this->minizone->model('squad_model');

		if(!isset($this->livescore_model)){
			$this->livescore_model = $this->minizone->model('livescore_model');
		}
		
		/*-------------REDIS----------------*/
		$this->redis_server = '192.168.1.78-6379-9';
		$this->redis = $this->minizone->library('Predis');
		$this->redis->setConfig($redis_server);

    }
    
   ///////////////////////////////////////////////// Index /////////////////////////////////////////////////
    function index() {
        $this->view->assign('mem_lib', $this->mem_lib);
        
		if(intval(date('G'))<6){
			$LoadDate 					= 		date('Y-m-d',strtotime("-1 days"));
		}else{
			$LoadDate 					= 		date('Y-m-d');
		}
		$LiveScore_MC 		= 		$this->mem_lib->get('Football2014-LiveScore-' . $LoadDate);
		$expire				=		600;
		
		if($LiveScore_MC){
			$ProgramContents1 	=	$LiveScore_MC;
			
			if($_REQUEST['pong_debug']==1){
				var_dump($ProgramContents1);
				echo 'Yep!!!';
			}
		}else{
			$ProgramContents1 	= 		$this->livescore_model->getProgramByDate($LoadDate);
			$ProgramContents1['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $LoadDate, $ProgramContents1, MEMCACHE_COMPRESSED, $expire);
		}
        
        $this->view->assign('ProgramContents1',$ProgramContents1);
		
		$this->view->assign('tv_lib', $this->minizone->library('tv_lib'));

        $breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = 'ผลบอลสด';
        $breadcrum[1]['active'] = true;
        parent::_setBreadcrum($breadcrum);
		
		$data['seo_title'] = "ผลบอลสด livescore ผลบอลสดวันนี้ เช็คผลบอลสด รายงานผลบอลล่าสุด";
		$data['seo_description'] = "ผลบอลสด livescore ดู ผลบอลสดวันนี้ เช็คผลบอลสด ผลบอลสดมีเสียง รายงานผลบอลสด อัพเดทสกอร์ ผลบอลสดภาษาไทย ผลบอลสดมีเสียง ผลบอลสดทุกลีก คลิกเลย";
		$data['seo_keywords'] = "ผลบอลสด, livescore, ผลบอลสดวันนี้, เช็คผลบอลสด, ผลบอลสดมีเสียง, รายงานผลบอลสด, ดูผลบอลสด, ผลบอลสดวันนี้, ผลบอลสดภาษาไทย";

		$data['fb_title'] = "ผลบอลสด livescore ผลบอลสดวันนี้ รายงานผลบอลล่าสุด";
		$data['fb_description'] = "ผลบอลสด livescore ผลบอลสดวันนี้ เช็คผลบอลสด ผลบอลสดภาษาไทย ผลบอลสดทุกลีก";
        //$data['fb_img'] = 'http://football.kapook.com/assets/og-football/set1/livescore.jpg';
		$data['fb_img'] = 'http://football.kapook.com/assets/og/2014-11/page/livescore.jpg';
		parent::_setSocial($data);		

        $this->_header();
		//if($_GET['pong_debug']!='1'){
		//	$this->view->render($this->root_view.'/livescore/livescore.tpl');
		//}else{
			$this->view->render($this->root_view.'/livescore/livescore_new.tpl');
		//}
        $this->_footer();
    }
	
    function analyze() {
    //var_dump($this->mem_lib);
		//$this->mem_lib->delete('Football2014-MatchAnalysis');
		if(date('G')>=6){
			$dateAnalyze	=	date("Y-m-d");
		}else{
			$dateAnalyze	=	date("Y-m-d",strtotime('-1 day'));
		}
		$MatchAnalysis_MC = $this->mem_lib->get('Football2014-MatchAnalysis-' . $dateAnalyze );
		if($_REQUEST['tbug']==1){
			var_dump($MatchAnalysis_MC);
		}
		if($MatchAnalysis_MC && ($_REQUEST['remove_cache']!=1)){
            $MatchAnalyzeContensByDate = $MatchAnalysis_MC;            
        }else{   
			$MatchContents = $this->livescore_model->getMatchAnalysis();
			//echo "<pre>"; var_dump($MatchContents); echo "</pre>";
			for($i=1;$i<=$MatchContents['TotalMatch'];$i++){				
				if(strval($MatchContents[$i]['Analysis']) != '' || strval($MatchContents[$i]['Predict']) != ''){
					$MatchDateTime = $MatchContents[$i]['MatchDate'].' '.$MatchContents[$i]['MatchTime'].':00';
					if($MatchDateTime<date("Y-m-d 06:00:00")){
						$MatchAnalyzeContensByDate['Yesterday'][] = $MatchContents[$i];
					}else{
						$MatchAnalyzeContensByDate['Today'][] = $MatchContents[$i];
					}	
				}
			}
			$expire = (3600*1);
			$this->mem_lib->set('Football2014-MatchAnalysis-' . $dateAnalyze ,$MatchAnalyzeContensByDate,0,$expire);
		}
        $this->view->assign('MatchContents',$MatchAnalyzeContensByDate);
		
		$this->view->assign('tv_lib', $this->minizone->library('tv_lib'));
            
        $breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = 'วิเคราะห์บอล';
        $breadcrum[1]['active'] = true;
        parent::_setBreadcrum($breadcrum);
		
		$data['seo_title'] = "วิเคราะห์บอล วิเคราะห์บอลวันนี้ ทีเด็ด วิเคราะห์บอลแม่นๆ คลิกเลย";
		$data['seo_description'] = "วิเคราะห์บอล วิเคราะห์บอลวันนี้ ทีเด็ด แม่นๆ เช็คราคาบอล ราคาบอลคืนนี้ ราคาบอลล่าสุด วิเคราะห์บอลคืนนี้ ทีเด็ด วิเคราะห์ ฟันธง วิเคราะห์บอลวันนี้ทุกคู่ วิเคราะห์ผลบอล คลิกเลย";
		$data['seo_keywords'] = "วิเคราะห์บอล, วิเคราะห์บอลวันนี้, ทีเด็ด, ราคาบอล, ราคาบอลคืนนี้, ราคาบอลไหล, ราคาบอลล่าสุด, วิเคราะห์บอลคืนนี้, วิเคราะห์บอลวันนี้ทุกคู่, วิเคราะห์ผลบอล";

		$data['fb_title'] = "วิเคราะห์บอล ทีเด็ดฟุตบอล วิเคราะห์บอลแม่นๆ คลิกเลย";
		$data['fb_description'] = "วิเคราะห์บอล วิเคราะห์บอลวันนี้ ทีเด็ด ฟันธง แม่นๆ เช็คราคาบอล อัตราต่อรองล่าสุด";
        
        //$data['fb_img'] = 'http://football.kapook.com/assets/og-football/set1/วิเคราะห์บอล.jpg';
		$data['fb_img'] = 'http://football.kapook.com/assets/og/2014-11/page/วิเคราะห์บอล.jpg';
		parent::_setSocial($data);
		
		

        $this->_header();
		$this->view->render($this->root_view.'/home/analyze.tpl');
		$this->_footer();
    }
        
		
    
    function analyze_match($MatchID) {
		$MatchContents = $this->mem_lib->get('Football2014-MatchContents-'.$id);
		if((!$MatchContents) || ($_REQUEST['remove_cache']==1)){
			$MatchContents = $this->livescore_model->getLiveScoreMatch($MatchID);
		}
        $this->view->assign('MatchTmp',$MatchContents["MatchInfo"]);
		$this->view->assign('tv_lib', $this->minizone->library('tv_lib'));
		$this->view->render($this->root_view.'/livescore/tded_modal.tpl');
    }
        
		
    function match($id){
        
		if($_REQUEST['comment_no']>0){     
			$parent = $_REQUEST['comment_no'];
			$this->comment_model = $this->minizone->model('comment_model');
			$comment = json_decode($this->comment_model->get_by_id($parent), true);
				
			//var_dump($comment);
			//echo '<hr>';
			/*
				
			$kid = $comment['kid'];
			$p = file_get_contents('http://kapi.kapook.com/profile/member/userid/' . $kid);
			$MemberProfile = json_decode($p, true);
			$this->view->assign('MemberProfile', $MemberProfile);
			var_dump($MemberProfile);*/
				
		}
		
		
        $this->view->assign('mem_lib', $this->mem_lib);
        
        if($_REQUEST['remove_cache']==1){
            $this->mem_lib->delete('Football2014-MatchContents-'.$id);
        }

		$ViewMatchMC	= $this->mem_lib->get('Football2014-ViewMatch-'.$id);
		$ViewMatch		= (intval($ViewMatchMC)+1);
		$this->mem_lib->set('Football2014-ViewMatch-'.$id,$ViewMatch);
		
		$this->view->assign('ViewMatch', $ViewMatch);
        
        $MatchContents_MC = $this->mem_lib->get('Football2014-MatchContents-'.$id);
        
        $MatchDateTime    = $MatchContents_MC['MatchInfo']['MatchDateTime'];
        $CurrentDateTime  = date("Y-m-d H:i:s");
        
        if(($CurrentDateTime<$MatchDateTime) || ($MatchContents_MC['MatchInfo']['MatchStatus']=='Fin') || ($MatchContents_MC['MatchInfo']['MatchStatus']=='Post') || ($MatchContents_MC['MatchInfo']['MatchStatus']=='Canc')){
            $LoadCacheStatus = true;
        }else{
            $LoadCacheStatus = false;
        }
                
        if($MatchContents_MC && $LoadCacheStatus){
            
            $MatchContents = $MatchContents_MC;
            $Remark = 'Load MC';
        }else{
            $MatchContents = $this->livescore_model->getLiveScoreMatch($id);
            if(($MatchContents['MatchInfo']['MatchStatus']=='Fin') || ($MatchContents['MatchInfo']['MatchStatus']=='Post') || ($MatchContents['MatchInfo']['MatchStatus']=='Canc')){
                $expire = (3600*12); 
				$this->mem_lib->set('Football2014-MatchContents-'.$id,$MatchContents['MatchInfo'], MEMCACHE_COMPRESSED, $expire);
            }else{    
                $expire = 3600;
                $this->mem_lib->set('Football2014-MatchContents-'.$id,$MatchContents['MatchInfo'], MEMCACHE_COMPRESSED, $expire);
            }
            $Remark = 'No Load';
            
        }   
        
        if($_REQUEST['debug']==1){		
            var_dump($MatchContents); 
            echo '<hr>';
            var_dump($LoadCacheStatus); 
            echo '<hr>';
            echo $CurrentDateTime.'=>'.$MatchDateTime.' : '.$Remark;
            echo 67890;
            if($CurrentDateTime<$MatchDateTime){ echo 'Waiting'; }else{ echo 'Running'; }
            echo 98765;
            echo '=>'.$MatchContents_MC['MatchInfo']['Status'];
            exit();
        }
		
        $this->view->assign('MatchContents',$MatchContents);
        $MatchDateTime    	=		$MatchContents['MatchInfo']['MatchDateTime'];
       
        $MatchOfTeam1_MC 	=  		$this->mem_lib->get('Football2014-MatchOfTeam-'.$MatchContents['MatchInfo']['Team1KPID']);  
        if(($MatchOfTeam1_MC)&&($_REQUEST['remove_cache']!=1)){
            $MatchOfTeam1 	= 		$MatchOfTeam1_MC;
        }else{
		
			if($_REQUEST['remove_cache']==1){
				$teamInfo = $this->squad_model->getTeamInfo($MatchContents['MatchInfo']['Team1KPID'],true);
			}
			else
			{
				$teamInfo = $this->squad_model->getTeamInfo($MatchContents['MatchInfo']['Team1KPID']);
			}
			if(!empty($teamInfo['DuplicateList'])){
				$list_all	=	implode( ',' , $teamInfo['DuplicateList'] );
				$list_all	=	implode( ',' , array($list_all,$MatchContents['MatchInfo']['Team1KPID']) );
			}else{
				$list_all	=	$MatchContents['MatchInfo']['Team1KPID'];
			}
			
            $MatchOfTeam1 	= 		$this->livescore_model->getMatchByTeam($list_all,5,"2014-01-01",date("Y-m-d",strtotime(' - 1 day')));
            $expire = (3600*24*2); 
            $this->mem_lib->set('Football2014-MatchOfTeam-'.$MatchContents['MatchInfo']['Team1KPID'],$MatchOfTeam1, MEMCACHE_COMPRESSED, $expire);
        }       
        $this->view->assign('MatchOfTeam1',$MatchOfTeam1);
        
        
        $MatchOfTeam2_MC =  $this->mem_lib->get('Football2014-MatchOfTeam-'.$MatchContents['MatchInfo']['Team2KPID']);
        if(($MatchOfTeam2_MC)&&($_REQUEST['remove_cache']!=1)){
            $MatchOfTeam2 = $MatchOfTeam2_MC;
        }else{
		
			if($_REQUEST['remove_cache']==1){
				$teamInfo = $this->squad_model->getTeamInfo($MatchContents['MatchInfo']['Team2KPID'],true);
			}
			else
			{
				$teamInfo = $this->squad_model->getTeamInfo($MatchContents['MatchInfo']['Team2KPID']);
			}
			if(!empty($teamInfo['DuplicateList'])){
				$list_all	=	implode( ',' , $teamInfo['DuplicateList'] );
				$list_all	=	implode( ',' , array($list_all,$MatchContents['MatchInfo']['Team2KPID']) );
			}else{
				$list_all	=	$MatchContents['MatchInfo']['Team2KPID'];
			}
			
            $MatchOfTeam2 = $this->livescore_model->getMatchByTeam($list_all,5,"2014-01-01",date("Y-m-d",strtotime(' - 1 day')));
            $expire = (3600*24*2); 
            $this->mem_lib->set('Football2014-MatchOfTeam-'.$MatchContents['MatchInfo']['Team2KPID'],$MatchOfTeam2, MEMCACHE_COMPRESSED, $expire);
        }       
		if($_REQUEST['tbug']==1){
			var_dump($MatchOfTeam2);
			exit;
		}
        $this->view->assign('MatchOfTeam2',$MatchOfTeam2);

        $MapLeaguageArr[227]	= 1;
        $MapLeaguageArr[236] 	= 2;
		$MapLeaguageArr[284] 	= 2;
        $MapLeaguageArr[241] 	= 3;
        $MapLeaguageArr[48] 	= 4;
        $MapLeaguageArr[56] 	= 4;
        $MapLeaguageArr[75] 	= 4;
        $MapLeaguageArr[124] 	= 4;
        $MapLeaguageArr[125] 	= 4;
        $MapLeaguageArr[127] 	= 4;
        $MapLeaguageArr[130] 	= 4;
        $MapLeaguageArr[217] 	= 4;
        $MapLeaguageArr[225] 	= 4;
        $MapLeaguageArr[248] 	= 4;
        $MapLeaguageArr[106] 	= 6;
        $MapLeaguageArr[9] 		= 8;
        $MapLeaguageArr[109] 	= 8;
        $MapLeaguageArr[157] 	= 8;
        $MapLeaguageArr[129] 	= 10;
        $MapLeaguageArr[135] 	= 10;
        $MapLeaguageArr[169] 	= 10;
        $MapLeaguageArr[170] 	= 10;
        $MapLeaguageArr[207] 	= 10;
        $MapLeaguageArr[114] 	= 11;
        $MapLeaguageArr[171] 	= 12;

        $LID = $MapLeaguageArr[$MatchContents['MatchInfo']['KPLeagueID']];
		if($_REQUEST['tbug']==1){ var_dump($MatchContents['MatchInfo']['KPLeagueID']);}
		
        $NewsLeagueContents_MC =  $this->news_model->getNewsLeague($LID, $page, $_REQUEST['remove_cache'] == 1);
        if($NewsLeagueContents_MC){
            $NewsLeagueContents = $NewsLeagueContents_MC;
        }              

		$NewsMatchContents = $this->news_model->getNewsByTeam($MatchContents['MatchInfo']['Team1KPID'].','.$MatchContents['MatchInfo']['Team2KPID'],1,$_REQUEST['remove_cache'] == 1);
		if($NewsMatchContents){
			$NewsLeagueContents = $NewsMatchContents;
		}
		
        $this->view->assign('NewsLeagueContents', $NewsLeagueContents['data']);
        
        $team1 = $this->mem_lib->get('Football2014-Team-NameTHShort-'.$MatchContents['MatchInfo']['Team1KPID']);
        $team2 = $this->mem_lib->get('Football2014-Team-NameTHShort-'.$MatchContents['MatchInfo']['Team2KPID']);
        $leaguename = $LeagueName = $this->mem_lib->get('Football2014-League-NameTHShort-'.$MatchContents['MatchInfo']['KPLeagueID']);
        
        $breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = $LeagueName;
		
		$tmpArrLink = explode('?',$_SERVER['REQUEST_URI']);
		$breadcrum[2]['text'] = $team1.' - '.$team2;
		$breadcrum[2]['link'] = $tmpArrLink[0];
						
		if($_REQUEST['comment_no']>0){   
			$breadcrum[3]['link'] = $_SERVER['REQUEST_URI'];
			$breadcrum[3]['text'] = 'ความคิดเห็นที่ '.$comment["no"];
        	//$breadcrum[3]['active'] = true;
		}
		
        parent::_setBreadcrum($breadcrum);
		
		
		$team1l = $this->mem_lib->get('Football2014-Team-NameTH-'.$MatchContents['MatchInfo']['Team1KPID']);
		$team2l = $this->mem_lib->get('Football2014-Team-NameTH-'.$MatchContents['MatchInfo']['Team2KPID']);
		
		if($MatchContents['MatchInfo']['MatchStatus']=='Fin'){
			if(strlen($MatchContents['MatchInfo']['PNScore'])>1)
			{
				$PNScore	=	$MatchContents['MatchInfo']['PNScore'];
				if(strlen($MatchContents['MatchInfo']['ETScore'])>1){
					$ETScore	=	$MatchContents['MatchInfo']['ETScore'];
					$data['seo_title'] = "$team1 $ETScore($PNScore) $team2 Livescore ผลบอล $leaguename";
				}else{
					$FTScore	=	$MatchContents['MatchInfo']['FTScore'];
					$data['seo_title'] = "$team1 $FTScore($PNScore) $team2 Livescore ผลบอล $leaguename";
				}
			}else if(strlen($MatchContents['MatchInfo']['ETScore'])>1){
				$ETScore	=	$MatchContents['MatchInfo']['ETScore'];
				$data['seo_title'] = "$team1 $ETScore $team2 Livescore ผลบอล $leaguename";
			}else{
				$FTScore	=	$MatchContents['MatchInfo']['FTScore'];
				$data['seo_title'] = "$team1 $FTScore $team2 Livescore ผลบอล $leaguename";
			}
			$data['seo_description'] = "$team1 vs $team2 ผลบอล$leaguename วิเคราะห์บอล $team1l vs $team2l ติดตาม livescore คลิปไฮไลต์ $leaguename ข่าว$team1 ข่าว$team2";
			$data['seo_keywords'] = "$team1, $team2, $team1l, $team2l, $leaguename, ผลบอล, livescore, วิเคราะห์บอล, คลิปไฮไลต์";
		}else{
			$data['seo_title'] = "$team1 vs $team2 Livescore ผลบอล $leaguename";
			$data['seo_description'] = "$team1 vs $team2 ผลบอล $leaguename วิเคราะห์บอล $team1l vs $team2l ติดตามผลบอลสด livescore $leaguename รายงานสด ผลบอล $team1 ผลบอล $team2 ราคาบอล ข้อมูล สถิติย้อนหลังฟุตบอล $leaguename คลิกเลย";
			$data['seo_keywords'] = "$team1, $team2, $team1l, $team2l, $leaguename, ผลบอล, livescore, ผลบอลสด, รายงานสด, วิเคราะห์บอล, ราคาบอล";
		}

		if($MatchContents['MatchInfo']['MatchStatus']=='Fin'){
			if(strlen($MatchContents['MatchInfo']['PNScore'])>1)
			{
				$PNScore	=	$MatchContents['MatchInfo']['PNScore'];
				if(strlen($MatchContents['MatchInfo']['ETScore'])>1){
					$ETScore	=	$MatchContents['MatchInfo']['ETScore'];
					$data['fb_title'] = "$team1 $ETScore($PNScore) $team2 Livescore ผลบอล $leaguename";
				}else{
					$FTScore	=	$MatchContents['MatchInfo']['FTScore'];
					$data['fb_title'] = "$team1 $FTScore($PNScore) $team2 Livescore ผลบอล $leaguename";
				}
			}else if(strlen($MatchContents['MatchInfo']['ETScore'])>1){
				$ETScore	=	$MatchContents['MatchInfo']['ETScore'];
				$data['fb_title'] = "$team1 $ETScore $team2 Livescore ผลบอล $leaguename";
			}else{
				$FTScore	=	$MatchContents['MatchInfo']['FTScore'];
				$data['fb_title'] = "$team1 $FTScore $team2 Livescore ผลบอล $leaguename";
			}
			$data['fb_description'] = "$team1 vs $team2 ผลบอล$leaguename วิเคราะห์บอล $team1l vs $team2l ติดตาม livescore คลิปไฮไลต์ $leaguename ข่าว$team1 ข่าว$team2";
			$data['fb_img'] = empty($MatchContents['MatchInfo']['PictureOG']) ? 'http://football.kapook.com/assets/og/2014-11/page/og-football-new.jpg' : $MatchContents['MatchInfo']['PictureOG'];
		}else{
			$data['fb_title'] = "$team1 vs $team2 Livescore ผลบอล $leaguename";
			$data['fb_description'] = "$team1 vs $team2 ผลบอล $leaguename วิเคราะห์บอล $team1l vs $team2l ติดตามผลบอลสด livescore $leaguename รายงานสด ผลบอล $team1 ผลบอล $team2 ราคาบอล ข้อมูล สถิติย้อนหลังฟุตบอล $leaguename คลิกเลย";
			$data['fb_img'] = empty($MatchContents['MatchInfo']['PictureOG']) ? 'http://football.kapook.com/assets/og/2014-11/page/og-football-new.jpg' : $MatchContents['MatchInfo']['PictureOG'];
		}
		
		if($_REQUEST['comment_no']>0){       		
				$data['seo_title'] 	= 	$data['seo_title'].' ความคิดเห็นที่ '.$comment["no"];
				$data['fb_title'] 		=  $data['fb_title'].' ความคิดเห็นที่ '.$comment["no"];
		}
		parent::_setSocial($data);
                
        $this->info_lib = $this->minizone->library('info_lib');
        $content_display = $this->info_lib->get_match($id);
        $this->view->assign('content_display', $content_display);
		
		$this->view->assign('tv_lib', $this->minizone->library('tv_lib'));


		if($_REQUEST['comment_no']>0){       
		
        	$this->view->assign('comment', $comment);
			$this->_header();
			//$this->view->render($this->root_view.'/livescore/match_page_comment.tpl');
			$this->view->render($this->root_view.'/livescore/match_page.tpl');
			$this->_footer();
		 
		}else if($_REQUEST['debug']==2){

			$this->_header();
			$this->view->render($this->root_view.'/livescore/match_page_sound.tpl');
			$this->_footer();
				
		}else if($_REQUEST['v2_debug']==1){
			$this->_header();
			$this->view->render($this->root_view.'/livescore/match_page_v2.tpl');
			$this->_footer();
		}else{
			$this->_header();
			$this->view->render($this->root_view.'/livescore/match_page.tpl');
			$this->_footer();
		}
    }
        
    function program(){        
        $this->view->assign('mem_lib', $this->mem_lib);
		
		if(intval(date('G'))<6){
			$date = date("Y-m-d",strtotime("-1 days"));
		}else{
			$date = date("Y-m-d");
		}

		$DateProgram[0]=date("Y-m-d",strtotime($date . ' - 1 day'));
        $DateProgram[1]=date("Y-m-d",strtotime($date . ' + 0 day'));
        $DateProgram[2]=date("Y-m-d",strtotime($date . ' + 1 day'));
        $DateProgram[3]=date("Y-m-d",strtotime($date . ' + 2 day'));
        $DateProgram[4]=date("Y-m-d",strtotime($date . ' + 3 day'));
        $DateProgram[5]=date("Y-m-d",strtotime($date . ' + 4 day'));
        
		$ProgramContents0 =	$this->mem_lib->get('Football2014-LiveScore-' . $DateProgram[0]);
		$ProgramContents1 =	$this->mem_lib->get('Football2014-LiveScore-' . $DateProgram[1]);
		$ProgramContents2 =	$this->mem_lib->get('Football2014-LiveScore-' . $DateProgram[2]);
		$ProgramContents3 =	$this->mem_lib->get('Football2014-LiveScore-' . $DateProgram[3]);
		$ProgramContents4 =	$this->mem_lib->get('Football2014-LiveScore-' . $DateProgram[4]);
		$ProgramContents5 =	$this->mem_lib->get('Football2014-LiveScore-' . $DateProgram[5]);
		
		if((!$ProgramContents0)||($_REQUEST['remove_cache']==1)){
			$expire		=		432000;
			$ProgramContents0 = $this->livescore_model->getProgramByDate($DateProgram[0]);
			$ProgramContents0['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $DateProgram[0], $ProgramContents0, MEMCACHE_COMPRESSED, $expire);
		}
		if((!$ProgramContents1)||($_REQUEST['remove_cache']==1)){
			$expire		=		600;
			$ProgramContents1 = $this->livescore_model->getProgramByDate($DateProgram[1]);
			$ProgramContents1['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $DateProgram[1], $ProgramContents1, MEMCACHE_COMPRESSED, $expire);
		}
		if((!$ProgramContents2)||($_REQUEST['remove_cache']==1)){
			$expire		=		600;
			$ProgramContents2 = $this->livescore_model->getProgramByDate($DateProgram[2]);
			$ProgramContents2['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $DateProgram[2], $ProgramContents2, MEMCACHE_COMPRESSED, $expire);
		}
		if((!$ProgramContents3)||($_REQUEST['remove_cache']==1)){
			$expire		=		600;
			$ProgramContents3 = $this->livescore_model->getProgramByDate($DateProgram[3]);
			$ProgramContents3['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $DateProgram[3], $ProgramContents3, MEMCACHE_COMPRESSED, $expire);
		}
		if((!$ProgramContents4)||($_REQUEST['remove_cache']==1)){
			$expire		=		600;
			$ProgramContents4 = $this->livescore_model->getProgramByDate($DateProgram[4]);
			$ProgramContents4['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $DateProgram[4], $ProgramContents4, MEMCACHE_COMPRESSED, $expire);
		}
		if((!$ProgramContents5)||($_REQUEST['remove_cache']==1)){
			$expire		=		600;
			$ProgramContents5 = $this->livescore_model->getProgramByDate($DateProgram[5]);
			$ProgramContents5['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $DateProgram[5], $ProgramContents5, MEMCACHE_COMPRESSED, $expire);
		}       
  
        $this->view->assign('ProgramContents0',$ProgramContents0);
        $this->view->assign('ProgramContents1',$ProgramContents1);
        $this->view->assign('ProgramContents2',$ProgramContents2);
        $this->view->assign('ProgramContents3',$ProgramContents3);
        $this->view->assign('ProgramContents4',$ProgramContents4);
        $this->view->assign('ProgramContents5',$ProgramContents5);
		
		$this->view->assign('tv_lib', $this->minizone->library('tv_lib'));
        
        $breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = 'โปรแกรมบอล';
        parent::_setBreadcrum($breadcrum);
		
		$data['seo_title'] = "ตารางบอล บอลวันนี้ โปรแกรมบอล โปรแกรมถ่ายทอดสด คลิกเลย";
		$data['seo_description'] = "ตารางบอล บอลวันนี้ โปรแกรมบอล ดูตารางบอลวันนี้ โปรแกรมบอลพรุ่งนี้ โปรแกรมถ่ายทอดสด โปรแกรมบอลทุกลีก โปรแกรมพรีเมียร์ลีก โปรแกรมไทยลีก โปรแกรมบอลล่วงหน้า คลิกเลย";
		$data['seo_keywords'] = "ตารางบอล, ตารางบอลวันนี้, โปรแกรมบอลพรุ่งนี้, ตารางบอลพรุ่งนี้, โปรแกรมบอลล่วงหน้า, ดูบอล, โปรแกรมถ่ายทอดสด, โปรแกรมถ่ายทอดสดฟุตบอล, โปรแกรมพรีเมียร์ลีก, ตารางถ่ายทอดสด, โปรแกรมถ่ายทอดสดฟุตบอล";

		$data['fb_title'] = "ตารางบอล บอลวันนี้ เช็คโปรแกรมถ่ายทอดสดฟุตบอล คลิกเลย";
		$data['fb_description'] = "ตารางบอล บอลวันนี้ โปรแกรมบอล ดูตารางบอลวันนี้ โปรแกรมถ่ายทอดสดทุกลีก";
        //$data['fb_img'] = 'http://football.kapook.com/assets/og-football/set1/โปรแกรมบอล.jpg';
		$data['fb_img'] = 'http://football.kapook.com/assets/og/2014-11/page/โปรแกรมบอล.jpg';
		parent::_setSocial($data);
        $this->_header();
        $this->view->render($this->root_view.'/livescore/program.tpl');
        $this->_footer();
        
    }

    
        
    function result(){        
        $this->view->assign('mem_lib', $this->mem_lib);
		
		if(intval(date('G'))<6){
			$date = date("Y-m-d",strtotime("-1 days"));
		}else{
			$date = date("Y-m-d");
		}
		
        $DateProgram[5]=date("Y-m-d",strtotime($date . ' - 0 day'));
        $DateProgram[4]=date("Y-m-d",strtotime($date . ' - 1 day'));
        $DateProgram[3]=date("Y-m-d",strtotime($date . ' - 2 day'));
        $DateProgram[2]=date("Y-m-d",strtotime($date . ' - 3 day'));
        $DateProgram[1]=date("Y-m-d",strtotime($date . ' - 4 day'));
		
		$ProgramContents1 =	$this->mem_lib->get('Football2014-LiveScore-' . $DateProgram[1]);
		$ProgramContents2 =	$this->mem_lib->get('Football2014-LiveScore-' . $DateProgram[2]);
		$ProgramContents3 =	$this->mem_lib->get('Football2014-LiveScore-' . $DateProgram[3]);
		$ProgramContents4 =	$this->mem_lib->get('Football2014-LiveScore-' . $DateProgram[4]);
		$ProgramContents5 =	$this->mem_lib->get('Football2014-LiveScore-' . $DateProgram[5]);
        
		if((!$ProgramContents1)||($_REQUEST['remove_cache']==1)){
			$expire		=		86400;
			$ProgramContents1 = $this->livescore_model->getProgramByDate($DateProgram[1]);
			$ProgramContents1['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $DateProgram[1], $ProgramContents1, MEMCACHE_COMPRESSED, $expire);
		}
		if((!$ProgramContents2)||($_REQUEST['remove_cache']==1)){
			$expire		=		172800;
			$ProgramContents2 = $this->livescore_model->getProgramByDate($DateProgram[2]);
			$ProgramContents2['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $DateProgram[2], $ProgramContents2, MEMCACHE_COMPRESSED, $expire);
		}
		if((!$ProgramContents3)||($_REQUEST['remove_cache']==1)){
			$expire		=		259200;
			$ProgramContents3 = $this->livescore_model->getProgramByDate($DateProgram[3]);
			$ProgramContents3['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $DateProgram[3], $ProgramContents3, MEMCACHE_COMPRESSED, $expire);
		}
		if((!$ProgramContents4)||($_REQUEST['remove_cache']==1)){
			$expire		=		345600;
			$ProgramContents4 = $this->livescore_model->getProgramByDate($DateProgram[4]);
			$ProgramContents4['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $DateProgram[4], $ProgramContents4, MEMCACHE_COMPRESSED, $expire);
		}
		if((!$ProgramContents5)||($_REQUEST['remove_cache']==1)){
			$expire		=		600;
			$ProgramContents5 = $this->livescore_model->getProgramByDate($DateProgram[5]);
			$ProgramContents5['cache_time'] = $expire;
			$this->mem_lib->set('Football2014-LiveScore-' . $DateProgram[5], $ProgramContents5, MEMCACHE_COMPRESSED, $expire);
		}
		
        $this->view->assign('ProgramContents1',$ProgramContents1);
        $this->view->assign('ProgramContents2',$ProgramContents2);
        $this->view->assign('ProgramContents3',$ProgramContents3);
        $this->view->assign('ProgramContents4',$ProgramContents4);
        $this->view->assign('ProgramContents5',$ProgramContents5);
		
		$this->view->assign('tv_lib', $this->minizone->library('tv_lib'));
		
		$breadcrum[0]['text'] = 'หน้าแรกฟุตบอล';
        $breadcrum[0]['link'] = BASE_HREF;
        $breadcrum[1]['text'] = 'ผลบอล';
        parent::_setBreadcrum($breadcrum);
		
		$data['seo_title'] = "ผลบอล ผลบอลเมื่อคืน ผลบอลล่าสุด เช็คผลบอลทุกลีก ผลบอลวันนี้";
		$data['seo_description'] = "ผลบอล ผลบอลเมื่อคืน ผลบอลวันนี้ เช็คผลบอล ผลบอลล่าสุด อัพเดทผลบอล ผลบอลพรีเมียร์ลีก ผลบอลไทย ผลบอลเมื่อคืนทุกลีก ดูผลบอล ผลฟุตบอล ผลบอลภาษาไทย ผลบอลล่าสุดวันนี้";
		$data['seo_keywords'] = "ผลบอล, ผลบอลเมื่อคืน, ผลบอลวันนี้, ผลบอลล่าสุด, เช็คผลบอล, ผลบอลเมื่อคืน, ผลบอลพรีเมียร์ลีก, ผลบอลภาษาไทย, ดูผลบอลผล, ผลบอลไทย, ผลบอลไทยลีก, ผลบอลทุกลีก, ผลบอลภาษาไทย";

		$data['fb_title'] = "ผลบอล เช็คผลบอลเมื่อคืน ผลบอลล่าสุด ผลบอลทุกลีก คลิกเลย";
		$data['fb_description'] = "ผลบอล ผลบอลเมื่อคืน เช็คผลบอล ผลบอลวันนี้ ผลบอลล่าสุด อัพเดทผลบอลทุกลีก ผลบอลพรีเมียร์ลีก ผลบอลไทย";
		//$data['fb_img'] = 'http://football.kapook.com/assets/og-football/set1/ผลบอล.jpg';
		$data['fb_img'] = 'http://football.kapook.com/assets/og/2014-11/page/ผลบอล.jpg';
		parent::_setSocial($data);
        $this->_header();
        $this->view->render($this->root_view.'/livescore/result.tpl');
        $this->_footer();
        
    }
    function debug() {
        $getNews_result_MC = $this->mem_lib->get('Football2014-getNews_result');
        echo 'getNews_result_MC : <br>'; 
        var_dump($getNews_result_MC);
        
    }
    
    
}