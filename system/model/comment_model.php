<?php

class Comment_Model {

    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() {
        $this->minizone = minizone::getzone();
        $this->curl_lib = $this->minizone->library('curl_lib');
        $this->API_BASE_URL = 'http://kapi.kapook.com/hilight/hilight/content/';
    }
    
    
    
    function saveReply($data) {
        return json_decode($this->curl_lib->getCurl(API_BASE_URL.'hilight/comment/saveReply', $data),true);
    }
    
    function updateMemberComment($comment_id,$userIP) {
        $data['comment_id'] = $comment_id;
        $data['uid'] = uid;
        $data['userIP'] = $userIP;
        
        return $this->curl_lib->getCurl(API_BASE_URL.'hilight/comment/updateMemberComment', $data);
    }
    
    function get_by_id($_id) {
        return $this->curl_lib->getCurl(API_BASE_URL.'hilight/comment/getCommentByID/'.$_id, array());
    }
    
    function get_id($_id) {
        return $this->curl_lib->getCurl(API_BASE_URL.'hilight/comment/getComment/'.$_id, array());
    }

}

/* End of file tan_model.php */
/* Location: ./system/model/tan_model.php */
